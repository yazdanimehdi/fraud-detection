import pandas as pd
import jdatetime
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

df = pd.read_excel('erfan2.xlsx')


def cat(y):
    if y == 'سلف':
        return 1
    elif y == 'نقدی':
        return 2
    elif y == 'نقدی (مچینگ)':
        return 3
    elif y == 'سلف (مچینگ)':
        return 4


def shamsi_to_miladi(y):
    shamsi = jdatetime.datetime.strptime(y, format="%Y/%m/%d")
    # a = jdatetime.JalaliToGregorian(shamsi.year, shamsi.month, shamsi.day)
    # shamsi.time()
    return shamsi.timestamp()


df['نوع قرارداد'] = df['نوع قرارداد'].apply(cat)
df.drop(['قیمت پایانی میانگین موزون', 'ارزش معامله (هزارریال)', 'بالاترین', 'حجم معامله (تن)'], axis=1, inplace=True)
df['تاریخ معامله'] = df['تاریخ معامله'].apply(shamsi_to_miladi)
df['تاریخ تحویل'] = df['تاریخ تحویل'].apply(shamsi_to_miladi)
# df.drop(['open دلار', 'close دلار'], axis=1, inplace=True)
# df['تاریخ معامله'] = pd.to_datetime(df['تاریخ معامله'], format="%Y-%m-%d")
# df['تاریخ تحویل'] = pd.to_datetime(df['تاریخ تحویل'], format="%Y-%m-%d")
df.dropna(axis=0, inplace=True)
X = df.drop(['پایین ترین', 'تاریخ معامله', 'تاریخ تحویل'], axis=1)
y = df['پایین ترین']
X = pd.get_dummies(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

lr = LinearRegression()
lr.fit(X_train, y_train)
print(lr.score(X_test, y_test))


