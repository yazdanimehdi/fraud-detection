
from sklearn.model_selection import RepeatedStratifiedKFold, KFold, cross_val_score, StratifiedKFold, train_test_split
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.metrics import log_loss, roc_auc_score, accuracy_score, confusion_matrix, f1_score
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from imblearn.over_sampling import ADASYN, BorderlineSMOTE, KMeansSMOTE, RandomOverSampler, SMOTE, SVMSMOTE
from imblearn.under_sampling import ClusterCentroids, CondensedNearestNeighbour, EditedNearestNeighbours, RepeatedEditedNearestNeighbours, AllKNN, InstanceHardnessThreshold, NearMiss, NeighbourhoodCleaningRule, OneSidedSelection, RandomUnderSampler, TomekLinks

df = pd.read_csv('yeast2.csv')


def cat(y):
    if y == 'negative':
        return 0
    elif y == 'positive':
        return 1


df['negative'] = df['negative'].apply(cat)
df.dropna(axis=0, inplace=True)
# df.drop(['F'], axis=1, inplace=True)
X = df.drop('negative', axis=1).as_matrix()
y = df['negative'].as_matrix()

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=1)

sm = KMeansSMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)

X_train = X_res
y_train = y_res

n_clusters = []
cluster_min = KMeans(n_clusters=12, max_iter=1000)
cluster_min.fit(X_train[y_train == 1])
labels_min = cluster_min.labels_
cluster_maj = KMeans(n_clusters=12, max_iter=1000)
cluster_maj.fit(X_train[y_train == 0])
labels_maj = cluster_maj.labels_
labels_maj_set = set(labels_maj)
labels_min_set = set(labels_min)

clusters_maj = [[] for j in range(len(labels_maj_set))]
for item in labels_maj_set:
    for i in range(0, X_train[y_train == 0].shape[0]):
        if labels_maj[i] == item:
            clusters_maj[item].append(X_train[y_train == 0][i])

clusters_min = [[] for j in range(len(labels_min_set))]
for item in labels_min_set:
    for i in range(0, X_train[y_train == 1].shape[0]):
        if labels_min[i] == item:
            clusters_min[item].append(X_train[y_train == 1][i])

lr = LogisticRegression(max_iter=5000)
# lr = SVC(probability=True, random_state=4)
lr.fit(X_train, y_train)

proba_min_cluster = [[] for i in range(len(labels_min_set))]
for item in range(len(clusters_min)):
    for node in clusters_min[item]:
        proba_min_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

proba_maj_cluster = [[] for i in range(len(labels_maj_set))]
for item in range(len(clusters_maj)):
    for node in clusters_maj[item]:
        proba_maj_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

new_maj = []
for cl in proba_maj_cluster:
    array = np.array(cl)
    median = np.median(array)
    flag = False
    # while not flag:
    # flag = True
    B = plt.boxplot(array)
    # median = np.median(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    for item in array:
        if item > high[1] or item < low[1]:
            # flag = False
            i, = np.where(array == item)
            array = np.delete(array, i[0])
    new_maj.append(array)

new_min = []
for cl in proba_min_cluster:
    array = np.array(cl)
    median = np.median(array)
    # flag = False
    # while not flag:
    # flag = True
    B = plt.boxplot(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    for item in array:
        if item > high[1] or item < low[1]:
            # flag = False
            i, = np.where(array == item)
            array = np.delete(array, i[0])
    new_min.append(array)


min_data = []
for item in proba_min_cluster:
    array = np.array(item)
    min_data.append((np.average(array), np.std(array)))

maj_data = []
for item in proba_maj_cluster:
    array = np.array(item)
    maj_data.append((np.average(array), np.std(array)))

y_pred_new = []
for j in range(X_test.shape[0]):
    label_new_min = cluster_min.predict(X_test[j].reshape(1, -1))
    label_new_maj = cluster_maj.predict(X_test[j].reshape(1, -1))

    if lr.predict_proba(X_test[j].reshape(1, -1))[0][1] >= 0.7:
        label_new = int(label_new_min)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if min_data[label_new][0] + min_data[label_new][1] * 3 >= probab >= min_data[label_new][0] - min_data[label_new][1] * 3:
            y_pred_new.append(1)
        else:
            label_new = int(label_new_maj)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= maj_data[label_new][0] + maj_data[label_new][1]:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)


    else:
        label_new = int(label_new_maj)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if maj_data[label_new][0] + maj_data[label_new][1] * 3 >= probab >= maj_data[label_new][0] - maj_data[label_new][1] * 3:
            y_pred_new.append(0)
        else:
            label_new = int(label_new_min)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= min_data[label_new][0] - min_data[label_new][1]:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)


Matrix = confusion_matrix(y_test, y_pred_new)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy = (TP + TN) / (TP + FP + TN + FN)
precision = TP / (TP + FP)
recall = TP / (TP + FN)
fp_rate = FP/(FP + TN)
specificity = TN / (TN + FP)
F1_score = (2 * precision * recall) / (precision + recall)

y_pred_without = lr.predict(X_test)

Matrix = confusion_matrix(y_test, y_pred_without)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_w = (TP + TN) / (TP + FP + TN + FN)
precision_w = TP / (TP + FP)
recall_w = TP / (TP + FN)
specificity_w = TN / (TN + FP)
fp_rate_w = FP/(FP + TN)
F1_score_w = (2 * precision_w * recall_w) / (precision_w + recall_w)
n_clusters_w = []
n_clusters.append((Accuracy, precision, recall, specificity, F1_score))
n_clusters_w.append((Accuracy_w, precision_w, recall_w, specificity_w, F1_score_w))

print(n_clusters)
print(n_clusters_w)
