import os
import pickle
import itertools
import logging
import re
import time
import glob
import inspect
from sklearn.neighbors import NearestCentroid
import scipy.stats as st

# used to parallelize evaluation
from joblib import Parallel, delayed

# numerical methods and arrays
import numpy as np
import pandas as pd

# import packages used for the implementation of sampling methods
from sklearn.model_selection import RepeatedStratifiedKFold, KFold, cross_val_score, StratifiedKFold, train_test_split
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.metrics import log_loss, roc_auc_score, accuracy_score, confusion_matrix, f1_score
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
from sklearn.manifold import LocallyLinearEmbedding, TSNE, Isomap
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.mixture import GaussianMixture
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.calibration import CalibratedClassifierCV
#from sklearn.calibration import CalibratedClassifierCV
from sklearn.base import clone, BaseEstimator, ClassifierMixin

# some statistical methods
from scipy.stats import skew
import scipy.signal as ssignal
import scipy.spatial as sspatial
import scipy.optimize as soptimize
import scipy.special as sspecial
from scipy.stats.mstats import gmean

# self-organizing map implementation
import minisom

_logger= logging.getLogger('smote_variants')
_logger.setLevel(logging.DEBUG)
_logger_ch= logging.StreamHandler()
_logger_ch.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s:%(message)s"))
_logger.addHandler(_logger_ch)


class StatisticsMixin:
    """
    Mixin to compute class statistics and determine minority/majority labels
    """
    def class_label_statistics(self, X, y):
        """
        determines class sizes and minority and majority labels
        Args:
            X (np.array): features
            y (np.array): target labels
        """
        unique, counts= np.unique(y, return_counts= True)
        self.class_stats= dict(zip(unique, counts))
        self.minority_label= unique[0] if counts[0] < counts[1] else unique[1]
        self.majority_label= unique[1] if counts[0] < counts[1] else unique[0]


class ParameterCheckingMixin:
    """
    Mixin to check if parameters come from a valid range
    """

    def check_in_range(self, x, name, r):
        """
        Check if parameter is in range
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            r (list-like(2)): the lower and upper bound of a range
        Throws:
            ValueError
        """
        if x < r[0] or x > r[1]:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s outside the range [%f,%f] is not allowed: %f" % (
                name, r[0], r[1], x))

    def check_out_range(self, x, name, r):
        """
        Check if parameter is outside of range
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            r (list-like(2)): the lower and upper bound of a range
        Throws:
            ValueError
        """
        if x >= r[0] and x <= r[1]:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s outside the range [%f,%f] is not allowed: %f" % (
                name, r[0], r[1], x))

    def check_less_or_equal(self, x, name, val):
        """
        Check if parameter is less than or equal to value
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            val (numeric): value to compare to
        Throws:
            ValueError
        """
        if x > val:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s greater than %f is not allowed: %f > %f" % (
                name, val, x, val))

    def check_less_or_equal_par(self, x, name_x, y, name_y):
        """
        Check if parameter is less than or equal to another parameter
        Args:
            x (numeric): the parameter value
            name_x (str): the parameter name
            y (numeric): the other parameter value
            name_y (str): the other parameter name
        Throws:
            ValueError
        """
        if x > y:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s greater than parameter %s is not allowed: %f > %f" % (
                name_x, name_y, x, y))

    def check_less(self, x, name, val):
        """
        Check if parameter is less than value
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            val (numeric): value to compare to
        Throws:
            ValueError
        """
        if x >= val:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s greater than or equal to %f is not allowed: %f >= %f" % (
                name, val, x, val))

    def check_less_par(self, x, name_x, y, name_y):
        """
        Check if parameter is less than another parameter
        Args:
            x (numeric): the parameter value
            name_x (str): the parameter name
            y (numeric): the other parameter value
            name_y (str): the other parameter name
        Throws:
            ValueError
        """
        if x >= y:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s greater than or equal to parameter %s is not allowed: %f >= %f" % (
                name_x, name_y, x, y))

    def check_greater_or_equal(self, x, name, val):
        """
        Check if parameter is greater than or equal to value
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            val (numeric): value to compare to
        Throws:
            ValueError
        """
        if x < val:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s less than %f is not allowed: %f < %f" % (
                name, val, x, val))

    def check_greater_or_equal_par(self, x, name_x, y, name_y):
        """
        Check if parameter is less than or equal to another parameter
        Args:
            x (numeric): the parameter value
            name_x (str): the parameter name
            y (numeric): the other parameter value
            name_y (str): the other parameter name
        Throws:
            ValueError
        """
        if x < y:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s less than parameter %s is not allowed: %f < %f" % (
                name_x, name_y, x, y))

    def check_greater(self, x, name, val):
        """
        Check if parameter is greater than value
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            val (numeric): value to compare to
        Throws:
            ValueError
        """
        if x <= val:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s less than or equal to %f is not allowed: %f < %f" % (
                name, val, x, val))

    def check_greater_par(self, x, name_x, y, name_y):
        """
        Check if parameter is greater than or equal to another parameter
        Args:
            x (numeric): the parameter value
            name_x (str): the parameter name
            y (numeric): the other parameter value
            name_y (str): the other parameter name
        Throws:
            ValueError
        """
        if x <= y:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s less than or equal to parameter %s is not allowed: %f <= %f" % (
                name_x, name_y, x, y))

    def check_equal(self, x, name, val):
        """
        Check if parameter is equal to value
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            val (numeric): value to compare to
        Throws:
            ValueError
        """
        if x == val:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s equal to parameter %f is not allowed: %f == %f" % (
                name, val, x, val))

    def check_equal_par(self, x, name_x, y, name_y):
        """
        Check if parameter is equal to another parameter
        Args:
            x (numeric): the parameter value
            name_x (str): the parameter name
            y (numeric): the other parameter value
            name_y (str): the other parameter name
        Throws:
            ValueError
        """
        if x == y:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s equal to parameter %s is not allowed: %f == %f" % (
                name_x, name_y, x, y))

    def check_isin(self, x, name, l):
        """
        Check if parameter is in list
        Args:
            x (numeric): the parameter value
            name (str): the parameter name
            l (list): list to check if parameter is in it
        Throws:
            ValueError
        """
        if not x in l:
            raise ValueError(
                self.__class__.__name__ + ": " + "Value for parameter %s not in list %s is not allowed: %s" % (
                name, str(l), str(x)))

    def check_n_jobs(self, x, name):
        """
        Check n_jobs parameter
        Args:
            x (int/None): number of jobs
            name (str): the parameter name
        Throws:
            ValueError
        """
        if (x is None) or (not x is None and isinstance(x, int) and not x == 0):
            pass
        else:
            raise ValueError(self.__class__.__name__ + ": " + "Value for parameter n_jobs is not allowed: %s" % str(x))


class ParameterCombinationsMixin:
    """
    Mixin to generate parameter combinations
    """

    @classmethod
    def generate_parameter_combinations(cls, dictionary, num=None):
        """
        Generates reasonable paramter combinations
        Args:
            dictionary (dict): dictionary of paramter ranges
            num (int): maximum number of combinations to generate
        """
        combinations = [dict(zip(list(dictionary.keys()), p)) for p in
                        list(itertools.product(*list(dictionary.values())))]
        if num is None:
            return combinations
        else:
            if hasattr(cls, 'random_state'):
                return cls.random_state.choice(combinations, num, replace=False)
            else:
                return np.random.choice(combinations, num, replace=False)


class NoiseFilter(StatisticsMixin, ParameterCheckingMixin, ParameterCombinationsMixin):
    """
    Parent class of noise filtering methods
    """

    def __init__(self):
        """
        Constructor
        """
        pass

    def remove_noise(self, X, y):
        """
        Removes noise
        Args:
            X (np.array): features
            y (np.array): target labels
        """
        pass

    def get_params(self, deep=False):
        """
        Return parameters

        Returns:
            dict: dictionary of parameters
        """

        return {}

    def set_params(self, **params):
        """
        Set parameters

        Args:
            params (dict): dictionary of parameters
        """

        for key, value in params.items():
            setattr(self, key, value)

        return self


class TomekLinkRemoval(NoiseFilter):
    """
    Tomek link removal

    References:
        * BibTex::

            @article{smoteNoise0,
                     author = {Batista, Gustavo E. A. P. A. and Prati, Ronaldo C. and Monard, Maria Carolina},
                     title = {A Study of the Behavior of Several Methods for Balancing Machine Learning Training Data},
                     journal = {SIGKDD Explor. Newsl.},
                     issue_date = {June 2004},
                     volume = {6},
                     number = {1},
                     month = jun,
                     year = {2004},
                     issn = {1931-0145},
                     pages = {20--29},
                     numpages = {10},
                     url = {http://doi.acm.org/10.1145/1007730.1007735},
                     doi = {10.1145/1007730.1007735},
                     acmid = {1007735},
                     publisher = {ACM},
                     address = {New York, NY, USA}
                    }
    """

    def __init__(self, strategy='remove_majority', n_jobs=1):
        """
        Constructor of the noise filter.

        Args:
            strategy (str): noise removal strategy: 'remove_majority'/'remove_both'
            n_jobs (int): number of jobs
        """
        super().__init__()

        self.check_isin(strategy, 'strategy', ['remove_majority', 'remove_both'])
        self.check_n_jobs(n_jobs, 'n_jobs')

        self.strategy = strategy
        self.n_jobs = n_jobs

    def remove_noise(self, X, y):
        """
        Removes noise from dataset

        Args:
            X (np.matrix): features
            y (np.array): target labels

        Returns:
            np.matrix, np.array: dataset after noise removal
        """
        _logger.info(self.__class__.__name__ + ": " + "Running noise removal via %s" % self.__class__.__name__)
        self.class_label_statistics(X, y)

        # using 2 neighbors because the first neighbor is the point itself
        nn = NearestNeighbors(n_neighbors=2, n_jobs=self.n_jobs)
        distances, indices = nn.fit(X).kneighbors(X)

        # identify links
        links = []
        for i in range(len(indices)):
            if indices[indices[i][1]][1] == i:
                if not y[indices[i][1]] == y[indices[indices[i][1]][1]]:
                    links.append((i, indices[i][1]))

        # determine links to be removed
        to_remove = []
        for l in links:
            if self.strategy == 'remove_majority':
                if y[l[0]] == self.minority_label:
                    to_remove.append(l[1])
                else:
                    to_remove.append(l[0])
            elif self.strategy == 'remove_both':
                to_remove.append(l[0])
                to_remove.append(l[1])
            else:
                raise ValueError(
                    self.__class__.__name__ + ": " + 'No Tomek link removal strategy %s implemented' % self.strategy)

        to_remove = list(set(to_remove))

        return np.delete(X, to_remove, axis=0), np.delete(y, to_remove)


class RandomStateMixin:
    """
    Mixin to set random state
    """

    def set_random_state(self, random_state):
        """
        sets the random_state member of the object

        Args:
            random_state (int/np.random.RandomState/None): the random state initializer
        """

        self._random_state_init = random_state

        if random_state is None:
            self.random_state = np.random
        elif isinstance(random_state, int):
            self.random_state = np.random.RandomState(random_state)
        elif isinstance(random_state, np.random.RandomState):
            self.random_state = random_state
        elif random_state is np.random:
            self.random_state = random_state
        else:
            raise ValueError("random state cannot be initialized by " + str(random_state))



class OverSampling(StatisticsMixin, ParameterCheckingMixin, ParameterCombinationsMixin, RandomStateMixin):
    """
    Base class of oversampling methods
    """

    categories = []

    cat_noise_removal = 'NR'
    cat_dim_reduction = 'DR'
    cat_uses_classifier = 'Clas'
    cat_sample_componentwise = 'SCmp'
    cat_sample_ordinary = 'SO'
    cat_sample_copy = 'SCpy'
    cat_memetic = 'M'
    cat_density_estimation = 'DE'
    cat_density_based = 'DB'
    cat_extensive = 'Ex'
    cat_changes_majority = 'CM'
    cat_uses_clustering = 'Clus'
    cat_borderline = 'BL'
    cat_application = 'A'


    def __init__(self):
        pass


    def number_of_instances_to_sample(self, strategy, n_maj, n_min):
        """
        Determines the number of samples to generate
        Args:
            strategy (str/float): if float, the fraction of the difference of
                                    the minority and majority numbers to generate, like
                                    0.1 means that 10% of the difference will be generated
                                    if str, like 'min2maj', the minority class will be upsampled
                                    to match the cardinality of the majority class
        """
        if isinstance(strategy, float) or isinstance(strategy, int):
            return max([0, int((n_maj - n_min) * strategy)])
        else:
            raise ValueError(self.__class__.__name__ + ": " + "Value %s for parameter strategy is not supported" % strategy)


    def sample_between_points(self, x, y):
        """
        Sample randomly along the line between two points.
        Args:
            x (np.array): point 1
            y (np.array): point 2
        Returns:
            np.array: the new sample
        """
        return x + (y - x) * self.random_state.random_sample()


    def sample_between_points_componentwise(self, x, y, mask=None):
        """
        Sample each dimension separately between the two points.
        Args:
            x (np.array): point 1
            y (np.array): point 2
            mask (np.array): array of 0,1s - specifies which dimensions to sample
        Returns:
            np.array: the new sample being generated
        """
        if mask is None:
            return x + (y - x) * self.random_state.random_sample()
        else:
            return x + (y - x) * self.random_state.random_sample() * mask


    def sample_by_jittering(self, x, std):
        """
        Sample by jittering.
        Args:
            x (np.array): base point
            std (float): standard deviation
        Returns:
            np.array: the new sample
        """
        return x + (self.random_state.random_sample() - 0.5) * 2.0 * std


    def sample_by_jittering_componentwise(self, x, std):
        """
        Sample by jittering componentwise.
        Args:
            x (np.array): base point
            std (np.array): standard deviation
        Returns:
            np.array: the new sample
        """
        return x + (self.random_state.random_sample(len(x)) - 0.5) * 2.0 * std


    def sample_by_gaussian_jittering(self, x, std):
        """
        Sample by Gaussian jittering
        Args:
            x (np.array): base point
            std (np.array): standard deviation
        Returns:
            np.array: the new sample
        """
        return self.random_state.normal(x, std)


    def sample(self, X, y):
        """
        The samplig function reimplemented in child classes
        Args:
            X (np.matrix): features
            y (np.array): labels
        Returns:
            np.matrix, np.array: sampled X and y
        """
        return X, y


    def sample_with_timing(self, X, y):
        begin = time.time()
        X_samp, y_samp = self.sample(X, y)
        _logger.info(self.__class__.__name__ + ": " + ("runtime: %f" % (time.time() - begin)))
        return X_samp, y_samp


    def transform(self, X):
        """
        Transforms new data according to the possible transformation implemented
        by the function "sample".
        Args:
            X (np.matrix): features
        Returns:
            np.matrix: transformed features
        """
        return X


    def get_params(self, deep=False):
        """
        Returns the parameters of the object as a dictionary.
        Returns:
            dict: the parameters of the object
        """
        pass


    def set_params(self, **params):
        """
        Set parameters

        Args:
            params (dict): dictionary of parameters
        """

        for key, value in params.items():
            setattr(self, key, value)

        return self


    def descriptor(self):
        """
        Returns:
            str: JSON description of the current sampling object
        """
        return str((self.__class__.__name__, str(self.get_params())))


    def __str__(self):
        return self.descriptor()


class A_SUWO(OverSampling):
    """
    References:
        * BibTex::

            @article{a_suwo,
                        title = "Adaptive semi-unsupervised weighted oversampling (A-SUWO) for imbalanced datasets",
                        journal = "Expert Systems with Applications",
                        volume = "46",
                        pages = "405 - 416",
                        year = "2016",
                        issn = "0957-4174",
                        doi = "https://doi.org/10.1016/j.eswa.2015.10.031",
                        url = "http://www.sciencedirect.com/science/article/pii/S0957417415007356",
                        author = "Iman Nekooeimehr and Susana K. Lai-Yuen",
                        keywords = "Imbalanced dataset, Classification, Clustering, Oversampling"
                        }

    Notes:
        * Equation (7) misses a division by R_j.
        * It is not specified how to sample from clusters with 1 instances.
    """

    categories = [OverSampling.cat_extensive,
                  OverSampling.cat_uses_clustering,
                  OverSampling.cat_density_based,
                  OverSampling.cat_noise_removal]

    def __init__(self, proportion=1.0, n_neighbors=5, n_clus_maj=20, c_thres=0.8, n_jobs=1, random_state=None):
        """
        Constructor of the sampling object

        Args:
            proportion (float): proportion of the difference of n_maj and n_min to sample
                                    e.g. 1.0 means that after sampling the number of minority
                                    samples will be equal to the number of majority samples
            n_neighbors (int): number of neighbors
            n_clus_maj (int): number of majority clusters
            c_thres (float): threshold on distances
            n_jobs (int): number of parallel jobs
            random_state (int/RandomState/None): initializer of random_state, like in sklearn
        """
        super().__init__()
        self.check_greater_or_equal(proportion, "proportion", 0)
        self.check_greater_or_equal(n_neighbors, "n_neighbors", 1)
        self.check_greater_or_equal(n_clus_maj, "n_clus_maj", 1)
        self.check_greater_or_equal(c_thres, "c_thres", 0)
        self.check_n_jobs(n_jobs, 'n_jobs')

        self.proportion = proportion
        self.n_neighbors = n_neighbors
        self.n_clus_maj = n_clus_maj
        self.c_thres = c_thres
        self.n_jobs = n_jobs

        self.set_random_state(random_state)

    @classmethod
    def parameter_combinations(cls):
        """
        Generates reasonable paramter combinations.

        Returns:
            list(dict): a list of meaningful paramter combinations
        """
        return cls.generate_parameter_combinations({'proportion': [0.1, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0],
                                                    'n_neighbors': [3, 5, 7],
                                                    'n_clus_maj': [5, 7, 9],
                                                    'c_thres': [0.5, 0.8]})

    def sample(self, X, y):
        """
        Does the sample generation according to the class paramters.

        Args:
            X (np.ndarray): training set
            y (np.array): target labels

        Returns:
            (np.ndarray, np.array): the extended training set and target labels
        """
        _logger.info(self.__class__.__name__ + ": " + "Running sampling via %s" % self.descriptor())

        self.class_label_statistics(X, y)

        num_to_sample = self.number_of_instances_to_sample(self.proportion, self.class_stats[self.majority_label],
                                                           self.class_stats[self.minority_label])

        if num_to_sample == 0:
            _logger.warning(self.__class__.__name__ + ": " + "Sampling is not needed")
            return X.copy(), y.copy()

        X_orig, y_orig = X, y

        # fitting nearest neighbors to find neighbors of all samples
        nn = NearestNeighbors(n_neighbors=min([len(X), self.n_neighbors + 1]), n_jobs=self.n_jobs)
        nn.fit(X)
        # dist, ind = nn.kneighbors(X)

        # identifying as noise those samples which do not have neighbors of the same label
        # noise = np.where(np.array([np.sum(y[ind[i][1:]] == y[i]) == 0 for i in range(len(X))]))[0]
        #
        # # removing noise
        # X = np.delete(X, noise, axis=0)
        # y = np.delete(y, noise)

        # extarcting modified minority and majority datasets
        X_min = X[y == 1]
        X_maj = X[y == 0]

        if len(X_min) == 0:
            _logger.info("All minority samples removed as noise")
            return X_orig.copy(), y_orig.copy()

        n_clus_maj = min([len(X_maj), self.n_clus_maj])

        # clustering majority samples
        ac = AgglomerativeClustering(n_clusters=n_clus_maj)
        ac.fit(X_maj)
        maj_clusters = [np.where(ac.labels_ == i)[0] for i in range(n_clus_maj)]

        if len(maj_clusters) == 0:
            return X_orig.copy(), y_orig.copy()

        # initialize minority clusters
        min_clusters = [np.array([i]) for i in range(len(X_min))]

        # compute minority distance matrix of cluster
        dm_min = pairwise_distances(X_min)
        for i in range(len(dm_min)):
            dm_min[i, i] = np.inf

        # compute distance matrix of minority and majority clusters
        dm_maj = np.zeros(shape=(len(X_min), len(maj_clusters)))
        for i in range(len(X_min)):
            for j in range(len(maj_clusters)):
                dm_maj[i, j] = np.min(pairwise_distances(X_min[min_clusters[i]], X_maj[maj_clusters[j]]))

        # compute threshold
        nn = NearestNeighbors(n_neighbors=len(X_min), n_jobs=self.n_jobs)
        nn.fit(X_min)
        dist, ind = nn.kneighbors(X_min)
        d_med = np.median(dist, axis=1)
        T = np.mean(d_med) * self.c_thres

        # do the clustering of minority samples
        while True:
            # finding minimum distance between minority clusters
            pi = np.min(dm_min)

            # if the minimum distance is higher than the threshold, stop
            if pi > T:
                break

            # find cluster pair of minimum distance
            min_dist_pair = np.where(dm_min == pi)
            min_i = min_dist_pair[0][0]
            min_j = min_dist_pair[1][0]

            # Step 3 - find majority clusters closer than pi
            A = np.where(np.logical_and(dm_maj[min_i] < pi, dm_maj[min_j] < pi))[0]

            # Step 4 - checking if there is a majority cluster between the minority ones
            if len(A) > 0:
                dm_min[min_i, min_j] = np.inf
                dm_min[min_j, min_i] = np.inf
            else:
                # Step 5
                # unifying minority clusters
                min_clusters[min_i] = np.hstack([min_clusters[min_i], min_clusters[min_j]])
                # removing one of them
                min_clusters = np.delete(min_clusters, min_j)

                # updating the minority distance matrix
                dm_min[min_i] = np.min(np.vstack([dm_min[min_i], dm_min[min_j]]), axis=0)
                dm_min[:, min_i] = dm_min[min_i]
                # removing jth row and column (merged in i)
                dm_min = np.delete(dm_min, min_j, axis=0)
                dm_min = np.delete(dm_min, min_j, axis=1)

                # fixing the diagonal elements
                for i in range(len(dm_min)):
                    dm_min[i, i] = np.inf

                # updating the minority-majority distance matrix
                dm_maj[min_i] = np.min(np.vstack([dm_maj[min_i], dm_maj[min_j]]), axis=0)
                dm_maj = np.delete(dm_maj, min_j, axis=0)

        # adaptive sub-cluster sizing
        proba = []
        # going through all minority clusters
        for c in min_clusters:
            # checking if cluster size is higher than 1
            if len(c) > 1:
                k = min([len(c), 5])
                kfold = KFold(k, random_state=self.random_state)
                preds = []
                # executing k-fold cross validation with linear discriminant analysis
                for train, test in kfold.split(X_min[c]):
                    X_train = np.vstack([X_maj, X_min[c][train]])
                    y_train = np.hstack([np.repeat(self.majority_label, len(X_maj)),
                                         np.repeat(self.minority_label, len(X_min[c][train]))])
                    preds.append(LogisticRegression().fit(X_train, y_train).predict_proba(X_min[test])[:1])
                preds = np.hstack(preds)
                # extracting error rate
                proba.append((c, np.sum(preds[: 1]) / len(c)))
            else:
                proba.append((c, 1.0))
        return min_clusters, proba, maj_clusters

        # synthetic instance generation - determining within cluster distribution
        # finding majority neighbor distances of minority samples
        nn = NearestNeighbors(n_neighbors=1, n_jobs=self.n_jobs)
        nn.fit(X_maj)
        dist, ind = nn.kneighbors(X_min)
        dist = dist / len(X[0])
        dist = 1.0 / dist

        # computing the THs
        THs = []
        for c in min_clusters:
            THs.append(np.mean(dist[c, 0]))

        # determining within cluster distributions
        within_cluster_dist = []
        for i, c in enumerate(min_clusters):
            Gamma = dist[c, 0]
            Gamma[Gamma > THs[i]] = THs[i]
            within_cluster_dist.append(Gamma / np.sum(Gamma))

        # extracting within cluster neighbors
        within_cluster_neighbors = []
        for c in min_clusters:
            nn = NearestNeighbors(n_neighbors=min([len(c), self.n_neighbors]), n_jobs=self.n_jobs)
            nn.fit(X_min[c])
            within_cluster_neighbors.append(nn.kneighbors(X_min[c])[1])

        # do the sampling
        samples = []
        while len(samples) < num_to_sample:
            # choose random cluster index
            cluster_idx = self.random_state.choice(np.arange(len(min_clusters)), p=min_cluster_dist)
            if len(min_clusters[cluster_idx]) > 1:
                # if the cluster has at least two elemenets
                sample_idx = self.random_state.choice(np.arange(len(min_clusters[cluster_idx])),
                                                      p=within_cluster_dist[cluster_idx])
                neighbor_idx = self.random_state.choice(within_cluster_neighbors[cluster_idx][sample_idx][1:])
                point = X_min[min_clusters[cluster_idx][sample_idx]]
                neighbor = X_min[min_clusters[cluster_idx][neighbor_idx]]
                samples.append(self.sample_between_points(point, neighbor))
            else:
                samples.append(X_min[min_clusters[cluster_idx][0]])

        return np.vstack([X, np.vstack(samples)]), np.hstack([y, np.repeat(self.minority_label, len(samples))])

    def get_params(self, deep=False):
        """
        Returns:
            dict: the parameters of the current sampling object
        """
        return {'proportion': self.proportion,
                'n_neighbors': self.n_neighbors,
                'n_clus_maj': self.n_clus_maj,
                'c_thres': self.c_thres,
                'n_jobs': self.n_jobs,
                'random_state': self._random_state_init}


# df = pd.read_csv('creditcard.csv')
# df.drop('Time', axis=1, inplace=True)
# df.dropna(axis=0, inplace=True)
# bad_loans = len(df[df['Class'] == 1])
# good_loan_indices = df[df.Class == 0].index
# random_indices = np.random.choice(good_loan_indices, 10000, replace=False)
# bad_loans_indices = df[df.Class == 1].index
# under_sample_indices = np.concatenate([bad_loans_indices, random_indices])
# under_sample = df.loc[under_sample_indices]
# under_sample = under_sample.dropna(axis=0)
# X = under_sample.drop('Class', axis=1).as_matrix()
# y = under_sample['Class'].as_matrix()
df = pd.read_csv('pima.csv')


def cat(y):
    if y == 'negative':
        return 0
    elif y == 'positive':
        return 1


df['positive'] = df['positive'].apply(cat)
X = df.drop('positive', axis=1).as_matrix()
y = df['positive'].as_matrix()
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=4)
min_clusters, p, maj = A_SUWO().sample(X=X_train, y=y_train)
cluster = []
label = []
i = 0
for item in p:
    for node in item[0]:
        cluster.append(X_train[y_train == 1][node])
        label.append(f'min{i}')
    i += 1
i = 0
for item in maj:
    for node in item:
        cluster.append(X_train[y_train == 0][node])
        label.append(f'maj{i}')
    i += 1
cluster = np.array(cluster)
label = np.array(label)
clf = NearestCentroid()
clf.fit(cluster, label)
lr = LogisticRegression()
lr.fit(X_train, y_train)
pred = lr.predict_proba(X_test)
y_pred = []
min_nodes = []
for j in range(min_clusters.shape[0]):
    proba = 0
    prob_all = []
    for node in min_clusters[j]:
        proba += lr.predict_proba(X_train[y_train == 1][node].reshape(1, -1))[0][1]
        prob_all.append(proba)

    prob_all = np.array(prob_all)
    min_nodes.append((proba/min_clusters[j].shape[0], np.std(prob_all)))


maj_nodes = []
for j in range(len(maj)):
    proba = 0
    prob_all = []
    for node in maj[j]:
        proba += lr.predict_proba(X_train[y_train == 0][node].reshape(1, -1))[0][1]
        prob_all.append(proba)
    prob_all = np.array(prob_all)
    maj_nodes.append((proba/maj[j].shape[0], np.std(prob_all)))

for j in range(X_test.shape[0]):
    labels = clf.predict(X_test[j].reshape(1, -1))[0]
    if labels[1] == 'i':
        if pred[j][1] >= min_nodes[int(labels[3])][0] - min_nodes[int(labels[3])][1] - 0.1:
            y_pred.append(1)
            if y_test[j] == 0:
                print('min', y_test[j])

        else:
            y_pred.append(0)
    else:
        if pred[j][1] >= maj_nodes[int(labels[3])][0]:
            y_pred.append(1)

        else:
            y_pred.append(0)
            if y_test[j] == 1:
                print('maj', y_test[j])


Matrix = confusion_matrix(y_test, y_pred)
TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc = TN / (TN + FN)

y_pred_without = lr.predict(X_test)
Matrix = confusion_matrix(y_test, y_pred_without)

TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy_w = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc_w = TN / (TN + FN)

print(Accuracy)
print(Fraud_acc)
print(Accuracy_w)
print(Fraud_acc_w)



def best_fit_distribution(data, bins=200, ax=None):
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)

