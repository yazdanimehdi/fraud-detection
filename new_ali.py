import warnings

from sklearn.model_selection import RepeatedStratifiedKFold, KFold, cross_val_score, StratifiedKFold, train_test_split
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.metrics import log_loss, roc_auc_score, accuracy_score, confusion_matrix, f1_score
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from imblearn.over_sampling import ADASYN, BorderlineSMOTE, KMeansSMOTE, RandomOverSampler, SMOTE, SVMSMOTE
from imblearn.under_sampling import ClusterCentroids, CondensedNearestNeighbour, EditedNearestNeighbours, RepeatedEditedNearestNeighbours, AllKNN, InstanceHardnessThreshold, NearMiss, NeighbourhoodCleaningRule, OneSidedSelection, RandomUnderSampler, TomekLinks

df = pd.read_csv('glass.csv')


def cat(y):
    if y == 'negative':
        return 0
    elif y == 'positive':
        return 1


df['negative'] = df['negative'].apply(cat)
df.dropna(axis=0, inplace=True)
df.drop(['F'], axis=1, inplace=True)
X = df.drop('negative', axis=1).as_matrix()
y = df['negative'].as_matrix()

# df = pd.read_csv('creditcard.csv')
# df.drop('Time', axis=1, inplace=True)
# df.dropna(axis=0, inplace=True)
# bad_loans = len(df[df['Class'] == 1])
# good_loan_indices = df[df.Class == 0].index
# random_indices = np.random.choice(good_loan_indices, 10000, replace=False)
# bad_loans_indices = df[df.Class == 1].index
# under_sample_indices = np.concatenate([bad_loans_indices, random_indices])
# under_sample = df.loc[under_sample_indices]
# under_sample = under_sample.dropna(axis=0)
# X = under_sample.drop('Class', axis=1).as_matrix()
# y = under_sample['Class'].as_matrix()
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=1)
n_clusters = []
cluster_min = KMeans(n_clusters=10, max_iter=1000, random_state=10)
cluster_min.fit(X_train[y_train == 1])
labels_min = cluster_min.labels_
cluster_maj = KMeans(n_clusters=14, max_iter=1000, random_state=10)
cluster_maj.fit(X_train[y_train == 0])
labels_maj = cluster_maj.labels_
labels_maj_set = set(labels_maj)
labels_min_set = set(labels_min)

clusters_maj = [[] for j in range(len(labels_maj_set))]
for item in labels_maj_set:
    for i in range(0, X_train[y_train == 0].shape[0]):
        if labels_maj[i] == item:
            clusters_maj[item].append(X_train[y_train == 0][i])

clusters_min = [[] for j in range(len(labels_min_set))]
for item in labels_min_set:
    for i in range(0, X_train[y_train == 1].shape[0]):
        if labels_min[i] == item:
            clusters_min[item].append(X_train[y_train == 1][i])

# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_train, y_train)

proba_min_cluster = [[] for i in range(len(labels_min_set))]
for item in range(len(clusters_min)):
    for node in clusters_min[item]:
        proba_min_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

proba_maj_cluster = [[] for i in range(len(labels_maj_set))]
for item in range(len(clusters_maj)):
    for node in clusters_maj[item]:
        proba_maj_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

new_maj = []
for cl in proba_maj_cluster:
    array = np.array(cl)
    median = np.median(array)
    flag = False
    # while not flag:
    # flag = True
    B = plt.boxplot(array)
    # median = np.median(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    for item in array:
        if item > high[1] or item < low[1]:
            # flag = False
            i, = np.where(array == item)
            array = np.delete(array, i[0])
    new_maj.append(array)

new_min = []
for cl in proba_min_cluster:
    array = np.array(cl)
    median = np.median(array)
    # flag = False
    # while not flag:
    # flag = True
    B = plt.boxplot(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    for item in array:
        if item > high[1] or item < low[1]:
            # flag = False
            i, = np.where(array == item)
            array = np.delete(array, i[0])
    new_min.append(array)


min_data = []
for item in proba_min_cluster:
    array = np.array(item)
    min_data.append((np.average(array), np.std(array)))

maj_data = []
for item in proba_maj_cluster:
    array = np.array(item)
    maj_data.append((np.average(array), np.std(array)))

y_pred_new = []
for j in range(X_test.shape[0]):
    label_new_min = cluster_min.predict(X_test[j].reshape(1, -1))
    label_new_maj = cluster_maj.predict(X_test[j].reshape(1, -1))

    if lr.predict_proba(X_test[j].reshape(1, -1))[0][1] >= 0.05:
        label_new = int(label_new_min)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if min_data[label_new][0] + min_data[label_new][1] * 3 >= probab >= min_data[label_new][0] - min_data[label_new][1] * 3:
            y_pred_new.append(1)
        else:
            label_new = int(label_new_maj)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= maj_data[label_new][0] + maj_data[label_new][1]:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)


    else:
        label_new = int(label_new_maj)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if maj_data[label_new][0] + maj_data[label_new][1] * 3 >= probab >= maj_data[label_new][0] - maj_data[label_new][1] * 3:
            y_pred_new.append(0)
        else:
            label_new = int(label_new_min)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= min_data[label_new][0] - min_data[label_new][1]:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)


Matrix = confusion_matrix(y_test, y_pred_new)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy = (TP + TN) / (TP + FP + TN + FN)
precision = TP / (TP + FP)
recall = TP / (TP + FN)
fp_rate = FP/(FP + TN)
specificity = TN / (TN + FP)
F1_score = (2 * precision * recall) / (precision + recall)

y_pred_without = lr.predict(X_test)

Matrix = confusion_matrix(y_test, y_pred_without)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_w = (TP + TN) / (TP + FP + TN + FN)
precision_w = TP / (TP + FP)
recall_w = TP / (TP + FN)
specificity_w = TN / (TN + FP)
fp_rate_w = FP/(FP + TN)
F1_score_w = (2 * precision_w * recall_w) / (precision_w + recall_w)
n_clusters_w = []
n_clusters.append((Accuracy, precision, recall, specificity, F1_score))
n_clusters_w.append((Accuracy_w, precision_w, recall_w, specificity_w, F1_score_w))

print(n_clusters)
print(n_clusters_w)

# new_maj = []
# for cl in proba_maj_cluster:
#     array = np.array(cl)
#     median = np.median(array)
#     flag = False
#     while not flag:
#         flag = True
#         B = plt.boxplot(array)
#         median = np.median(array)
#         low = B['whiskers'][0].get_ydata()
#         high = B['whiskers'][1].get_ydata()
#         for item in array:
#             if item > high[1] or item < low[1]:
#                 flag = False
#                 i, = np.where(array == item)
#                 array = np.delete(array, i[0])
#     new_maj.append(array)


# def best_fit_distribution(data, bins=200, ax=None):
#     # Get histogram of original data
#     y, x = np.histogram(data, bins=bins, density=True)
#     x = (x + np.roll(x, -1))[:-1] / 2.0
#
#     # Distributions to check
#     DISTRIBUTIONS = [
#         st.alpha,st.chi,st.chi2,st.cosine,
#         st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm, st.gennorm,st.genexpon,
#         st.genextreme,st.gausshyper,st.gamma,st.gengamma
#     ]
#
#     # Best holders
#     best_distribution = st.norm
#     best_params = (0.0, 1.0)
#     best_sse = np.inf
#
#     # Estimate distribution parameters from data
#     for distribution in DISTRIBUTIONS:
#
#         # Try to fit the distribution
#         try:
#             # Ignore warnings from data that can't be fit
#             with warnings.catch_warnings():
#                 warnings.filterwarnings('ignore')
#
#                 # fit dist to data
#                 params = distribution.fit(data)
#                 # Separate parts of parameters
#                 arg = params[:-2]
#                 loc = params[-2]
#                 scale = params[-1]
#
#                 # Calculate fitted PDF and error with fit in distribution
#                 pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
#                 sse = np.sum(np.power(y - pdf, 2.0))
#
#                 # if axis pass in add to plot
#                 try:
#                     if ax:
#                         pd.Series(pdf, x).plot(ax=ax)
#                 except Exception:
#                     pass
#
#                 # identify if this distribution is better
#                 if best_sse > sse > 0:
#                     best_distribution = distribution
#                     best_params = params
#                     best_sse = sse
#
#         except Exception as e:
#             print(e)
#             pass
#
#     return (best_distribution.name, best_params)
#
# for item in new_maj:
#     print(best_fit_distribution(item))

ada = ADASYN(random_state=42)
X_res, y_res = ada.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_ADASYN = lr.predict(X_test)
n_clusters_ADASYN = []
Matrix = confusion_matrix(y_test, y_pred_ADASYN)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_ADASYN = (TP + TN) / (TP + FP + TN + FN)
precision_ADASYN = TP / (TP + FP)
recall_ADASYN = TP / (TP + FN)
specificity_ADASYN = TN / (TN + FP)
fp_rate_ADASYN = FP/(FP + TN)
F1_score_ADASYN= (2 * precision_ADASYN * recall_ADASYN) / (precision_ADASYN + recall_ADASYN)
n_clusters_ADASYN.append((Accuracy_ADASYN, precision_ADASYN, recall_ADASYN, specificity_ADASYN, F1_score_ADASYN))
print(n_clusters_ADASYN)

sm = BorderlineSMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_BorderlineSMOTE= lr.predict(X_test)
n_clusters_BorderlineSMOTE = []
Matrix = confusion_matrix(y_test, y_pred_BorderlineSMOTE)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_BorderlineSMOTE = (TP + TN) / (TP + FP + TN + FN)
precision_BorderlineSMOTE = TP / (TP + FP)
recall_BorderlineSMOTE = TP / (TP + FN)
specificity_BorderlineSMOTE = TN / (TN + FP)
fp_rate_BorderlineSMOTE = FP/(FP + TN)
F1_score_BorderlineSMOTE = (2 * precision_BorderlineSMOTE * recall_BorderlineSMOTE) / (precision_BorderlineSMOTE + recall_BorderlineSMOTE)
n_clusters_BorderlineSMOTE.append((Accuracy_BorderlineSMOTE, precision_BorderlineSMOTE, recall_BorderlineSMOTE, specificity_BorderlineSMOTE, F1_score_BorderlineSMOTE))
print(n_clusters_BorderlineSMOTE)


sm = KMeansSMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)
# # lr = LogisticRegression(max_iter=5000, random_state=4)
# lr = SVC(probability=True, random_state=4)
# lr.fit(X_res, y_res)
#
# y_pred_KMeansSMOTE = lr.predict(X_test)
# n_clusters_KMeansSMOTE = []
# Matrix = confusion_matrix(y_test, y_pred_KMeansSMOTE)
# TN = Matrix[0][0]
# FN = Matrix[0][1]
# FP = Matrix[1][0]
# TP = Matrix[1][1]
# Accuracy_KMeansSMOTE = (TP + TN) / (TP + FP + TN + FN)
# precision_KMeansSMOTE = TP / (TP + FP)
# recall_KMeansSMOTE = TP / (TP + FN)
# specificity_KMeansSMOTE = TN / (TN + FP)
# fp_rate_KMeansSMOTE = FP/(FP + TN)
# F1_score_KMeansSMOTE = (2 * precision_KMeansSMOTE * recall_KMeansSMOTE) / (precision_KMeansSMOTE + recall_KMeansSMOTE)
# n_clusters_KMeansSMOTE.append((Accuracy_KMeansSMOTE, precision_KMeansSMOTE, recall_KMeansSMOTE, specificity_KMeansSMOTE, F1_score_KMeansSMOTE))
# print(n_clusters_KMeansSMOTE)

ros = RandomOverSampler(random_state=42)
X_res, y_res = ros.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_RandomOverSampler = lr.predict(X_test)
n_clusters_RandomOverSampler = []
Matrix = confusion_matrix(y_test, y_pred_RandomOverSampler)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_RandomOverSampler = (TP + TN) / (TP + FP + TN + FN)
precision_RandomOverSampler = TP / (TP + FP)
recall_RandomOverSampler = TP / (TP + FN)
specificity_RandomOverSampler = TN / (TN + FP)
fp_rate_RandomOverSampler = FP/(FP + TN)
F1_score_RandomOverSampler = (2 * precision_RandomOverSampler * recall_RandomOverSampler) / (precision_RandomOverSampler + recall_RandomOverSampler)
n_clusters_RandomOverSampler.append((Accuracy_RandomOverSampler, precision_RandomOverSampler, recall_RandomOverSampler, specificity_RandomOverSampler, F1_score_RandomOverSampler))
print(n_clusters_RandomOverSampler)


sm = SMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_SMOTE = lr.predict(X_test)
n_clusters_SMOTE = []
Matrix = confusion_matrix(y_test, y_pred_SMOTE)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_SMOTE = (TP + TN) / (TP + FP + TN + FN)
precision_SMOTE = TP / (TP + FP)
recall_SMOTE = TP / (TP + FN)
specificity_SMOTE = TN / (TN + FP)
fp_rate_SMOTE = FP/(FP + TN)
F1_score_SMOTE = (2 * precision_SMOTE * recall_SMOTE) / (precision_SMOTE + recall_SMOTE)
n_clusters_SMOTE.append((Accuracy_SMOTE, precision_SMOTE, recall_SMOTE, specificity_SMOTE, F1_score_SMOTE))
print(n_clusters_SMOTE)


sm = SMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_SMOTE = lr.predict(X_test)
n_clusters_SMOTE = []
Matrix = confusion_matrix(y_test, y_pred_SMOTE)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_SMOTE = (TP + TN) / (TP + FP + TN + FN)
precision_SMOTE = TP / (TP + FP)
recall_SMOTE = TP / (TP + FN)
specificity_SMOTE = TN / (TN + FP)
fp_rate_SMOTE = FP/(FP + TN)
F1_score_SMOTE = (2 * precision_SMOTE * recall_SMOTE) / (precision_SMOTE + recall_SMOTE)
n_clusters_SMOTE.append((Accuracy_SMOTE, precision_SMOTE, recall_SMOTE, specificity_SMOTE, F1_score_SMOTE))
print(n_clusters_SMOTE)


sm = SVMSMOTE(random_state=42)
X_res, y_res = sm.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_SVMSMOTE = lr.predict(X_test)
n_clusters_SVMSMOTE = []
Matrix = confusion_matrix(y_test, y_pred_SVMSMOTE)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_SVMSMOTE = (TP + TN) / (TP + FP + TN + FN)
precision_SVMSMOTE = TP / (TP + FP)
recall_SVMSMOTE = TP / (TP + FN)
specificity_SVMSMOTE = TN / (TN + FP)
fp_rate_SVMSMOTE = FP/(FP + TN)
F1_score_SVMSMOTE = (2 * precision_SVMSMOTE * recall_SVMSMOTE) / (precision_SVMSMOTE + recall_SVMSMOTE)
n_clusters_SVMSMOTE.append((Accuracy_SVMSMOTE, precision_SVMSMOTE, recall_SVMSMOTE, specificity_SVMSMOTE, F1_score_SVMSMOTE))
print(n_clusters_SVMSMOTE)

# UnderSampling

cc = ClusterCentroids(random_state=42)
X_res, y_res = cc.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_ClusterCentroids = lr.predict(X_test)
n_clusters_ClusterCentroids = []
Matrix = confusion_matrix(y_test, y_pred_ClusterCentroids)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_ClusterCentroids = (TP + TN) / (TP + FP + TN + FN)
precision_ClusterCentroids = TP / (TP + FP)
recall_ClusterCentroids = TP / (TP + FN)
specificity_ClusterCentroids = TN / (TN + FP)
fp_rate_ClusterCentroids = FP/(FP + TN)
F1_score_ClusterCentroids = (2 * precision_ClusterCentroids * recall_ClusterCentroids) / (precision_ClusterCentroids + recall_ClusterCentroids)
n_clusters_ClusterCentroids.append((Accuracy_ClusterCentroids, precision_ClusterCentroids, recall_ClusterCentroids, specificity_ClusterCentroids, F1_score_ClusterCentroids))
print(n_clusters_ClusterCentroids)


cnn = CondensedNearestNeighbour(random_state=42)
X_res, y_res = cnn.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_CondensedNearestNeighbour = lr.predict(X_test)
n_clusters_CondensedNearestNeighbour = []
Matrix = confusion_matrix(y_test, y_pred_CondensedNearestNeighbour)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_CondensedNearestNeighbour = (TP + TN) / (TP + FP + TN + FN)
precision_CondensedNearestNeighbour = TP / (TP + FP)
recall_CondensedNearestNeighbour = TP / (TP + FN)
specificity_CondensedNearestNeighbour = TN / (TN + FP)
fp_rate_CondensedNearestNeighbour = FP/(FP + TN)
F1_score_CondensedNearestNeighbour = (2 * precision_CondensedNearestNeighbour * recall_CondensedNearestNeighbour) / (precision_CondensedNearestNeighbour + recall_CondensedNearestNeighbour)
n_clusters_CondensedNearestNeighbour.append((Accuracy_CondensedNearestNeighbour, precision_CondensedNearestNeighbour, recall_CondensedNearestNeighbour, specificity_CondensedNearestNeighbour, F1_score_CondensedNearestNeighbour))
print(n_clusters_CondensedNearestNeighbour)


enn = EditedNearestNeighbours()
X_res, y_res = enn.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_EditedNearestNeighbours = lr.predict(X_test)
n_clusters_EditedNearestNeighbours = []
Matrix = confusion_matrix(y_test, y_pred_EditedNearestNeighbours)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_EditedNearestNeighbours = (TP + TN) / (TP + FP + TN + FN)
precision_EditedNearestNeighbours = TP / (TP + FP)
recall_EditedNearestNeighbours = TP / (TP + FN)
specificity_EditedNearestNeighbours = TN / (TN + FP)
fp_rate_EditedNearestNeighbours = FP/(FP + TN)
F1_score_EditedNearestNeighbours = (2 * precision_EditedNearestNeighbours * recall_EditedNearestNeighbours) / (precision_EditedNearestNeighbours + recall_EditedNearestNeighbours)
n_clusters_EditedNearestNeighbours.append((Accuracy_EditedNearestNeighbours, precision_EditedNearestNeighbours, recall_EditedNearestNeighbours, specificity_EditedNearestNeighbours, F1_score_EditedNearestNeighbours))
print(n_clusters_EditedNearestNeighbours)


renn = RepeatedEditedNearestNeighbours()
X_res, y_res = renn.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_RepeatedEditedNearestNeighbours = lr.predict(X_test)
n_clusters_RepeatedEditedNearestNeighbours = []
Matrix = confusion_matrix(y_test, y_pred_RepeatedEditedNearestNeighbours)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_RepeatedEditedNearestNeighbours = (TP + TN) / (TP + FP + TN + FN)
precision_RepeatedEditedNearestNeighbours = TP / (TP + FP)
recall_RepeatedEditedNearestNeighbours = TP / (TP + FN)
specificity_RepeatedEditedNearestNeighbours = TN / (TN + FP)
fp_rate_RepeatedEditedNearestNeighbours = FP/(FP + TN)
F1_score_RepeatedEditedNearestNeighbours = (2 * precision_RepeatedEditedNearestNeighbours * recall_RepeatedEditedNearestNeighbours) / (precision_RepeatedEditedNearestNeighbours + recall_RepeatedEditedNearestNeighbours)
n_clusters_RepeatedEditedNearestNeighbours.append((Accuracy_RepeatedEditedNearestNeighbours, precision_RepeatedEditedNearestNeighbours, recall_RepeatedEditedNearestNeighbours, specificity_RepeatedEditedNearestNeighbours, F1_score_RepeatedEditedNearestNeighbours))
print(n_clusters_RepeatedEditedNearestNeighbours)




allknn = AllKNN()
X_res, y_res = allknn.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_AllKNN = lr.predict(X_test)
n_clusters_AllKNN = []
Matrix = confusion_matrix(y_test, y_pred_AllKNN)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_AllKNN = (TP + TN) / (TP + FP + TN + FN)
precision_AllKNN = TP / (TP + FP)
recall_AllKNN = TP / (TP + FN)
specificity_AllKNN = TN / (TN + FP)
fp_rate_AllKNN = FP/(FP + TN)
F1_score_AllKNN = (2 * precision_AllKNN * recall_AllKNN) / (precision_AllKNN + recall_AllKNN)
n_clusters_AllKNN.append((Accuracy_AllKNN, precision_AllKNN, recall_AllKNN, specificity_AllKNN, F1_score_AllKNN))
print(n_clusters_AllKNN)


iht = InstanceHardnessThreshold(random_state=42)
X_res, y_res = iht.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)
#
# y_pred_InstanceHardnessThreshold = lr.predict(X_test)
n_clusters_InstanceHardnessThreshold = [(0, 0, 0, 0, 0)]
# Matrix = confusion_matrix(y_test, y_pred_InstanceHardnessThreshold)
# TN = Matrix[0][0]
# FN = Matrix[0][1]
# FP = Matrix[1][0]
# TP = Matrix[1][1]
# Accuracy_InstanceHardnessThreshold = (TP + TN) / (TP + FP + TN + FN)
# precision_InstanceHardnessThreshold = TP / (TP + FP)
# recall_InstanceHardnessThreshold = TP / (TP + FN)
# specificity_InstanceHardnessThreshold = TN / (TN + FP)
# fp_rate_InstanceHardnessThreshold = FP/(FP + TN)
# F1_score_InstanceHardnessThreshold = (2 * precision_InstanceHardnessThreshold * recall_InstanceHardnessThreshold) / (precision_InstanceHardnessThreshold + recall_InstanceHardnessThreshold)
# n_clusters_InstanceHardnessThreshold.append((Accuracy_InstanceHardnessThreshold, precision_InstanceHardnessThreshold, recall_InstanceHardnessThreshold, specificity_InstanceHardnessThreshold, F1_score_InstanceHardnessThreshold))
# print(n_clusters_InstanceHardnessThreshold)


nm = NearMiss()
X_res, y_res = nm.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_NearMiss = lr.predict(X_test)
n_clusters_NearMiss = []
Matrix = confusion_matrix(y_test, y_pred_NearMiss)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_NearMiss = (TP + TN) / (TP + FP + TN + FN)
precision_NearMiss = TP / (TP + FP)
recall_NearMiss = TP / (TP + FN)
specificity_NearMiss = TN / (TN + FP)
fp_rate_NearMiss = FP/(FP + TN)
F1_score_NearMiss = (2 * precision_NearMiss * recall_NearMiss) / (precision_NearMiss + recall_NearMiss)
n_clusters_NearMiss.append((Accuracy_NearMiss, precision_NearMiss, recall_NearMiss, specificity_NearMiss, F1_score_NearMiss))
print(n_clusters_NearMiss)


ncr = NeighbourhoodCleaningRule()
X_res, y_res = ncr.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_NeighbourhoodCleaningRule = lr.predict(X_test)
n_clusters_NeighbourhoodCleaningRule = []
Matrix = confusion_matrix(y_test, y_pred_NeighbourhoodCleaningRule)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_NeighbourhoodCleaningRule = (TP + TN) / (TP + FP + TN + FN)
precision_NeighbourhoodCleaningRule = TP / (TP + FP)
recall_NeighbourhoodCleaningRule = TP / (TP + FN)
specificity_NeighbourhoodCleaningRule = TN / (TN + FP)
fp_rate_NeighbourhoodCleaningRule = FP/(FP + TN)
F1_score_NeighbourhoodCleaningRule = (2 * precision_NeighbourhoodCleaningRule * recall_NeighbourhoodCleaningRule) / (precision_NeighbourhoodCleaningRule + recall_NeighbourhoodCleaningRule)
n_clusters_NeighbourhoodCleaningRule.append((Accuracy_NeighbourhoodCleaningRule, precision_NeighbourhoodCleaningRule, recall_NeighbourhoodCleaningRule, specificity_NeighbourhoodCleaningRule, F1_score_NeighbourhoodCleaningRule))
print(n_clusters_NeighbourhoodCleaningRule)



oss = OneSidedSelection(random_state=42)
X_res, y_res = oss.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_OneSidedSelection = lr.predict(X_test)
n_clusters_OneSidedSelection = []
Matrix = confusion_matrix(y_test, y_pred_OneSidedSelection)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_OneSidedSelection = (TP + TN) / (TP + FP + TN + FN)
precision_OneSidedSelection = TP / (TP + FP)
recall_OneSidedSelection = TP / (TP + FN)
specificity_OneSidedSelection = TN / (TN + FP)
fp_rate_OneSidedSelection = FP/(FP + TN)
F1_score_OneSidedSelection = (2 * precision_OneSidedSelection * recall_OneSidedSelection) / (precision_OneSidedSelection + recall_OneSidedSelection)
n_clusters_OneSidedSelection.append((Accuracy_OneSidedSelection, precision_OneSidedSelection, recall_OneSidedSelection, specificity_OneSidedSelection, F1_score_OneSidedSelection))
print(n_clusters_OneSidedSelection)


rus = RandomUnderSampler(random_state=42)
X_res, y_res = rus.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_RandomUnderSampler = lr.predict(X_test)
n_clusters_RandomUnderSampler = []
Matrix = confusion_matrix(y_test, y_pred_RandomUnderSampler)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_RandomUnderSampler = (TP + TN) / (TP + FP + TN + FN)
precision_RandomUnderSampler = TP / (TP + FP)
recall_RandomUnderSampler = TP / (TP + FN)
specificity_RandomUnderSampler = TN / (TN + FP)
fp_rate_RandomUnderSampler = FP/(FP + TN)
F1_score_RandomUnderSampler = (2 * precision_RandomUnderSampler * recall_RandomUnderSampler) / (precision_RandomUnderSampler + recall_RandomUnderSampler)
n_clusters_RandomUnderSampler.append((Accuracy_RandomUnderSampler, precision_RandomUnderSampler, recall_RandomUnderSampler, specificity_RandomUnderSampler, F1_score_RandomUnderSampler))
print(n_clusters_RandomUnderSampler)


tl = TomekLinks()
X_res, y_res = tl.fit_resample(X_train, y_train)
# lr = LogisticRegression(max_iter=5000, random_state=4)
lr = SVC(probability=True, random_state=4)
lr.fit(X_res, y_res)

y_pred_TomekLinks = lr.predict(X_test)
n_clusters_TomekLinks = []
Matrix = confusion_matrix(y_test, y_pred_TomekLinks)
TN = Matrix[0][0]
FN = Matrix[0][1]
FP = Matrix[1][0]
TP = Matrix[1][1]
Accuracy_TomekLinks = (TP + TN) / (TP + FP + TN + FN)
precision_TomekLinks = TP / (TP + FP)
recall_TomekLinks = TP / (TP + FN)
specificity_TomekLinks = TN / (TN + FP)
fp_rate_TomekLinks = FP/(FP + TN)
F1_score_TomekLinks = (2 * precision_TomekLinks * recall_TomekLinks) / (precision_TomekLinks + recall_TomekLinks)
n_clusters_TomekLinks.append((Accuracy_TomekLinks, precision_TomekLinks, recall_TomekLinks, specificity_TomekLinks, F1_score_TomekLinks))
print(n_clusters_TomekLinks)

dict_res = dict()
dict_res['our_algorithm'] = n_clusters[0]
dict_res['without any method'] = n_clusters_w[0]
dict_res['over_ADASYN'] = n_clusters_ADASYN[0]
dict_res['over_smoth'] = n_clusters_SMOTE[0]
dict_res['over_borderline_smoth'] = n_clusters_BorderlineSMOTE[0]
# dict_res['over_kmeansmoth'] = n_clusters_KMeansSMOTE[0]
dict_res['over_randomoversampling'] = n_clusters_RandomOverSampler[0]
dict_res['over_svmsmoth'] = n_clusters_SVMSMOTE[0]
dict_res['under_clustercentroid'] = n_clusters_ClusterCentroids[0]
dict_res['under_condensed nearest neighbour'] = n_clusters_CondensedNearestNeighbour[0]
dict_res['under_edited nearest neighbour'] = n_clusters_EditedNearestNeighbours[0]
dict_res['under_repeated edited nearest neighbour'] = n_clusters_RepeatedEditedNearestNeighbours[0]
dict_res['under_AllKNN'] = n_clusters_AllKNN[0]
dict_res['under_ instance hardness threshold'] = n_clusters_InstanceHardnessThreshold[0]
dict_res['under_NearMiss'] = n_clusters_NearMiss[0]
dict_res['under_ neighbourhood cleaning rule'] = n_clusters_NeighbourhoodCleaningRule[0]
dict_res['under_one-sided selection'] = n_clusters_OneSidedSelection[0]
dict_res['under_random under-sampling'] = n_clusters_RandomUnderSampler[0]
dict_res['under_Tomek’s links'] = n_clusters_TomekLinks[0]

res = pd.DataFrame(dict_res)
df_new = res.rename(index={0: 'Accuracy', 1: 'Precision', 2: 'recall', 3: 'Specificity', 4: 'F1_score'})
df_new.to_excel('results_glass_svm.xlsx')






