import pandas as pd
import smote_variants as sv
import numpy as np
import imbalanced_databases as imbd


# df = pd.read_csv('creditcard.csv')
# df.drop('Time', axis=1, inplace=True)
# df.dropna(axis=0, inplace=True)
# bad_loans = len(df[df['Class'] == 1])
# good_loan_indices = df[df.Class == 0].index
# random_indices = np.random.choice(good_loan_indices, 10000, replace=False)
# bad_loans_indices = df[df.Class == 1].index
# under_sample_indices = np.concatenate([bad_loans_indices, random_indices])
# under_sample = df.loc[under_sample_indices]
# under_sample = under_sample.dropna(axis=0)
# X = under_sample.drop('Class', axis=1)
# y = under_sample['Class']
dataset= imbd.load_iris0()
X, y= dataset['data'], dataset['target']

p = sv.A_SUWO().sample(X, y)
