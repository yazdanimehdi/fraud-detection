var AUTOBAHNJS_VERSION = "?.?.?",
	AUTOBAHNJS_DEBUG = !0,
	ab = window.ab = {};
ab._version = AUTOBAHNJS_VERSION, Array.prototype.indexOf || (Array.prototype.indexOf = function (o) {
	if (null === this) throw new TypeError;
	var e = new Object(this),
		n = e.length >>> 0;
	if (0 === n) return -1;
	var s = 0;
	if (arguments.length > 0 && ((s = Number(arguments[1])) != s ? s = 0 : 0 !== s && s !== 1 / 0 && s !== -1 / 0 && (s = (s > 0 || -1) * Math.floor(Math.abs(s)))), s >= n) return -1;
	for (var r = s >= 0 ? s : Math.max(n - Math.abs(s), 0); r < n; r++)
		if (r in e && e[r] === o) return r;
	return -1
}), Array.prototype.forEach || (Array.prototype.forEach = function (o, e) {
	var n, s;
	if (null === this) throw new TypeError(" this is null or not defined");
	var r = new Object(this),
		i = r.length >>> 0;
	if ("[object Function]" !== {}.toString.call(o)) throw new TypeError(o + " is not a function");
	for (e && (n = e), s = 0; s < i;) {
		var t;
		s in r && (t = r[s], o.call(n, t, s, r)), s++
	}
}), ab._sliceUserAgent = function (o, e, n) {
	var s = [],
		r = navigator.userAgent,
		i = r.indexOf(o),
		t = r.indexOf(e, i);
	t < 0 && (t = r.length);
	for (var c = r.slice(i, t).split(n), _ = c[1].split("."), l = 0; l < _.length; ++l) s.push(parseInt(_[l], 10));
	return {
		name: c[0],
		version: s
	}
}, ab.getBrowser = function () {
	var o = navigator.userAgent;
	return o.indexOf("Chrome") > -1 ? ab._sliceUserAgent("Chrome", " ", "/") : o.indexOf("Safari") > -1 ? ab._sliceUserAgent("Safari", " ", "/") : o.indexOf("Firefox") > -1 ? ab._sliceUserAgent("Firefox", " ", "/") : o.indexOf("MSIE") > -1 ? ab._sliceUserAgent("MSIE", ";", " ") : null
}, ab.browserNotSupportedMessage = "Browser does not support WebSockets (RFC6455)", ab._idchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", ab._idlen = 16, ab._subprotocol = "wamp", ab._newid = function () {
	for (var o = "", e = 0; e < ab._idlen; e += 1) o += ab._idchars.charAt(Math.floor(Math.random() * ab._idchars.length));
	return o
}, ab.log = function (o) {
	if (window.console && console.log)
		if (arguments.length > 1) {
			console.group("Log Item");
			for (var e = 0; e < arguments.length; e += 1) console.log(arguments[e]);
			console.groupEnd()
		} else console.log(arguments[0])
}, ab._debugrpc = !1, ab._debugpubsub = !1, ab._debugws = !1, ab.debug = function (o, e) {
	if (!("console" in window)) throw "browser does not support console object";
	ab._debugrpc = o, ab._debugpubsub = o, ab._debugws = e
}, ab.version = function () {
	return ab._version
}, ab.PrefixMap = function () {
	this._index = {}, this._rindex = {}
}, ab.PrefixMap.prototype.get = function (o) {
	return this._index[o]
}, ab.PrefixMap.prototype.set = function (o, e) {
	this._index[o] = e, this._rindex[e] = o
}, ab.PrefixMap.prototype.setDefault = function (o) {
	this._index[""] = o, this._rindex[o] = ""
}, ab.PrefixMap.prototype.remove = function (o) {
	var e = this,
		n = e._index[o];
	n && (delete e._index[o], delete e._rindex[n])
}, ab.PrefixMap.prototype.resolve = function (o, e) {
	var n = o.indexOf(":");
	if (n >= 0) {
		var s = o.substring(0, n);
		if (this._index[s]) return this._index[s] + o.substring(n + 1)
	}
	return 1 == e ? o : null
}, ab.PrefixMap.prototype.shrink = function (o, e) {
	if (-1 == (n = o.indexOf(":")))
		for (var n = o.length; n > 0; n -= 1) {
			var s = o.substring(0, n),
				r = this._rindex[s];
			if (r) return r + ":" + o.substring(n)
		}
	return 1 == e ? o : null
}, ab._MESSAGE_TYPEID_WELCOME = 0, ab._MESSAGE_TYPEID_PREFIX = 1, ab._MESSAGE_TYPEID_CALL = 2, ab._MESSAGE_TYPEID_CALL_RESULT = 3, ab._MESSAGE_TYPEID_CALL_ERROR = 4, ab._MESSAGE_TYPEID_SUBSCRIBE = 5, ab._MESSAGE_TYPEID_UNSUBSCRIBE = 6, ab._MESSAGE_TYPEID_PUBLISH = 7, ab._MESSAGE_TYPEID_EVENT = 8, ab.CONNECTION_CLOSED = 0, ab.CONNECTION_LOST = 1, ab.CONNECTION_UNREACHABLE = 2, ab.CONNECTION_UNSUPPORTED = 3, ab.Session = function (o, e, n, s) {
	var r = this;
	if (r._wsuri = o, r._options = s, r._websocket_onopen = e, r._websocket_onclose = n, r._websocket = null, r._websocket_connected = !1, r._session_id = null, r._calls = {}, r._subscriptions = {}, r._prefixes = new ab.PrefixMap, r._txcnt = 0, r._rxcnt = 0, "WebSocket" in window) r._websocket = new WebSocket(r._wsuri, [ab._subprotocol]);
	else {
		if (!("MozWebSocket" in window)) {
			if (void 0 !== n) return void n(ab.CONNECTION_UNSUPPORTED);
			throw ab.browserNotSupportedMessage
		}
		r._websocket = new MozWebSocket(r._wsuri, [ab._subprotocol])
	}
	r._websocket.onmessage = function (o) {
		ab._debugws && (r._rxcnt += 1, console.group("WS Receive"), console.info(r._wsuri + "  [" + r._session_id + "]"), console.log(r._rxcnt), console.log(o.data), console.groupEnd());
		var e = JSON.parse(o.data);
		if (e[1] in r._calls) {
			if (e[0] === ab._MESSAGE_TYPEID_CALL_RESULT) {
				var n = r._calls[e[1]],
					s = e[2];
				if (ab._debugrpc && void 0 !== n._ab_callobj) {
					console.group("WAMP Call", n._ab_callobj[2]), console.timeEnd(n._ab_tid), console.group("Arguments");
					for (var i = 3; i < n._ab_callobj.length; i += 1) {
						var t = n._ab_callobj[i];
						if (void 0 === t) break;
						console.log(t)
					}
					console.groupEnd(), console.group("Result"), console.log(s), console.groupEnd(), console.groupEnd()
				}
				n.resolve(s)
			} else if (e[0] === ab._MESSAGE_TYPEID_CALL_ERROR) {
				var c = r._calls[e[1]],
					_ = e[2],
					l = e[3],
					a = e[4];
				if (ab._debugrpc && void 0 !== c._ab_callobj) {
					console.group("WAMP Call", c._ab_callobj[2]), console.timeEnd(c._ab_tid), console.group("Arguments");
					for (var b = 3; b < c._ab_callobj.length; b += 1) {
						var u = c._ab_callobj[b];
						if (void 0 === u) break;
						console.log(u)
					}
					console.groupEnd(), console.group("Error"), console.log(_), console.log(l), void 0 !== a && console.log(a), console.groupEnd(), console.groupEnd()
				}
				void 0 !== a ? c.reject(_, l, a) : c.reject(_, l)
			}
			delete r._calls[e[1]]
		} else if (e[0] === ab._MESSAGE_TYPEID_EVENT) {
			var p = r._prefixes.resolve(e[1], !0);
			if (p in r._subscriptions) {
				var d = e[1],
					f = e[2];
				ab._debugpubsub && (console.group("WAMP Event"), console.info(r._wsuri + "  [" + r._session_id + "]"), console.log(d), console.log(f), console.groupEnd()), r._subscriptions[p].forEach(function (o) {
					o(d, f)
				})
			}
		} else if (e[0] === ab._MESSAGE_TYPEID_WELCOME) {
			if (null !== r._session_id) throw "protocol error (welcome message received more than once)";
			r._session_id = e[1], r._wamp_version = e[2], r._server = e[3], (ab._debugrpc || ab._debugpubsub) && (console.group("WAMP Welcome"), console.info(r._wsuri + "  [" + r._session_id + "]"), console.log(r._wamp_version), console.log(r._server), console.groupEnd()), null !== r._websocket_onopen && r._websocket_onopen(r._session_id, r._wamp_version, r._server)
		}
	}, r._websocket.onopen = function (o) {
		if (r._websocket.protocol !== ab._subprotocol)
			if (void 0 === r._websocket.protocol) ab._debugws && (console.group("WS Warning"), console.info(r._wsuri), console.log("WebSocket object has no protocol attribute: WAMP subprotocol check skipped!"), console.groupEnd());
			else {
				if (!r._options || !r._options.skipSubprotocolCheck) throw r._websocket.close(1e3, "server does not speak WAMP"), "server does not speak WAMP (but '" + r._websocket.protocol + "' !)";
				ab._debugws && (console.group("WS Warning"), console.info(r._wsuri), console.log("Server does not speak WAMP, but subprotocol check disabled by option!"), console.log(r._websocket.protocol), console.groupEnd())
			} ab._debugws && (console.group("WAMP Connect"), console.info(r._wsuri), console.log(r._websocket.protocol), console.groupEnd()), r._websocket_connected = !0
	}, r._websocket.onerror = function (o) {}, r._websocket.onclose = function (o) {
		ab._debugws && (r._websocket_connected ? console.log("Autobahn connection to " + r._wsuri + " lost (code " + o.code + ", reason '" + o.reason + "', wasClean " + o.wasClean + ").") : console.log("Autobahn could not connect to " + r._wsuri + " (code " + o.code + ", reason '" + o.reason + "', wasClean " + o.wasClean + ").")), void 0 !== r._websocket_onclose && (r._websocket_connected ? o.wasClean ? r._websocket_onclose(ab.CONNECTION_CLOSED) : r._websocket_onclose(ab.CONNECTION_LOST) : r._websocket_onclose(ab.CONNECTION_UNREACHABLE)), r._websocket_connected = !1, r._wsuri = null, r._websocket_onopen = null, r._websocket_onclose = null, r._websocket = null
	}
}, ab.Session.prototype._send = function (o) {
	var e = this;
	if (!e._websocket_connected) throw "Autobahn not connected";
	var n = JSON.stringify(o);
	e._websocket.send(n), e._txcnt += 1, ab._debugws && (console.group("WS Send"), console.info(e._wsuri + "  [" + e._session_id + "]"), console.log(e._txcnt), console.log(n), console.groupEnd())
}, ab.Session.prototype.close = function () {
	if (!this._websocket_connected) throw "Autobahn not connected";
	this._websocket.close()
}, ab.Session.prototype.sessionid = function () {
	return this._session_id
}, ab.Session.prototype.shrink = function (o, e) {
	return this._prefixes.shrink(o, e)
}, ab.Session.prototype.resolve = function (o, e) {
	return this._prefixes.resolve(o, e)
}, ab.Session.prototype.prefix = function (o, e) {
	var n = this;
	if (void 0 !== n._prefixes.get(o)) throw "prefix '" + o + "' already defined";
	n._prefixes.set(o, e), (ab._debugrpc || ab._debugpubsub) && (console.group("WAMP Prefix"), console.info(n._wsuri + "  [" + n._session_id + "]"), console.log(o), console.log(e), console.groupEnd());
	var s = [ab._MESSAGE_TYPEID_PREFIX, o, e];
	n._send(s)
}, ab.Session.prototype.call = function () {
	for (var o, e = this, n = new when.defer;
		(o = ab._newid()) in e._calls;);
	e._calls[o] = n;
	for (var s = e._prefixes.shrink(arguments[0], !0), r = [ab._MESSAGE_TYPEID_CALL, o, s], i = 1; i < arguments.length; i += 1) r.push(arguments[i]);
	return e._send(r), ab._debugrpc && (n._ab_callobj = r, n._ab_tid = e._wsuri + "  [" + e._session_id + "][" + o + "]", console.time(n._ab_tid), console.info()), n
}, ab.Session.prototype.subscribe = function (o, e) {
	var n = this,
		s = n._prefixes.resolve(o, !0);
	if (!(s in n._subscriptions)) {
		ab._debugpubsub && (console.group("WAMP Subscribe"), console.info(n._wsuri + "  [" + n._session_id + "]"), console.log(o), console.log(e), console.groupEnd());
		var r = [ab._MESSAGE_TYPEID_SUBSCRIBE, o];
		n._send(r), n._subscriptions[s] = []
	}
	if (-1 !== n._subscriptions[s].indexOf(e)) throw "callback " + e + " already subscribed for topic " + s;
	n._subscriptions[s].push(e)
}, ab.Session.prototype.unsubscribe = function (o, e) {
	var n, s = this,
		r = s._prefixes.resolve(o, !0);
	if (!(r in s._subscriptions)) throw "not subscribed to topic " + r;
	if (void 0 !== e) {
		var i = s._subscriptions[r].indexOf(e);
		if (-1 === i) throw "no callback " + e + " subscribed on topic " + r;
		n = e, s._subscriptions[r].splice(i, 1)
	} else n = s._subscriptions[r].slice(), s._subscriptions[r] = [];
	if (0 === s._subscriptions[r].length) {
		delete s._subscriptions[r], ab._debugpubsub && (console.group("WAMP Unsubscribe"), console.info(s._wsuri + "  [" + s._session_id + "]"), console.log(o), console.log(n), console.groupEnd());
		var t = [ab._MESSAGE_TYPEID_UNSUBSCRIBE, o];
		s._send(t)
	}
}, ab.Session.prototype.publish = function () {
	var o = arguments[0],
		e = arguments[1],
		n = null,
		s = null,
		r = null,
		i = null;
	if (arguments.length > 3) {
		if (!(arguments[2] instanceof Array)) throw "invalid argument type(s)";
		if (!(arguments[3] instanceof Array)) throw "invalid argument type(s)";
		s = arguments[2], r = arguments[3], i = [ab._MESSAGE_TYPEID_PUBLISH, o, e, s, r]
	} else if (arguments.length > 2)
		if ("boolean" == typeof arguments[2]) n = arguments[2], i = [ab._MESSAGE_TYPEID_PUBLISH, o, e, n];
		else {
			if (!(arguments[2] instanceof Array)) throw "invalid argument type(s)";
			s = arguments[2], i = [ab._MESSAGE_TYPEID_PUBLISH, o, e, s]
		}
	else i = [ab._MESSAGE_TYPEID_PUBLISH, o, e];
	ab._debugpubsub && (console.group("WAMP Publish"), console.info(this._wsuri + "  [" + this._session_id + "]"), console.log(o), console.log(e), null !== n ? console.log(n) : null !== s && (console.log(s), null !== r && console.log(r)), console.groupEnd()), this._send(i)
};
! function (a, b) {
	"function" == typeof define && define.amd ? define("sifter", b) : "object" == typeof exports ? module.exports = b() : a.Sifter = b()
}(this, function () {
	var a = function (a, b) {
		this.items = a, this.settings = b || {
			diacritics: !0
		}
	};
	a.prototype.tokenize = function (a) {
		if (a = e(String(a || "").toLowerCase()), !a || !a.length) return [];
		var b, c, d, g, i = [],
			j = a.split(/ +/);
		for (b = 0, c = j.length; b < c; b++) {
			if (d = f(j[b]), this.settings.diacritics)
				for (g in h) h.hasOwnProperty(g) && (d = d.replace(new RegExp(g, "g"), h[g]));
			i.push({
				string: j[b],
				regex: new RegExp(d, "i")
			})
		}
		return i
	}, a.prototype.iterator = function (a, b) {
		var c;
		c = g(a) ? Array.prototype.forEach || function (a) {
			for (var b = 0, c = this.length; b < c; b++) a(this[b], b, this)
		} : function (a) {
			for (var b in this) this.hasOwnProperty(b) && a(this[b], b, this)
		}, c.apply(a, [b])
	}, a.prototype.getScoreFunction = function (a, b) {
		var c, e, f, g, h;
		c = this, a = c.prepareSearch(a, b), f = a.tokens, e = a.options.fields, g = f.length, h = a.options.nesting;
		var i = function (a, b) {
				var c, d;
				return a ? (a = String(a || ""), d = a.search(b.regex), d === -1 ? 0 : (c = b.string.length / a.length, 0 === d && (c += .5), c)) : 0
			},
			j = function () {
				var a = e.length;
				return a ? 1 === a ? function (a, b) {
					return i(d(b, e[0], h), a)
				} : function (b, c) {
					for (var f = 0, g = 0; f < a; f++) g += i(d(c, e[f], h), b);
					return g / a
				} : function () {
					return 0
				}
			}();
		return g ? 1 === g ? function (a) {
			return j(f[0], a)
		} : "and" === a.options.conjunction ? function (a) {
			for (var b, c = 0, d = 0; c < g; c++) {
				if (b = j(f[c], a), b <= 0) return 0;
				d += b
			}
			return d / g
		} : function (a) {
			for (var b = 0, c = 0; b < g; b++) c += j(f[b], a);
			return c / g
		} : function () {
			return 0
		}
	}, a.prototype.getSortFunction = function (a, c) {
		var e, f, g, h, i, j, k, l, m, n, o;
		if (g = this, a = g.prepareSearch(a, c), o = !a.query && c.sort_empty || c.sort, m = function (a, b) {
				return "$score" === a ? b.score : d(g.items[b.id], a, c.nesting)
			}, i = [], o)
			for (e = 0, f = o.length; e < f; e++)(a.query || "$score" !== o[e].field) && i.push(o[e]);
		if (a.query) {
			for (n = !0, e = 0, f = i.length; e < f; e++)
				if ("$score" === i[e].field) {
					n = !1;
					break
				} n && i.unshift({
				field: "$score",
				direction: "desc"
			})
		} else
			for (e = 0, f = i.length; e < f; e++)
				if ("$score" === i[e].field) {
					i.splice(e, 1);
					break
				} for (l = [], e = 0, f = i.length; e < f; e++) l.push("desc" === i[e].direction ? -1 : 1);
		return j = i.length, j ? 1 === j ? (h = i[0].field, k = l[0], function (a, c) {
			return k * b(m(h, a), m(h, c))
		}) : function (a, c) {
			var d, e, f;
			for (d = 0; d < j; d++)
				if (f = i[d].field, e = l[d] * b(m(f, a), m(f, c))) return e;
			return 0
		} : null
	}, a.prototype.prepareSearch = function (a, b) {
		if ("object" == typeof a) return a;
		b = c({}, b);
		var d = b.fields,
			e = b.sort,
			f = b.sort_empty;
		return d && !g(d) && (b.fields = [d]), e && !g(e) && (b.sort = [e]), f && !g(f) && (b.sort_empty = [f]), {
			options: b,
			query: String(a || "").toLowerCase(),
			tokens: this.tokenize(a),
			total: 0,
			items: []
		}
	}, a.prototype.search = function (a, b) {
		var c, d, e, f, g = this;
		return d = this.prepareSearch(a, b), b = d.options, a = d.query, f = b.score || g.getScoreFunction(d), a.length ? g.iterator(g.items, function (a, e) {
			c = f(a), (b.filter === !1 || c > 0) && d.items.push({
				score: c,
				id: e
			})
		}) : g.iterator(g.items, function (a, b) {
			d.items.push({
				score: 1,
				id: b
			})
		}), e = g.getSortFunction(d, b), e && d.items.sort(e), d.total = d.items.length, "number" == typeof b.limit && (d.items = d.items.slice(0, b.limit)), d
	};
	var b = function (a, b) {
			return "number" == typeof a && "number" == typeof b ? a > b ? 1 : a < b ? -1 : 0 : (a = i(String(a || "")), b = i(String(b || "")), a > b ? 1 : b > a ? -1 : 0)
		},
		c = function (a, b) {
			var c, d, e, f;
			for (c = 1, d = arguments.length; c < d; c++)
				if (f = arguments[c])
					for (e in f) f.hasOwnProperty(e) && (a[e] = f[e]);
			return a
		},
		d = function (a, b, c) {
			if (a && b) {
				if (!c) return a[b];
				for (var d = b.split("."); d.length && (a = a[d.shift()]););
				return a
			}
		},
		e = function (a) {
			return (a + "").replace(/^\s+|\s+$|/g, "")
		},
		f = function (a) {
			return (a + "").replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
		},
		g = Array.isArray || "undefined" != typeof $ && $.isArray || function (a) {
			return "[object Array]" === Object.prototype.toString.call(a)
		},
		h = {
			a: "[aḀḁĂăÂâǍǎȺⱥȦȧẠạÄäÀàÁáĀāÃãÅåąĄÃąĄ]",
			b: "[b␢βΒB฿𐌁ᛒ]",
			c: "[cĆćĈĉČčĊċC̄c̄ÇçḈḉȻȼƇƈɕᴄＣｃ]",
			d: "[dĎďḊḋḐḑḌḍḒḓḎḏĐđD̦d̦ƉɖƊɗƋƌᵭᶁᶑȡᴅＤｄð]",
			e: "[eÉéÈèÊêḘḙĚěĔĕẼẽḚḛẺẻĖėËëĒēȨȩĘęᶒɆɇȄȅẾếỀềỄễỂểḜḝḖḗḔḕȆȇẸẹỆệⱸᴇＥｅɘǝƏƐε]",
			f: "[fƑƒḞḟ]",
			g: "[gɢ₲ǤǥĜĝĞğĢģƓɠĠġ]",
			h: "[hĤĥĦħḨḩẖẖḤḥḢḣɦʰǶƕ]",
			i: "[iÍíÌìĬĭÎîǏǐÏïḮḯĨĩĮįĪīỈỉȈȉȊȋỊịḬḭƗɨɨ̆ᵻᶖİiIıɪＩｉ]",
			j: "[jȷĴĵɈɉʝɟʲ]",
			k: "[kƘƙꝀꝁḰḱǨǩḲḳḴḵκϰ₭]",
			l: "[lŁłĽľĻļĹĺḶḷḸḹḼḽḺḻĿŀȽƚⱠⱡⱢɫɬᶅɭȴʟＬｌ]",
			n: "[nŃńǸǹŇňÑñṄṅŅņṆṇṊṋṈṉN̈n̈ƝɲȠƞᵰᶇɳȵɴＮｎŊŋ]",
			o: "[oØøÖöÓóÒòÔôǑǒŐőŎŏȮȯỌọƟɵƠơỎỏŌōÕõǪǫȌȍՕօ]",
			p: "[pṔṕṖṗⱣᵽƤƥᵱ]",
			q: "[qꝖꝗʠɊɋꝘꝙq̃]",
			r: "[rŔŕɌɍŘřŖŗṘṙȐȑȒȓṚṛⱤɽ]",
			s: "[sŚśṠṡṢṣꞨꞩŜŝŠšŞşȘșS̈s̈]",
			t: "[tŤťṪṫŢţṬṭƮʈȚțṰṱṮṯƬƭ]",
			u: "[uŬŭɄʉỤụÜüÚúÙùÛûǓǔŰűŬŭƯưỦủŪūŨũŲųȔȕ∪]",
			v: "[vṼṽṾṿƲʋꝞꝟⱱʋ]",
			w: "[wẂẃẀẁŴŵẄẅẆẇẈẉ]",
			x: "[xẌẍẊẋχ]",
			y: "[yÝýỲỳŶŷŸÿỸỹẎẏỴỵɎɏƳƴ]",
			z: "[zŹźẐẑŽžŻżẒẓẔẕƵƶ]"
		},
		i = function () {
			var a, b, c, d, e = "",
				f = {};
			for (c in h)
				if (h.hasOwnProperty(c))
					for (d = h[c].substring(2, h[c].length - 1), e += d, a = 0, b = d.length; a < b; a++) f[d.charAt(a)] = c;
			var g = new RegExp("[" + e + "]", "g");
			return function (a) {
				return a.replace(g, function (a) {
					return f[a]
				}).toLowerCase()
			}
		}();
	return a
}),
function (a, b) {
	"function" == typeof define && define.amd ? define("microplugin", b) : "object" == typeof exports ? module.exports = b() : a.MicroPlugin = b()
}(this, function () {
	var a = {};
	a.mixin = function (a) {
		a.plugins = {}, a.prototype.initializePlugins = function (a) {
			var c, d, e, f = this,
				g = [];
			if (f.plugins = {
					names: [],
					settings: {},
					requested: {},
					loaded: {}
				}, b.isArray(a))
				for (c = 0, d = a.length; c < d; c++) "string" == typeof a[c] ? g.push(a[c]) : (f.plugins.settings[a[c].name] = a[c].options, g.push(a[c].name));
			else if (a)
				for (e in a) a.hasOwnProperty(e) && (f.plugins.settings[e] = a[e], g.push(e));
			for (; g.length;) f.require(g.shift())
		}, a.prototype.loadPlugin = function (b) {
			var c = this,
				d = c.plugins,
				e = a.plugins[b];
			if (!a.plugins.hasOwnProperty(b)) throw new Error('Unable to find "' + b + '" plugin');
			d.requested[b] = !0, d.loaded[b] = e.fn.apply(c, [c.plugins.settings[b] || {}]), d.names.push(b)
		}, a.prototype.require = function (a) {
			var b = this,
				c = b.plugins;
			if (!b.plugins.loaded.hasOwnProperty(a)) {
				if (c.requested[a]) throw new Error('Plugin has circular dependency ("' + a + '")');
				b.loadPlugin(a)
			}
			return c.loaded[a]
		}, a.define = function (b, c) {
			a.plugins[b] = {
				name: b,
				fn: c
			}
		}
	};
	var b = {
		isArray: Array.isArray || function (a) {
			return "[object Array]" === Object.prototype.toString.call(a)
		}
	};
	return a
}),
function (a, b) {
	"function" == typeof define && define.amd ? define("selectize", ["jquery", "sifter", "microplugin"], b) : "object" == typeof exports ? module.exports = b(require("jquery"), require("sifter"), require("microplugin")) : a.Selectize = b(a.jQuery, a.Sifter, a.MicroPlugin)
}(this, function (a, b, c) {
	"use strict";
	var d = function (a, b) {
		if ("string" != typeof b || b.length) {
			var c = "string" == typeof b ? new RegExp(b, "i") : b,
				d = function (a) {
					var b = 0;
					if (3 === a.nodeType) {
						var e = a.data.search(c);
						if (e >= 0 && a.data.length > 0) {
							var f = a.data.match(c),
								g = document.createElement("span");
							g.className = "highlight";
							var h = a.splitText(e),
								i = (h.splitText(f[0].length), h.cloneNode(!0));
							g.appendChild(i), h.parentNode.replaceChild(g, h), b = 1
						}
					} else if (1 === a.nodeType && a.childNodes && !/(script|style)/i.test(a.tagName))
						for (var j = 0; j < a.childNodes.length; ++j) j += d(a.childNodes[j]);
					return b
				};
			return a.each(function () {
				d(this)
			})
		}
	};
	a.fn.removeHighlight = function () {
		return this.find("span.highlight").each(function () {
			this.parentNode.firstChild.nodeName;
			var a = this.parentNode;
			a.replaceChild(this.firstChild, this), a.normalize()
		}).end()
	};
	var e = function () {};
	e.prototype = {
		on: function (a, b) {
			this._events = this._events || {}, this._events[a] = this._events[a] || [], this._events[a].push(b)
		},
		off: function (a, b) {
			var c = arguments.length;
			return 0 === c ? delete this._events : 1 === c ? delete this._events[a] : (this._events = this._events || {}, void(a in this._events != !1 && this._events[a].splice(this._events[a].indexOf(b), 1)))
		},
		trigger: function (a) {
			if (this._events = this._events || {}, a in this._events != !1)
				for (var b = 0; b < this._events[a].length; b++) this._events[a][b].apply(this, Array.prototype.slice.call(arguments, 1))
		}
	}, e.mixin = function (a) {
		for (var b = ["on", "off", "trigger"], c = 0; c < b.length; c++) a.prototype[b[c]] = e.prototype[b[c]]
	};
	var f = /Mac/.test(navigator.userAgent),
		g = 65,
		h = 13,
		i = 27,
		j = 37,
		k = 38,
		l = 80,
		m = 39,
		n = 40,
		o = 78,
		p = 8,
		q = 46,
		r = 16,
		s = f ? 91 : 17,
		t = f ? 18 : 17,
		u = 9,
		v = 1,
		w = 2,
		x = !/android/i.test(window.navigator.userAgent) && !!document.createElement("input").validity,
		y = function (a) {
			return "undefined" != typeof a
		},
		z = function (a) {
			return "undefined" == typeof a || null === a ? null : "boolean" == typeof a ? a ? "1" : "0" : a + ""
		},
		A = function (a) {
			return (a + "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")
		},
		B = {};
	B.before = function (a, b, c) {
		var d = a[b];
		a[b] = function () {
			return c.apply(a, arguments), d.apply(a, arguments)
		}
	}, B.after = function (a, b, c) {
		var d = a[b];
		a[b] = function () {
			var b = d.apply(a, arguments);
			return c.apply(a, arguments), b
		}
	};
	var C = function (a) {
			var b = !1;
			return function () {
				b || (b = !0, a.apply(this, arguments))
			}
		},
		D = function (a, b) {
			var c;
			return function () {
				var d = this,
					e = arguments;
				window.clearTimeout(c), c = window.setTimeout(function () {
					a.apply(d, e)
				}, b)
			}
		},
		E = function (a, b, c) {
			var d, e = a.trigger,
				f = {};
			a.trigger = function () {
				var c = arguments[0];
				return b.indexOf(c) === -1 ? e.apply(a, arguments) : void(f[c] = arguments)
			}, c.apply(a, []), a.trigger = e;
			for (d in f) f.hasOwnProperty(d) && e.apply(a, f[d])
		},
		F = function (a, b, c, d) {
			a.on(b, c, function (b) {
				for (var c = b.target; c && c.parentNode !== a[0];) c = c.parentNode;
				return b.currentTarget = c, d.apply(this, [b])
			})
		},
		G = function (a) {
			var b = {};
			if ("selectionStart" in a) b.start = a.selectionStart, b.length = a.selectionEnd - b.start;
			else if (document.selection) {
				a.focus();
				var c = document.selection.createRange(),
					d = document.selection.createRange().text.length;
				c.moveStart("character", -a.value.length), b.start = c.text.length - d, b.length = d
			}
			return b
		},
		H = function (a, b, c) {
			var d, e, f = {};
			if (c)
				for (d = 0, e = c.length; d < e; d++) f[c[d]] = a.css(c[d]);
			else f = a.css();
			b.css(f)
		},
		I = function (b, c) {
			if (!b) return 0;
			var d = a("<test>").css({
				position: "absolute",
				top: -99999,
				left: -99999,
				width: "auto",
				padding: 0,
				whiteSpace: "pre"
			}).text(b).appendTo("body");
			H(c, d, ["letterSpacing", "fontSize", "fontFamily", "fontWeight", "textTransform"]);
			var e = d.width();
			return d.remove(), e
		},
		J = function (a) {
			var b = null,
				c = function (c, d) {
					var e, f, g, h, i, j, k, l;
					c = c || window.event || {}, d = d || {}, c.metaKey || c.altKey || (d.force || a.data("grow") !== !1) && (e = a.val(), c.type && "keydown" === c.type.toLowerCase() && (f = c.keyCode, g = f >= 97 && f <= 122 || f >= 65 && f <= 90 || f >= 48 && f <= 57 || 32 === f, f === q || f === p ? (l = G(a[0]), l.length ? e = e.substring(0, l.start) + e.substring(l.start + l.length) : f === p && l.start ? e = e.substring(0, l.start - 1) + e.substring(l.start + 1) : f === q && "undefined" != typeof l.start && (e = e.substring(0, l.start) + e.substring(l.start + 1))) : g && (j = c.shiftKey, k = String.fromCharCode(c.keyCode), k = j ? k.toUpperCase() : k.toLowerCase(), e += k)), h = a.attr("placeholder"), !e && h && (e = h), i = I(e, a) + 4, i !== b && (b = i, a.width(i), a.triggerHandler("resize")))
				};
			a.on("keydown keyup update blur", c), c()
		},
		K = function (a) {
			var b = document.createElement("div");
			return b.appendChild(a.cloneNode(!0)), b.innerHTML
		},
		L = function (a, b) {
			b || (b = {});
			var c = "Selectize";
			console.error(c + ": " + a), b.explanation && (console.group && console.group(), console.error(b.explanation), console.group && console.groupEnd())
		},
		M = function (c, d) {
			var e, f, g, h, i = this;
			h = c[0], h.selectize = i;
			var j = window.getComputedStyle && window.getComputedStyle(h, null);
			if (g = j ? j.getPropertyValue("direction") : h.currentStyle && h.currentStyle.direction, g = g || c.parents("[dir]:first").attr("dir") || "", a.extend(i, {
					order: 0,
					settings: d,
					$input: c,
					tabIndex: c.attr("tabindex") || "",
					tagType: "select" === h.tagName.toLowerCase() ? v : w,
					rtl: /rtl/i.test(g),
					eventNS: ".selectize" + ++M.count,
					highlightedValue: null,
					isOpen: !1,
					isDisabled: !1,
					isRequired: c.is("[required]"),
					isInvalid: !1,
					isLocked: !1,
					isFocused: !1,
					isInputHidden: !1,
					isSetup: !1,
					isShiftDown: !1,
					isCmdDown: !1,
					isCtrlDown: !1,
					ignoreFocus: !1,
					ignoreBlur: !1,
					ignoreHover: !1,
					hasOptions: !1,
					currentResults: null,
					lastValue: "",
					caretPos: 0,
					loading: 0,
					loadedSearches: {},
					$activeOption: null,
					$activeItems: [],
					optgroups: {},
					options: {},
					userOptions: {},
					items: [],
					renderCache: {},
					onSearchChange: null === d.loadThrottle ? i.onSearchChange : D(i.onSearchChange, d.loadThrottle)
				}), i.sifter = new b(this.options, {
					diacritics: d.diacritics
				}), i.settings.options) {
				for (e = 0, f = i.settings.options.length; e < f; e++) i.registerOption(i.settings.options[e]);
				delete i.settings.options
			}
			if (i.settings.optgroups) {
				for (e = 0, f = i.settings.optgroups.length; e < f; e++) i.registerOptionGroup(i.settings.optgroups[e]);
				delete i.settings.optgroups
			}
			i.settings.mode = i.settings.mode || (1 === i.settings.maxItems ? "single" : "multi"), "boolean" != typeof i.settings.hideSelected && (i.settings.hideSelected = "multi" === i.settings.mode), i.initializePlugins(i.settings.plugins), i.setupCallbacks(), i.setupTemplates(), i.setup()
		};
	return e.mixin(M), "undefined" != typeof c ? c.mixin(M) : L("Dependency MicroPlugin is missing", {
		explanation: 'Make sure you either: (1) are using the "standalone" version of Selectize, or (2) require MicroPlugin before you load Selectize.'
	}), a.extend(M.prototype, {
		setup: function () {
			var b, c, d, e, g, h, i, j, k, l, m = this,
				n = m.settings,
				o = m.eventNS,
				p = a(window),
				q = a(document),
				u = m.$input;
			if (i = m.settings.mode, j = u.attr("class") || "", b = a("<div>").addClass(n.wrapperClass).addClass(j).addClass(i), c = a("<div>").addClass(n.inputClass).addClass("items").appendTo(b), d = a('<input type="text" autocomplete="off" />').appendTo(c).attr("tabindex", u.is(":disabled") ? "-1" : m.tabIndex), h = a(n.dropdownParent || b), e = a("<div>").addClass(n.dropdownClass).addClass(i).hide().appendTo(h), g = a("<div>").addClass(n.dropdownContentClass).appendTo(e), (l = u.attr("id")) && (d.attr("id", l + "-selectized"), a("label[for='" + l + "']").attr("for", l + "-selectized")), m.settings.copyClassesToDropdown && e.addClass(j), b.css({
					width: u[0].style.width
				}), m.plugins.names.length && (k = "plugin-" + m.plugins.names.join(" plugin-"), b.addClass(k), e.addClass(k)), (null === n.maxItems || n.maxItems > 1) && m.tagType === v && u.attr("multiple", "multiple"), m.settings.placeholder && d.attr("placeholder", n.placeholder), !m.settings.splitOn && m.settings.delimiter) {
				var w = m.settings.delimiter.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
				m.settings.splitOn = new RegExp("\\s*" + w + "+\\s*")
			}
			u.attr("autocorrect") && d.attr("autocorrect", u.attr("autocorrect")), u.attr("autocapitalize") && d.attr("autocapitalize", u.attr("autocapitalize")), m.$wrapper = b, m.$control = c, m.$control_input = d, m.$dropdown = e, m.$dropdown_content = g, e.on("mouseenter", "[data-selectable]", function () {
				return m.onOptionHover.apply(m, arguments)
			}), e.on("mousedown click", "[data-selectable]", function () {
				return m.onOptionSelect.apply(m, arguments)
			}), F(c, "mousedown", "*:not(input)", function () {
				return m.onItemSelect.apply(m, arguments)
			}), J(d), c.on({
				mousedown: function () {
					return m.onMouseDown.apply(m, arguments)
				},
				click: function () {
					return m.onClick.apply(m, arguments)
				}
			}), d.on({
				mousedown: function (a) {
					a.stopPropagation()
				},
				keydown: function () {
					return m.onKeyDown.apply(m, arguments)
				},
				keyup: function () {
					return m.onKeyUp.apply(m, arguments)
				},
				keypress: function () {
					return m.onKeyPress.apply(m, arguments)
				},
				resize: function () {
					m.positionDropdown.apply(m, [])
				},
				blur: function () {
					return m.onBlur.apply(m, arguments)
				},
				focus: function () {
					return m.ignoreBlur = !1, m.onFocus.apply(m, arguments)
				},
				paste: function () {
					return m.onPaste.apply(m, arguments)
				}
			}), q.on("keydown" + o, function (a) {
				m.isCmdDown = a[f ? "metaKey" : "ctrlKey"], m.isCtrlDown = a[f ? "altKey" : "ctrlKey"], m.isShiftDown = a.shiftKey
			}), q.on("keyup" + o, function (a) {
				a.keyCode === t && (m.isCtrlDown = !1), a.keyCode === r && (m.isShiftDown = !1), a.keyCode === s && (m.isCmdDown = !1)
			}), q.on("mousedown" + o, function (a) {
				if (m.isFocused) {
					if (a.target === m.$dropdown[0] || a.target.parentNode === m.$dropdown[0]) return !1;
					m.$control.has(a.target).length || a.target === m.$control[0] || m.blur(a.target)
				}
			}), p.on(["scroll" + o, "resize" + o].join(" "), function () {
				m.isOpen && m.positionDropdown.apply(m, arguments)
			}), p.on("mousemove" + o, function () {
				m.ignoreHover = !1
			}), this.revertSettings = {
				$children: u.children().detach(),
				tabindex: u.attr("tabindex")
			}, u.attr("tabindex", -1).hide().after(m.$wrapper), a.isArray(n.items) && (m.setValue(n.items), delete n.items), x && u.on("invalid" + o, function (a) {
				a.preventDefault(), m.isInvalid = !0, m.refreshState()
			}), m.updateOriginalInput(), m.refreshItems(), m.refreshState(), m.updatePlaceholder(), m.isSetup = !0, u.is(":disabled") && m.disable(), m.on("change", this.onChange), u.data("selectize", m), u.addClass("selectized"), m.trigger("initialize"), n.preload === !0 && m.onSearchChange("")
		},
		setupTemplates: function () {
			var b = this,
				c = b.settings.labelField,
				d = b.settings.optgroupLabelField,
				e = {
					optgroup: function (a) {
						return '<div class="optgroup">' + a.html + "</div>"
					},
					optgroup_header: function (a, b) {
						return '<div class="optgroup-header">' + b(a[d]) + "</div>"
					},
					option: function (a, b) {
						return '<div class="option">' + b(a[c]) + "</div>"
					},
					item: function (a, b) {
						return '<div class="item">' + b(a[c]) + "</div>"
					},
					option_create: function (a, b) {
						return '<div class="create">Add <strong>' + b(a.input) + "</strong>&hellip;</div>"
					}
				};
			b.settings.render = a.extend({}, e, b.settings.render)
		},
		setupCallbacks: function () {
			var a, b, c = {
				initialize: "onInitialize",
				change: "onChange",
				item_add: "onItemAdd",
				item_remove: "onItemRemove",
				clear: "onClear",
				option_add: "onOptionAdd",
				option_remove: "onOptionRemove",
				option_clear: "onOptionClear",
				optgroup_add: "onOptionGroupAdd",
				optgroup_remove: "onOptionGroupRemove",
				optgroup_clear: "onOptionGroupClear",
				dropdown_open: "onDropdownOpen",
				dropdown_close: "onDropdownClose",
				type: "onType",
				load: "onLoad",
				focus: "onFocus",
				blur: "onBlur"
			};
			for (a in c) c.hasOwnProperty(a) && (b = this.settings[c[a]], b && this.on(a, b))
		},
		onClick: function (a) {
			var b = this;
			b.isFocused || (b.focus(), a.preventDefault())
		},
		onMouseDown: function (b) {
			var c = this,
				d = b.isDefaultPrevented();
			a(b.target);
			if (c.isFocused) {
				if (b.target !== c.$control_input[0]) return "single" === c.settings.mode ? c.isOpen ? c.close() : c.open() : d || c.setActiveItem(null), !1
			} else d || window.setTimeout(function () {
				c.focus()
			}, 0)
		},
		onChange: function () {
			this.$input.trigger("change")
		},
		onPaste: function (b) {
			var c = this;
			return c.isFull() || c.isInputHidden || c.isLocked ? void b.preventDefault() : void(c.settings.splitOn && setTimeout(function () {
				var b = c.$control_input.val();
				if (b.match(c.settings.splitOn))
					for (var d = a.trim(b).split(c.settings.splitOn), e = 0, f = d.length; e < f; e++) c.createItem(d[e])
			}, 0))
		},
		onKeyPress: function (a) {
			if (this.isLocked) return a && a.preventDefault();
			var b = String.fromCharCode(a.keyCode || a.which);
			return this.settings.create && "multi" === this.settings.mode && b === this.settings.delimiter ? (this.createItem(), a.preventDefault(), !1) : void 0
		},
		onKeyDown: function (a) {
			var b = (a.target === this.$control_input[0], this);
			if (b.isLocked) return void(a.keyCode !== u && a.preventDefault());
			switch (a.keyCode) {
				case g:
					if (b.isCmdDown) return void b.selectAll();
					break;
				case i:
					return void(b.isOpen && (a.preventDefault(), a.stopPropagation(), b.close()));
				case o:
					if (!a.ctrlKey || a.altKey) break;
				case n:
					if (!b.isOpen && b.hasOptions) b.open();
					else if (b.$activeOption) {
						b.ignoreHover = !0;
						var c = b.getAdjacentOption(b.$activeOption, 1);
						c.length && b.setActiveOption(c, !0, !0)
					}
					return void a.preventDefault();
				case l:
					if (!a.ctrlKey || a.altKey) break;
				case k:
					if (b.$activeOption) {
						b.ignoreHover = !0;
						var d = b.getAdjacentOption(b.$activeOption, -1);
						d.length && b.setActiveOption(d, !0, !0)
					}
					return void a.preventDefault();
				case h:
					return void(b.isOpen && b.$activeOption && (b.onOptionSelect({
						currentTarget: b.$activeOption
					}), a.preventDefault()));
				case j:
					return void b.advanceSelection(-1, a);
				case m:
					return void b.advanceSelection(1, a);
				case u:
					return b.settings.selectOnTab && b.isOpen && b.$activeOption && (b.onOptionSelect({
						currentTarget: b.$activeOption
					}), b.isFull() || a.preventDefault()), void(b.settings.create && b.createItem() && a.preventDefault());
				case p:
				case q:
					return void b.deleteSelection(a)
			}
			return !b.isFull() && !b.isInputHidden || (f ? a.metaKey : a.ctrlKey) ? void 0 : void a.preventDefault()
		},
		onKeyUp: function (a) {
			var b = this;
			if (b.isLocked) return a && a.preventDefault();
			var c = b.$control_input.val() || "";
			b.lastValue !== c && (b.lastValue = c, b.onSearchChange(c), b.refreshOptions(), b.trigger("type", c))
		},
		onSearchChange: function (a) {
			var b = this,
				c = b.settings.load;
			c && (b.loadedSearches.hasOwnProperty(a) || (b.loadedSearches[a] = !0, b.load(function (d) {
				c.apply(b, [a, d])
			})))
		},
		onFocus: function (a) {
			var b = this,
				c = b.isFocused;
			return b.isDisabled ? (b.blur(), a && a.preventDefault(), !1) : void(b.ignoreFocus || (b.isFocused = !0, "focus" === b.settings.preload && b.onSearchChange(""), c || b.trigger("focus"), b.$activeItems.length || (b.showInput(), b.setActiveItem(null), b.refreshOptions(!!b.settings.openOnFocus)), b.refreshState()))
		},
		onBlur: function (a, b) {
			var c = this;
			if (c.isFocused && (c.isFocused = !1, !c.ignoreFocus)) {
				if (!c.ignoreBlur && document.activeElement === c.$dropdown_content[0]) return c.ignoreBlur = !0, void c.onFocus(a);
				var d = function () {
					c.close(), c.setTextboxValue(""), c.setActiveItem(null), c.setActiveOption(null), c.setCaret(c.items.length), c.refreshState(), b && b.focus && b.focus(), c.ignoreFocus = !1, c.trigger("blur")
				};
				c.ignoreFocus = !0, c.settings.create && c.settings.createOnBlur ? c.createItem(null, !1, d) : d()
			}
		},
		onOptionHover: function (a) {
			this.ignoreHover || this.setActiveOption(a.currentTarget, !1)
		},
		onOptionSelect: function (b) {
			var c, d, e = this;
			b.preventDefault && (b.preventDefault(), b.stopPropagation()), d = a(b.currentTarget), d.hasClass("create") ? e.createItem(null, function () {
				e.settings.closeAfterSelect && e.close()
			}) : (c = d.attr("data-value"), "undefined" != typeof c && (e.lastQuery = null, e.setTextboxValue(""), e.addItem(c), e.settings.closeAfterSelect ? e.close() : !e.settings.hideSelected && b.type && /mouse/.test(b.type) && e.setActiveOption(e.getOption(c))))
		},
		onItemSelect: function (a) {
			var b = this;
			b.isLocked || "multi" === b.settings.mode && (a.preventDefault(), b.setActiveItem(a.currentTarget, a))
		},
		load: function (a) {
			var b = this,
				c = b.$wrapper.addClass(b.settings.loadingClass);
			b.loading++, a.apply(b, [function (a) {
				b.loading = Math.max(b.loading - 1, 0), a && a.length && (b.addOption(a), b.refreshOptions(b.isFocused && !b.isInputHidden)), b.loading || c.removeClass(b.settings.loadingClass), b.trigger("load", a)
			}])
		},
		setTextboxValue: function (a) {
			var b = this.$control_input,
				c = b.val() !== a;
			c && (b.val(a).triggerHandler("update"), this.lastValue = a)
		},
		getValue: function () {
			return this.tagType === v && this.$input.attr("multiple") ? this.items : this.items.join(this.settings.delimiter)
		},
		setValue: function (a, b) {
			var c = b ? [] : ["change"];
			E(this, c, function () {
				this.clear(b), this.addItems(a, b)
			})
		},
		setActiveItem: function (b, c) {
			var d, e, f, g, h, i, j, k, l = this;
			if ("single" !== l.settings.mode) {
				if (b = a(b), !b.length) return a(l.$activeItems).removeClass("active"), l.$activeItems = [], void(l.isFocused && l.showInput());
				if (d = c && c.type.toLowerCase(), "mousedown" === d && l.isShiftDown && l.$activeItems.length) {
					for (k = l.$control.children(".active:last"), g = Array.prototype.indexOf.apply(l.$control[0].childNodes, [k[0]]), h = Array.prototype.indexOf.apply(l.$control[0].childNodes, [b[0]]), g > h && (j = g, g = h, h = j), e = g; e <= h; e++) i = l.$control[0].childNodes[e], l.$activeItems.indexOf(i) === -1 && (a(i).addClass("active"), l.$activeItems.push(i));
					c.preventDefault()
				} else "mousedown" === d && l.isCtrlDown || "keydown" === d && this.isShiftDown ? b.hasClass("active") ? (f = l.$activeItems.indexOf(b[0]), l.$activeItems.splice(f, 1), b.removeClass("active")) : l.$activeItems.push(b.addClass("active")[0]) : (a(l.$activeItems).removeClass("active"), l.$activeItems = [b.addClass("active")[0]]);
				l.hideInput(), this.isFocused || l.focus()
			}
		},
		setActiveOption: function (b, c, d) {
			var e, f, g, h, i, j = this;
			j.$activeOption && j.$activeOption.removeClass("active"), j.$activeOption = null, b = a(b), b.length && (j.$activeOption = b.addClass("active"), !c && y(c) || (e = j.$dropdown_content.height(), f = j.$activeOption.outerHeight(!0), c = j.$dropdown_content.scrollTop() || 0, g = j.$activeOption.offset().top - j.$dropdown_content.offset().top + c, h = g, i = g - e + f, g + f > e + c ? j.$dropdown_content.stop().animate({
				scrollTop: i
			}, d ? j.settings.scrollDuration : 0) : g < c && j.$dropdown_content.stop().animate({
				scrollTop: h
			}, d ? j.settings.scrollDuration : 0)))
		},
		selectAll: function () {
			var a = this;
			"single" !== a.settings.mode && (a.$activeItems = Array.prototype.slice.apply(a.$control.children(":not(input)").addClass("active")), a.$activeItems.length && (a.hideInput(), a.close()), a.focus())
		},
		hideInput: function () {
			var a = this;
			a.setTextboxValue(""), a.$control_input.css({
				opacity: 0,
				position: "absolute",
				left: a.rtl ? 1e4 : -1e4
			}), a.isInputHidden = !0
		},
		showInput: function () {
			this.$control_input.css({
				opacity: 1,
				position: "relative",
				left: 0
			}), this.isInputHidden = !1
		},
		focus: function () {
			var a = this;
			a.isDisabled || (a.ignoreFocus = !0, a.$control_input[0].focus(), window.setTimeout(function () {
				a.ignoreFocus = !1, a.onFocus()
			}, 0))
		},
		blur: function (a) {
			this.$control_input[0].blur(), this.onBlur(null, a)
		},
		getScoreFunction: function (a) {
			return this.sifter.getScoreFunction(a, this.getSearchOptions())
		},
		getSearchOptions: function () {
			var a = this.settings,
				b = a.sortField;
			return "string" == typeof b && (b = [{
				field: b
			}]), {
				fields: a.searchField,
				conjunction: a.searchConjunction,
				sort: b
			}
		},
		search: function (b) {
			var c, d, e, f = this,
				g = f.settings,
				h = this.getSearchOptions();
			if (g.score && (e = f.settings.score.apply(this, [b]), "function" != typeof e)) throw new Error('Selectize "score" setting must be a function that returns a function');
			if (b !== f.lastQuery ? (f.lastQuery = b, d = f.sifter.search(b, a.extend(h, {
					score: e
				})), f.currentResults = d) : d = a.extend(!0, {}, f.currentResults), g.hideSelected)
				for (c = d.items.length - 1; c >= 0; c--) f.items.indexOf(z(d.items[c].id)) !== -1 && d.items.splice(c, 1);
			return d
		},
		refreshOptions: function (b) {
			var c, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s;
			"undefined" == typeof b && (b = !0);
			var t = this,
				u = a.trim(t.$control_input.val()),
				v = t.search(u),
				w = t.$dropdown_content,
				x = t.$activeOption && z(t.$activeOption.attr("data-value"));
			for (g = v.items.length, "number" == typeof t.settings.maxOptions && (g = Math.min(g, t.settings.maxOptions)), h = {}, i = [], c = 0; c < g; c++)
				for (j = t.options[v.items[c].id], k = t.render("option", j), l = j[t.settings.optgroupField] || "", m = a.isArray(l) ? l : [l], e = 0, f = m && m.length; e < f; e++) l = m[e], t.optgroups.hasOwnProperty(l) || (l = ""), h.hasOwnProperty(l) || (h[l] = document.createDocumentFragment(), i.push(l)), h[l].appendChild(k);
			for (this.settings.lockOptgroupOrder && i.sort(function (a, b) {
					var c = t.optgroups[a].$order || 0,
						d = t.optgroups[b].$order || 0;
					return c - d
				}), n = document.createDocumentFragment(), c = 0, g = i.length; c < g; c++) l = i[c], t.optgroups.hasOwnProperty(l) && h[l].childNodes.length ? (o = document.createDocumentFragment(), o.appendChild(t.render("optgroup_header", t.optgroups[l])), o.appendChild(h[l]), n.appendChild(t.render("optgroup", a.extend({}, t.optgroups[l], {
				html: K(o),
				dom: o
			})))) : n.appendChild(h[l]);
			if (w.html(n), t.settings.highlight && v.query.length && v.tokens.length)
				for (w.removeHighlight(), c = 0, g = v.tokens.length; c < g; c++) d(w, v.tokens[c].regex);
			if (!t.settings.hideSelected)
				for (c = 0, g = t.items.length; c < g; c++) t.getOption(t.items[c]).addClass("selected");
			p = t.canCreate(u), p && (w.prepend(t.render("option_create", {
				input: u
			})), s = a(w[0].childNodes[0])), t.hasOptions = v.items.length > 0 || p, t.hasOptions ? (v.items.length > 0 ? (r = x && t.getOption(x), r && r.length ? q = r : "single" === t.settings.mode && t.items.length && (q = t.getOption(t.items[0])), q && q.length || (q = s && !t.settings.addPrecedence ? t.getAdjacentOption(s, 1) : w.find("[data-selectable]:first"))) : q = s, t.setActiveOption(q), b && !t.isOpen && t.open()) : (t.setActiveOption(null), b && t.isOpen && t.close())
		},
		addOption: function (b) {
			var c, d, e, f = this;
			if (a.isArray(b))
				for (c = 0, d = b.length; c < d; c++) f.addOption(b[c]);
			else(e = f.registerOption(b)) && (f.userOptions[e] = !0, f.lastQuery = null, f.trigger("option_add", e, b))
		},
		registerOption: function (a) {
			var b = z(a[this.settings.valueField]);
			return "undefined" != typeof b && null !== b && !this.options.hasOwnProperty(b) && (a.$order = a.$order || ++this.order, this.options[b] = a, b)
		},
		registerOptionGroup: function (a) {
			var b = z(a[this.settings.optgroupValueField]);
			return !!b && (a.$order = a.$order || ++this.order, this.optgroups[b] = a, b)
		},
		addOptionGroup: function (a, b) {
			b[this.settings.optgroupValueField] = a, (a = this.registerOptionGroup(b)) && this.trigger("optgroup_add", a, b)
		},
		removeOptionGroup: function (a) {
			this.optgroups.hasOwnProperty(a) && (delete this.optgroups[a], this.renderCache = {}, this.trigger("optgroup_remove", a))
		},
		clearOptionGroups: function () {
			this.optgroups = {}, this.renderCache = {}, this.trigger("optgroup_clear")
		},
		updateOption: function (b, c) {
			var d, e, f, g, h, i, j, k = this;
			if (b = z(b), f = z(c[k.settings.valueField]), null !== b && k.options.hasOwnProperty(b)) {
				if ("string" != typeof f) throw new Error("Value must be set in option data");
				j = k.options[b].$order, f !== b && (delete k.options[b], g = k.items.indexOf(b), g !== -1 && k.items.splice(g, 1, f)), c.$order = c.$order || j, k.options[f] = c, h = k.renderCache.item, i = k.renderCache.option, h && (delete h[b], delete h[f]), i && (delete i[b], delete i[f]), k.items.indexOf(f) !== -1 && (d = k.getItem(b), e = a(k.render("item", c)), d.hasClass("active") && e.addClass("active"), d.replaceWith(e)), k.lastQuery = null, k.isOpen && k.refreshOptions(!1)
			}
		},
		removeOption: function (a, b) {
			var c = this;
			a = z(a);
			var d = c.renderCache.item,
				e = c.renderCache.option;
			d && delete d[a], e && delete e[a], delete c.userOptions[a], delete c.options[a], c.lastQuery = null, c.trigger("option_remove", a), c.removeItem(a, b)
		},
		clearOptions: function () {
			var a = this;
			a.loadedSearches = {}, a.userOptions = {}, a.renderCache = {}, a.options = a.sifter.items = {}, a.lastQuery = null, a.trigger("option_clear"), a.clear()
		},
		getOption: function (a) {
			return this.getElementWithValue(a, this.$dropdown_content.find("[data-selectable]"))
		},
		getAdjacentOption: function (b, c) {
			var d = this.$dropdown.find("[data-selectable]"),
				e = d.index(b) + c;
			return e >= 0 && e < d.length ? d.eq(e) : a()
		},
		getElementWithValue: function (b, c) {
			if (b = z(b), "undefined" != typeof b && null !== b)
				for (var d = 0, e = c.length; d < e; d++)
					if (c[d].getAttribute("data-value") === b) return a(c[d]);
			return a()
		},
		getItem: function (a) {
			return this.getElementWithValue(a, this.$control.children())
		},
		addItems: function (b, c) {
			for (var d = a.isArray(b) ? b : [b], e = 0, f = d.length; e < f; e++) this.isPending = e < f - 1, this.addItem(d[e], c)
		},
		addItem: function (b, c) {
			var d = c ? [] : ["change"];
			E(this, d, function () {
				var d, e, f, g, h, i = this,
					j = i.settings.mode;
				return b = z(b), i.items.indexOf(b) !== -1 ? void("single" === j && i.close()) : void(i.options.hasOwnProperty(b) && ("single" === j && i.clear(c), "multi" === j && i.isFull() || (d = a(i.render("item", i.options[b])), h = i.isFull(), i.items.splice(i.caretPos, 0, b), i.insertAtCaret(d), (!i.isPending || !h && i.isFull()) && i.refreshState(), i.isSetup && (f = i.$dropdown_content.find("[data-selectable]"), i.isPending || (e = i.getOption(b), g = i.getAdjacentOption(e, 1).attr("data-value"), i.refreshOptions(i.isFocused && "single" !== j), g && i.setActiveOption(i.getOption(g))), !f.length || i.isFull() ? i.close() : i.positionDropdown(), i.updatePlaceholder(), i.trigger("item_add", b, d), i.updateOriginalInput({
					silent: c
				})))))
			})
		},
		removeItem: function (b, c) {
			var d, e, f, g = this;
			d = b instanceof a ? b : g.getItem(b), b = z(d.attr("data-value")), e = g.items.indexOf(b), e !== -1 && (d.remove(), d.hasClass("active") && (f = g.$activeItems.indexOf(d[0]), g.$activeItems.splice(f, 1)), g.items.splice(e, 1), g.lastQuery = null, !g.settings.persist && g.userOptions.hasOwnProperty(b) && g.removeOption(b, c), e < g.caretPos && g.setCaret(g.caretPos - 1), g.refreshState(), g.updatePlaceholder(), g.updateOriginalInput({
				silent: c
			}), g.positionDropdown(), g.trigger("item_remove", b, d))
		},
		createItem: function (b, c) {
			var d = this,
				e = d.caretPos;
			b = b || a.trim(d.$control_input.val() || "");
			var f = arguments[arguments.length - 1];
			if ("function" != typeof f && (f = function () {}), "boolean" != typeof c && (c = !0), !d.canCreate(b)) return f(), !1;
			d.lock();
			var g = "function" == typeof d.settings.create ? this.settings.create : function (a) {
					var b = {};
					return b[d.settings.labelField] = a, b[d.settings.valueField] = a, b
				},
				h = C(function (a) {
					if (d.unlock(), !a || "object" != typeof a) return f();
					var b = z(a[d.settings.valueField]);
					return "string" != typeof b ? f() : (d.setTextboxValue(""), d.addOption(a), d.setCaret(e), d.addItem(b), d.refreshOptions(c && "single" !== d.settings.mode), void f(a))
				}),
				i = g.apply(this, [b, h]);
			return "undefined" != typeof i && h(i), !0
		},
		refreshItems: function () {
			this.lastQuery = null, this.isSetup && this.addItem(this.items), this.refreshState(), this.updateOriginalInput()
		},
		refreshState: function () {
			this.refreshValidityState(), this.refreshClasses()
		},
		refreshValidityState: function () {
			if (!this.isRequired) return !1;
			var a = !this.items.length;
			this.isInvalid = a, this.$control_input.prop("required", a), this.$input.prop("required", !a)
		},
		refreshClasses: function () {
			var b = this,
				c = b.isFull(),
				d = b.isLocked;
			b.$wrapper.toggleClass("rtl", b.rtl), b.$control.toggleClass("focus", b.isFocused).toggleClass("disabled", b.isDisabled).toggleClass("required", b.isRequired).toggleClass("invalid", b.isInvalid).toggleClass("locked", d).toggleClass("full", c).toggleClass("not-full", !c).toggleClass("input-active", b.isFocused && !b.isInputHidden).toggleClass("dropdown-active", b.isOpen).toggleClass("has-options", !a.isEmptyObject(b.options)).toggleClass("has-items", b.items.length > 0), b.$control_input.data("grow", !c && !d)
		},
		isFull: function () {
			return null !== this.settings.maxItems && this.items.length >= this.settings.maxItems
		},
		updateOriginalInput: function (a) {
			var b, c, d, e, f = this;
			if (a = a || {}, f.tagType === v) {
				for (d = [], b = 0, c = f.items.length; b < c; b++) e = f.options[f.items[b]][f.settings.labelField] || "", d.push('<option value="' + A(f.items[b]) + '" selected="selected">' + A(e) + "</option>");
				d.length || this.$input.attr("multiple") || d.push('<option value="" selected="selected"></option>'), f.$input.html(d.join(""))
			} else f.$input.val(f.getValue()), f.$input.attr("value", f.$input.val());
			f.isSetup && (a.silent || f.trigger("change", f.$input.val()))
		},
		updatePlaceholder: function () {
			if (this.settings.placeholder) {
				var a = this.$control_input;
				this.items.length ? a.removeAttr("placeholder") : a.attr("placeholder", this.settings.placeholder), a.triggerHandler("update", {
					force: !0
				})
			}
		},
		open: function () {
			var a = this;
			a.isLocked || a.isOpen || "multi" === a.settings.mode && a.isFull() || (a.focus(), a.isOpen = !0, a.refreshState(), a.$dropdown.css({
				visibility: "hidden",
				display: "block"
			}), a.positionDropdown(), a.$dropdown.css({
				visibility: "visible"
			}), a.trigger("dropdown_open", a.$dropdown))
		},
		close: function () {
			var a = this,
				b = a.isOpen;
			"single" === a.settings.mode && a.items.length && (a.hideInput(), a.$control_input.blur()), a.isOpen = !1, a.$dropdown.hide(), a.setActiveOption(null), a.refreshState(), b && a.trigger("dropdown_close", a.$dropdown)
		},
		positionDropdown: function () {
			var a = this.$control,
				b = "body" === this.settings.dropdownParent ? a.offset() : a.position();
			b.top += a.outerHeight(!0), this.$dropdown.css({
				width: a.outerWidth(),
				top: b.top,
				left: b.left
			})
		},
		clear: function (a) {
			var b = this;
			b.items.length && (b.$control.children(":not(input)").remove(), b.items = [], b.lastQuery = null, b.setCaret(0), b.setActiveItem(null), b.updatePlaceholder(), b.updateOriginalInput({
				silent: a
			}), b.refreshState(), b.showInput(), b.trigger("clear"))
		},
		insertAtCaret: function (b) {
			var c = Math.min(this.caretPos, this.items.length);
			0 === c ? this.$control.prepend(b) : a(this.$control[0].childNodes[c]).before(b), this.setCaret(c + 1)
		},
		deleteSelection: function (b) {
			var c, d, e, f, g, h, i, j, k, l = this;
			if (e = b && b.keyCode === p ? -1 : 1, f = G(l.$control_input[0]), l.$activeOption && !l.settings.hideSelected && (i = l.getAdjacentOption(l.$activeOption, -1).attr("data-value")), g = [], l.$activeItems.length) {
				for (k = l.$control.children(".active:" + (e > 0 ? "last" : "first")), h = l.$control.children(":not(input)").index(k), e > 0 && h++, c = 0, d = l.$activeItems.length; c < d; c++) g.push(a(l.$activeItems[c]).attr("data-value"));
				b && (b.preventDefault(), b.stopPropagation())
			} else(l.isFocused || "single" === l.settings.mode) && l.items.length && (e < 0 && 0 === f.start && 0 === f.length ? g.push(l.items[l.caretPos - 1]) : e > 0 && f.start === l.$control_input.val().length && g.push(l.items[l.caretPos]));
			if (!g.length || "function" == typeof l.settings.onDelete && l.settings.onDelete.apply(l, [g]) === !1) return !1;
			for ("undefined" != typeof h && l.setCaret(h); g.length;) l.removeItem(g.pop());
			return l.showInput(), l.positionDropdown(), l.refreshOptions(!0), i && (j = l.getOption(i), j.length && l.setActiveOption(j)), !0
		},
		advanceSelection: function (a, b) {
			var c, d, e, f, g, h, i = this;
			0 !== a && (i.rtl && (a *= -1), c = a > 0 ? "last" : "first", d = G(i.$control_input[0]), i.isFocused && !i.isInputHidden ? (f = i.$control_input.val().length, g = a < 0 ? 0 === d.start && 0 === d.length : d.start === f, g && !f && i.advanceCaret(a, b)) : (h = i.$control.children(".active:" + c), h.length && (e = i.$control.children(":not(input)").index(h), i.setActiveItem(null), i.setCaret(a > 0 ? e + 1 : e))))
		},
		advanceCaret: function (a, b) {
			var c, d, e = this;
			0 !== a && (c = a > 0 ? "next" : "prev", e.isShiftDown ? (d = e.$control_input[c](), d.length && (e.hideInput(), e.setActiveItem(d), b && b.preventDefault())) : e.setCaret(e.caretPos + a))
		},
		setCaret: function (b) {
			var c = this;
			if (b = "single" === c.settings.mode ? c.items.length : Math.max(0, Math.min(c.items.length, b)), !c.isPending) {
				var d, e, f, g;
				for (f = c.$control.children(":not(input)"), d = 0, e = f.length; d < e; d++) g = a(f[d]).detach(), d < b ? c.$control_input.before(g) : c.$control.append(g)
			}
			c.caretPos = b
		},
		lock: function () {
			this.close(), this.isLocked = !0, this.refreshState()
		},
		unlock: function () {
			this.isLocked = !1, this.refreshState()
		},
		disable: function () {
			var a = this;
			a.$input.prop("disabled", !0), a.$control_input.prop("disabled", !0).prop("tabindex", -1), a.isDisabled = !0, a.lock()
		},
		enable: function () {
			var a = this;
			a.$input.prop("disabled", !1), a.$control_input.prop("disabled", !1).prop("tabindex", a.tabIndex), a.isDisabled = !1, a.unlock()
		},
		destroy: function () {
			var b = this,
				c = b.eventNS,
				d = b.revertSettings;
			b.trigger("destroy"), b.off(), b.$wrapper.remove(), b.$dropdown.remove(), b.$input.html("").append(d.$children).removeAttr("tabindex").removeClass("selectized").attr({
				tabindex: d.tabindex
			}).show(), b.$control_input.removeData("grow"), b.$input.removeData("selectize"), a(window).off(c), a(document).off(c), a(document.body).off(c), delete b.$input[0].selectize
		},
		render: function (b, c) {
			var d, e, f = "",
				g = !1,
				h = this;
			return "option" !== b && "item" !== b || (d = z(c[h.settings.valueField]), g = !!d), g && (y(h.renderCache[b]) || (h.renderCache[b] = {}), h.renderCache[b].hasOwnProperty(d)) ? h.renderCache[b][d] : (f = a(h.settings.render[b].apply(this, [c, A])), "option" === b || "option_create" === b ? f.attr("data-selectable", "") : "optgroup" === b && (e = c[h.settings.optgroupValueField] || "", f.attr("data-group", e)), "option" !== b && "item" !== b || f.attr("data-value", d || ""), g && (h.renderCache[b][d] = f[0]), f[0])
		},
		clearCache: function (a) {
			var b = this;
			"undefined" == typeof a ? b.renderCache = {} : delete b.renderCache[a]
		},
		canCreate: function (a) {
			var b = this;
			if (!b.settings.create) return !1;
			var c = b.settings.createFilter;
			return a.length && ("function" != typeof c || c.apply(b, [a])) && ("string" != typeof c || new RegExp(c).test(a)) && (!(c instanceof RegExp) || c.test(a))
		}
	}), M.count = 0, M.defaults = {
		options: [],
		optgroups: [],
		plugins: [],
		delimiter: ",",
		splitOn: null,
		persist: !0,
		diacritics: !0,
		create: !1,
		createOnBlur: !1,
		createFilter: null,
		highlight: !0,
		openOnFocus: !0,
		maxOptions: 1e3,
		maxItems: null,
		hideSelected: null,
		addPrecedence: !1,
		selectOnTab: !1,
		preload: !1,
		allowEmptyOption: !1,
		closeAfterSelect: !1,
		scrollDuration: 60,
		loadThrottle: 300,
		loadingClass: "loading",
		dataAttr: "data-data",
		optgroupField: "optgroup",
		valueField: "value",
		labelField: "text",
		optgroupLabelField: "label",
		optgroupValueField: "value",
		lockOptgroupOrder: !1,
		sortField: "$order",
		searchField: ["text"],
		searchConjunction: "and",
		mode: null,
		wrapperClass: "selectize-control",
		inputClass: "selectize-input",
		dropdownClass: "selectize-dropdown",
		dropdownContentClass: "selectize-dropdown-content",
		dropdownParent: null,
		copyClassesToDropdown: !0,
		render: {}
	}, a.fn.selectize = function (b) {
		var c = a.fn.selectize.defaults,
			d = a.extend({}, c, b),
			e = d.dataAttr,
			f = d.labelField,
			g = d.valueField,
			h = d.optgroupField,
			i = d.optgroupLabelField,
			j = d.optgroupValueField,
			k = function (b, c) {
				var h, i, j, k, l = b.attr(e);
				if (l)
					for (c.options = JSON.parse(l), h = 0, i = c.options.length; h < i; h++) c.items.push(c.options[h][g]);
				else {
					var m = a.trim(b.val() || "");
					if (!d.allowEmptyOption && !m.length) return;
					for (j = m.split(d.delimiter), h = 0, i = j.length; h < i; h++) k = {}, k[f] = j[h], k[g] = j[h], c.options.push(k);
					c.items = j
				}
			},
			l = function (b, c) {
				var k, l, m, n, o = c.options,
					p = {},
					q = function (a) {
						var b = e && a.attr(e);
						return "string" == typeof b && b.length ? JSON.parse(b) : null
					},
					r = function (b, e) {
						b = a(b);
						var i = z(b.val());
						if (i || d.allowEmptyOption)
							if (p.hasOwnProperty(i)) {
								if (e) {
									var j = p[i][h];
									j ? a.isArray(j) ? j.push(e) : p[i][h] = [j, e] : p[i][h] = e
								}
							} else {
								var k = q(b) || {};
								k[f] = k[f] || b.text(), k[g] = k[g] || i, k[h] = k[h] || e, p[i] = k, o.push(k), b.is(":selected") && c.items.push(i)
							}
					},
					s = function (b) {
						var d, e, f, g, h;
						for (b = a(b), f = b.attr("label"), f && (g = q(b) || {}, g[i] = f, g[j] = f, c.optgroups.push(g)), h = a("option", b), d = 0, e = h.length; d < e; d++) r(h[d], f)
					};
				for (c.maxItems = b.attr("multiple") ? null : 1, n = b.children(), k = 0, l = n.length; k < l; k++) m = n[k].tagName.toLowerCase(), "optgroup" === m ? s(n[k]) : "option" === m && r(n[k])
			};
		return this.each(function () {
			if (!this.selectize) {
				var e, f = a(this),
					g = this.tagName.toLowerCase(),
					h = f.attr("placeholder") || f.attr("data-placeholder");
				h || d.allowEmptyOption || (h = f.children('option[value=""]').text());
				var i = {
					placeholder: h,
					options: [],
					optgroups: [],
					items: []
				};
				"select" === g ? l(f, i) : k(f, i), e = new M(f, a.extend(!0, {}, c, i, b))
			}
		})
	}, a.fn.selectize.defaults = M.defaults, a.fn.selectize.support = {
		validity: x
	}, M.define("drag_drop", function (b) {
		if (!a.fn.sortable) throw new Error('The "drag_drop" plugin requires jQuery UI "sortable".');
		if ("multi" === this.settings.mode) {
			var c = this;
			c.lock = function () {
				var a = c.lock;
				return function () {
					var b = c.$control.data("sortable");
					return b && b.disable(), a.apply(c, arguments)
				}
			}(), c.unlock = function () {
				var a = c.unlock;
				return function () {
					var b = c.$control.data("sortable");
					return b && b.enable(), a.apply(c, arguments)
				}
			}(), c.setup = function () {
				var b = c.setup;
				return function () {
					b.apply(this, arguments);
					var d = c.$control.sortable({
						items: "[data-value]",
						forcePlaceholderSize: !0,
						disabled: c.isLocked,
						start: function (a, b) {
							b.placeholder.css("width", b.helper.css("width")), d.css({
								overflow: "visible"
							})
						},
						stop: function () {
							d.css({
								overflow: "hidden"
							});
							var b = c.$activeItems ? c.$activeItems.slice() : null,
								e = [];
							d.children("[data-value]").each(function () {
								e.push(a(this).attr("data-value"))
							}), c.setValue(e), c.setActiveItem(b)
						}
					})
				}
			}()
		}
	}), M.define("dropdown_header", function (b) {
		var c = this;
		b = a.extend({
			title: "Untitled",
			headerClass: "selectize-dropdown-header",
			titleRowClass: "selectize-dropdown-header-title",
			labelClass: "selectize-dropdown-header-label",
			closeClass: "selectize-dropdown-header-close",
			html: function (a) {
				return '<div class="' + a.headerClass + '"><div class="' + a.titleRowClass + '"><span class="' + a.labelClass + '">' + a.title + '</span><a href="javascript:void(0)" class="' + a.closeClass + '">&times;</a></div></div>'
			}
		}, b), c.setup = function () {
			var d = c.setup;
			return function () {
				d.apply(c, arguments), c.$dropdown_header = a(b.html(b)), c.$dropdown.prepend(c.$dropdown_header)
			}
		}()
	}), M.define("optgroup_columns", function (b) {
		var c = this;
		b = a.extend({
			equalizeWidth: !0,
			equalizeHeight: !0
		}, b), this.getAdjacentOption = function (b, c) {
			var d = b.closest("[data-group]").find("[data-selectable]"),
				e = d.index(b) + c;
			return e >= 0 && e < d.length ? d.eq(e) : a()
		}, this.onKeyDown = function () {
			var a = c.onKeyDown;
			return function (b) {
				var d, e, f, g;
				return !this.isOpen || b.keyCode !== j && b.keyCode !== m ? a.apply(this, arguments) : (c.ignoreHover = !0, g = this.$activeOption.closest("[data-group]"), d = g.find("[data-selectable]").index(this.$activeOption), g = b.keyCode === j ? g.prev("[data-group]") : g.next("[data-group]"), f = g.find("[data-selectable]"), e = f.eq(Math.min(f.length - 1, d)), void(e.length && this.setActiveOption(e)))
			}
		}();
		var d = function () {
				var a, b = d.width,
					c = document;
				return "undefined" == typeof b && (a = c.createElement("div"), a.innerHTML = '<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>', a = a.firstChild, c.body.appendChild(a), b = d.width = a.offsetWidth - a.clientWidth, c.body.removeChild(a)), b
			},
			e = function () {
				var e, f, g, h, i, j, k;
				if (k = a("[data-group]", c.$dropdown_content), f = k.length, f && c.$dropdown_content.width()) {
					if (b.equalizeHeight) {
						for (g = 0, e = 0; e < f; e++) g = Math.max(g, k.eq(e).height());
						k.css({
							height: g
						})
					}
					b.equalizeWidth && (j = c.$dropdown_content.innerWidth() - d(), h = Math.round(j / f), k.css({
						width: h
					}), f > 1 && (i = j - h * (f - 1), k.eq(f - 1).css({
						width: i
					})))
				}
			};
		(b.equalizeHeight || b.equalizeWidth) && (B.after(this, "positionDropdown", e), B.after(this, "refreshOptions", e))
	}), M.define("remove_button", function (b) {
		b = a.extend({
			label: "&times;",
			title: "Remove",
			className: "remove",
			append: !0
		}, b);
		var c = function (b, c) {
				c.className = "remove-single";
				var d = b,
					e = '<a href="javascript:void(0)" class="' + c.className + '" tabindex="-1" title="' + A(c.title) + '">' + c.label + "</a>",
					f = function (a, b) {
						return a + b
					};
				b.setup = function () {
					var g = d.setup;
					return function () {
						if (c.append) {
							var h = a(d.$input.context).attr("id"),
								i = (a("#" + h), d.settings.render.item);
							d.settings.render.item = function (a) {
								return f(i.apply(b, arguments), e)
							}
						}
						g.apply(b, arguments), b.$control.on("click", "." + c.className, function (a) {
							a.preventDefault(), d.isLocked || d.clear()
						})
					}
				}()
			},
			d = function (b, c) {
				var d = b,
					e = '<a href="javascript:void(0)" class="' + c.className + '" tabindex="-1" title="' + A(c.title) + '">' + c.label + "</a>",
					f = function (a, b) {
						var c = a.search(/(<\/[^>]+>\s*)$/);
						return a.substring(0, c) + b + a.substring(c)
					};
				b.setup = function () {
					var g = d.setup;
					return function () {
						if (c.append) {
							var h = d.settings.render.item;
							d.settings.render.item = function (a) {
								return f(h.apply(b, arguments), e)
							}
						}
						g.apply(b, arguments), b.$control.on("click", "." + c.className, function (b) {
							if (b.preventDefault(), !d.isLocked) {
								var c = a(b.currentTarget).parent();
								d.setActiveItem(c), d.deleteSelection() && d.setCaret(d.items.length)
							}
						})
					}
				}()
			};
		return "single" === this.settings.mode ? void c(this, b) : void d(this, b)
	}), M.define("restore_on_backspace", function (a) {
		var b = this;
		a.text = a.text || function (a) {
			return a[this.settings.labelField]
		}, this.onKeyDown = function () {
			var c = b.onKeyDown;
			return function (b) {
				var d, e;
				return b.keyCode === p && "" === this.$control_input.val() && !this.$activeItems.length && (d = this.caretPos - 1, d >= 0 && d < this.items.length) ? (e = this.options[this.items[d]], this.deleteSelection(b) && (this.setTextboxValue(a.text.apply(this, [e])), this.refreshOptions(!0)), void b.preventDefault()) : c.apply(this, arguments)
			}
		}()
	}), M
});
! function (a, b) {
	"use strict";
	"function" == typeof define && define.amd ? define(b) : "object" == typeof exports ? module.exports = b() : a.baguetteBox = b()
}(this, function () {
	"use strict";

	function B(a, b) {
		f.transforms = V(), f.svg = W(), F(), E(a), C(a, b)
	}

	function C(a, b) {
		var c = document.querySelectorAll(a),
			d = {
				galleries: [],
				nodeList: c
			};
		q[a] = d, [].forEach.call(c, function (a) {
			b && b.filter && (p = b.filter);
			var c = [];
			if (c = "A" === a.tagName ? [a] : a.getElementsByTagName("a"), c = [].filter.call(c, function (a) {
					return p.test(a.href)
				}), 0 !== c.length) {
				var e = [];
				[].forEach.call(c, function (a, c) {
					var d = function (a) {
							a.preventDefault ? a.preventDefault() : a.returnValue = !1, J(e, b), L(c)
						},
						f = {
							eventHandler: d,
							imageElement: a
						};
					Z(a, "click", d), e.push(f)
				}), d.galleries.push(e)
			}
		})
	}

	function D() {
		for (var a in q) q.hasOwnProperty(a) && E(a)
	}

	function E(a) {
		if (q.hasOwnProperty(a)) {
			var b = q[a].galleries;
			[].forEach.call(b, function (a) {
				[].forEach.call(a, function (a) {
					$(a.imageElement, "click", a.eventHandler)
				}), l === a && (l = [])
			}), delete q[a]
		}
	}

	function F() {
		if (g = _("baguetteBox-overlay")) return h = _("baguetteBox-slider"), i = _("previous-button"), j = _("next-button"), void(k = _("close-button"));
		g = aa("div"), g.setAttribute("role", "dialog"), g.id = "baguetteBox-overlay", document.getElementsByTagName("body")[0].appendChild(g), h = aa("div"), h.id = "baguetteBox-slider", g.appendChild(h), i = aa("button"), i.setAttribute("type", "button"), i.id = "previous-button", i.setAttribute("aria-label", "Previous"), i.innerHTML = f.svg ? a : "&lt;", g.appendChild(i), j = aa("button"), j.setAttribute("type", "button"), j.id = "next-button", j.setAttribute("aria-label", "Next"), j.innerHTML = f.svg ? b : "&gt;", g.appendChild(j), k = aa("button"), k.setAttribute("type", "button"), k.id = "close-button", k.setAttribute("aria-label", "Close"), k.innerHTML = f.svg ? c : "&times;", g.appendChild(k), i.className = j.className = k.className = "baguetteBox-button", H()
	}

	function G(a) {
		switch (a.keyCode) {
			case 37:
				T();
				break;
			case 39:
				S();
				break;
			case 27:
				P()
		}
	}

	function H() {
		Z(g, "click", t), Z(i, "click", u), Z(j, "click", v), Z(k, "click", w), Z(g, "touchstart", x), Z(g, "touchmove", y), Z(g, "touchend", z), Z(document, "focus", A, !0)
	}

	function I() {
		$(g, "click", t), $(i, "click", u), $(j, "click", v), $(k, "click", w), $(g, "touchstart", x), $(g, "touchmove", y), $(g, "touchend", z), $(document, "focus", A, !0)
	}

	function J(a, b) {
		if (l !== a) {
			for (l = a, K(b); h.firstChild;) h.removeChild(h.firstChild);
			r.length = 0;
			for (var f, c = [], d = [], e = 0; e < a.length; e++) f = aa("div"), f.className = "full-image", f.id = "baguette-img-" + e, r.push(f), c.push("baguetteBox-figure-" + e), d.push("baguetteBox-figcaption-" + e), h.appendChild(r[e]);
			g.setAttribute("aria-labelledby", c.join(" ")), g.setAttribute("aria-describedby", d.join(" "))
		}
	}

	function K(a) {
		a || (a = {});
		for (var b in e) d[b] = e[b], void 0 !== a[b] && (d[b] = a[b]);
		h.style.transition = h.style.webkitTransition = "fadeIn" === d.animation ? "opacity .4s ease" : "slideIn" === d.animation ? "" : "none", "auto" === d.buttons && ("ontouchstart" in window || 1 === l.length) && (d.buttons = !1), i.style.display = j.style.display = d.buttons ? "" : "none";
		try {
			g.style.backgroundColor = d.overlayBackgroundColor
		} catch (a) {}
	}

	function L(a) {
		d.noScrollbars && (document.documentElement.style.overflowY = "hidden", document.body.style.overflowY = "scroll"), "block" !== g.style.display && (Z(document, "keydown", G), m = a, n = {
			count: 0,
			startX: null,
			startY: null
		}, Q(m, function () {
			X(m), Y(m)
		}), U(), g.style.display = "block", d.fullScreen && N(), setTimeout(function () {
			g.className = "visible", d.afterShow && d.afterShow()
		}, 50), d.onChange && d.onChange(m, r.length), s = document.activeElement, M())
	}

	function M() {
		d.buttons ? i.focus() : k.focus()
	}

	function N() {
		g.requestFullscreen ? g.requestFullscreen() : g.webkitRequestFullscreen ? g.webkitRequestFullscreen() : g.mozRequestFullScreen && g.mozRequestFullScreen()
	}

	function O() {
		document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen()
	}

	function P() {
		d.noScrollbars && (document.documentElement.style.overflowY = "auto", document.body.style.overflowY = "auto"), "none" !== g.style.display && ($(document, "keydown", G), g.className = "", setTimeout(function () {
			g.style.display = "none", O(), d.afterHide && d.afterHide()
		}, 500), s.focus())
	}

	function Q(a, b) {
		var c = r[a];
		if (void 0 !== c) {
			if (c.getElementsByTagName("img")[0]) return void(b && b());
			var e = l[a].imageElement,
				f = e.getElementsByTagName("img")[0],
				g = "function" == typeof d.captions ? d.captions.call(l, e) : e.getAttribute("data-caption") || e.title,
				h = R(e),
				i = aa("figure");
			if (i.id = "baguetteBox-figure-" + a, i.innerHTML = '<div class="baguetteBox-spinner"><div class="baguetteBox-double-bounce1"></div><div class="baguetteBox-double-bounce2"></div></div>', d.captions && g) {
				var j = aa("figcaption");
				j.id = "baguetteBox-figcaption-" + a, j.innerHTML = g, i.appendChild(j)
			}
			c.appendChild(i);
			var k = aa("img");
			k.onload = function () {
				var c = document.querySelector("#baguette-img-" + a + " .baguetteBox-spinner");
				i.removeChild(c), !d.async && b && b()
			}, k.setAttribute("src", h), k.alt = f ? f.alt || "" : "", d.titleTag && g && (k.title = g), i.appendChild(k), d.async && b && b()
		}
	}

	function R(a) {
		var b = a.href;
		if (a.dataset) {
			var c = [];
			for (var d in a.dataset) "at-" !== d.substring(0, 3) || isNaN(d.substring(3)) || (c[d.replace("at-", "")] = a.dataset[d]);
			for (var e = Object.keys(c).sort(function (a, b) {
					return parseInt(a, 10) < parseInt(b, 10) ? -1 : 1
				}), f = window.innerWidth * window.devicePixelRatio, g = 0; g < e.length - 1 && e[g] < f;) g++;
			b = c[e[g]] || b
		}
		return b
	}

	function S() {
		var a;
		return m <= r.length - 2 ? (m++, U(), X(m), a = !0) : d.animation && (h.className = "bounce-from-right", setTimeout(function () {
			h.className = ""
		}, 400), a = !1), d.onChange && d.onChange(m, r.length), a
	}

	function T() {
		var a;
		return m >= 1 ? (m--, U(), Y(m), a = !0) : d.animation && (h.className = "bounce-from-left", setTimeout(function () {
			h.className = ""
		}, 400), a = !1), d.onChange && d.onChange(m, r.length), a
	}

	function U() {
		var a = 100 * -m + "%";
		"fadeIn" === d.animation ? (h.style.opacity = 0, setTimeout(function () {
			f.transforms ? h.style.transform = h.style.webkitTransform = "translate3d(" + a + ",0,0)" : h.style.left = a, h.style.opacity = 1
		}, 400)) : f.transforms ? h.style.transform = h.style.webkitTransform = "translate3d(" + a + ",0,0)" : h.style.left = a
	}

	function V() {
		var a = aa("div");
		return void 0 !== a.style.perspective || void 0 !== a.style.webkitPerspective
	}

	function W() {
		var a = aa("div");
		return a.innerHTML = "<svg/>", "http://www.w3.org/2000/svg" === (a.firstChild && a.firstChild.namespaceURI)
	}

	function X(a) {
		a - m >= d.preload || Q(a + 1, function () {
			X(a + 1)
		})
	}

	function Y(a) {
		m - a >= d.preload || Q(a - 1, function () {
			Y(a - 1)
		})
	}

	function Z(a, b, c, d) {
		a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent("on" + b, c)
	}

	function $(a, b, c, d) {
		a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent("on" + b, c)
	}

	function _(a) {
		return document.getElementById(a)
	}

	function aa(a) {
		return document.createElement(a)
	}

	function ba() {
		I(), D(), $(document, "keydown", G), document.getElementsByTagName("body")[0].removeChild(document.getElementById("baguetteBox-overlay")), q = {}, l = [], m = 0
	}
	var g, h, i, j, k, a = '<svg width="44" height="60"><polyline points="30 10 10 30 30 50" stroke="rgba(255,255,255,0.5)" stroke-width="4"stroke-linecap="butt" fill="none" stroke-linejoin="round"/></svg>',
		b = '<svg width="44" height="60"><polyline points="14 10 34 30 14 50" stroke="rgba(255,255,255,0.5)" stroke-width="4"stroke-linecap="butt" fill="none" stroke-linejoin="round"/></svg>',
		c = '<svg width="30" height="30"><g stroke="rgb(160,160,160)" stroke-width="4"><line x1="5" y1="5" x2="25" y2="25"/><line x1="5" y1="25" x2="25" y2="5"/></g></svg>',
		d = {},
		e = {
			captions: !0,
			fullScreen: !1,
			noScrollbars: !1,
			titleTag: !1,
			buttons: "auto",
			async: !1,
			preload: 2,
			animation: "slideIn",
			afterShow: null,
			afterHide: null,
			onChange: null,
			overlayBackgroundColor: "rgba(0,0,0,.8)"
		},
		f = {},
		l = [],
		m = 0,
		n = {},
		o = !1,
		p = /.+\.(gif|jpe?g|png|webp)/i,
		q = {},
		r = [],
		s = null,
		t = function (a) {
			-1 !== a.target.id.indexOf("baguette-img") && P()
		},
		u = function (a) {
			a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0, T()
		},
		v = function (a) {
			a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0, S()
		},
		w = function (a) {
			a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0, P()
		},
		x = function (a) {
			n.count++, n.count > 1 && (n.multitouch = !0), n.startX = a.changedTouches[0].pageX, n.startY = a.changedTouches[0].pageY
		},
		y = function (a) {
			if (!o && !n.multitouch) {
				a.preventDefault ? a.preventDefault() : a.returnValue = !1;
				var b = a.touches[0] || a.changedTouches[0];
				b.pageX - n.startX > 40 ? (o = !0, T()) : b.pageX - n.startX < -40 ? (o = !0, S()) : n.startY - b.pageY > 100 && P()
			}
		},
		z = function () {
			n.count--, n.count <= 0 && (n.multitouch = !1), o = !1
		},
		A = function (a) {
			"block" !== g.style.display || g.contains(a.target) || (a.stopPropagation(), M())
		};
	return [].forEach || (Array.prototype.forEach = function (a, b) {
		for (var c = 0; c < this.length; c++) a.call(b, this[c], c, this)
	}), [].filter || (Array.prototype.filter = function (a, b, c, d, e) {
		for (c = this, d = [], e = 0; e < c.length; e++) a.call(b, c[e], e, c) && d.push(c[e]);
		return d
	}), {
		run: B,
		destroy: ba,
		showNext: S,
		showPrevious: T
	}
});

function tinyxhr(url, cb, method, post, contenttype) {
	var c = url,
		a = cb,
		i = method,
		f = post,
		b = contenttype;
	var d, h;
	try {
		h = new XMLHttpRequest()
	} catch (g) {
		try {
			h = new ActiveXObject("Msxml2.XMLHTTP")
		} catch (g) {
			if (console) {
				console.log("tinyxhr: XMLHttpRequest not supported")
			}
			return null
		}
	}
	d = setTimeout(function () {
		h.abort();
		a(new Error("tinyxhr: aborted by a timeout"), "", h)
	}, 10000);
	h.onreadystatechange = function () {
		if (h.readyState != 4) {
			return
		}
		clearTimeout(d);
		a(h.status != 200 ? new Error("tinyxhr: server respnse status is " + h.status) : !1, h.responseText, h)
	};
	h.open(i ? i.toUpperCase() : "GET", c, !0);
	if (!f) {
		h.send()
	} else {
		h.setRequestHeader("Content-type", b ? b : "application/x-www-form-urlencoded");
		h.send(f)
	}
};
! function (t) {
	function e() {}

	function i(t) {
		function i(e) {
			e.prototype.option || (e.prototype.option = function (e) {
				t.isPlainObject(e) && (this.options = t.extend(!0, this.options, e))
			})
		}

		function o(e, i) {
			t.fn[e] = function (o) {
				if ("string" == typeof o) {
					for (var s = n.call(arguments, 1), a = 0, l = this.length; l > a; a++) {
						var c = this[a],
							h = t.data(c, e);
						if (h)
							if (t.isFunction(h[o]) && "_" !== o.charAt(0)) {
								var p = h[o].apply(h, s);
								if (void 0 !== p) return p
							} else r("no such method '" + o + "' for " + e + " instance");
						else r("cannot call methods on " + e + " prior to initialization; attempted to call '" + o + "'")
					}
					return this
				}
				return this.each(function () {
					var n = t.data(this, e);
					n ? (n.option(o), n._init()) : (n = new i(this, o), t.data(this, e, n))
				})
			}
		}
		if (t) {
			var r = "undefined" == typeof console ? e : function (t) {
				console.error(t)
			};
			return t.bridget = function (t, e) {
				i(e), o(t, e)
			}, t.bridget
		}
	}
	var n = Array.prototype.slice;
	"function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], i) : i("object" == typeof exports ? require("jquery") : t.jQuery)
}(window),
function (t) {
	function e(t) {
		return new RegExp("(^|\\s+)" + t + "(\\s+|$)")
	}

	function i(t, e) {
		var i = n(t, e) ? r : o;
		i(t, e)
	}
	var n, o, r;
	"classList" in document.documentElement ? (n = function (t, e) {
		return t.classList.contains(e)
	}, o = function (t, e) {
		t.classList.add(e)
	}, r = function (t, e) {
		t.classList.remove(e)
	}) : (n = function (t, i) {
		return e(i).test(t.className)
	}, o = function (t, e) {
		n(t, e) || (t.className = t.className + " " + e)
	}, r = function (t, i) {
		t.className = t.className.replace(e(i), " ")
	});
	var s = {
		hasClass: n,
		addClass: o,
		removeClass: r,
		toggleClass: i,
		has: n,
		add: o,
		remove: r,
		toggle: i
	};
	"function" == typeof define && define.amd ? define("classie/classie", s) : "object" == typeof exports ? module.exports = s : t.classie = s
}(window),
function () {
	function t() {}

	function e(t, e) {
		for (var i = t.length; i--;)
			if (t[i].listener === e) return i;
		return -1
	}

	function i(t) {
		return function () {
			return this[t].apply(this, arguments)
		}
	}
	var n = t.prototype,
		o = this,
		r = o.EventEmitter;
	n.getListeners = function (t) {
		var e, i, n = this._getEvents();
		if (t instanceof RegExp) {
			e = {};
			for (i in n) n.hasOwnProperty(i) && t.test(i) && (e[i] = n[i])
		} else e = n[t] || (n[t] = []);
		return e
	}, n.flattenListeners = function (t) {
		var e, i = [];
		for (e = 0; e < t.length; e += 1) i.push(t[e].listener);
		return i
	}, n.getListenersAsObject = function (t) {
		var e, i = this.getListeners(t);
		return i instanceof Array && (e = {}, e[t] = i), e || i
	}, n.addListener = function (t, i) {
		var n, o = this.getListenersAsObject(t),
			r = "object" == typeof i;
		for (n in o) o.hasOwnProperty(n) && -1 === e(o[n], i) && o[n].push(r ? i : {
			listener: i,
			once: !1
		});
		return this
	}, n.on = i("addListener"), n.addOnceListener = function (t, e) {
		return this.addListener(t, {
			listener: e,
			once: !0
		})
	}, n.once = i("addOnceListener"), n.defineEvent = function (t) {
		return this.getListeners(t), this
	}, n.defineEvents = function (t) {
		for (var e = 0; e < t.length; e += 1) this.defineEvent(t[e]);
		return this
	}, n.removeListener = function (t, i) {
		var n, o, r = this.getListenersAsObject(t);
		for (o in r) r.hasOwnProperty(o) && (n = e(r[o], i), -1 !== n && r[o].splice(n, 1));
		return this
	}, n.off = i("removeListener"), n.addListeners = function (t, e) {
		return this.manipulateListeners(!1, t, e)
	}, n.removeListeners = function (t, e) {
		return this.manipulateListeners(!0, t, e)
	}, n.manipulateListeners = function (t, e, i) {
		var n, o, r = t ? this.removeListener : this.addListener,
			s = t ? this.removeListeners : this.addListeners;
		if ("object" != typeof e || e instanceof RegExp)
			for (n = i.length; n--;) r.call(this, e, i[n]);
		else
			for (n in e) e.hasOwnProperty(n) && (o = e[n]) && ("function" == typeof o ? r.call(this, n, o) : s.call(this, n, o));
		return this
	}, n.removeEvent = function (t) {
		var e, i = typeof t,
			n = this._getEvents();
		if ("string" === i) delete n[t];
		else if (t instanceof RegExp)
			for (e in n) n.hasOwnProperty(e) && t.test(e) && delete n[e];
		else delete this._events;
		return this
	}, n.removeAllListeners = i("removeEvent"), n.emitEvent = function (t, e) {
		var i, n, o, r, s = this.getListenersAsObject(t);
		for (o in s)
			if (s.hasOwnProperty(o))
				for (n = s[o].length; n--;) i = s[o][n], i.once === !0 && this.removeListener(t, i.listener), r = i.listener.apply(this, e || []), r === this._getOnceReturnValue() && this.removeListener(t, i.listener);
		return this
	}, n.trigger = i("emitEvent"), n.emit = function (t) {
		var e = Array.prototype.slice.call(arguments, 1);
		return this.emitEvent(t, e)
	}, n.setOnceReturnValue = function (t) {
		return this._onceReturnValue = t, this
	}, n._getOnceReturnValue = function () {
		return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
	}, n._getEvents = function () {
		return this._events || (this._events = {})
	}, t.noConflict = function () {
		return o.EventEmitter = r, t
	}, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function () {
		return t
	}) : "object" == typeof module && module.exports ? module.exports = t : o.EventEmitter = t
}.call(this),
	function (t) {
		function e(e) {
			var i = t.event;
			return i.target = i.target || i.srcElement || e, i
		}
		var i = document.documentElement,
			n = function () {};
		i.addEventListener ? n = function (t, e, i) {
			t.addEventListener(e, i, !1)
		} : i.attachEvent && (n = function (t, i, n) {
			t[i + n] = n.handleEvent ? function () {
				var i = e(t);
				n.handleEvent.call(n, i)
			} : function () {
				var i = e(t);
				n.call(t, i)
			}, t.attachEvent("on" + i, t[i + n])
		});
		var o = function () {};
		i.removeEventListener ? o = function (t, e, i) {
			t.removeEventListener(e, i, !1)
		} : i.detachEvent && (o = function (t, e, i) {
			t.detachEvent("on" + e, t[e + i]);
			try {
				delete t[e + i]
			} catch (n) {
				t[e + i] = void 0
			}
		});
		var r = {
			bind: n,
			unbind: o
		};
		"function" == typeof define && define.amd ? define("eventie/eventie", r) : "object" == typeof exports ? module.exports = r : t.eventie = r
	}(window),
	function (t) {
		function e(t) {
			if (t) {
				if ("string" == typeof n[t]) return t;
				t = t.charAt(0).toUpperCase() + t.slice(1);
				for (var e, o = 0, r = i.length; r > o; o++)
					if (e = i[o] + t, "string" == typeof n[e]) return e
			}
		}
		var i = "Webkit Moz ms Ms O".split(" "),
			n = document.documentElement.style;
		"function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function () {
			return e
		}) : "object" == typeof exports ? module.exports = e : t.getStyleProperty = e
	}(window),
	function (t) {
		function e(t) {
			var e = parseFloat(t),
				i = -1 === t.indexOf("%") && !isNaN(e);
			return i && e
		}

		function i() {}

		function n() {
			for (var t = {
					width: 0,
					height: 0,
					innerWidth: 0,
					innerHeight: 0,
					outerWidth: 0,
					outerHeight: 0
				}, e = 0, i = s.length; i > e; e++) {
				var n = s[e];
				t[n] = 0
			}
			return t
		}

		function o(i) {
			function o() {
				if (!d) {
					d = !0;
					var n = t.getComputedStyle;
					if (c = function () {
							var t = n ? function (t) {
								return n(t, null)
							} : function (t) {
								return t.currentStyle
							};
							return function (e) {
								var i = t(e);
								return i || r("Style returned " + i + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), i
							}
						}(), h = i("boxSizing")) {
						var o = document.createElement("div");
						o.style.width = "200px", o.style.padding = "1px 2px 3px 4px", o.style.borderStyle = "solid", o.style.borderWidth = "1px 2px 3px 4px", o.style[h] = "border-box";
						var s = document.body || document.documentElement;
						s.appendChild(o);
						var a = c(o);
						p = 200 === e(a.width), s.removeChild(o)
					}
				}
			}

			function a(t) {
				if (o(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
					var i = c(t);
					if ("none" === i.display) return n();
					var r = {};
					r.width = t.offsetWidth, r.height = t.offsetHeight;
					for (var a = r.isBorderBox = !(!h || !i[h] || "border-box" !== i[h]), d = 0, u = s.length; u > d; d++) {
						var f = s[d],
							v = i[f];
						v = l(t, v);
						var y = parseFloat(v);
						r[f] = isNaN(y) ? 0 : y
					}
					var g = r.paddingLeft + r.paddingRight,
						m = r.paddingTop + r.paddingBottom,
						b = r.marginLeft + r.marginRight,
						S = r.marginTop + r.marginBottom,
						x = r.borderLeftWidth + r.borderRightWidth,
						w = r.borderTopWidth + r.borderBottomWidth,
						C = a && p,
						E = e(i.width);
					E !== !1 && (r.width = E + (C ? 0 : g + x));
					var P = e(i.height);
					return P !== !1 && (r.height = P + (C ? 0 : m + w)), r.innerWidth = r.width - (g + x), r.innerHeight = r.height - (m + w), r.outerWidth = r.width + b, r.outerHeight = r.height + S, r
				}
			}

			function l(e, i) {
				if (t.getComputedStyle || -1 === i.indexOf("%")) return i;
				var n = e.style,
					o = n.left,
					r = e.runtimeStyle,
					s = r && r.left;
				return s && (r.left = e.currentStyle.left), n.left = i, i = n.pixelLeft, n.left = o, s && (r.left = s), i
			}
			var c, h, p, d = !1;
			return a
		}
		var r = "undefined" == typeof console ? i : function (t) {
				console.error(t)
			},
			s = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
		"function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], o) : "object" == typeof exports ? module.exports = o(require("desandro-get-style-property")) : t.getSize = o(t.getStyleProperty)
	}(window),
	function (t) {
		function e(t) {
			"function" == typeof t && (e.isReady ? t() : s.push(t))
		}

		function i(t) {
			var i = "readystatechange" === t.type && "complete" !== r.readyState;
			e.isReady || i || n()
		}

		function n() {
			e.isReady = !0;
			for (var t = 0, i = s.length; i > t; t++) {
				var n = s[t];
				n()
			}
		}

		function o(o) {
			return "complete" === r.readyState ? n() : (o.bind(r, "DOMContentLoaded", i), o.bind(r, "readystatechange", i), o.bind(t, "load", i)), e
		}
		var r = t.document,
			s = [];
		e.isReady = !1, "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], o) : "object" == typeof exports ? module.exports = o(require("eventie")) : t.docReady = o(t.eventie)
	}(window),
	function (t) {
		function e(t, e) {
			return t[s](e)
		}

		function i(t) {
			if (!t.parentNode) {
				var e = document.createDocumentFragment();
				e.appendChild(t)
			}
		}

		function n(t, e) {
			i(t);
			for (var n = t.parentNode.querySelectorAll(e), o = 0, r = n.length; r > o; o++)
				if (n[o] === t) return !0;
			return !1
		}

		function o(t, n) {
			return i(t), e(t, n)
		}
		var r, s = function () {
			if (t.matches) return "matches";
			if (t.matchesSelector) return "matchesSelector";
			for (var e = ["webkit", "moz", "ms", "o"], i = 0, n = e.length; n > i; i++) {
				var o = e[i],
					r = o + "MatchesSelector";
				if (t[r]) return r
			}
		}();
		if (s) {
			var a = document.createElement("div"),
				l = e(a, "div");
			r = l ? e : o
		} else r = n;
		"function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function () {
			return r
		}) : "object" == typeof exports ? module.exports = r : window.matchesSelector = r
	}(Element.prototype),
	function (t, e) {
		"function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("doc-ready"), require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.docReady, t.matchesSelector)
	}(window, function (t, e, i) {
		var n = {};
		n.extend = function (t, e) {
			for (var i in e) t[i] = e[i];
			return t
		}, n.modulo = function (t, e) {
			return (t % e + e) % e
		};
		var o = Object.prototype.toString;
		n.isArray = function (t) {
			return "[object Array]" == o.call(t)
		}, n.makeArray = function (t) {
			var e = [];
			if (n.isArray(t)) e = t;
			else if (t && "number" == typeof t.length)
				for (var i = 0, o = t.length; o > i; i++) e.push(t[i]);
			else e.push(t);
			return e
		}, n.indexOf = Array.prototype.indexOf ? function (t, e) {
			return t.indexOf(e)
		} : function (t, e) {
			for (var i = 0, n = t.length; n > i; i++)
				if (t[i] === e) return i;
			return -1
		}, n.removeFrom = function (t, e) {
			var i = n.indexOf(t, e); - 1 != i && t.splice(i, 1)
		}, n.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function (t) {
			return t instanceof HTMLElement
		} : function (t) {
			return t && "object" == typeof t && 1 == t.nodeType && "string" == typeof t.nodeName
		}, n.setText = function () {
			function t(t, i) {
				e = e || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText"), t[e] = i
			}
			var e;
			return t
		}(), n.getParent = function (t, e) {
			for (; t != document.body;)
				if (t = t.parentNode, i(t, e)) return t
		}, n.getQueryElement = function (t) {
			return "string" == typeof t ? document.querySelector(t) : t
		}, n.handleEvent = function (t) {
			var e = "on" + t.type;
			this[e] && this[e](t)
		}, n.filterFindElements = function (t, e) {
			t = n.makeArray(t);
			for (var o = [], r = 0, s = t.length; s > r; r++) {
				var a = t[r];
				if (n.isElement(a))
					if (e) {
						i(a, e) && o.push(a);
						for (var l = a.querySelectorAll(e), c = 0, h = l.length; h > c; c++) o.push(l[c])
					} else o.push(a)
			}
			return o
		}, n.debounceMethod = function (t, e, i) {
			var n = t.prototype[e],
				o = e + "Timeout";
			t.prototype[e] = function () {
				var t = this[o];
				t && clearTimeout(t);
				var e = arguments,
					r = this;
				this[o] = setTimeout(function () {
					n.apply(r, e), delete r[o]
				}, i || 100)
			}
		}, n.toDashed = function (t) {
			return t.replace(/(.)([A-Z])/g, function (t, e, i) {
				return e + "-" + i
			}).toLowerCase()
		};
		var r = t.console;
		return n.htmlInit = function (i, o) {
			e(function () {
				for (var e = n.toDashed(o), s = document.querySelectorAll(".js-" + e), a = "data-" + e + "-options", l = 0, c = s.length; c > l; l++) {
					var h, p = s[l],
						d = p.getAttribute(a);
					try {
						h = d && JSON.parse(d)
					} catch (u) {
						r && r.error("Error parsing " + a + " on " + p.nodeName.toLowerCase() + (p.id ? "#" + p.id : "") + ": " + u);
						continue
					}
					var f = new i(p, h),
						v = t.jQuery;
					v && v.data(p, o, f)
				}
			})
		}, n
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/cell", ["get-size/get-size"], function (i) {
			return e(t, i)
		}) : "object" == typeof exports ? module.exports = e(t, require("get-size")) : (t.Flickity = t.Flickity || {}, t.Flickity.Cell = e(t, t.getSize))
	}(window, function (t, e) {
		function i(t, e) {
			this.element = t, this.parent = e, this.create()
		}
		var n = "attachEvent" in t;
		return i.prototype.create = function () {
			this.element.style.position = "absolute", n && this.element.setAttribute("unselectable", "on"), this.x = 0, this.shift = 0
		}, i.prototype.destroy = function () {
			this.element.style.position = "";
			var t = this.parent.originSide;
			this.element.style[t] = ""
		}, i.prototype.getSize = function () {
			this.size = e(this.element)
		}, i.prototype.setPosition = function (t) {
			this.x = t, this.setDefaultTarget(), this.renderPosition(t)
		}, i.prototype.setDefaultTarget = function () {
			var t = "left" == this.parent.originSide ? "marginLeft" : "marginRight";
			this.target = this.x + this.size[t] + this.size.width * this.parent.cellAlign
		}, i.prototype.renderPosition = function (t) {
			var e = this.parent.originSide;
			this.element.style[e] = this.parent.getPositionValue(t)
		}, i.prototype.wrapShift = function (t) {
			this.shift = t, this.renderPosition(this.x + this.parent.slideableWidth * t)
		}, i.prototype.remove = function () {
			this.element.parentNode.removeChild(this.element)
		}, i
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/animate", ["get-style-property/get-style-property", "fizzy-ui-utils/utils"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("desandro-get-style-property"), require("fizzy-ui-utils")) : (t.Flickity = t.Flickity || {}, t.Flickity.animatePrototype = e(t, t.getStyleProperty, t.fizzyUIUtils))
	}(window, function (t, e, i) {
		for (var n, o = 0, r = "webkit moz ms o".split(" "), s = t.requestAnimationFrame, a = t.cancelAnimationFrame, l = 0; l < r.length && (!s || !a); l++) n = r[l], s = s || t[n + "RequestAnimationFrame"], a = a || t[n + "CancelAnimationFrame"] || t[n + "CancelRequestAnimationFrame"];
		s && a || (s = function (e) {
			var i = (new Date).getTime(),
				n = Math.max(0, 16 - (i - o)),
				r = t.setTimeout(function () {
					e(i + n)
				}, n);
			return o = i + n, r
		}, a = function (e) {
			t.clearTimeout(e)
		});
		var c = {};
		c.startAnimation = function () {
			this.isAnimating || (this.isAnimating = !0, this.restingFrames = 0, this.animate())
		}, c.animate = function () {
			this.applyDragForce(), this.applySelectedAttraction();
			var t = this.x;
			if (this.integratePhysics(), this.positionSlider(), this.settle(t), this.isAnimating) {
				var e = this;
				s(function () {
					e.animate()
				})
			}
		};
		var h = e("transform"),
			p = !!e("perspective");
		return c.positionSlider = function () {
			var t = this.x;
			this.options.wrapAround && this.cells.length > 1 && (t = i.modulo(t, this.slideableWidth), t -= this.slideableWidth, this.shiftWrapCells(t)), t += this.cursorPosition, t = this.options.rightToLeft && h ? -t : t;
			var e = this.getPositionValue(t);
			h ? this.slider.style[h] = p && this.isAnimating ? "translate3d(" + e + ",0,0)" : "translateX(" + e + ")" : this.slider.style[this.originSide] = e
		}, c.positionSliderAtSelected = function () {
			if (this.cells.length) {
				var t = this.cells[this.selectedIndex];
				this.x = -t.target, this.positionSlider()
			}
		}, c.getPositionValue = function (t) {
			return this.options.percentPosition ? .01 * Math.round(t / this.size.innerWidth * 1e4) + "%" : Math.round(t) + "px"
		}, c.settle = function (t) {
			this.isPointerDown || Math.round(100 * this.x) != Math.round(100 * t) || this.restingFrames++, this.restingFrames > 2 && (this.isAnimating = !1, delete this.isFreeScrolling, p && this.positionSlider(), this.dispatchEvent("settle"))
		}, c.shiftWrapCells = function (t) {
			var e = this.cursorPosition + t;
			this._shiftCells(this.beforeShiftCells, e, -1);
			var i = this.size.innerWidth - (t + this.slideableWidth + this.cursorPosition);
			this._shiftCells(this.afterShiftCells, i, 1)
		}, c._shiftCells = function (t, e, i) {
			for (var n = 0, o = t.length; o > n; n++) {
				var r = t[n],
					s = e > 0 ? i : 0;
				r.wrapShift(s), e -= r.size.outerWidth
			}
		}, c._unshiftCells = function (t) {
			if (t && t.length)
				for (var e = 0, i = t.length; i > e; e++) t[e].wrapShift(0)
		}, c.integratePhysics = function () {
			this.velocity += this.accel, this.x += this.velocity, this.velocity *= this.getFrictionFactor(), this.accel = 0
		}, c.applyForce = function (t) {
			this.accel += t
		}, c.getFrictionFactor = function () {
			return 1 - this.options[this.isFreeScrolling ? "freeScrollFriction" : "friction"]
		}, c.getRestingPosition = function () {
			return this.x + this.velocity / (1 - this.getFrictionFactor())
		}, c.applyDragForce = function () {
			if (this.isPointerDown) {
				var t = this.dragX - this.x,
					e = t - this.velocity;
				this.applyForce(e)
			}
		}, c.applySelectedAttraction = function () {
			var t = this.cells.length;
			if (!this.isPointerDown && !this.isFreeScrolling && t) {
				var e = this.cells[this.selectedIndex],
					i = this.options.wrapAround && t > 1 ? this.slideableWidth * Math.floor(this.selectedIndex / t) : 0,
					n = -1 * (e.target + i) - this.x,
					o = n * this.options.selectedAttraction;
				this.applyForce(o)
			}
		}, c
	}),
	function (t, e) {
		if ("function" == typeof define && define.amd) define("flickity/js/flickity", ["classie/classie", "eventEmitter/EventEmitter", "eventie/eventie", "get-size/get-size", "fizzy-ui-utils/utils", "./cell", "./animate"], function (i, n, o, r, s, a, l) {
			return e(t, i, n, o, r, s, a, l)
		});
		else if ("object" == typeof exports) module.exports = e(t, require("desandro-classie"), require("wolfy87-eventemitter"), require("eventie"), require("get-size"), require("fizzy-ui-utils"), require("./cell"), require("./animate"));
		else {
			var i = t.Flickity;
			t.Flickity = e(t, t.classie, t.EventEmitter, t.eventie, t.getSize, t.fizzyUIUtils, i.Cell, i.animatePrototype)
		}
	}(window, function (t, e, i, n, o, r, s, a) {
		function l(t, e) {
			for (t = r.makeArray(t); t.length;) e.appendChild(t.shift())
		}

		function c(t, e) {
			var i = r.getQueryElement(t);
			return i ? (this.element = i, h && (this.$element = h(this.element)), this.options = r.extend({}, this.constructor.defaults), this.option(e), void this._create()) : void(d && d.error("Bad element for Flickity: " + (i || t)))
		}
		var h = t.jQuery,
			p = t.getComputedStyle,
			d = t.console,
			u = 0,
			f = {};
		c.defaults = {
			accessibility: !0,
			cellAlign: "center",
			freeScrollFriction: .075,
			friction: .28,
			percentPosition: !0,
			resize: !0,
			selectedAttraction: .025,
			setGallerySize: !0
		}, c.createMethods = [], r.extend(c.prototype, i.prototype), c.prototype._create = function () {
			var e = this.guid = ++u;
			this.element.flickityGUID = e, f[e] = this, this.selectedIndex = this.options.initialIndex || 0, this.restingFrames = 0, this.x = 0, this.velocity = 0, this.accel = 0, this.originSide = this.options.rightToLeft ? "right" : "left", this.viewport = document.createElement("div"), this.viewport.className = "flickity-viewport", c.setUnselectable(this.viewport), this._createSlider(), (this.options.resize || this.options.watchCSS) && (n.bind(t, "resize", this), this.isResizeBound = !0);
			for (var i = 0, o = c.createMethods.length; o > i; i++) {
				var r = c.createMethods[i];
				this[r]()
			}
			this.options.watchCSS ? this.watchCSS() : this.activate()
		}, c.prototype.option = function (t) {
			r.extend(this.options, t)
		}, c.prototype.activate = function () {
			if (!this.isActive) {
				this.isActive = !0, e.add(this.element, "flickity-enabled"), this.options.rightToLeft && e.add(this.element, "flickity-rtl"), this.getSize();
				var t = this._filterFindCellElements(this.element.children);
				l(t, this.slider), this.viewport.appendChild(this.slider), this.element.appendChild(this.viewport), this.reloadCells(), this.options.accessibility && (this.element.tabIndex = 0, n.bind(this.element, "keydown", this)), this.emit("activate"), this.positionSliderAtSelected(), this.select(this.selectedIndex)
			}
		}, c.prototype._createSlider = function () {
			var t = document.createElement("div");
			t.className = "flickity-slider", t.style[this.originSide] = 0, this.slider = t
		}, c.prototype._filterFindCellElements = function (t) {
			return r.filterFindElements(t, this.options.cellSelector)
		}, c.prototype.reloadCells = function () {
			this.cells = this._makeCells(this.slider.children), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize()
		}, c.prototype._makeCells = function (t) {
			for (var e = this._filterFindCellElements(t), i = [], n = 0, o = e.length; o > n; n++) {
				var r = e[n],
					a = new s(r, this);
				i.push(a)
			}
			return i
		}, c.prototype.getLastCell = function () {
			return this.cells[this.cells.length - 1]
		}, c.prototype.positionCells = function () {
			this._sizeCells(this.cells), this._positionCells(0)
		}, c.prototype._positionCells = function (t) {
			t = t || 0, this.maxCellHeight = t ? this.maxCellHeight || 0 : 0;
			var e = 0;
			if (t > 0) {
				var i = this.cells[t - 1];
				e = i.x + i.size.outerWidth
			}
			for (var n, o = this.cells.length, r = t; o > r; r++) n = this.cells[r], n.setPosition(e), e += n.size.outerWidth, this.maxCellHeight = Math.max(n.size.outerHeight, this.maxCellHeight);
			this.slideableWidth = e, this._containCells()
		}, c.prototype._sizeCells = function (t) {
			for (var e = 0, i = t.length; i > e; e++) {
				var n = t[e];
				n.getSize()
			}
		}, c.prototype._init = c.prototype.reposition = function () {
			this.positionCells(), this.positionSliderAtSelected()
		}, c.prototype.getSize = function () {
			this.size = o(this.element), this.setCellAlign(), this.cursorPosition = this.size.innerWidth * this.cellAlign
		};
		var v = {
			center: {
				left: .5,
				right: .5
			},
			left: {
				left: 0,
				right: 1
			},
			right: {
				right: 0,
				left: 1
			}
		};
		c.prototype.setCellAlign = function () {
			var t = v[this.options.cellAlign];
			this.cellAlign = t ? t[this.originSide] : this.options.cellAlign
		}, c.prototype.setGallerySize = function () {
			this.options.setGallerySize && (this.viewport.style.height = this.maxCellHeight + "px")
		}, c.prototype._getWrapShiftCells = function () {
			if (this.options.wrapAround) {
				this._unshiftCells(this.beforeShiftCells), this._unshiftCells(this.afterShiftCells);
				var t = this.cursorPosition,
					e = this.cells.length - 1;
				this.beforeShiftCells = this._getGapCells(t, e, -1), t = this.size.innerWidth - this.cursorPosition, this.afterShiftCells = this._getGapCells(t, 0, 1)
			}
		}, c.prototype._getGapCells = function (t, e, i) {
			for (var n = []; t > 0;) {
				var o = this.cells[e];
				if (!o) break;
				n.push(o), e += i, t -= o.size.outerWidth
			}
			return n
		}, c.prototype._containCells = function () {
			if (this.options.contain && !this.options.wrapAround && this.cells.length)
				for (var t = this.options.rightToLeft ? "marginRight" : "marginLeft", e = this.options.rightToLeft ? "marginLeft" : "marginRight", i = this.cells[0].size[t], n = this.getLastCell(), o = this.slideableWidth - n.size[e], r = o - this.size.innerWidth * (1 - this.cellAlign), s = o < this.size.innerWidth, a = 0, l = this.cells.length; l > a; a++) {
					var c = this.cells[a];
					c.setDefaultTarget(), s ? c.target = o * this.cellAlign : (c.target = Math.max(c.target, this.cursorPosition + i), c.target = Math.min(c.target, r))
				}
		}, c.prototype.dispatchEvent = function (t, e, i) {
			var n = [e].concat(i);
			if (this.emitEvent(t, n), h && this.$element)
				if (e) {
					var o = h.Event(e);
					o.type = t, this.$element.trigger(o, i)
				} else this.$element.trigger(t, i)
		}, c.prototype.select = function (t, e) {
			if (this.isActive) {
				var i = this.cells.length;
				this.options.wrapAround && i > 1 && (0 > t ? this.x -= this.slideableWidth : t >= i && (this.x += this.slideableWidth)), (this.options.wrapAround || e) && (t = r.modulo(t, i)), this.cells[t] && (this.selectedIndex = t, this.setSelectedCell(), this.startAnimation(), this.dispatchEvent("cellSelect"))
			}
		}, c.prototype.previous = function (t) {
			this.select(this.selectedIndex - 1, t)
		}, c.prototype.next = function (t) {
			this.select(this.selectedIndex + 1, t)
		}, c.prototype.setSelectedCell = function () {
			this._removeSelectedCellClass(), this.selectedCell = this.cells[this.selectedIndex], this.selectedElement = this.selectedCell.element, e.add(this.selectedElement, "is-selected")
		}, c.prototype._removeSelectedCellClass = function () {
			this.selectedCell && e.remove(this.selectedCell.element, "is-selected")
		}, c.prototype.getCell = function (t) {
			for (var e = 0, i = this.cells.length; i > e; e++) {
				var n = this.cells[e];
				if (n.element == t) return n
			}
		}, c.prototype.getCells = function (t) {
			t = r.makeArray(t);
			for (var e = [], i = 0, n = t.length; n > i; i++) {
				var o = t[i],
					s = this.getCell(o);
				s && e.push(s)
			}
			return e
		}, c.prototype.getCellElements = function () {
			for (var t = [], e = 0, i = this.cells.length; i > e; e++) t.push(this.cells[e].element);
			return t
		}, c.prototype.getParentCell = function (t) {
			var e = this.getCell(t);
			return e ? e : (t = r.getParent(t, ".flickity-slider > *"), this.getCell(t))
		}, c.prototype.getAdjacentCellElements = function (t, e) {
			if (!t) return [this.selectedElement];
			e = void 0 === e ? this.selectedIndex : e;
			var i = this.cells.length;
			if (1 + 2 * t >= i) return this.getCellElements();
			for (var n = [], o = e - t; e + t >= o; o++) {
				var s = this.options.wrapAround ? r.modulo(o, i) : o,
					a = this.cells[s];
				a && n.push(a.element)
			}
			return n
		}, c.prototype.uiChange = function () {
			this.emit("uiChange")
		}, c.prototype.childUIPointerDown = function (t) {
			this.emitEvent("childUIPointerDown", [t])
		}, c.prototype.onresize = function () {
			this.watchCSS(), this.resize()
		}, r.debounceMethod(c, "onresize", 150), c.prototype.resize = function () {
			this.isActive && (this.getSize(), this.options.wrapAround && (this.x = r.modulo(this.x, this.slideableWidth)), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize(), this.positionSliderAtSelected())
		};
		var y = c.supportsConditionalCSS = function () {
			var t;
			return function () {
				if (void 0 !== t) return t;
				if (!p) return void(t = !1);
				var e = document.createElement("style"),
					i = document.createTextNode('body:after { content: "foo"; display: none; }');
				e.appendChild(i), document.head.appendChild(e);
				var n = p(document.body, ":after").content;
				return t = -1 != n.indexOf("foo"), document.head.removeChild(e), t
			}
		}();
		c.prototype.watchCSS = function () {
			var t = this.options.watchCSS;
			if (t) {
				var e = y();
				if (!e) {
					var i = "fallbackOn" == t ? "activate" : "deactivate";
					return void this[i]()
				}
				var n = p(this.element, ":after").content; - 1 != n.indexOf("flickity") ? this.activate() : this.deactivate()
			}
		}, c.prototype.onkeydown = function (t) {
			if (this.options.accessibility && (!document.activeElement || document.activeElement == this.element))
				if (37 == t.keyCode) {
					var e = this.options.rightToLeft ? "next" : "previous";
					this.uiChange(), this[e]()
				} else if (39 == t.keyCode) {
				var i = this.options.rightToLeft ? "previous" : "next";
				this.uiChange(), this[i]()
			}
		}, c.prototype.deactivate = function () {
			if (this.isActive) {
				e.remove(this.element, "flickity-enabled"), e.remove(this.element, "flickity-rtl");
				for (var t = 0, i = this.cells.length; i > t; t++) {
					var o = this.cells[t];
					o.destroy()
				}
				this._removeSelectedCellClass(), this.element.removeChild(this.viewport), l(this.slider.children, this.element), this.options.accessibility && (this.element.removeAttribute("tabIndex"), n.unbind(this.element, "keydown", this)), this.isActive = !1, this.emit("deactivate")
			}
		}, c.prototype.destroy = function () {
			this.deactivate(), this.isResizeBound && n.unbind(t, "resize", this), this.emit("destroy"), h && this.$element && h.removeData(this.element, "flickity"), delete this.element.flickityGUID, delete f[this.guid]
		}, r.extend(c.prototype, a);
		var g = "attachEvent" in t;
		return c.setUnselectable = function (t) {
			g && t.setAttribute("unselectable", "on")
		}, c.data = function (t) {
			t = r.getQueryElement(t);
			var e = t && t.flickityGUID;
			return e && f[e]
		}, r.htmlInit(c, "flickity"), h && h.bridget && h.bridget("flickity", c), c.Cell = s, c
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("unipointer/unipointer", ["eventEmitter/EventEmitter", "eventie/eventie"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("wolfy87-eventemitter"), require("eventie")) : t.Unipointer = e(t, t.EventEmitter, t.eventie)
	}(window, function (t, e, i) {
		function n() {}

		function o() {}
		o.prototype = new e, o.prototype.bindStartEvent = function (t) {
			this._bindStartEvent(t, !0)
		}, o.prototype.unbindStartEvent = function (t) {
			this._bindStartEvent(t, !1)
		}, o.prototype._bindStartEvent = function (e, n) {
			n = void 0 === n ? !0 : !!n;
			var o = n ? "bind" : "unbind";
			t.navigator.pointerEnabled ? i[o](e, "pointerdown", this) : t.navigator.msPointerEnabled ? i[o](e, "MSPointerDown", this) : (i[o](e, "mousedown", this), i[o](e, "touchstart", this))
		}, o.prototype.handleEvent = function (t) {
			var e = "on" + t.type;
			this[e] && this[e](t)
		}, o.prototype.getTouch = function (t) {
			for (var e = 0, i = t.length; i > e; e++) {
				var n = t[e];
				if (n.identifier == this.pointerIdentifier) return n
			}
		}, o.prototype.onmousedown = function (t) {
			var e = t.button;
			e && 0 !== e && 1 !== e || this._pointerDown(t, t)
		}, o.prototype.ontouchstart = function (t) {
			this._pointerDown(t, t.changedTouches[0])
		}, o.prototype.onMSPointerDown = o.prototype.onpointerdown = function (t) {
			this._pointerDown(t, t)
		}, o.prototype._pointerDown = function (t, e) {
			this.isPointerDown || (this.isPointerDown = !0, this.pointerIdentifier = void 0 !== e.pointerId ? e.pointerId : e.identifier, this.pointerDown(t, e))
		}, o.prototype.pointerDown = function (t, e) {
			this._bindPostStartEvents(t), this.emitEvent("pointerDown", [t, e])
		};
		var r = {
			mousedown: ["mousemove", "mouseup"],
			touchstart: ["touchmove", "touchend", "touchcancel"],
			pointerdown: ["pointermove", "pointerup", "pointercancel"],
			MSPointerDown: ["MSPointerMove", "MSPointerUp", "MSPointerCancel"]
		};
		return o.prototype._bindPostStartEvents = function (e) {
			if (e) {
				for (var n = r[e.type], o = e.preventDefault ? t : document, s = 0, a = n.length; a > s; s++) {
					var l = n[s];
					i.bind(o, l, this)
				}
				this._boundPointerEvents = {
					events: n,
					node: o
				}
			}
		}, o.prototype._unbindPostStartEvents = function () {
			var t = this._boundPointerEvents;
			if (t && t.events) {
				for (var e = 0, n = t.events.length; n > e; e++) {
					var o = t.events[e];
					i.unbind(t.node, o, this)
				}
				delete this._boundPointerEvents
			}
		}, o.prototype.onmousemove = function (t) {
			this._pointerMove(t, t)
		}, o.prototype.onMSPointerMove = o.prototype.onpointermove = function (t) {
			t.pointerId == this.pointerIdentifier && this._pointerMove(t, t)
		}, o.prototype.ontouchmove = function (t) {
			var e = this.getTouch(t.changedTouches);
			e && this._pointerMove(t, e)
		}, o.prototype._pointerMove = function (t, e) {
			this.pointerMove(t, e)
		}, o.prototype.pointerMove = function (t, e) {
			this.emitEvent("pointerMove", [t, e])
		}, o.prototype.onmouseup = function (t) {
			this._pointerUp(t, t)
		}, o.prototype.onMSPointerUp = o.prototype.onpointerup = function (t) {
			t.pointerId == this.pointerIdentifier && this._pointerUp(t, t)
		}, o.prototype.ontouchend = function (t) {
			var e = this.getTouch(t.changedTouches);
			e && this._pointerUp(t, e)
		}, o.prototype._pointerUp = function (t, e) {
			this._pointerDone(), this.pointerUp(t, e)
		}, o.prototype.pointerUp = function (t, e) {
			this.emitEvent("pointerUp", [t, e])
		}, o.prototype._pointerDone = function () {
			this.isPointerDown = !1, delete this.pointerIdentifier, this._unbindPostStartEvents(), this.pointerDone()
		}, o.prototype.pointerDone = n, o.prototype.onMSPointerCancel = o.prototype.onpointercancel = function (t) {
			t.pointerId == this.pointerIdentifier && this._pointerCancel(t, t)
		}, o.prototype.ontouchcancel = function (t) {
			var e = this.getTouch(t.changedTouches);
			e && this._pointerCancel(t, e)
		}, o.prototype._pointerCancel = function (t, e) {
			this._pointerDone(), this.pointerCancel(t, e)
		}, o.prototype.pointerCancel = function (t, e) {
			this.emitEvent("pointerCancel", [t, e])
		}, o.getPointerPoint = function (t) {
			return {
				x: void 0 !== t.pageX ? t.pageX : t.clientX,
				y: void 0 !== t.pageY ? t.pageY : t.clientY
			}
		}, o
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("unidragger/unidragger", ["eventie/eventie", "unipointer/unipointer"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("eventie"), require("unipointer")) : t.Unidragger = e(t, t.eventie, t.Unipointer)
	}(window, function (t, e, i) {
		function n() {}

		function o(t) {
			t.preventDefault ? t.preventDefault() : t.returnValue = !1
		}

		function r() {}

		function s() {
			return !1
		}
		r.prototype = new i, r.prototype.bindHandles = function () {
			this._bindHandles(!0)
		}, r.prototype.unbindHandles = function () {
			this._bindHandles(!1)
		};
		var a = t.navigator;
		r.prototype._bindHandles = function (t) {
			t = void 0 === t ? !0 : !!t;
			var i;
			i = a.pointerEnabled ? function (e) {
				e.style.touchAction = t ? "none" : ""
			} : a.msPointerEnabled ? function (e) {
				e.style.msTouchAction = t ? "none" : ""
			} : function () {
				t && c(s)
			};
			for (var n = t ? "bind" : "unbind", o = 0, r = this.handles.length; r > o; o++) {
				var s = this.handles[o];
				this._bindStartEvent(s, t), i(s), e[n](s, "click", this)
			}
		};
		var l = "attachEvent" in document.documentElement,
			c = l ? function (t) {
				"IMG" == t.nodeName && (t.ondragstart = s);
				for (var e = t.querySelectorAll("img"), i = 0, n = e.length; n > i; i++) {
					var o = e[i];
					o.ondragstart = s
				}
			} : n;
		r.prototype.pointerDown = function (i, n) {
			if ("INPUT" == i.target.nodeName && "range" == i.target.type) return this.isPointerDown = !1, void delete this.pointerIdentifier;
			this._dragPointerDown(i, n);
			var o = document.activeElement;
			o && o.blur && o.blur(), this._bindPostStartEvents(i), this.pointerDownScroll = r.getScrollPosition(), e.bind(t, "scroll", this), this.emitEvent("pointerDown", [i, n])
		}, r.prototype._dragPointerDown = function (t, e) {
			this.pointerDownPoint = i.getPointerPoint(e);
			var n = "touchstart" == t.type,
				r = t.target.nodeName;
			n || "SELECT" == r || o(t)
		}, r.prototype.pointerMove = function (t, e) {
			var i = this._dragPointerMove(t, e);
			this.emitEvent("pointerMove", [t, e, i]), this._dragMove(t, e, i)
		}, r.prototype._dragPointerMove = function (t, e) {
			var n = i.getPointerPoint(e),
				o = {
					x: n.x - this.pointerDownPoint.x,
					y: n.y - this.pointerDownPoint.y
				};
			return !this.isDragging && this.hasDragStarted(o) && this._dragStart(t, e), o
		}, r.prototype.hasDragStarted = function (t) {
			return Math.abs(t.x) > 3 || Math.abs(t.y) > 3
		}, r.prototype.pointerUp = function (t, e) {
			this.emitEvent("pointerUp", [t, e]), this._dragPointerUp(t, e)
		}, r.prototype._dragPointerUp = function (t, e) {
			this.isDragging ? this._dragEnd(t, e) : this._staticClick(t, e)
		}, i.prototype.pointerDone = function () {
			e.unbind(t, "scroll", this)
		}, r.prototype._dragStart = function (t, e) {
			this.isDragging = !0, this.dragStartPoint = r.getPointerPoint(e), this.isPreventingClicks = !0, this.dragStart(t, e)
		}, r.prototype.dragStart = function (t, e) {
			this.emitEvent("dragStart", [t, e])
		}, r.prototype._dragMove = function (t, e, i) {
			this.isDragging && this.dragMove(t, e, i)
		}, r.prototype.dragMove = function (t, e, i) {
			o(t), this.emitEvent("dragMove", [t, e, i])
		}, r.prototype._dragEnd = function (t, e) {
			this.isDragging = !1;
			var i = this;
			setTimeout(function () {
				delete i.isPreventingClicks
			}), this.dragEnd(t, e)
		}, r.prototype.dragEnd = function (t, e) {
			this.emitEvent("dragEnd", [t, e])
		}, r.prototype.pointerDone = function () {
			e.unbind(t, "scroll", this), delete this.pointerDownScroll
		}, r.prototype.onclick = function (t) {
			this.isPreventingClicks && o(t)
		}, r.prototype._staticClick = function (t, e) {
			if (!this.isIgnoringMouseUp || "mouseup" != t.type) {
				var i = t.target.nodeName;
				if (("INPUT" == i || "TEXTAREA" == i) && t.target.focus(), this.staticClick(t, e), "mouseup" != t.type) {
					this.isIgnoringMouseUp = !0;
					var n = this;
					setTimeout(function () {
						delete n.isIgnoringMouseUp
					}, 400)
				}
			}
		}, r.prototype.staticClick = function (t, e) {
			this.emitEvent("staticClick", [t, e])
		}, r.prototype.onscroll = function () {
			var t = r.getScrollPosition(),
				e = this.pointerDownScroll.x - t.x,
				i = this.pointerDownScroll.y - t.y;
			(Math.abs(e) > 3 || Math.abs(i) > 3) && this._pointerDone()
		}, r.getPointerPoint = function (t) {
			return {
				x: void 0 !== t.pageX ? t.pageX : t.clientX,
				y: void 0 !== t.pageY ? t.pageY : t.clientY
			}
		};
		var h = void 0 !== t.pageYOffset;
		return r.getScrollPosition = function () {
			return {
				x: h ? t.pageXOffset : document.body.scrollLeft,
				y: h ? t.pageYOffset : document.body.scrollTop
			}
		}, r.getPointerPoint = i.getPointerPoint, r
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/drag", ["classie/classie", "eventie/eventie", "./flickity", "unidragger/unidragger", "fizzy-ui-utils/utils"], function (i, n, o, r, s) {
			return e(t, i, n, o, r, s)
		}) : "object" == typeof exports ? module.exports = e(t, require("desandro-classie"), require("eventie"), require("./flickity"), require("unidragger"), require("fizzy-ui-utils")) : t.Flickity = e(t, t.classie, t.eventie, t.Flickity, t.Unidragger, t.fizzyUIUtils)
	}(window, function (t, e, i, n, o, r) {
		function s(t) {
			t.preventDefault ? t.preventDefault() : t.returnValue = !1
		}

		function a(e) {
			var i = o.getPointerPoint(e);
			return i.y - t.pageYOffset
		}
		r.extend(n.defaults, {
			draggable: !0,
			touchVerticalScroll: !0
		}), n.createMethods.push("_createDrag"), r.extend(n.prototype, o.prototype), n.prototype._createDrag = function () {
			this.on("activate", this.bindDrag), this.on("uiChange", this._uiChangeDrag), this.on("childUIPointerDown", this._childUIPointerDownDrag), this.on("deactivate", this.unbindDrag)
		}, n.prototype.bindDrag = function () {
			this.options.draggable && !this.isDragBound && (e.add(this.element, "is-draggable"), this.handles = [this.viewport], this.bindHandles(), this.isDragBound = !0)
		}, n.prototype.unbindDrag = function () {
			this.isDragBound && (e.remove(this.element, "is-draggable"), this.unbindHandles(), delete this.isDragBound)
		}, n.prototype._uiChangeDrag = function () {
			delete this.isFreeScrolling
		}, n.prototype._childUIPointerDownDrag = function (t) {
			s(t), this.pointerDownFocus(t)
		}, n.prototype.pointerDown = function (n, r) {
			if ("INPUT" == n.target.nodeName && "range" == n.target.type) return this.isPointerDown = !1, void delete this.pointerIdentifier;
			this._dragPointerDown(n, r);
			var s = document.activeElement;
			s && s.blur && s != this.element && s != document.body && s.blur(), this.pointerDownFocus(n), this.dragX = this.x, e.add(this.viewport, "is-pointer-down"), this._bindPostStartEvents(n), this.pointerDownScroll = o.getScrollPosition(), i.bind(t, "scroll", this), this.dispatchEvent("pointerDown", n, [r])
		};
		var l = {
				touchstart: !0,
				MSPointerDown: !0
			},
			c = {
				INPUT: !0,
				SELECT: !0
			};
		n.prototype.pointerDownFocus = function (t) {
			!this.options.accessibility || l[t.type] || c[t.target.nodeName] || this.element.focus()
		}, n.prototype.pointerMove = function (t, e) {
			var i = this._dragPointerMove(t, e);
			this.touchVerticalScrollMove(t, e, i), this._dragMove(t, e, i), this.dispatchEvent("pointerMove", t, [e, i])
		}, n.prototype.hasDragStarted = function (t) {
			return !this.isTouchScrolling && Math.abs(t.x) > 3
		}, n.prototype.pointerUp = function (t, i) {
			delete this.isTouchScrolling, e.remove(this.viewport, "is-pointer-down"), this.dispatchEvent("pointerUp", t, [i]), this._dragPointerUp(t, i)
		};
		var h = {
			touchmove: !0,
			MSPointerMove: !0
		};
		return n.prototype.touchVerticalScrollMove = function (e, i, n) {
			var o = this.options.touchVerticalScroll,
				r = "withDrag" == o ? !o : this.isDragging || !o;
			!r && h[e.type] && !this.isTouchScrolling && Math.abs(n.y) > 10 && (this.startScrollY = t.pageYOffset, this.pointerWindowStartY = a(i), this.isTouchScrolling = !0)
		}, n.prototype.dragStart = function (t, e) {
			this.dragStartPosition = this.x, this.startAnimation(), this.dispatchEvent("dragStart", t, [e])
		}, n.prototype.dragMove = function (t, e, i) {
			s(t), this.previousDragX = this.dragX;
			var n = this.options.rightToLeft ? -1 : 1,
				o = this.dragStartPosition + i.x * n;
			if (!this.options.wrapAround && this.cells.length) {
				var r = Math.max(-this.cells[0].target, this.dragStartPosition);
				o = o > r ? .5 * (o + r) : o;
				var a = Math.min(-this.getLastCell().target, this.dragStartPosition);
				o = a > o ? .5 * (o + a) : o
			}
			this.dragX = o, this.dragMoveTime = new Date, this.dispatchEvent("dragMove", t, [e, i])
		}, n.prototype.dragEnd = function (t, e) {
			this.options.freeScroll && (this.isFreeScrolling = !0);
			var i = this.dragEndRestingSelect();
			if (this.options.freeScroll && !this.options.wrapAround) {
				var n = this.getRestingPosition();
				this.isFreeScrolling = -n > this.cells[0].target && -n < this.getLastCell().target
			} else this.options.freeScroll || i != this.selectedIndex || (i += this.dragEndBoostSelect());
			delete this.previousDragX, this.select(i), this.dispatchEvent("dragEnd", t, [e])
		}, n.prototype.dragEndRestingSelect = function () {
			var t = this.getRestingPosition(),
				e = Math.abs(this.getCellDistance(-t, this.selectedIndex)),
				i = this._getClosestResting(t, e, 1),
				n = this._getClosestResting(t, e, -1),
				o = i.distance < n.distance ? i.index : n.index;
			return o
		}, n.prototype._getClosestResting = function (t, e, i) {
			for (var n = this.selectedIndex, o = 1 / 0, r = this.options.contain && !this.options.wrapAround ? function (t, e) {
					return e >= t
				} : function (t, e) {
					return e > t
				}; r(e, o) && (n += i, o = e, e = this.getCellDistance(-t, n), null !== e);) e = Math.abs(e);
			return {
				distance: o,
				index: n - i
			}
		}, n.prototype.getCellDistance = function (t, e) {
			var i = this.cells.length,
				n = this.options.wrapAround && i > 1,
				o = n ? r.modulo(e, i) : e,
				s = this.cells[o];
			if (!s) return null;
			var a = n ? this.slideableWidth * Math.floor(e / i) : 0;
			return t - (s.target + a)
		}, n.prototype.dragEndBoostSelect = function () {
			if (void 0 === this.previousDragX || !this.dragMoveTime || new Date - this.dragMoveTime > 100) return 0;
			var t = this.getCellDistance(-this.dragX, this.selectedIndex),
				e = this.previousDragX - this.dragX;
			return t > 0 && e > 0 ? 1 : 0 > t && 0 > e ? -1 : 0
		}, n.prototype.staticClick = function (t, e) {
			var i = this.getParentCell(t.target),
				n = i && i.element,
				o = i && r.indexOf(this.cells, i);
			this.dispatchEvent("staticClick", t, [e, n, o])
		}, n
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("tap-listener/tap-listener", ["unipointer/unipointer"], function (i) {
			return e(t, i)
		}) : "object" == typeof exports ? module.exports = e(t, require("unipointer")) : t.TapListener = e(t, t.Unipointer)
	}(window, function (t, e) {
		function i(t) {
			t.preventDefault ? t.preventDefault() : t.returnValue = !1
		}

		function n(t) {
			this.bindTap(t)
		}
		n.prototype = new e, n.prototype.bindTap = function (t) {
			t && (this.unbindTap(), this.tapElement = t, this._bindStartEvent(t, !0))
		}, n.prototype.unbindTap = function () {
			this.tapElement && (this._bindStartEvent(this.tapElement, !0), delete this.tapElement)
		};
		var o = n.prototype.pointerDown;
		n.prototype.pointerDown = function (t) {
			"touchstart" == t.type && i(t), o.apply(this, arguments)
		};
		var r = void 0 !== t.pageYOffset;
		return n.prototype.pointerUp = function (i, n) {
			var o = e.getPointerPoint(n),
				s = this.tapElement.getBoundingClientRect(),
				a = r ? t.pageXOffset : document.body.scrollLeft,
				l = r ? t.pageYOffset : document.body.scrollTop,
				c = o.x >= s.left + a && o.x <= s.right + a && o.y >= s.top + l && o.y <= s.bottom + l;
			c && this.emitEvent("tap", [i, n])
		}, n.prototype.destroy = function () {
			this.pointerDone(), this.unbindTap()
		}, n
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/prev-next-button", ["eventie/eventie", "./flickity", "tap-listener/tap-listener", "fizzy-ui-utils/utils"], function (i, n, o, r) {
			return e(t, i, n, o, r)
		}) : "object" == typeof exports ? module.exports = e(t, require("eventie"), require("./flickity"), require("tap-listener"), require("fizzy-ui-utils")) : e(t, t.eventie, t.Flickity, t.TapListener, t.fizzyUIUtils)
	}(window, function (t, e, i, n, o) {
		function r(t, e) {
			this.direction = t, this.parent = e, this._create()
		}

		function s(t) {
			return "string" == typeof t ? t : "M " + t.x0 + ",50 L " + t.x1 + "," + (t.y1 + 50) + " L " + t.x2 + "," + (t.y2 + 50) + " L " + t.x3 + ",50  L " + t.x2 + "," + (50 - t.y2) + " L " + t.x1 + "," + (50 - t.y1) + " Z"
		}
		var a = "http://www.w3.org/2000/svg",
			l = function () {
				function t() {
					if (void 0 !== e) return e;
					var t = document.createElement("div");
					return t.innerHTML = "<svg/>", e = (t.firstChild && t.firstChild.namespaceURI) == a
				}
				var e;
				return t
			}();
		return r.prototype = new n, r.prototype._create = function () {
			this.isEnabled = !0, this.isPrevious = -1 == this.direction;
			var t = this.parent.options.rightToLeft ? 1 : -1;
			this.isLeft = this.direction == t;
			var e = this.element = document.createElement("button");
			if (e.className = "flickity-prev-next-button", e.className += this.isPrevious ? " previous" : " next", e.setAttribute("type", "button"), i.setUnselectable(e), l()) {
				var n = this.createSVG();
				e.appendChild(n)
			} else this.setArrowText(), e.className += " no-svg";
			var o = this;
			this.onCellSelect = function () {
				o.update()
			}, this.parent.on("cellSelect", this.onCellSelect), this.on("tap", this.onTap), this.on("pointerDown", function (t, e) {
				o.parent.childUIPointerDown(e)
			})
		}, r.prototype.activate = function () {
			this.update(), this.bindTap(this.element), e.bind(this.element, "click", this), this.parent.element.appendChild(this.element)
		}, r.prototype.deactivate = function () {
			this.parent.element.removeChild(this.element), n.prototype.destroy.call(this), e.unbind(this.element, "click", this)
		}, r.prototype.createSVG = function () {
			var t = document.createElementNS(a, "svg");
			t.setAttribute("viewBox", "0 0 100 100");
			var e = document.createElementNS(a, "path"),
				i = s(this.parent.options.arrowShape);
			return e.setAttribute("d", i), e.setAttribute("class", "arrow"), this.isLeft || e.setAttribute("transform", "translate(100, 100) rotate(180) "), t.appendChild(e), t
		}, r.prototype.setArrowText = function () {
			var t = this.parent.options,
				e = this.isLeft ? t.leftArrowText : t.rightArrowText;
			o.setText(this.element, e)
		}, r.prototype.onTap = function () {
			if (this.isEnabled) {
				this.parent.uiChange();
				var t = this.isPrevious ? "previous" : "next";
				this.parent[t]()
			}
		}, r.prototype.handleEvent = o.handleEvent, r.prototype.onclick = function () {
			var t = document.activeElement;
			t && t == this.element && this.onTap()
		}, r.prototype.enable = function () {
			this.isEnabled || (this.element.disabled = !1, this.isEnabled = !0)
		}, r.prototype.disable = function () {
			this.isEnabled && (this.element.disabled = !0, this.isEnabled = !1)
		}, r.prototype.update = function () {
			var t = this.parent.cells;
			if (this.parent.options.wrapAround && t.length > 1) return void this.enable();
			var e = t.length ? t.length - 1 : 0,
				i = this.isPrevious ? 0 : e,
				n = this.parent.selectedIndex == i ? "disable" : "enable";
			this[n]()
		}, r.prototype.destroy = function () {
			this.deactivate()
		}, o.extend(i.defaults, {
			prevNextButtons: !0,
			leftArrowText: "â€¹",
			rightArrowText: "â€º",
			arrowShape: {
				x0: 10,
				x1: 60,
				y1: 50,
				x2: 70,
				y2: 40,
				x3: 30
			}
		}), i.createMethods.push("_createPrevNextButtons"), i.prototype._createPrevNextButtons = function () {
			this.options.prevNextButtons && (this.prevButton = new r(-1, this), this.nextButton = new r(1, this), this.on("activate", this.activatePrevNextButtons))
		}, i.prototype.activatePrevNextButtons = function () {
			this.prevButton.activate(), this.nextButton.activate(), this.on("deactivate", this.deactivatePrevNextButtons)
		}, i.prototype.deactivatePrevNextButtons = function () {
			this.prevButton.deactivate(), this.nextButton.deactivate(), this.off("deactivate", this.deactivatePrevNextButtons)
		}, i.PrevNextButton = r, i
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/page-dots", ["eventie/eventie", "./flickity", "tap-listener/tap-listener", "fizzy-ui-utils/utils"], function (i, n, o, r) {
			return e(t, i, n, o, r)
		}) : "object" == typeof exports ? module.exports = e(t, require("eventie"), require("./flickity"), require("tap-listener"), require("fizzy-ui-utils")) : e(t, t.eventie, t.Flickity, t.TapListener, t.fizzyUIUtils)
	}(window, function (t, e, i, n, o) {
		function r(t) {
			this.parent = t, this._create()
		}
		return r.prototype = new n, r.prototype._create = function () {
			this.holder = document.createElement("ol"), this.holder.className = "flickity-page-dots", i.setUnselectable(this.holder), this.dots = [];
			var t = this;
			this.onCellSelect = function () {
				t.updateSelected()
			}, this.parent.on("cellSelect", this.onCellSelect), this.on("tap", this.onTap), this.on("pointerDown", function (e, i) {
				t.parent.childUIPointerDown(i)
			})
		}, r.prototype.activate = function () {
			this.setDots(), this.updateSelected(), this.bindTap(this.holder), this.parent.element.appendChild(this.holder)
		}, r.prototype.deactivate = function () {
			this.parent.element.removeChild(this.holder), n.prototype.destroy.call(this)
		}, r.prototype.setDots = function () {
			var t = this.parent.cells.length - this.dots.length;
			t > 0 ? this.addDots(t) : 0 > t && this.removeDots(-t)
		}, r.prototype.addDots = function (t) {
			for (var e = document.createDocumentFragment(), i = []; t;) {
				var n = document.createElement("li");
				n.className = "dot", e.appendChild(n), i.push(n), t--
			}
			this.holder.appendChild(e), this.dots = this.dots.concat(i)
		}, r.prototype.removeDots = function (t) {
			for (var e = this.dots.splice(this.dots.length - t, t), i = 0, n = e.length; n > i; i++) {
				var o = e[i];
				this.holder.removeChild(o)
			}
		}, r.prototype.updateSelected = function () {
			this.selectedDot && (this.selectedDot.className = "dot"), this.dots.length && (this.selectedDot = this.dots[this.parent.selectedIndex], this.selectedDot.className = "dot is-selected")
		}, r.prototype.onTap = function (t) {
			var e = t.target;
			if ("LI" == e.nodeName) {
				this.parent.uiChange();
				var i = o.indexOf(this.dots, e);
				this.parent.select(i)
			}
		}, r.prototype.destroy = function () {
			this.deactivate()
		}, i.PageDots = r, o.extend(i.defaults, {
			pageDots: !0
		}), i.createMethods.push("_createPageDots"), i.prototype._createPageDots = function () {
			this.options.pageDots && (this.pageDots = new r(this), this.on("activate", this.activatePageDots), this.on("cellAddedRemoved", this.onCellAddedRemovedPageDots), this.on("deactivate", this.deactivatePageDots))
		}, i.prototype.activatePageDots = function () {
			this.pageDots.activate()
		}, i.prototype.onCellAddedRemovedPageDots = function () {
			this.pageDots.setDots()
		}, i.prototype.deactivatePageDots = function () {
			this.pageDots.deactivate()
		}, i.PageDots = r, i
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/player", ["eventEmitter/EventEmitter", "eventie/eventie", "./flickity"], function (t, i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(require("wolfy87-eventemitter"), require("eventie"), require("./flickity")) : e(t.EventEmitter, t.eventie, t.Flickity)
	}(window, function (t, e, i) {
		function n(t) {
			if (this.isPlaying = !1, this.parent = t, r) {
				var e = this;
				this.onVisibilityChange = function () {
					e.visibilityChange()
				}
			}
		}
		var o, r;
		return "hidden" in document ? (o = "hidden", r = "visibilitychange") : "webkitHidden" in document && (o = "webkitHidden", r = "webkitvisibilitychange"), n.prototype = new t, n.prototype.play = function () {
			this.isPlaying = !0, delete this.isPaused, r && document.addEventListener(r, this.onVisibilityChange, !1), this.tick()
		}, n.prototype.tick = function () {
			if (this.isPlaying && !this.isPaused) {
				this.tickTime = new Date;
				var t = this.parent.options.autoPlay;
				t = "number" == typeof t ? t : 3e3;
				var e = this;
				this.timeout = setTimeout(function () {
					e.parent.next(!0), e.tick()
				}, t)
			}
		}, n.prototype.stop = function () {
			this.isPlaying = !1, delete this.isPaused, this.clear(), r && document.removeEventListener(r, this.onVisibilityChange, !1)
		}, n.prototype.clear = function () {
			clearTimeout(this.timeout)
		}, n.prototype.pause = function () {
			this.isPlaying && (this.isPaused = !0, this.clear())
		}, n.prototype.unpause = function () {
			this.isPaused && this.play()
		}, n.prototype.visibilityChange = function () {
			var t = document[o];
			this[t ? "pause" : "unpause"]()
		}, i.createMethods.push("_createPlayer"), i.prototype._createPlayer = function () {
			this.player = new n(this), this.on("activate", this.activatePlayer), this.on("uiChange", this.stopPlayer), this.on("pointerDown", this.stopPlayer), this.on("deactivate", this.deactivatePlayer)
		}, i.prototype.activatePlayer = function () {
			this.options.autoPlay && (this.player.play(), e.bind(this.element, "mouseenter", this), this.isMouseenterBound = !0)
		}, i.prototype.stopPlayer = function () {
			this.player.stop()
		}, i.prototype.deactivatePlayer = function () {
			this.player.stop(), this.isMouseenterBound && (e.unbind(this.element, "mouseenter", this), delete this.isMouseenterBound)
		}, i.prototype.onmouseenter = function () {
			this.player.pause(), e.bind(this.element, "mouseleave", this)
		}, i.prototype.onmouseleave = function () {
			this.player.unpause(), e.unbind(this.element, "mouseleave", this)
		}, i.Player = n, i
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/add-remove-cell", ["./flickity", "fizzy-ui-utils/utils"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("./flickity"), require("fizzy-ui-utils")) : e(t, t.Flickity, t.fizzyUIUtils)
	}(window, function (t, e, i) {
		function n(t) {
			for (var e = document.createDocumentFragment(), i = 0, n = t.length; n > i; i++) {
				var o = t[i];
				e.appendChild(o.element)
			}
			return e
		}
		return e.prototype.insert = function (t, e) {
			var i = this._makeCells(t);
			if (i && i.length) {
				var o = this.cells.length;
				e = void 0 === e ? o : e;
				var r = n(i),
					s = e == o;
				if (s) this.slider.appendChild(r);
				else {
					var a = this.cells[e].element;
					this.slider.insertBefore(r, a)
				}
				if (0 === e) this.cells = i.concat(this.cells);
				else if (s) this.cells = this.cells.concat(i);
				else {
					var l = this.cells.splice(e, o - e);
					this.cells = this.cells.concat(i).concat(l)
				}
				this._sizeCells(i);
				var c = e > this.selectedIndex ? 0 : i.length;
				this._cellAddedRemoved(e, c)
			}
		}, e.prototype.append = function (t) {
			this.insert(t, this.cells.length)
		}, e.prototype.prepend = function (t) {
			this.insert(t, 0)
		}, e.prototype.remove = function (t) {
			var e, n, o, r = this.getCells(t),
				s = 0;
			for (e = 0, n = r.length; n > e; e++) {
				o = r[e];
				var a = i.indexOf(this.cells, o) < this.selectedIndex;
				s -= a ? 1 : 0
			}
			for (e = 0, n = r.length; n > e; e++) o = r[e], o.remove(), i.removeFrom(this.cells, o);
			r.length && this._cellAddedRemoved(0, s)
		}, e.prototype._cellAddedRemoved = function (t, e) {
			e = e || 0, this.selectedIndex += e, this.selectedIndex = Math.max(0, Math.min(this.cells.length - 1, this.selectedIndex)), this.emitEvent("cellAddedRemoved", [t, e]), this.cellChange(t, !0)
		}, e.prototype.cellSizeChange = function (t) {
			var e = this.getCell(t);
			if (e) {
				e.getSize();
				var n = i.indexOf(this.cells, e);
				this.cellChange(n)
			}
		}, e.prototype.cellChange = function (t, e) {
			var i = this.slideableWidth;
			this._positionCells(t), this._getWrapShiftCells(), this.setGallerySize(), this.options.freeScroll ? (this.x += i - this.slideableWidth, this.positionSlider()) : (e && this.positionSliderAtSelected(), this.select(this.selectedIndex))
		}, e
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/lazyload", ["classie/classie", "eventie/eventie", "./flickity", "fizzy-ui-utils/utils"], function (i, n, o, r) {
			return e(t, i, n, o, r)
		}) : "object" == typeof exports ? module.exports = e(t, require("desandro-classie"), require("eventie"), require("./flickity"), require("fizzy-ui-utils")) : e(t, t.classie, t.eventie, t.Flickity, t.fizzyUIUtils)
	}(window, function (t, e, i, n, o) {
		function r(t) {
			if ("IMG" == t.nodeName && t.getAttribute("data-flickity-lazyload")) return [t];
			var e = t.querySelectorAll("img[data-flickity-lazyload]");
			return o.makeArray(e)
		}

		function s(t, e) {
			this.img = t, this.flickity = e, this.load()
		}
		return n.createMethods.push("_createLazyload"), n.prototype._createLazyload = function () {
			this.on("cellSelect", this.lazyLoad)
		}, n.prototype.lazyLoad = function () {
			var t = this.options.lazyLoad;
			if (t) {
				for (var e = "number" == typeof t ? t : 0, i = this.getAdjacentCellElements(e), n = [], o = 0, a = i.length; a > o; o++) {
					var l = i[o],
						c = r(l);
					n = n.concat(c)
				}
				for (o = 0, a = n.length; a > o; o++) {
					var h = n[o];
					new s(h, this)
				}
			}
		}, s.prototype.handleEvent = o.handleEvent, s.prototype.load = function () {
			i.bind(this.img, "load", this), i.bind(this.img, "error", this), this.img.src = this.img.getAttribute("data-flickity-lazyload"), this.img.removeAttribute("data-flickity-lazyload")
		}, s.prototype.onload = function (t) {
			this.complete(t, "flickity-lazyloaded")
		}, s.prototype.onerror = function () {
			this.complete(event, "flickity-lazyerror")
		}, s.prototype.complete = function (t, n) {
			i.unbind(this.img, "load", this), i.unbind(this.img, "error", this);
			var o = this.flickity.getParentCell(this.img),
				r = o && o.element;
			this.flickity.cellSizeChange(r), e.add(this.img, n), this.flickity.dispatchEvent("lazyLoad", t, r)
		}, n.LazyLoader = s, n
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity/js/index", ["./flickity", "./drag", "./prev-next-button", "./page-dots", "./player", "./add-remove-cell", "./lazyload"], e) : "object" == typeof exports && (module.exports = e(require("./flickity"), require("./drag"), require("./prev-next-button"), require("./page-dots"), require("./player"), require("./add-remove-cell"), require("./lazyload")))
	}(window, function (t) {
		return t
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("flickity-as-nav-for/as-nav-for", ["classie/classie", "flickity/js/index", "fizzy-ui-utils/utils"], function (i, n, o) {
			return e(t, i, n, o)
		}) : "object" == typeof exports ? module.exports = e(t, require("desandro-classie"), require("flickity"), require("fizzy-ui-utils")) : t.Flickity = e(t, t.classie, t.Flickity, t.fizzyUIUtils)
	}(window, function (t, e, i, n) {
		return i.createMethods.push("_createAsNavFor"), i.prototype._createAsNavFor = function () {
			this.on("activate", this.activateAsNavFor), this.on("deactivate", this.deactivateAsNavFor), this.on("destroy", this.destroyAsNavFor);
			var t = this.options.asNavFor;
			if (t) {
				var e = this;
				setTimeout(function () {
					e.setNavCompanion(t)
				})
			}
		}, i.prototype.setNavCompanion = function (t) {
			t = n.getQueryElement(t);
			var e = i.data(t);
			if (e && e != this) {
				this.navCompanion = e;
				var o = this;
				this.onNavCompanionSelect = function () {
					o.navCompanionSelect()
				}, e.on("cellSelect", this.onNavCompanionSelect), this.on("staticClick", this.onNavStaticClick), this.navCompanionSelect()
			}
		}, i.prototype.navCompanionSelect = function () {
			if (this.navCompanion) {
				var t = this.navCompanion.selectedIndex;
				this.select(t), this.removeNavSelectedElement(), this.selectedIndex == t && (this.navSelectedElement = this.cells[t].element, e.add(this.navSelectedElement, "is-nav-selected"))
			}
		}, i.prototype.activateAsNavFor = function () {
			this.navCompanionSelect()
		}, i.prototype.removeNavSelectedElement = function () {
			this.navSelectedElement && (e.remove(this.navSelectedElement, "is-nav-selected"), delete this.navSelectedElement)
		}, i.prototype.onNavStaticClick = function (t, e, i, n) {
			"number" == typeof n && this.navCompanion.select(n)
		}, i.prototype.deactivateAsNavFor = function () {
			this.removeNavSelectedElement()
		}, i.prototype.destroyAsNavFor = function () {
			this.navCompanion && (this.navCompanion.off("cellSelect", this.onNavCompanionSelect), this.off("staticClick", this.onNavStaticClick), delete this.navCompanion)
		}, i
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define("imagesloaded/imagesloaded", ["eventEmitter/EventEmitter", "eventie/eventie"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("wolfy87-eventemitter"), require("eventie")) : t.imagesLoaded = e(t, t.EventEmitter, t.eventie)
	}(window, function (t, e, i) {
		function n(t, e) {
			for (var i in e) t[i] = e[i];
			return t
		}

		function o(t) {
			return "[object Array]" === d.call(t)
		}

		function r(t) {
			var e = [];
			if (o(t)) e = t;
			else if ("number" == typeof t.length)
				for (var i = 0, n = t.length; n > i; i++) e.push(t[i]);
			else e.push(t);
			return e
		}

		function s(t, e, i) {
			if (!(this instanceof s)) return new s(t, e);
			"string" == typeof t && (t = document.querySelectorAll(t)), this.elements = r(t), this.options = n({}, this.options), "function" == typeof e ? i = e : n(this.options, e), i && this.on("always", i), this.getImages(), c && (this.jqDeferred = new c.Deferred);
			var o = this;
			setTimeout(function () {
				o.check()
			})
		}

		function a(t) {
			this.img = t
		}

		function l(t) {
			this.src = t, u[t] = this
		}
		var c = t.jQuery,
			h = t.console,
			p = "undefined" != typeof h,
			d = Object.prototype.toString;
		s.prototype = new e, s.prototype.options = {}, s.prototype.getImages = function () {
			this.images = [];
			for (var t = 0, e = this.elements.length; e > t; t++) {
				var i = this.elements[t];
				"IMG" === i.nodeName && this.addImage(i);
				var n = i.nodeType;
				if (n && (1 === n || 9 === n || 11 === n))
					for (var o = i.querySelectorAll("img"), r = 0, s = o.length; s > r; r++) {
						var a = o[r];
						this.addImage(a)
					}
			}
		}, s.prototype.addImage = function (t) {
			var e = new a(t);
			this.images.push(e)
		}, s.prototype.check = function () {
			function t(t, o) {
				return e.options.debug && p && h.log("confirm", t, o), e.progress(t), i++, i === n && e.complete(), !0
			}
			var e = this,
				i = 0,
				n = this.images.length;
			if (this.hasAnyBroken = !1, !n) return void this.complete();
			for (var o = 0; n > o; o++) {
				var r = this.images[o];
				r.on("confirm", t), r.check()
			}
		}, s.prototype.progress = function (t) {
			this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded;
			var e = this;
			setTimeout(function () {
				e.emit("progress", e, t), e.jqDeferred && e.jqDeferred.notify && e.jqDeferred.notify(e, t)
			})
		}, s.prototype.complete = function () {
			var t = this.hasAnyBroken ? "fail" : "done";
			this.isComplete = !0;
			var e = this;
			setTimeout(function () {
				if (e.emit(t, e), e.emit("always", e), e.jqDeferred) {
					var i = e.hasAnyBroken ? "reject" : "resolve";
					e.jqDeferred[i](e)
				}
			})
		}, c && (c.fn.imagesLoaded = function (t, e) {
			var i = new s(this, t, e);
			return i.jqDeferred.promise(c(this))
		}), a.prototype = new e, a.prototype.check = function () {
			var t = u[this.img.src] || new l(this.img.src);
			if (t.isConfirmed) return void this.confirm(t.isLoaded, "cached was confirmed");
			if (this.img.complete && void 0 !== this.img.naturalWidth) return void this.confirm(0 !== this.img.naturalWidth, "naturalWidth");
			var e = this;
			t.on("confirm", function (t, i) {
				return e.confirm(t.isLoaded, i), !0
			}), t.check()
		}, a.prototype.confirm = function (t, e) {
			this.isLoaded = t, this.emit("confirm", this, e)
		};
		var u = {};
		return l.prototype = new e, l.prototype.check = function () {
			if (!this.isChecked) {
				var t = new Image;
				i.bind(t, "load", this), i.bind(t, "error", this), t.src = this.src, this.isChecked = !0
			}
		}, l.prototype.handleEvent = function (t) {
			var e = "on" + t.type;
			this[e] && this[e](t)
		}, l.prototype.onload = function (t) {
			this.confirm(!0, "onload"), this.unbindProxyEvents(t)
		}, l.prototype.onerror = function (t) {
			this.confirm(!1, "onerror"), this.unbindProxyEvents(t)
		}, l.prototype.confirm = function (t, e) {
			this.isConfirmed = !0, this.isLoaded = t, this.emit("confirm", this, e)
		}, l.prototype.unbindProxyEvents = function (t) {
			i.unbind(t.target, "load", this), i.unbind(t.target, "error", this)
		}, s
	}),
	function (t, e) {
		"function" == typeof define && define.amd ? define(["flickity/js/index", "imagesloaded/imagesloaded"], function (i, n) {
			return e(t, i, n)
		}) : "object" == typeof exports ? module.exports = e(t, require("flickity"), require("imagesloaded")) : t.Flickity = e(t, t.Flickity, t.imagesLoaded)
	}(window, function (t, e, i) {
		return e.createMethods.push("_createImagesLoaded"), e.prototype._createImagesLoaded = function () {
			this.on("activate", this.imagesLoaded)
		}, e.prototype.imagesLoaded = function () {
			function t(t, i) {
				var n = e.getParentCell(i.img);
				e.cellSizeChange(n && n.element), e.options.freeScroll || e.positionSliderAtSelected()
			}
			if (this.options.imagesLoaded) {
				var e = this;
				i(this.slider).on("progress", t)
			}
		}, e
	});
window.world_map_countries_arr = [];

// function update_time(dt) {
// 	if (dt) {} else {
// 		var dt = document.getElementById('server-time').getAttribute('data-server');
// 		console.log('use default time: ' + dt)
// 	}
// 	var time_clock, time_clock_fn_d = 0;
// 	var datetime_date = dt.split(' ')[0].split('-');
// 	var datetime_time = dt.split(' ')[1].split(':');
// 	var datetime_full = new Date(Date.UTC(datetime_date[0], datetime_date[1], datetime_date[2], datetime_time[0], datetime_time[1], datetime_time[2]));
// 	setInterval(function () {
// 		if (!time_clock_fn_d || time_clock_fn_d == 0)
// 			time_clock_fn_d = datetime_full;
// 		time_clock_fn_d.setSeconds(time_clock_fn_d.getSeconds() + 1);
// 		var result = time_clock_fn_d.getFullYear() + '-' + (time_clock_fn_d.getMonth() < 10 ? ('0' + time_clock_fn_d.getMonth()) : time_clock_fn_d.getMonth()) + '-' + (time_clock_fn_d.getDate() < 10 ? ('0' + time_clock_fn_d.getDate()) : time_clock_fn_d.getDate()) + ' ' + (time_clock_fn_d.getHours() < 10 ? ('0' + time_clock_fn_d.getHours()) : time_clock_fn_d.getHours()) + ':' + (time_clock_fn_d.getMinutes() < 10 ? ('0' + time_clock_fn_d.getMinutes()) : time_clock_fn_d.getMinutes()) + ':' + (time_clock_fn_d.getSeconds() < 10 ? ('0' + time_clock_fn_d.getSeconds()) : time_clock_fn_d.getSeconds());
// 		document.getElementById('server-time').innerHTML = result
// 	}, 1000)
// }

$(document).ready(function () {
	if (!readCookie('locale-suggestion')) {
		setTimeout(function () {
			if ( typeof platform_info !== "undefined" && platform_info.hasOwnProperty('time') && platform_info.country && platform_info.country.subdomain != window.location.host.split('.')[0] ) {
				var user_country = platform_info.country;
				var locale_suggestion_html = '' + '<div class="locale-suggestion-window">' + '<div class="locale-suggestion-header"></div>' + '<div class="locale-suggestion-body clearfix">' + '<div class="locale-suggestion-img-wrapper">' + '</div>' + '<div class="locale-suggestion-info-wrapper">' + '<div></div>' + '<div></div>' + '</div>' + '</div>' + '<div class="locale-suggestion-footer">' + '<button class="locale-suggestion-btn-yes"></button>' + '<button class="locale-suggestion-btn-no"></button>' + '</div>' + '</div>';
				$('body').append(locale_suggestion_html);
				var locale_suggestion_window = $('.locale-suggestion-window');
				locale_suggestion_window.find('.locale-suggestion-img-wrapper').append('<img height="55px" src="http://static.tgju.org/images/flags/4x3/' + user_country.code.toLowerCase() + '.svg" alt="' + user_country.enName + '">');
				locale_suggestion_window.find('.locale-suggestion-info-wrapper > div:first-child').html(user_country.country_name);
				locale_suggestion_window.find('.locale-suggestion-header').html('Do you want to Change Edition?');
				locale_suggestion_window.find('.locale-suggestion-btn-yes').html('Change Edition');
				locale_suggestion_window.find('.locale-suggestion-btn-no').html('Remain in this Edition');
				locale_suggestion_window.addClass('ltr');
				setTimeout(function () {
					locale_suggestion_window.addClass('shown')
				}, 100);
				$(document).on('click', '.locale-suggestion-btn-yes', function () {
					createCookie('locale-suggestion', 'true', 1);
					window.location.href = 'http://' + user_country.subdomain + '.tgju.org'
				});
				$(document).on('click', '.locale-suggestion-btn-no', function () {
					$('.locale-suggestion-window').removeClass('shown');
					createCookie('locale-suggestion', 'true', 1)
				})
			}
		}, 10000)
	}
});

// $.ajax({
// 	url: "https://platform.tgju.org/info.php",
// 	dataType: 'json',
// 	success: function (data) {
// 		if (data.time) {
// 			id('server-time').setAttribute('data-value', data.time);
// 			id('server-time').setAttribute('server-updated', 'true');
// 			console.log('server-time: ' + data.time)
// 		}
// 		update_time(data.time);
// 		if (!readCookie('locale-suggestion') && data.country && data.country.subdomain != window.location.host.split('.')[0]) {
// 			setTimeout(function () {
// 				var user_country = data.country;
// 				var locale_suggestion_html = '' + '<div class="locale-suggestion-window">' + '<div class="locale-suggestion-header"></div>' + '<div class="locale-suggestion-body clearfix">' + '<div class="locale-suggestion-img-wrapper">' + '</div>' + '<div class="locale-suggestion-info-wrapper">' + '<div></div>' + '<div></div>' + '</div>' + '</div>' + '<div class="locale-suggestion-footer">' + '<button class="locale-suggestion-btn-yes"></button>' + '<button class="locale-suggestion-btn-no"></button>' + '</div>' + '</div>';
// 				$('body').append(locale_suggestion_html);
// 				var locale_suggestion_window = $('.locale-suggestion-window');
// 				locale_suggestion_window.find('.locale-suggestion-img-wrapper').append('<img height="55px" src="http://static.tgju.org/images/flags/4x3/' + user_country.code.toLowerCase() + '.svg" alt="' + user_country.enName + '">');
// 				locale_suggestion_window.find('.locale-suggestion-info-wrapper > div:first-child').html(user_country.country_name);
// 				locale_suggestion_window.find('.locale-suggestion-header').html('Do you want to Change Edition?');
// 				locale_suggestion_window.find('.locale-suggestion-btn-yes').html('Change Edition');
// 				locale_suggestion_window.find('.locale-suggestion-btn-no').html('Remain in this Edition');
// 				locale_suggestion_window.addClass('ltr');
// 				setTimeout(function () {
// 					locale_suggestion_window.addClass('shown')
// 				}, 100);
// 				$(document).on('click', '.locale-suggestion-btn-yes', function () {
// 					createCookie('locale-suggestion', 'true', 1);
// 					window.location.href = 'http://' + user_country.subdomain + '.tgju.org'
// 				});
// 				$(document).on('click', '.locale-suggestion-btn-no', function () {
// 					$('.locale-suggestion-window').removeClass('shown');
// 					createCookie('locale-suggestion', 'true', 1)
// 				})
// 			}, 10000)
// 		}
// 	}
// });

function getViewport() {
	var viewPortWidth;
	var viewPortHeight;
	if (typeof window.innerWidth != 'undefined') {
		viewPortWidth = window.innerWidth, viewPortHeight = window.innerHeight
	} else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
		viewPortWidth = document.documentElement.clientWidth, viewPortHeight = document.documentElement.clientHeight
	} else {
		viewPortWidth = document.getElementsByTagName('body')[0].clientWidth, viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
	}
	return [viewPortWidth, viewPortHeight]
};
var a, ad_block, ad_manage, ad_result, ad_type, ad_value, addEvent, ads_tab, advertise_i, advertise_interval, advertise_time, b, breakout_of_frame, check_time, convertor_input, convertor_type, convertor_value, d, e, el, exchange_iframe, exchange_iframe_close, faq_toggle, form_popup, g, get_selected, get_selected_text, get_url, getclass, gid, grid_history, id, invest_call, k, len, len1, len2, len3, len4, len5, len6, li, main_a, n, open_popup, open_popup_bottom, p, q, r, ref, ref1, ref2, ref3, ref4, ref5, ref6, reload_charts, reload_table, reload_table_row, removeEvent, scroll_affix, shuffle, span, start_time, switch_box, switch_news, switch_page_ad, table_time, table_timer, to_fa, tooltip, tr, u, v, w, x, y, indexOf = [].indexOf || function (item) {
	for (var i = 0, l = this.length; i < l; i++) {
		if (i in this && this[i] === item) return i
	}
	return -1
};
addEvent = void 0;
removeEvent = void 0;
if (window.addEventListener) {
	addEvent = function (obj, type, fn) {
		obj.addEventListener(type, fn, !1)
	};
	removeEvent = function (obj, type, fn) {
		obj.removeEventListener(type, fn, !1)
	}
} else if (document.attachEvent) {
	addEvent = function (obj, type, fn) {
		var eProp;
		eProp = type + fn;
		obj["e" + eProp] = fn;
		obj[eProp] = function () {
			obj["e" + eProp](window.event)
		};
		obj.attachEvent("on" + type, obj[eProp])
	};
	removeEvent = function (obj, type, fn) {
		var eProp;
		eProp = type + fn;
		obj.detachEvent("on" + type, obj[eProp]);
		obj[eProp] = null;
		obj["e" + eProp] = null
	}
}
id = function (id) {
	return document.getElementById(id)
};
get_selected = function (el_id) {
	return id(el_id).options[id(el_id).selectedIndex].value
};
get_selected_text = function (el_id) {
	return id(el_id).options[id(el_id).selectedIndex].text
};
getclass = function (name) {
	return document.getElementsByClassName(name)
};
gid = function (name) {
	var all, el, k, len, list;
	all = document.getElementsByTagName('*');
	list = [];
	for (k = 0, len = all.length; k < len; k++) {
		el = all[k];
		if (el.getAttribute('id') === name) {
			list.push(el)
		}
	}
	return list
};
get_url = function (name) {
	var regex, results;
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	results = regex.exec(location.search);
	if (results == null) {
		return ""
	} else {
		return decodeURIComponent(results[1].replace(/\+/g, " "))
	}
};

function toFixed(value, precision) {
	var precision = precision || 0,
		neg = value < 0,
		power = Math.pow(10, precision),
		value = Math.round(value * power),
		integral = String((neg ? Math.ceil : Math.floor)(value / power)),
		fraction = String((neg ? -value : value) % power),
		padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
	return precision ? integral + '.' + padding + fraction : integral
};
convertor_value = function (name) {
	var value;

	if ( window.source_price[name] ) {
		value = window.source_price[name];
	} else if (gid("f-" + name).length) {
		value = id("f-" + name).getElementsByTagName('td')[0].innerHTML.replace(/,/g, "")
	} else if ($('[data-market-row="' + name + '"]').size()) {
		value = $('[data-market-row="' + name + '"]')[0].getElementsByTagName('td')[0].innerHTML.replace(/,/g, "")
	} else if ((typeof convertor_values !== "undefined" && convertor_values !== null ? convertor_values[name] : void 0) != null) {
		value = convertor_values[name]
	}

	
	if ( !value ) value = $('[data-market-row="' + name + '"][data-price]').attr('data-price');
	if ( value && value != undefined ) value = value.replace(/,/g, "");

	return value
};
convertor_type = function (convertor_options) {
	var from, html, option, ref, to, type, value;
	type = id('convertor-type').options[id('convertor-type').selectedIndex].value;
	from = id('convertor-from');
	to = id('convertor-to');
	html = '<option value="">انتخاب کنید</option>';
	ref = convertor_options[type];
	for (value in ref) {
		option = ref[value];
		html += '<option value="' + value + '">' + option + '</option>'
	}
	from.innerHTML = html;
	return to.innerHTML = html
};
convertor_input = function () {
	var formula, formula_ex, from, result, to, val_from, val_to;
	from = id('convertor-from').options[id('convertor-from').selectedIndex].value;
	to = id('convertor-to').options[id('convertor-to').selectedIndex].value;
	formula = !1;
	formula_ex = !1;
	if (from === '' || to === '') {
		result = '-'
	} else {
		if (from === 'rial') {
			if (to === 'platinum' || to === 'palladium' || to === 'oil' || to === 'ons' || to === 'silver') {
				val_from = 1;
				formula = !1;
				formula_ex = !0;
				val_from = val_from * parseFloat(id('convertor-value').value.replace(/,/g, ""))
			} else {
				val_from = 1;
				val_from = val_from * parseFloat(id('convertor-value').value.replace(/,/g, ""))
			}
		} else {
			val_from = convertor_value(from);
			val_from = val_from * parseFloat(id('convertor-value').value.replace(/,/g, ""))
		}
		if (to === 'rial') {
			if (from === 'platinum' || from === 'palladium' || from === 'oil' || from === 'ons' || from === 'silver') {
				val_to = convertor_value('price_dollar_rl');
				formula = !0
			} else {
				val_to = 1
			}
		} else {
			val_to = convertor_value(to);
			if (formula_ex) {
				val_to = val_to * convertor_value('price_dollar_rl');
				console.log(val_to)
			}
		}
		result = !formula ? val_from / val_to : val_from * val_to;
		result = isNaN(result) || result < 0 ? '-' : toFixed(result, 2);
		result = result.toString().replace(".00", "")
	}
	return id('convertor-result').innerHTML = result
};
ad_result = function (price) {
	var block_value, rate;
	block_value = get_selected('ad-block');
	if (block_value === 'A') {
		rate = (function () {
			switch (price) {
				case 2000000:
					return 1350;
				case 3000000:
					return 1330;
				case 4000000:
					return 1310;
				case 5000000:
					return 1300;
				case 6000000:
					return 1290;
				case 7000000:
					return 1280;
				case 8000000:
					return 1270;
				case 9000000:
					return 1260;
				case 10000000:
					return 1250;
				case 12000000:
					return 1230;
				case 15000000:
					return 1200;
				case 20000000:
					return 1170;
				default:
					return 1280
			}
		})()
	} else {
		rate = (function () {
			switch (price) {
				case 2000000:
					return 1320;
				case 3000000:
					return 1315;
				case 4000000:
					return 1310;
				case 5000000:
					return 1300;
				case 6000000:
					return 1290;
				case 7000000:
					return 1280;
				case 8000000:
					return 1270;
				case 9000000:
					return 1260;
				case 10000000:
					return 1250;
				case 12000000:
					return 1245;
				case 15000000:
					return 1225;
				case 20000000:
					return 1200;
				default:
					return 1270
			}
		})()
	}
	return [Math.floor(price / rate), rate]
};
ad_value = function () {
	var block_value, click_num, text, value;
	value = get_selected('ad-value');
	if (value === '') {
		return id('ad-output').innerHTML = ''
	} else {
		value = parseInt(value);
		text = get_selected_text('ad-value');
		block_value = get_selected('ad-block');
		click_num = ad_result(value);
		return id('ad-output').innerHTML = "با بودجه ای برابر با <strong>" + text + "</strong> ، آگهی تجاری شما برای دریافت <strong>" + click_num[0] + "</strong> کلیک در باکس <strong>" + block_value + "</strong> نمایش داده خواهد شد.<br><p>هزینه هر کلیک: " + click_num[1] + " ریال</p>"
	}
};
ad_type = function () {
	var block_value, monthly_price, value;
	value = parseInt(get_selected('ad-type'));
	block_value = get_selected('ad-block');
	if (value === 1) {
		id('ad-value').disabled = !0;
		id('ad-input').style.display = 'none';
		if (block_value === 'B') {
			monthly_price = '4.300.000' + ' ریال'
		}
		if (block_value === 'E') {
			monthly_price = '6.400.000' + ' ریال'
		}
		return id('ad-output').innerHTML = "۳۰ روز نمایش در جایگاه <strong>" + block_value + "</strong> معادل : <strong>" + monthly_price + "</strong>"
	} else {
		id('ad-input').style.display = 'table-row';
		id('ad-value').disabled = !1;
		id('ad-value').value = '';
		return id('ad-output').innerHTML = ''
	}
};
ad_block = function () {
	var click_base, monthly_base, options, value;
	value = get_selected('ad-block');
	click_base = '<option value="2">بر مبنای  کلیک</option>';
	monthly_base = '<option value="1">دوره ای و ماهیانه</option>';
	options = value === 'B' || value === 'E' ? monthly_base : click_base;
	id('ad-type').disabled = !1;
	id('ad-type').innerHTML = options;
	return ad_type()
};
tooltip = {
	offset: 10,
	show: function (e, element, text) {
		var content;
		text = element.getAttribute('data-title') || element.getAttribute('title');
		content = document.createElement("div");
		content.setAttribute("id", "tooltip");
		document.body.appendChild(content);
		tooltip.move(e, element);
		content.style.opacity = "1.0";
		content.style.filter = "alpha(opacity=100)";
		content.style.msfilter = '"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)"';
		content.innerHTML = text;
		if (element.getAttribute('title')) {
			element.setAttribute('data-title', element.getAttribute('title'));
			element.removeAttribute('title')
		}
	},
	move: function (e, element) {
		var add_y, content, t_height, x, y;
		content = id('tooltip');
		y = e.clientY + window.pageYOffset;
		x = (e.clientX + window.pageXOffset) - 0;
		if (!element.hasAttribute('data-tooltip-bottom')) {
			y = y - 65
		}
		if (element.hasAttribute('data-tooltip-bottom')) {
			x = x - 200
		}
		if (element.hasAttribute('data-tooltip-left')) {
			x = x - 225
		}
		t_height = content.offsetHeight;
		add_y = t_height < 50 ? 0 : t_height - (t_height / 2.5);
		add_y = t_height > 150 ? t_height - (t_height / 3.3) : add_y;
		add_y = t_height > 200 ? t_height - (t_height / 4.0) : add_y;
		content.style.top = (y - add_y) + "px";
		return content.style.left = (x - (content.offsetWidth / 2)) + "px"
	},
	hide: function () {
		var el, k, len, ref;
		ref = document.getElementsByTagName('div');
		for (k = 0, len = ref.length; k < len; k++) {
			el = ref[k];
			if (el && el.getAttribute('id') === 'tooltip') {
				el.remove();
				el.className = "hide"
			}
		}
	},
	invoke: function (element) {
		addEvent(element, 'mouseover', (function (event) {
			return tooltip.show(event, this)
		}));
		addEvent(element, 'mousemove', (function (event) {
			return tooltip.move(event, this)
		}));
		return addEvent(element, 'mouseout', (function () {
			return tooltip.hide()
		}))
	}
};
ref = document.getElementsByTagName('span');
for (k = 0, len = ref.length; k < len; k++) {
	span = ref[k];
	if (span.getAttribute('title')) {
		tooltip.invoke(span)
	}
}
ref1 = document.getElementsByTagName('b');
for (n = 0, len1 = ref1.length; n < len1; n++) {
	b = ref1[n];
	if (b.getAttribute('title')) {
		tooltip.invoke(b)
	}
}
ref2 = document.getElementsByTagName('tr');
for (p = 0, len2 = ref2.length; p < len2; p++) {
	tr = ref2[p];
	if (tr.getAttribute('data-title')) {
		tooltip.invoke(tr)
	}
}
ref3 = document.getElementsByTagName('li');
for (q = 0, len3 = ref3.length; q < len3; q++) {
	li = ref3[q];
	if (li.getAttribute('title')) {
		tooltip.invoke(li)
	}
}
ref4 = document.getElementsByTagName('a');
for (r = 0, len4 = ref4.length; r < len4; r++) {
	a = ref4[r];
	if (!a.hasAttribute('data-no-tooltip')) {
		if (a.getAttribute('title')) {
			tooltip.invoke(a)
		}
	}
}
table_time = 20;
Element.prototype.remove = function () {
	this.parentElement.removeChild(this)
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
	for (var i = 0, len = this.length; i < len; i++) {
		if (this[i] && this[i].parentElement) {
			this[i].parentElement.removeChild(this[i])
		}
	}
};
if (gid("xhr-error").length) {
	id("xhr-error").innerHTML = 'دسترسی شما کاربر گرامی به وب سایت، برای دریافت نرخ های لحظه ای قطع گردیده است برای رفع این مشکل، لطفا از برقراری اینترنت و کارکرد سیستم خود اطمینان حاصل فرمایید.'
}
reload_charts = function () {
	var charts, el, len5, ref5, results1, src, u;
	if (gid('charts').length) {
		charts = id('charts').getElementsByTagName('img');
		ref5 = id('charts').getElementsByTagName('img');
		results1 = [];
		for (u = 0, len5 = ref5.length; u < len5; u++) {
			el = ref5[u];
			src = el.getAttribute('src').split("?")[0] + "?time=" + (new Date().getTime());
			results1.push(el.parentNode.innerHTML = '<img src="' + src + '" alt="">')
		}
		return results1
	}
};
grid_history = [];
if (document.location.pathname === '/economics/calendar') {
	tinyxhr("?act=economics-calendar&backend=true&noview&purpose=ajax", function (err, data, xhr) {
		var doc, parser;
		parser = new DOMParser();
		doc = parser.parseFromString(data, "text/html");
		return id('calendar-target').innerHTML = doc.getElementById('UpdatePanel1').innerHTML
	})
}
invest_call = function (obj) {
	var name;
	name = obj.value;
	return tinyxhr("?act=invest-json&noview&name=" + name, function (err, data, xhr) {
		var get, key, results1, value;
		get = JSON.parse(data);
		results1 = [];
		for (key in get) {
			value = get[key];
			results1.push(id("invest-block-" + key).innerHTML = value)
		}
		return results1
	})
};

function market_items() {
	var values = {
		l: {},
		f: {},
		row: {}
	};
	$('[data-market-row]').each(function () {
		var key = $(this).attr('data-market-row');
		var target_cell = 0;
		if ( key == 'ati1' || key == 'ati2' || key == 'ati3' || key == 'ati4' || key == 'ati5' )
			target_cell = 1;

		if ( !$(this).hasClass('not-allowed-item') ) {
			values.row[key] = $(this).find('td').eq(target_cell).text()
		}
	});
	$('[id^="f-"]').each(function () {
		var key = $(this).attr('id').replace('f-', '');
		var target_cell = 0;
		if ( key == 'ati1' || key == 'ati2' || key == 'ati3' || key == 'ati4' || key == 'ati5' )
			target_cell = 1;

		if ( !$(this).hasClass('not-allowed-item') ) {
			values.f[key] = $(this).find('td').eq(target_cell).text()
		}
	});
	$('[id^="l-"]').each(function () {
		var key = $(this).attr('id').replace('l-', '');

		if ( !$(this).hasClass('not-allowed-item') ) {
			values.l[key] = $(this).find('.info-price').text()
		}
	});
	return values
}

function market_row(object, key, item) {
	row = object[0];
	cells = row.getElementsByTagName('td');
	is_main = cells.length > 3;
	target_cell = 0;
	if ( key == 'ati1' || key == 'ati2' || key == 'ati3' || key == 'ati4' || key == 'ati5' )
		target_cell = 1;
	has_a = row.getAttribute('data-title');
	last = has_a ? "<hr>" + has_a : "";
	match = (ref5 = last.match("<div class='highlight'>(.*)</div>")) != null ? ref5[0] : void 0;
	last = last.split("<hr>").splice(0, 7).join('<hr>');
	if (match) {
		last = last.replace(match, '')
	}
	last = last + (match ? match : '');
	var is_fa = !0;
	if ($('html').attr('lang') != 'fa' || document.location.pathname === '/en') is_fa = !1;
	this_time = is_fa ? item.t : item.t_en;
	change = "<span class='type " + item.dt + "'>(" + item.dp + "%) " + item.d + "</span>";
	_in = is_fa ? 'در' : trans('data-world_market.in');
	row.setAttribute('data-title', "<div class='tooltip-row'>" + "<span class='tooltip-row-txt'>" + item.p + " " + _in + " " + this_time + "</span>" + "<span class='tooltip-row-change'>" + change + "</span>" + "</div><hr>" + last);
	if (!has_a) {
		tooltip.invoke(row)
	}
	if (is_main) {
		if (row.hasAttribute('data-change')) {
			cells[0].innerHTML = item.p;
			cells[1].setAttribute('class', item.dt);
			cells[2].setAttribute('class', item.dt);
			cells[1].innerHTML = "" + item.d;
			cells[2].innerHTML = item.dp + "%";
			cells[3].innerHTML = item.l;
			cells[4].innerHTML = item.h;
			cells[5].innerHTML = this_time
		} else {
			if (key.indexOf('crypto-') !== -1) {
				if (typeof crypto_title_set !== 'undefined' && crypto_title_set) {
					cells[0].innerHTML = item.p;
					cells[1].innerHTML = cells[1].innerHTML;
					cells[2].setAttribute('class', item.dt);
					cells[2].innerHTML = "(" + item.dp + "%) " + item.d;
					cells[3].innerHTML = item.l;
					cells[4].innerHTML = item.h;
					cells[5].innerHTML = this_time
				}
			} else {
				cells[0].innerHTML = item.p;
				cells[1].setAttribute('class', item.dt);
				cells[1].innerHTML = "(" + item.dp + "%) " + item.d;
				cells[2].innerHTML = item.l;
				cells[3].innerHTML = item.h;
				/*if (key === 'sekee' || key === 'sekeb' || key === 'nim' || key === 'rob' || key === 'gerami') {
					cells[4].innerHTML = item.r;
					cells[5].innerHTML = this_time
				} else */if ( key === 'ati1' || key === 'ati2' || key === 'ati3' || key === 'ati4' || key === 'ati5' ) {
					cells[0].innerHTML = item.settlement;
					cells[1].innerHTML = item.p;
					cells[1].removeAttribute('class');
					cells[2].setAttribute('class', item.dt);
					cells[2].innerHTML = "(" + item.dp + "%) " + item.d;
					cells[3].innerHTML = item.l;
					cells[4].innerHTML = item.h;
					cells[5].innerHTML = this_time
				} else {
					cells[4].innerHTML = this_time
				}
			}
		}
	} else {
		cells[0].innerHTML = item.p;
		cells[1].setAttribute('class', item.dt);
		cells[1].innerHTML = "(" + item.dp + "%) " + item.d
	}

	$(row).removeClass('tr- tr-high tr-low');
	row.setAttribute('class', 'row-'+item.dt+' tr-'+item.dt);


	// large tables like vip tables (tolerances)
	if ( $(row).find('.dt0').length ) {
		var tol_td = $(row).find('td.dt0');
		if ( tol_td.attr('data-price') && tol_td.attr('data-price') != '0' ) {
			var tol_dt_arr = dt_dp(dump_comma(item.p), tol_td.attr('data-price'));
			tol_td.text('(' + tol_dt_arr.dp + '%) ' + tol_dt_arr.d);
			tol_td.removeClass('high low').addClass(tol_dt_arr.dt);
		}
	}
	if ( $(row).find('.dt1').length ) {
		var tol_td = $(row).find('td.dt1');
		if ( tol_td.attr('data-price') && tol_td.attr('data-price') != '0' ) {
			var tol_dt_arr = dt_dp(dump_comma(item.p), tol_td.attr('data-price'));
			tol_td.text('(' + tol_dt_arr.dp + '%) ' + tol_dt_arr.d);
			tol_td.removeClass('high low').addClass(tol_dt_arr.dt);
		}
	}
	if ( $(row).find('.dt2').length ) {
		var tol_td = $(row).find('td.dt2');
		if ( tol_td.attr('data-price') && tol_td.attr('data-price') != '0' ) {
			var tol_dt_arr = dt_dp(dump_comma(item.p), tol_td.attr('data-price'));
			tol_td.text('(' + tol_dt_arr.dp + '%) ' + tol_dt_arr.d);
			tol_td.removeClass('high low').addClass(tol_dt_arr.dt);
		}
	}
	if ( $(row).find('.dt3').length ) {
		var tol_td = $(row).find('td.dt3');
		if ( tol_td.attr('data-price') && tol_td.attr('data-price') != '0' ) {
			var tol_dt_arr = dt_dp(dump_comma(item.p), tol_td.attr('data-price'));
			tol_td.text('(' + tol_dt_arr.dp + '%) ' + tol_dt_arr.d);
			tol_td.removeClass('high low').addClass(tol_dt_arr.dt);
		}
	}
}

function market_sync(data, market_items) {
	if (Object.keys(data).length) {
		var updated = 0;
		for (key in market_items.l) {
			val = market_items.l[key];
			if (data[key] && data[key].p !== val) {
				var item = data[key];
				$("#l-" + key).attr('class', item.dt);
				$("#l-" + key + " .info-price").text(item.p);
				$("#l-" + key + " .info-change").text(item.d + " (" + item.dp + "%)")
			}
		}
		for (key in market_items.f) {
			val = market_items.f[key];
			if (data[key] && data[key].p !== val) {
				var item = data[key];
				market_row($("#f-" + key), key, item);
				updated++
			}
		}
		for (key in market_items.row) {
			val = market_items.row[key];
			if (data[key] && data[key].p !== val) {
				var item = data[key];
				market_row($('[data-market-row="' + key + '"]'), key, item);
				if ($('[data-market-row="' + key + '"]').size() > 1)
					market_row($('[data-market-row="' + key + '"]').eq(1), key, item);
				updated++
			}
		}
		if (updated) {
			setTimeout(function () {
				$("tr.row-high, tr.row-low").removeClass('row-high row-low')
			}, 17000)
		}
	}
}
market_request_timestamp = new Date();

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min
}

function make_random_str(rand_limit) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < rand_limit; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

function round5(x) {
	return (x % 5) >= 2.5 ? parseInt(x / 5) * 5 + 5 : parseInt(x / 5) * 5
}

function dump_comma(str) {
	return Number(str.replace(/,/g, ''))
}

function dt_dp($price, $basis_price) {
	$d = $price - $basis_price;
	$dp = number_format((Math.abs($d) * 100) / $basis_price, 2);
	if ($d > 0) $dt = 'high';
	else if ($d < 0) $dt = 'low';
	else $dt = '';
	return {
		'd': number_format(Math.abs($d)),
		'dp': $dp,
		'dt': $dt
	}
}

function number_format(number, decimals, decPoint, thousandsSep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
	var n = !isFinite(+number) ? 0 : +number
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
	var s = ''
	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec)
		return '' + (Math.round(n * k) / k).toFixed(prec)
	}
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || ''
		s[1] += new Array(prec - s[1].length + 1).join('0')
	}
	return s.join(dec)
}

function number_format_decimal(num, decimal) {
	if ( typeof decimal == 'undefined' ) {
		var decimal = 0;
	}

	num = String(num);
	
	var number_parts = num.split('.');
	
	var formatted_num = number_format(number_parts[0]);
	
    if ( number_parts[1] && decimal ) {
		formatted_num += '.' + number_parts[1].substring(0, decimal);
	}

    return formatted_num;
}

function market_process(data, is_ajax) {
	if (data) {
		if (is_ajax) {
			var ajax_error = !1;
			try {
				var this_data = $.parseJSON(data.responseText)
			} catch (err) {
				ajax_error = !0
			}
			if (ajax_error || !data.responseText || data.responseText.length < 100) {
				setTimeout(function () {
					market_request(!1, !0)
				}, 2000);
				market_request_timestamp = new Date();
				console.log('Error in Ajax: Try Again.');
				return
			}
		} else {
			this_data = data
		}

		if ( typeof window.source_price == "undefined" ) window.source_price = new Object();
		window.source_price['price_dollar_rl'] = this_data.current.price_dollar_rl.p.replace(/,/g, '');
		window.source_price['price_dollar_realtime'] = this_data.current.price_dollar_realtime.p.replace(/,/g, '');
		window.source_price['rate_dollar_coin'] = this_data.current.rate_dollar_coin.p.replace(/,/g, '');
		window.source_price['price_eur'] = this_data.current.price_eur.p.replace(/,/g, '');
		window.source_price['price_gbp'] = this_data.current.price_gbp.p.replace(/,/g, '');
		window.source_price['price_aed'] = this_data.current.price_aed.p.replace(/,/g, '');
		window.source_price['mesghal'] = this_data.current.mesghal.p.replace(/,/g, '');
		window.source_price['geram18'] = this_data.current.geram18.p.replace(/,/g, '');
		window.source_price['geram24'] = this_data.current.geram24.p.replace(/,/g, '');
		window.source_price['sekee'] = this_data.current.sekee.p.replace(/,/g, '');
		window.source_price['sekeb'] = this_data.current.sekeb.p.replace(/,/g, '');
		window.source_price['rob'] = this_data.current.rob.p.replace(/,/g, '');
		window.source_price['nim'] = this_data.current.nim.p.replace(/,/g, '');
		window.source_price['gerami'] = this_data.current.gerami.p.replace(/,/g, '');

		market_sync(this_data.current, market_items());
		var table_header_summary_container = $('.table-header-summary-container');
		if (table_header_summary_container.length) {
			table_header_summary_container.each(function () {
				if ( !$(this).hasClass('not-allowed-item') ) {
					var this_indicator_index = $(this).attr('data-index');
					var this_indicator_data = this_data.current[this_indicator_index];
					var hs_now_price = $(this).find('.table-header-summary-bottom-price > div:first-child');
					if (typeof this_indicator_data != "undefined" && hs_now_price.text() != this_indicator_data.p) {
						hs_now_price.text(this_indicator_data.p);
						var hs_now_date = $(this).find('.table-header-summary-bottom-date > div:last-child span');
						hs_now_date.text(this_indicator_data.ts.split(" ")[1]);
						var hs_dt = $(this).find('.table-header-summary-bottom-dt > div:first-child');
						hs_dt.text(this_indicator_data.d + ' (' + this_indicator_data.dp + '%)');
						hs_dt.removeClass('high low').addClass(this_indicator_data.dt);
						var hs_dt1 = $(this).find('.table-header-summary-bottom-dt1 > div:first-child');
						var dt1_arr = dt_dp(dump_comma(this_indicator_data.p), hs_dt1.attr('data-price'));
						hs_dt1.text(dt1_arr.d + ' (' + dt1_arr.dp + '%)');
						hs_dt1.removeClass('high low').addClass(dt1_arr.dt);
						var hs_dt2 = $(this).find('.table-header-summary-bottom-dt2 > div:first-child');
						var dt2_arr = dt_dp(dump_comma(this_indicator_data.p), hs_dt2.attr('data-price'));
						hs_dt2.text(dt2_arr.d + ' (' + dt2_arr.dp + '%)');
						hs_dt2.removeClass('high low').addClass(dt2_arr.dt);
						var hs_dt3 = $(this).find('.table-header-summary-bottom-dt3 > div:first-child');
						var dt3_arr = dt_dp(dump_comma(this_indicator_data.p), hs_dt3.attr('data-price'));
						hs_dt3.text(dt3_arr.d + ' (' + dt3_arr.dp + '%)');
						hs_dt3.removeClass('high low').addClass(dt3_arr.dt)
					}
				}
			});
		}
		if ($('.worldmap').length) {
			var countries_arr = [];
			var map_tooltip_irator = 0;
			for (key in this_data.current) {
				var this_matches = key.match(/diff_(.*?)_(.*)/);
				if (this_matches) {
					var this_code = this_matches[1].toUpperCase().substr(0, 2);
					var this_title = this_matches[1].toUpperCase() + '/' + this_matches[2].toUpperCase();
					var this_p = this_data.current[key].p;
					var this_country_elm = $("path[data-code='" + this_code + "']");
					countries_arr[map_tooltip_irator] = {
						'code': this_code,
						'title': this_title,
						'price': this_p
					};
					map_tooltip_irator++
				}
			}
			if (countries_arr.length) {
				window.world_map_countries_arr = countries_arr
			}
		}
		if ( $(window).width() > 760 && $('#tolerance_low').length && $('#last').length ) {
			var is_fa = !0;
			if ($('html').attr('lang') != 'fa' || document.location.pathname === '/en') is_fa = !1;
			updated = 0;
			tolerance_types = ['tolerance_high', 'tolerance_low'];
			for (tolerance_i in ['tolerance_high', 'tolerance_low']) {
				tolerance_type = tolerance_types[tolerance_i];
				if (this_data[tolerance_type]) {
					var html = '';
					for (i in this_data[tolerance_type]) {
						var row = this_data[tolerance_type][i];

						var row_p = row.p;
						var row_dt = row.dt;
						var row_change = '(' + row.dp + '%) ' + row.d;

						var row_name_or_slug = row.name ? row.name : row.slug;
						var not_allowed = false;
						var not_allowed_class = '';
						if ( window.hide_items.indexOf(row_name_or_slug) != -1 ) {
							not_allowed = true;
							not_allowed_class = 'not-allowed-item';
							row_dt = '';
							row_p = row_change = '<span class="not-allowed-dash"></span>';
						}

						var addClass = '';
						if ($('#' + tolerance_type + ' tr[data-item-name-' + tolerance_type + '="' + row.item_id + '"][data-item-value="' + row.p + '"]').length == 0) {
							addClass = 'row-change';
							updated++
						}
						html += '<tr class="pointer ' + addClass + ' '+not_allowed_class+'" data-item-name-' + tolerance_type + '="' + row.item_id + '" data-item-value="' + row.p + '" onclick="window.location=\'chart/' + row_name_or_slug + '\'">' + '<th>' + (is_fa ? row.title : row.title_en) + '</th>' + '<td>' + row_p + '</td>' + '<td><span class="' + row_dt + '">'+row_change+'</span></td>' + '<td class="chart-td"><a data-tooltip="نمودار" class="chart-icon" target="_blank" href="chart/' + row_name_or_slug + '"></a></td>' + '</tr>'
					}
					$('#' + tolerance_type + ' tbody').html(html)
				}
			}
			if ( this_data.last ) {
				var html = '';
				for (i in this_data.last) {
					var row = this_data.last[i];

					var row_p = row.p;
					var row_dt = row.dt;
					var row_change = '(' + row.dp + '%) ' + row.d;

					var row_name_or_slug = row.name ? row.name : row.slug;
					var not_allowed = false;
					var not_allowed_class = '';
					if ( window.hide_items.indexOf(row_name_or_slug) != -1 ) {
						not_allowed = true;
						not_allowed_class = 'not-allowed-item';
						row_dt = '';
						row_p = row_change = '<span class="not-allowed-dash"></span>';
					}

					var addClass = '';
					if ($('#last tr[data-item-name-last="' + row.item_id + '"][data-item-value="' + row.p + '"]').length == 0) {
						addClass = 'row-change';
						updated++
					}
					html += '<tr class="pointer ' + addClass + ' '+not_allowed_class+'" data-item-name-last="' + row.item_id + '" data-item-value="' + row.p + '" onclick="window.location=\'chart/' + row_name_or_slug + '\'">' + '<th>' + (is_fa ? row.title : row.title_en) + '</th>' + '<td>' +row_p+ '</td>' + '<td><span class="' + row_dt + '">' + row_change + '</span></td>' + '<td class="chart-td"><a data-tooltip="نمودار" class="chart-icon" target="_blank" href="chart/' + row_name_or_slug + '"></a></td>' + '</tr>'
				}
				$('#last tbody').html(html)
			}
			if (updated) {
				setTimeout(function () {
					$('tr[data-item-name-tolerance_low].row-change').removeClass('row-change');
					$('tr[data-item-name-tolerance_high].row-change').removeClass('row-change');
					$('tr[data-item-name-last].row-change').removeClass('row-change')
				}, 3000)
			}
		}
	}
	if (is_ajax) {
		setTimeout(function () {
			market_request(!1, !0)
		}, 6000);
		market_request_timestamp = new Date()
	}
}
var websocket;
var websocket_date = new Date();
setInterval(function () {
	var seconds = Math.round(((new Date()) - websocket_date) / 1000);
	var diff = ((((new Date()) - market_request_timestamp) % 86400000) % 3600000) / 60000;
	if (seconds > 30 && diff >= 1.2) {
		market_request(!1, !0);
		console.log('Interval Fallback')
	}
}, 16000);

var call_subdomains = ['call2', 'call3', 'call4', 'call5'];
var call_subdomain  = call_subdomains[Math.floor(Math.random()*call_subdomains.length)];

function market_request(is_start, force_ajax) {
	var mitems = market_items();
	if (Object.keys(mitems.f).length || Object.keys(mitems.l).length || Object.keys(mitems.row).length || $('body').hasClass('atlas') || $('body').hasClass('economics')) {
		var t;
		if (document.getElementById('server-time').innerHTML) {
			t = document.getElementById('server-time').innerHTML
		} else {
			t = document.getElementById('server-time').getAttribute('data-value')
		}
		t = t.replace(/-/g, "");
		t = t.replace(/:/g, "");
		t = t.replace(/ /g, "");
		t = parseInt(round5(t));
		if (!document.getElementById('server-time').innerHTML) {
			t = t + '-start2'
		}
		var data_revision = document.getElementsByTagName("html")[0].getAttribute('data-revision');
		force_ajax = !0;
		if (force_ajax) {
			setTimeout(function() {
				$.ajax({
					url: 'http://' + call_subdomain + '.tgju.org/ajax.json?' + data_revision + '-' + t + '-' + make_random_str(20),
					complete: function (data) {
						market_process(data, !0)
					}
				})
			}, 100);
		} else {
			var websocket = new ab.Session('ws://platform.server.tgju.org:3005', function () {
				websocket.subscribe('', function (wsUserToken, data) {
					if (!data) return;
					if (data.event == 'market_price') {
						console.log(data);
						market_process(data.prices);
						websocket_date = new Date()
					}
				})
			}, function () {
				console.warn('WebSocket connection closed');
				market_request(!1, !0)
			}, {
				'skipSubprotocolCheck': !0
			})
		}
	}
}

function reload_table() {
	market_request(!0)
}

function call_data_fn() {
	tinyxhr("call-data-2.json?t=" + (new Date().getTime()), function (err, data, xhr) {
		data = JSON.parse(data);
		$(".homepage-charts img").each(function () {
			var t;
			if (document.getElementById('server-time').innerHTML) {
				t = document.getElementById('server-time').innerHTML
			} else {
				t = document.getElementById('server-time').getAttribute('data-value')
			}
			t = t.replace(/-/g, "");
			t = t.replace(/:/g, "");
			t = t.replace(/ /g, "");
			t = parseInt(round5(t));
			var src = $(this).attr('src').split('&')[0];
			$(this).attr('src', src + '&t=' + t)
		});
		for (key in data) {
			var value = data[key]
			if ($('[data-call-data="' + key + '"]').size()) {
				var main_element = $('[data-call-data="' + key + '"]');
				if (main_element.attr('data-call-compare')) {
					var data_cell_compare = main_element.attr('data-call-compare');
					var compare_value = main_element.find('tr').eq(0).find('[data-cell="' + data_cell_compare + '"]').html()
					if (compare_value != value[data_cell_compare]) {
						var new_data = '';
						var d_i = 0;
						new_data = '<th data-cell="' + d + '">' + value.Value + '</th>' + '<td data-cell="' + d + '">' + value.Date + '</td>';
						if (new_data) {
							var tr_count = main_element.find('tr').size();
							main_element.find('tr').eq(tr_count - 1).remove();
							main_element.prepend('<tr class="row-change">' + new_data + '</tr>')
						}
					}
				} else {
					var this_row_i = 0;
					main_element.find('tr').each(function () {
						$(this).find('[data-cell]').each(function () {
							var this_value = value[this_row_i][$(this).attr('data-cell')];
							if (this_value != $(this).html()) {
								$(this).html(this_value);
								$(this).parent().removeClass('row-change').addClass('row-change')
							}
						});
						this_row_i++
					})
				}
				setTimeout(function () {
					$('[data-call-data] .row-change').removeClass('row-change')
				}, 15000)
			}
		}
	})
}
if (!get_url('act') || ((get_url('act') === 'events' && get_url('url') === "") || (get_url('act') === 'news' && get_url('type') === "")) || get_url('act') === 'diff' || get_url('act') === 'currency' || get_url('act') === 'bank') {
	if (gid('counter-interval').length) {
		reload_table(!0);
		table_timer = setInterval(function () {
			var next_val;
			next_val = parseInt(id('counter-interval').innerHTML) - 1;
			if (next_val === 0) {} else {
				return id('counter-interval').innerHTML = next_val
			}
		}, 1000)
	}
} else {
	id('timer-handle').style.display = 'none'
}
advertise_time = 20;
advertise_i = {
	1: 0,
	2: 0,
	3: 0,
	4: 0,
	5: 0
};
advertise_interval = {
	1: '',
	2: '',
	3: '',
	4: '',
	5: ''
};
ad_manage = function (code, time) {};
shuffle = function (o) {
	var i, j, x;
	j = void 0;
	x = void 0;
	i = o.length;
	while (i) {
		j = Math.floor(Math.random() * i);
		x = o[--i];
		o[i] = o[j];
		o[j] = x
	}
	return o
};
switch_news = function (element, index) {
	if (element.className !== 'active') {
		id('tab-first').className = '';
		id('tab-last').className = '';
		id('tab-content-1').className = 'tab-content';
		id('tab-content-2').className = 'tab-content';
		element.className = 'active';
		id('tab-content-' + index).className = 'tab-content active'
	}
	return !1
};
switch_page_ad = function (code) {
	var el, len5, ref5, u;
	ref5 = getclass('ad-tab');
	for (u = 0, len5 = ref5.length; u < len5; u++) {
		el = ref5[u];
		el.style.display = 'none'
	}
	return id('tab-' + code).style.display = 'table'
};
faq_toggle = function (object) {
	var content, el, len5, ref5, this_article, u;
	ref5 = id('faq-article-list').getElementsByTagName('article');
	for (u = 0, len5 = ref5.length; u < len5; u++) {
		el = ref5[u];
		el.getElementsByTagName('section')[0].style.display = 'none';
		el.className = ''
	}
	this_article = content = object.parentNode;
	this_article.className = 'active';
	content = this_article.getElementsByTagName('section')[0];
	return content.style.display = content.style.display === 'none' ? 'block' : 'none'
};
w = window;
d = document;
e = d.documentElement;
g = d.getElementsByTagName('body')[0];
x = w.innerWidth || e.clientWidth || g.clientWidth;
y = w.innerHeight || e.clientHeight || g.clientHeight;
if (x <= 999) {
	ref5 = document.getElementsByClassName('hassub');
	for (u = 0, len5 = ref5.length; u < len5; u++) {
		el = ref5[u];
		main_a = el.getElementsByTagName('a')[0];
		main_a.href = 'javascript:void(0)';
		main_a.onclick = function () {
			var ul;
			ul = this.parentNode.getElementsByTagName('ul')[0];
			if (ul.getAttribute('data-show') != null) {
				return ul.removeAttribute('data-show')
			} else {
				return ul.setAttribute('data-show', 'true')
			}
		}
	}
	ref6 = document.getElementsByClassName('hassubsub');
	for (v = 0, len6 = ref6.length; v < len6; v++) {
		el = ref6[v];
		main_a = el.getElementsByTagName('a')[0];
		main_a.href = 'javascript:void(0)';
		main_a.onclick = function () {
			var ul;
			ul = this.parentNode.getElementsByTagName('ul')[0];
			if (ul.getAttribute('data-show') != null) {
				return ul.removeAttribute('data-show')
			} else {
				return ul.setAttribute('data-show', 'true')
			}
		}
	}
}
ads_tab = function (code, prefix) {
	var _id, aa, all, len7, len8, list_1, list_2, z;
	prefix = prefix ? prefix : "ads";
	list_1 = [];
	list_2 = [];
	all = document.getElementsByTagName('*');
	for (z = 0, len7 = all.length; z < len7; z++) {
		el = all[z];
		if (_id = el.getAttribute('id')) {
			if (_id.indexOf(prefix + "-tab") !== -1) {
				el.className = prefix + '-tab tab-box-content'
			}
		}
	}
	for (aa = 0, len8 = all.length; aa < len8; aa++) {
		el = all[aa];
		if (_id = el.getAttribute('id')) {
			if (_id.indexOf(prefix + "-handle") !== -1) {
				el.className = prefix + '-button big-button tab-box-button'
			}
		}
	}
	id("ads-tab-" + code).className = 'ads-tab tab-box-content active';
	return id("ads-handle-" + code).className = 'ads-button tab-box-button big-button active'
};
String.prototype.replaceArray = function (find, replace) {
	var i, regex, replaceString;
	replaceString = this;
	regex = void 0;
	i = 0;
	while (i < find.length) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
		i++
	}
	return replaceString
};
to_fa = function (string) {
	return string.toString().replaceArray(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'])
};
var number2en = function (string) {
	return string.toString().replaceArray(['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'], ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
};
start_time = function () {
	var h, m, s, t, today;
	today = new Date();
	h = today.getHours();
	m = today.getMinutes();
	s = today.getSeconds();
	m = check_time(m);
	s = check_time(s);
	if (typeof parin_lang == 'undefined' || parin_lang == 'fa') {
		h = to_fa(h);
		m = to_fa(m);
		s = to_fa(s)
	}
	document.getElementById("dynamic-clock").innerHTML = h + ":" + m + ":" + s;
	t = setTimeout(function () {
		start_time()
	}, 500)
};
check_time = function (i) {
	if (i < 10) {
		i = "0" + i
	}
	return i
};
start_time();
scroll_affix = function () {
	var scrollTop;
	scrollTop = window.pageYOffset;
	if (scrollTop > 363) {
		document.getElementsByTagName("html")[0].classList.add('affix');
	} else {
		document.getElementsByTagName("html")[0].classList.remove('affix');
	}
};
window.onscroll = scroll_affix;
breakout_of_frame = function () {
	if (top.location !== location) {
		return top.location.href = document.location.href
	}
};
open_popup = function (url, width, height) {
	var left, newwindow, top;
	left = (screen.width / 2) - (width / 2);
	top = (screen.height / 2) - (height / 2);
	newwindow = window.open(url, 'Calculator', "toolbar=1, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, height=" + height + ", width=" + width + ", top=" + top + ", left=" + left);
	if (window.focus) {
		newwindow.focus()
	}
	return !1
};
form_popup = function (url, width, height) {
	var left, newwindow, wLeft;
	wLeft = window.screenLeft ? window.screenLeft : window.screenX;
	left = wLeft + (window.innerWidth / 2) - (width / 2);
	newwindow = window.open(url, 'فرم', "toolbar=1, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, height=" + height + ", width=" + width + ", left=" + left);
	if (window.focus) {
		newwindow.focus()
	}
	return !1
};
open_popup_bottom = function (type) {
	var height, l, newwindow, screen_height, screen_width, t, width;
	width = 280;
	height = 440;
	screen_width = (indexOf.call(window, "innerWidth") >= 0) ? window.innerWidth : document.documentElement.offsetWidth;
	screen_height = (indexOf.call(window, "innerHeight") >= 0) ? window.innerHeight : document.documentElement.offsetHeight;
	l = screen_width - (width + 15);
	t = screen_height;
	newwindow = window.open('http://www.eghtesadban.com/tgju-splash' + (type === 1 ? '' : '-' + type), 'اقتصادی', 'toolbar=1, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, height=' + height + ', width=' + width + ', left=' + l + ', top=' + t);
	if (window.focus) {
		newwindow.focus()
	}
	return id('popup-layer').style.display = 'none'
};
!1;
if (document.location.pathname === '/popup-test') {
	document.getElementById('open-popup').onclick()
}
exchange_iframe = function (url) {
	if (url === 'http://www.bloomberg.com/energy/') {
		return window.open(url)
	} else {
		id('exchange-iframe-close').style.display = 'block';
		id('exchange-iframe').style.display = 'block';
		return id('exchange-iframe').innerHTML = '<iframe src="' + url + '"></iframe>'
	}
};
exchange_iframe_close = function () {
	id('exchange-iframe').style.display = 'none';
	return id('exchange-iframe-close').style.display = 'none'
};
switch_box = function (element, idn, index) {
	var obj;
	if (element.className !== 'active') {
		id('tab-handle-' + idn + '-1').className = '';
		id('tab-handle-' + idn + '-2').className = '';
		if (gid('tab-handle-' + idn + '-3').length) {
			id('tab-handle-' + idn + '-3').className = ''
		}
		if (gid('tab-handle-' + idn + '-4').length) {
			id('tab-handle-' + idn + '-4').className = ''
		}
		if (gid('tab-handle-' + idn + '-5').length) {
			id('tab-handle-' + idn + '-5').className = ''
		}
		if (gid('tab-handle-' + idn + '-6').length) {
			id('tab-handle-' + idn + '-6').className = ''
		}
		if (gid('tab-handle-' + idn + '-7').length) {
			id('tab-handle-' + idn + '-7').className = ''
		}
		if (gid('tab-handle-' + idn + '-8').length) {
			id('tab-handle-' + idn + '-8').className = ''
		}
		if (gid('tab-handle-' + idn + '-9').length) {
			id('tab-handle-' + idn + '-9').className = ''
		}
		if (gid('tab-handle-' + idn + '-10').length) {
			id('tab-handle-' + idn + '-10').className = ''
		}
		id('tab-content-' + idn + '-1').className = 'tab-content';
		id('tab-content-' + idn + '-2').className = 'tab-content';
		if (gid('tab-content-' + idn + '-3').length) {
			id('tab-content-' + idn + '-3').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-4').length) {
			id('tab-content-' + idn + '-4').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-5').length) {
			id('tab-content-' + idn + '-5').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-6').length) {
			id('tab-content-' + idn + '-6').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-7').length) {
			id('tab-content-' + idn + '-7').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-8').length) {
			id('tab-content-' + idn + '-8').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-9').length) {
			id('tab-content-' + idn + '-9').className = 'tab-content'
		}
		if (gid('tab-content-' + idn + '-10').length) {
			id('tab-content-' + idn + '-10').className = 'tab-content'
		}
		element.className = 'active';
		id('tab-handle-' + idn + '-' + index).className = 'active';
		id('tab-content-' + idn + '-' + index).className = 'tab-content active';
		if (id('tab-handle-' + idn + '-' + index).hasAttribute('data-xhr')) {
			obj = id('tab-content-' + idn + '-' + index).getElementsByTagName('ul')[0];
			obj.innerHTML = '<li class="loading" style="width: 100% !important;"><span>در حال بارگذاری ...</span></li>';
			tinyxhr(id('tab-handle-' + idn + '-' + index).getAttribute('data-xhr'), function (err, data, xhr) {
				var len7, ref7, z;
				obj.innerHTML = data;
				ref7 = document.getElementsByTagName('a');
				for (z = 0, len7 = ref7.length; z < len7; z++) {
					a = ref7[z];
					if (!a.hasAttribute('data-no-tooltip')) {
						if (a.getAttribute('title')) {
							tooltip.invoke(a)
						}
					}
				}
				if (id('tab-handle-' + idn + '-' + index).hasAttribute('data-xhr-done')) {
					return eval(id('tab-handle-' + idn + '-' + index).getAttribute('data-xhr-done'))
				}
			})
		}
	}
	return !1
};

function scrollTo(element, duration) {
	var e = document.documentElement;
	if (e.scrollTop === 0) {
		var t = e.scrollTop;
		++e.scrollTop;
		e = t + 1 === e.scrollTop-- ? e : document.body
	}
	scrollToC(e, e.scrollTop, element, duration)
}

function scrollToC(element, from, to, duration) {
	if (duration < 0) return;
	if (typeof from === "object") from = from.offsetTop;
	if (typeof to === "object") to = to.offsetTop;
	scrollToX(element, from, to, 0, 1 / duration, 20, easeOutCuaic)
}

function scrollToX(element, x1, x2, t, v, step, operacion) {
	if (t < 0 || t > 1 || v <= 0) return;
	element.scrollTop = x1 - (x1 - x2) * operacion(t);
	t += v * step;
	setTimeout(function () {
		scrollToX(element, x1, x2, t, v, step, operacion)
	}, step)
}

function linearTween(t) {
	return t
}

function easeInQuad(t) {
	return t * t
}

function easeOutQuad(t) {
	return -t * (t - 2)
}

function easeInOutQuad(t) {
	t /= 0.5;
	if (t < 1) return t * t / 2;
	t--;
	return (t * (t - 2) - 1) / 2
}

function easeInCuaic(t) {
	return t * t * t
}

function easeOutCuaic(t) {
	t--;
	return t * t * t + 1
}

function easeInOutCuaic(t) {
	t /= 0.5;
	if (t < 1) return t * t * t / 2;
	t -= 2;
	return (t * t * t + 2) / 2
}

function easeInQuart(t) {
	return t * t * t * t
}

function easeOutQuart(t) {
	t--;
	return -(t * t * t * t - 1)
}

function easeInOutQuart(t) {
	t /= 0.5;
	if (t < 1) return 0.5 * t * t * t * t;
	t -= 2;
	return -(t * t * t * t - 2) / 2
}

function easeInQuint(t) {
	return t * t * t * t * t
}

function easeOutQuint(t) {
	t--;
	return t * t * t * t * t + 1
}

function easeInOutQuint(t) {
	t /= 0.5;
	if (t < 1) return t * t * t * t * t / 2;
	t -= 2;
	return (t * t * t * t * t + 2) / 2
}

function easeInSine(t) {
	return -Mathf.Cos(t / (Mathf.PI / 2)) + 1
}

function easeOutSine(t) {
	return Mathf.Sin(t / (Mathf.PI / 2))
}

function easeInOutSine(t) {
	return -(Mathf.Cos(Mathf.PI * t) - 1) / 2
}

function easeInExpo(t) {
	return Mathf.Pow(2, 10 * (t - 1))
}

function easeOutExpo(t) {
	return -Mathf.Pow(2, -10 * t) + 1
}

function easeInOutExpo(t) {
	t /= 0.5;
	if (t < 1) return Mathf.Pow(2, 10 * (t - 1)) / 2;
	t--;
	return (-Mathf.Pow(2, -10 * t) + 2) / 2
}

function easeInCirc(t) {
	return -Mathf.Sqrt(1 - t * t) - 1
}

function easeOutCirc(t) {
	t--;
	return Mathf.Sqrt(1 - t * t)
}

function easeInOutCirc(t) {
	t /= 0.5;
	if (t < 1) return -(Mathf.Sqrt(1 - t * t) - 1) / 2;
	t -= 2;
	return (Mathf.Sqrt(1 - t * t) + 1) / 2
};

function show_chat() {
	close_help();
	$('.page-chat').addClass('page-chat-show');
	$('.page-chat-content').html('<iframe src="http://platform.tgju.org/fa/dashboard/standalone-chat"></iframe>')
}

function close_chat() {
	$('.page-chat').removeClass('page-chat-show')
}

function show_help(id) {
	$('.page-help').addClass('page-help-show');
	$('.page-help-content').html('<div class="page-help-wait"><span class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></span><strong>لطفا شکیبا باشید</strong></div>');
	$.ajax({
		url: '?act=help-load&noview&client=tgju&id=' + id
	}).done(function (data) {
		$('.page-help-content').html(data);

		function myTrim(x) {
			return x.replace(/^\s+|\s+$/gm, '')
		}
		var input = $('.page-help-content #filter-input')[0];
		var help_input_event = function () {
			var filter = myTrim(input.value);
			if (filter && filter.length > 2) {
				$.ajax({
					url: '?act=faq-search&action=search&noview&h4=true&q=' + filter
				}).done(function (data) {
					var field_length = myTrim($('.page-help-content #filter-input')[0].value).length;
					if (field_length > 2) {
						$('.page-help-content .faq-article-show').hide();
						$('.page-help-content #faq-article-list .result-data').html(data)
					}
				})
			} else if (filter.length == 0) {
				$('.page-help-content .result-data').html('');
				$('.page-help-content .faq-article-show').show()
			}
		}
		$(input).bind('keyup change search', help_input_event);
		$('.page-help-content img').each(function () {
			if ($(this).parent().prop('tagName').toLowerCase() != 'a')
				$(this).replaceWith('<a href="' + $(this).attr('src') + '" class="modal-image" target="_blank">' + this.outerHTML + '</a>')
		});
		baguetteBox.run('.modal-image')
	})
}

function close_help() {
	$('.page-help').removeClass('page-help-show')
}

function header_links_tab(obj, menu_index, tab) {
	var container = $('.header-links > ul > li').eq(menu_index);
	var tab_container = container.find('.header-links-tabs li');
	tab_container.removeClass('active');
	$(obj).parent().addClass('active');
	container.find('.section > div > ul > li').each(function () {
		if ($(this).attr('data-header-links-tab') == tab)
			$(this).css('display', 'table-cell');
		else if (!$(this).hasClass('header-links-icon'))
			$(this).hide()
	})
}

function archive_tool() {
	var _name = document.getElementById('archive-tool-name');
	var _year = document.getElementById('archive-tool-year');
	var _month = document.getElementById('archive-tool-month');
	var _day = document.getElementById('archive-tool-day');
	var _result = document.getElementById('archive-tool-result');
	var name = _name.options[_name.selectedIndex].value;
	var year = _year.options[_year.selectedIndex].value;
	var month = _month.options[_month.selectedIndex].value;
	var day = _day.options[_day.selectedIndex].value;
	if (name && year && month && day) {
		tinyxhr("?act=archive-tool&noview&client=ajax&v=200&name=" + name + "&year=" + year + "&month=" + month + "&day=" + day, function (err, data, xhr) {
			if (!err) {
				data = JSON.parse(data);
				_result.innerHTML = number_format(data.price);
			}
		})
	}
}

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toUTCString()
	} else var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/"
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
	}
	return null
}
var header_bar_open = !1;
var header_bar_loaded = !1;

function header_bar_load() {
	if (!header_bar_loaded) {
		$.ajax({
			url: '?act=bar-menu&noview&client=tgju&load=menu'
		}).done(function (data) {
			$('.bar-menu').html(data);
			$('.bar-menu a').each(function () {
				var sub = $(this).parent().find('> ul');
				if (sub.length) {
					$(this).append('<span class="bullet"></span>');
					$(this).parent().addClass('has-children');
					$(this).find('> span.bullet').click(function () {
						$(this).parent().parent().toggleClass('sub-open');
						$(this).parent().parent().siblings().each(function () {
							if ($(this).hasClass('sub-open')) {
								$(this).removeClass('sub-open');
								$(this).find('ul').slideUp('fast')
							}
						});
						sub.slideToggle('fast');
						return !1
					})
				}
			})
		});
		header_bar_loaded = !0
	}
}

function header_bar(action, from_menu) {
	if (from_menu && action == 'click' && $('.bar-menu').hasClass('show')) {
		action = 'close'
	}
	if ($(window).width() < 760 && action == 'hover') {
		return
	}
	if (action == 'hover') {
		header_bar_load();
		$('.bar-menu').removeClass('half-show');
		$('.bar-menu').addClass('show');
		header_bar_open = !0
	} else if (action == 'click') {
		header_bar_load();
		if ($(window).width() < 760) {
			$('.bar-menu').removeClass('half-show');
			$('.bar-menu').addClass('show')
		} else {
			if ($('.bar-menu').hasClass('half-show')) {
				$('.bar-menu').removeClass('half-show');
				$('.bar-menu').addClass('show')
			} else {
				$('.bar-menu').removeClass('show');
				$('.bar-menu').addClass('half-show')
			}
		}
		header_bar_open = !0
	} else if (action == 'close') {
		$('.bar-menu').removeClass('half-show');
		$('.bar-menu').removeClass('show');
		header_bar_open = !1
	}
	if (header_bar_open) {
		$(".bar-menu").bind("clickoutside", function (event) {
			if (!$(event.target).is('.header-bar'))
				header_bar('close')
		})
	}
}
$(document).ready(function () {
	if ($(window).width() < 760) {
		$("#currency-table").appendTo('.mobile-append-1');
		$(".archive-and-convertor").appendTo('.mobile-append-2');
		$(".news-box-10").appendTo('.mobile-append-3');
		$(".flightSBox").appendTo('.mobile-append-safarestan')
	}
	$('.price-columns .tab-content').each(function () {
		var th = [];
		$(this).find('.data-table-header th').each(function () {
			th.push($(this).text())
		});
		if ($(this).find('.data-bourse-table').size()) {
			$(this).find('.data-bourse-table thead th').each(function () {
				th.push($(this).text())
			});
			$(this).find('.data-bourse-table tbody tr').each(function () {
				var td_count = 0;
				$(this).find('td').each(function () {
					$(this).attr('data-title', th[td_count]);
					td_count++
				})
			})
		} else {
			$(this).find('.data-table-list-content table tbody tr').each(function () {
				var td_count = 0;
				$(this).find('td').each(function () {
					$(this).attr('data-title', th[td_count]);
					td_count++
				})
			})
		}
	});
	$('.market-page #section table.data-table.data-table-list').each(function () {
		var th = [];
		$(this).find('thead th').each(function () {
			th.push($(this).text())
		});
		$(this).find('tbody tr').each(function () {
			var td_count = 0;
			$(this).find('td').each(function () {
				$(this).attr('data-title', th[td_count]);
				td_count++
			})
		})
	})

	$(document).on('click', '.side-close-icon', function() {
		setTimeout(function() {
			$('.side-open-icon').removeClass('strong-hidden').addClass('show');
		}, 250);
		if ( $('html').attr('data-dir') == 'ltr' ) $('.side-buttons').css('right', '-100px');
		else $('.side-buttons').css('left', '-100px');
		createCookie('side-buttons-hide', 'true', 7);
	});

	$(document).on('click', '.side-open-icon', function() {
		$('.side-open-icon').addClass('strong-hidden').removeClass('show');
		if ( $('html').attr('data-dir') == 'ltr' ) $('.side-buttons').css('right', '0');
		else $('.side-buttons').css('left', '0');
		document.cookie = 'side-buttons-hide=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	});

	if ( readCookie('side-buttons-hide') ) {
		$('.side-open-icon').removeClass('strong-hidden').addClass('show');
		if ( $('html').attr('data-dir') == 'ltr' ) $('.side-buttons').css('right', '-100px');
		else $('.side-buttons').css('left', '-100px');
		setTimeout(function() {
			$('.side-buttons').removeClass('strong-hidden');
		}, 500);
	} else {
		$('.side-buttons').removeClass('strong-hidden');
	}

	$(document).on('click', '.switchable-market-table-tab', function() {
		var this_data_id = $(this).attr('data-id');
		var this_table = $(this).parents('table');

		this_table.find('.switchable-market-table-tab').removeClass('active');
		$(this).addClass('active');

		this_table.find('tbody').hide();
		this_table.find('tbody[data-id="'+this_data_id+'"]').show();
	});

	function elementScrolled(elem) {
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();
		var elemTop = $(elem).offset().top;
		return ((elemTop <= docViewBottom));
	}

	$(window).trigger('scroll');
	$(window).scroll(function() {
		if ( $('#footer').length && $('html').attr('lang') == 'fa' && elementScrolled('#footer') ) {
			if ( !$('.footer-links-2 .footer-links-column-double .fata-logo').length ) {
				var fata_logo = "<img class='fata-logo' style='cursor:pointer;height: 100px;margin-left: 25px;vertical-align: top;' onclick='window.open(\"https://www.cyberpolice.ir/contacts1\")' alt='logo-samandehi' src='http://static.tgju.org/views/default/images/ir-fata.png' />";
				$('.footer-links-2 .footer-links-column-double').append(fata_logo);
			}

			if ( !$('.footer-links-2 .footer-links-column-double .samandehi-nemad').length ) {
				var samandehi_nemad = "<img class='samandehi-nemad' id='nbqefukzapfuwlaowlao' style='cursor:pointer;height:  110px;' onclick='window.open(\"https://logo.samandehi.ir/Verify.aspx?id=26544&p=uiwkgvkadshwaodsaods\", \"Popup\",\"toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30\")' alt='logo-samandehi' src='https://logo.samandehi.ir/logo.aspx?id=26544&p=odrfwlbqujynshwlshwl' />";
				$('.footer-links-2 .footer-links-column-double').append(samandehi_nemad);
			}
		}
	});

	// homepage exchanges widget
	$("#freeCurrency-select").change(function() {
		$.ajax({
			url: "getExchangePrices/" + $(this).val(),
			beforeSend: function(data) {
				$('.freeCurrency-container').append('<img class="freeCurrency-preload" src="http://img.cdn.tgju.org/25.gif" alt="preload" />');
				$('.freeCurrency-overlay').fadeIn();
			},
			success: function(data) {
				$('.freeCurrency-table tbody').remove();
				$('.freeCurrency-table').append(data);
				$('.freeCurrency-container .freeCurrency-preload').remove();
				$('.freeCurrency-overlay').fadeOut();
			}
		});
	});

	// TGJU homepage Starting popup Banner
	if ( $('html').attr('lang') == 'fa' && $('body').hasClass('homepage') && !readCookie('tgju-popup'))
	{
		var terms_popup = false;
		if ( terms_popup ) {
			setTimeout(function() {
				$.ajax("?act=terms-text&noview=1&v3").done(function (data) {
					$('#popup-layer-container').addClass('terms');
					$('#popup-layer-container').find('.modal-frame').css('width', '80%');
					$('#popup-layer-container').html('<div class="modal-close-layer" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';";></div>'
						+ '<div class="modal-frame">'
						+ '<a href="javascript:;" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';" class="modal-close">X</a>'
							+ '<div class="modal-body">'
									+ data
							+ '</div>'
						+ '</div>')
					.addClass('show');
				});

				createCookie('tgju-popup', 'true', 0.5);
			}, 5000);
		} else if ($(window).width() > 760) {

			setTimeout(function() {
				if ( popup_ad ) {
					// customer's popup if it is enabled
					$('#popup-layer-container').html('<div class="modal-close-layer" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';"></div>'
					+ '<div class="modal-frame" style="width: '+popup_ad.width+' !important;">'
					+ '<a href="javascript:;" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';" class="modal-close">X</a>'
						+ '<div class="modal-body">'
							+ '<div style="position: relative;">'
								+ '<div><a href="'+popup_ad.url+'" target="_blank"><img src="'+popup_ad.img+'" alt="" style="display: block;"></a></div>'
							+ '</div>'
						+ '</div>'
					+ '</div>')
					.addClass('show');
					$('#popup-layer-container').find('.modal-frame').css('width', popup_ad.width);
				} else {
					// our popup
					$('#popup-layer-container').html('<div class="modal-close-layer" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';"></div>'
						+ '<div class="modal-frame" style="width: 770px !important">'
						+ '<a href="javascript:;" onclick="document.getElementById(\'popup-layer-container\').className = \'modal-dialog\';" class="modal-close">X</a>'
							+ '<div class="modal-body">'
								+ '<div style="position: relative;">'
									+ '<div><img src="http://static.tgju.org/views/default/images/appdl-bg.png" alt="" style="display: block; width: 770px; height: 460px"></div>'
									+ '<div class="app-link" style="background: #000; margin: 0; padding: 13px 0 7px;">'
										+ '<a class="bazaar" style="margin: 0 3px;" target="_blank" href="https://cafebazaar.ir/app/com.vandaw.tgju/?l=fa"></a>'
										+ '<a class="google" style="margin: 0 3px;" target="_blank" href="https://play.google.com/store/apps/details?id=com.vandaw.tgju"></a>'
										+ '<a class="apple" style="margin: 0 3px;" target="_blank" href="https://itunes.apple.com/us/app/%D8%A7%D9%BE%D9%84%DB%8C%DA%A9%DB%8C%D8%B4%D9%86-%D9%82%DB%8C%D9%85%D8%AA-%D9%87%D8%A7/id1216096760?ls=1&mt=8"></a>'
										+ '<a class="windows" style="margin: 0 3px;" target="_blank" href="http://pc.cd/Qk1"></a>'
									+ '</div>'
								+ '</div>'
							+ '</div>'
						+ '</div>')
					.addClass('show');
				}

				createCookie('tgju-popup', 'true', 0.5);
			}, 5000);
			
		}
	}
});
if ($('select.market-search').length) {
	$('select.market-search').selectize({
		valueField: 'url',
		labelField: 'title',
		searchField: 'title',
		options: [],
		create: !1,
		render: {
			option: function (item, escape) {
				var desc = '';
				var cats = {};
				if (item.section == 'market') {
					for (cat in item.categories) {
						cats[item.categories[cat].title] = item.categories[cat].title
					}
				} else if (item.section == 'category') {
					if (item.parent)
						cats[item.parent.title] = item.parent.title
				} else if (item.section == 'old_market') {
					for (cat in item.categories) {
						cats[item.categories[cat].title] = item.categories[cat].title;
						item.url = item.url.replace('/chart', '/echart')
					}
				}
				for (cat in cats)
					desc += '<span>' + cats[cat] + '</span>';
				return '<div class="search-row">' + '<span class="description">' + desc + '</span>' + '<span class="title">' + (item.title ? '<span class="name title">' + escape(item.title) + '</span>' : '') + (item.title_second ? '<span class="name title-second">' + escape(item.title_second) + '</span>' : '') + '</span>' + '</div>'
			}
		},
		onChange: function (url) {
			if (url.substr(0, 5) == 'chart')
				window.location.href = url.replace('chart/', 'echart/');
			else window.location.href = url
		},
		load: function (query, callback) {
			if (!query.length) return callback();
			$.ajax({
				url: 'https://platform.tgju.org/fa/api/market-search',
				type: 'GET',
				dataType: 'json',
				data: {
					query: query,
					limit: 10,
					client: 'search',
					ajax: !0
				},
				error: function () {
					callback()
				},
				success: function (res) {
					var result = [];
					for (r in res) {
						for (rr in res[r]) {
							res[r][rr].section = r;
							if (r == 'old_market') {
								res[r][rr].url = 'chart/' + res[r][rr].link
							} else if (r == 'market') {
								res[r][rr].url = 'chart/market/' + res[r][rr].slug
							} else if (r == 'category') {
								res[r][rr].url = 'market/' + res[r][rr].slug
							}
							result.push(res[r][rr])
						}
					}
					callback(result)
				}
			})
		}
	})
}

function load_header_links(element, name) {
	var $container = $(element).find(' > .section > div');
	var header_menu_url = 'http://www.tgju.org/app/site-header-menu.php';
	if (typeof parin_lang != 'undefined' && parin_lang != 'fa') {
		header_menu_url = 'http://' + window.location.hostname + '/app/site-header-menu.global.php'
	}
	if ($container.html() === '') {
		tinyxhr(header_menu_url + '?client=ajax&name=' + name, function (err, data, xhr) {
			if (!err) {
				$container.html(data)
			}
		})
	}
}

function show_notification(message) {
	hide_notification();
	$('body').addClass('notification-open');
	$('body').prepend('<div class="website-notification-panel">' + '<div class="website-notification-container">' + '<div class="website-notification-options">' + '<span class="fa fa-times" onclick="hide_notification(true);"></span>' + '</div>' + '<div class="website-notification-text">' + message + '</div>' + '</div>' + '</div>');
	$('.website-notification-panel').css('height', 40)
}

function hide_notification(user_action) {
	if (user_action)
		createCookie('tgju-notification', 'true', .25);
	$('body').removeClass('notification-open');
	$('.website-notification-panel').remove()
}

function gold_calculator(geram18) {
	var pay = $('#gold-calculator-pay').val();
	var weight = $('#gold-calculator-weight').val();
	var geram18 = $('#gold-calculator-geram18').val();
	var result = parseInt(geram18) + parseInt(pay);
	result = ((7 / 100) * result) + result;
	result = ((9 / 100) * result) + result;
	result = parseInt(result);
	result = result * parseFloat(weight);
	if (isNaN(result))
		result = '-';
	else result = number_format(result) + ' ریال';
	$('#gold-calculator-result').text(result)
}

function blob_calculator(ons) {
	if (!ons) {
		var ons = $('[data-market-row="ons"] td').eq(0).text();
		ons = parseFloat(ons.replace(/,/g, ""))
	}
	var usd = $('#blob-calculator-usd').val();
	var type = $('#blob-calculator-type').val();
	var mintage = parseInt($('#blob-calculator-mintage').val());

	var current_price = '';
	if ( window.source_price[type] ) current_price = window.source_price[type];
	else current_price = $('[data-market-row="' + type + '"] td').eq(0).text();
	if ( !current_price ) current_price = $('[data-market-row="' + type + '"][data-price]').attr('data-price');

	current_price = parseInt(current_price.replace(/,/g, ""));
	if (type == 'sekee' || type == 'sekeb')
		weight = 8.133;
	if (type == 'nim')
		weight = 4.0665;
	if (type == 'rob')
		weight = 2.03325;
	if (type == 'gerami')
		weight = 1;
	var real = (((ons * usd) / (31.1)) * 0.900 * weight) + mintage;
	real = real - current_price;
	if (isNaN(real))
		result = '-';
	else result = Math.ceil(real);
	if (result < 0)
		$('#blob-calculator-result').css({
			'color': 'red'
		});
	else $('#blob-calculator-result').css({
		'color': '#239400'
	});
	if (result !== '-')
		result = number_format(Math.abs(result));
	$('#blob-calculator-result').text(result)
}

function diagrams_render(type) {
	var link = document.createElement('link');
	link.setAttribute('rel', 'stylesheet');
	link.setAttribute('href', 'components/hcolumns/css/hcolumns.css?v=14');
	document.getElementsByTagName('head')[0].appendChild(link);
	$.getScript("components/hcolumns/js/jquery.hcolumns.js?v=23", function (data, textStatus, jqxhr) {
		$("#columns").hColumns({
			noContentString: $('html').attr('lang') == 'fa' ? 'شاخصی در این بخش وجود ندارد' : 'There is no index in this section',
			searchable: !0,
			searchPlaceholderString: $('html').attr('lang') == 'fa' ? 'فیلتر' : 'Filter',
			customNodeTypeIndicator: {},
			customNodeTypeHandler: {},
			nodeSource: function (node_id, callback) {
				if (!$('.column-loading').is(':visible')) {
					$('.column-loading').show();
					var category_id = node_id == null ? 0 : node_id;
					var isMarket = $("#columns.column-view-container .column > ul > li[data-node-id='" + node_id + "']").data('market');
					if (typeof isMarket == "undefined") isMarket = !1;

					$.ajax({
						type: 'get',
						url: "https://api.tgju.online/v1/market/finder/list",
						data: {
							type: (type) ? type : '',
							market: isMarket,
							id: category_id,
							lang: $('html').attr('lang')
						},
						headers: {
							'X-Client-Name': ( site_mode == 'main' ) ? 'main_site' : 'global_site',
							'X-Client-Version': '1',
							'X-Client-SubSystem': site_mode,
						}
					})
					.done(function (data) {
						data = data.response.items;
						$('.column-loading').hide();
						return callback(null, data)
					}).fail(function () {
						return callback("AJAX error")
					})
				}
			}
		});
		$('.column-view-composition').append('<span class="column-loading">' + trans('profile.loading') + '</span>')
	})
}

function diagrams_render2(type) {
	var link = document.createElement('link');
	link.setAttribute('rel', 'stylesheet');
	link.setAttribute('href', 'components/hcolumns/css/hcolumns.css?v=14');
	document.getElementsByTagName('head')[0].appendChild(link);
	$.getScript("components/hcolumns/js/jquery.hcolumns2.js?v=23", function (data, textStatus, jqxhr) {
		$("#columns2").hColumns2({
			noContentString: $('html').attr('lang') == 'fa' ? 'شاخصی در این بخش وجود ندارد' : 'There is no index in this section',
			searchable: !0,
			searchPlaceholderString: $('html').attr('lang') == 'fa' ? 'فیلتر' : 'Filter',
			customNodeTypeIndicator: {},
			customNodeTypeHandler: {},
			nodeSource: function (node_id, callback) {
				if (!$('.column-loading').is(':visible')) {
					$('.column-loading').show();
					var category_id = node_id == null ? 0 : node_id;
					var isMarket = $("#columns2.column-view-container .column > ul > li[data-node-id='" + node_id + "']").data('market');
					if (typeof isMarket == "undefined") isMarket = !1;

					$.ajax({
						type: 'get',
						url: "https://api.tgju.online/v1/market/finder/list",
						data: {
							type: (type) ? type : '',
							market: isMarket,
							id: category_id,
							lang: $('html').attr('lang')
						},
						headers: {
							'X-Client-Name': ( site_mode == 'main' ) ? 'main_site' : 'global_site',
							'X-Client-Version': '1',
							'X-Client-SubSystem': site_mode,
						}
					})
					.done(function (data) {
						data = data.response.items;
						$('.column-loading').hide();
						return callback(null, data)
					}).fail(function () {
						return callback("AJAX error")
					})
				}
			}
		});
		$('#columns2 .column-view-composition').append('<span class="column-loading">' + trans('profile.loading') + '</span>')
	})
}

function tv_select(type) {
	$('body').append('<div class="diagrams-wrapper">' + '<a class="fa fa-times" onclick="$(\'.diagrams-wrapper\').remove(); return false;"></a>' + '<div class="diagrams-wrapper-2">' + '<div class="diagrams-wrapper-3">' + '<div class="diagrams-wrapper-close" onclick="$(\'.diagrams-wrapper\').remove();"></div>' + '<div id="columns">' + '</div>' + '</div>' + '</div>' + '</div>');
	diagrams_render(type)
}
$(document).ready(function () {
	if (window.outerWidth > 760) {
		var chart_second_link, chart_td_a, float, html;
		var html_tag_dir = $('html').attr('data-dir');
		$(".market-table:not(.chart-second-link-free) tbody tr").each(function () {
			if (html_tag_dir == 'ltr') {
				float = ($(this).parents('.market-table').hasClass('market-section-left')) ? 'left' : 'right'
			} else {
				float = ($(this).parents('.market-table').hasClass('market-section-right')) ? 'right' : 'left'
			}
			chart_td_a = $(this).find('.chart-td a');
			if (chart_td_a.length) {
				chart_second_link = chart_td_a.attr('href') + '/trading';
				html = "<div class='chart-second-link chart-second-link-" + float + "'><a href='" + chart_second_link + "' target='_blank'></a></div>";
				$(this).append(html)
			}
		});
		$(".chart-second-link").mouseenter(function () {
			var tr_parent = $(this).parent();
			var tr_parent_url = tr_parent.attr('onclick');
			tr_parent.attr('data-url', tr_parent_url);
			tr_parent.attr('onclick', '')
		});
		$(".chart-second-link").mouseout(function () {
			var tr_parent = $(this).parent();
			var tr_parent_url = tr_parent.attr('data-url');
			tr_parent.attr('data-url', '');
			tr_parent.attr('onclick', tr_parent_url)
		})
	}
});
! function (a) {
	"function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof module && module.exports ? module.exports = function (b, c) {
		return void 0 === c && (c = "undefined" != typeof window ? require("jquery") : require("jquery")(b)), a(c), c
	} : a(jQuery)
}(function (a) {
	var b = function () {
			if (a && a.fn && a.fn.select2 && a.fn.select2.amd) var b = a.fn.select2.amd;
			var b;
			return function () {
				if (!b || !b.requirejs) {
					b ? c = b : b = {};
					var a, c, d;
					! function (b) {
						function e(a, b) {
							return v.call(a, b)
						}

						function f(a, b) {
							var c, d, e, f, g, h, i, j, k, l, m, n, o = b && b.split("/"),
								p = t.map,
								q = p && p["*"] || {};
							if (a) {
								for (a = a.split("/"), g = a.length - 1, t.nodeIdCompat && x.test(a[g]) && (a[g] = a[g].replace(x, "")), "." === a[0].charAt(0) && o && (n = o.slice(0, o.length - 1), a = n.concat(a)), k = 0; k < a.length; k++)
									if ("." === (m = a[k])) a.splice(k, 1), k -= 1;
									else if (".." === m) {
									if (0 === k || 1 === k && ".." === a[2] || ".." === a[k - 1]) continue;
									k > 0 && (a.splice(k - 1, 2), k -= 2)
								}
								a = a.join("/")
							}
							if ((o || q) && p) {
								for (c = a.split("/"), k = c.length; k > 0; k -= 1) {
									if (d = c.slice(0, k).join("/"), o)
										for (l = o.length; l > 0; l -= 1)
											if ((e = p[o.slice(0, l).join("/")]) && (e = e[d])) {
												f = e, h = k;
												break
											} if (f) break;
									!i && q && q[d] && (i = q[d], j = k)
								}!f && i && (f = i, h = j), f && (c.splice(0, h, f), a = c.join("/"))
							}
							return a
						}

						function g(a, c) {
							return function () {
								var d = w.call(arguments, 0);
								return "string" != typeof d[0] && 1 === d.length && d.push(null), o.apply(b, d.concat([a, c]))
							}
						}

						function h(a) {
							return function (b) {
								return f(b, a)
							}
						}

						function i(a) {
							return function (b) {
								r[a] = b
							}
						}

						function j(a) {
							if (e(s, a)) {
								var c = s[a];
								delete s[a], u[a] = !0, n.apply(b, c)
							}
							if (!e(r, a) && !e(u, a)) throw new Error("No " + a);
							return r[a]
						}

						function k(a) {
							var b, c = a ? a.indexOf("!") : -1;
							return c > -1 && (b = a.substring(0, c), a = a.substring(c + 1, a.length)), [b, a]
						}

						function l(a) {
							return a ? k(a) : []
						}

						function m(a) {
							return function () {
								return t && t.config && t.config[a] || {}
							}
						}
						var n, o, p, q, r = {},
							s = {},
							t = {},
							u = {},
							v = Object.prototype.hasOwnProperty,
							w = [].slice,
							x = /\.js$/;
						p = function (a, b) {
							var c, d = k(a),
								e = d[0],
								g = b[1];
							return a = d[1], e && (e = f(e, g), c = j(e)), e ? a = c && c.normalize ? c.normalize(a, h(g)) : f(a, g) : (a = f(a, g), d = k(a), e = d[0], a = d[1], e && (c = j(e))), {
								f: e ? e + "!" + a : a,
								n: a,
								pr: e,
								p: c
							}
						}, q = {
							require: function (a) {
								return g(a)
							},
							exports: function (a) {
								var b = r[a];
								return void 0 !== b ? b : r[a] = {}
							},
							module: function (a) {
								return {
									id: a,
									uri: "",
									exports: r[a],
									config: m(a)
								}
							}
						}, n = function (a, c, d, f) {
							var h, k, m, n, o, t, v, w = [],
								x = typeof d;
							if (f = f || a, t = l(f), "undefined" === x || "function" === x) {
								for (c = !c.length && d.length ? ["require", "exports", "module"] : c, o = 0; o < c.length; o += 1)
									if (n = p(c[o], t), "require" === (k = n.f)) w[o] = q.require(a);
									else if ("exports" === k) w[o] = q.exports(a), v = !0;
								else if ("module" === k) h = w[o] = q.module(a);
								else if (e(r, k) || e(s, k) || e(u, k)) w[o] = j(k);
								else {
									if (!n.p) throw new Error(a + " missing " + k);
									n.p.load(n.n, g(f, !0), i(k), {}), w[o] = r[k]
								}
								m = d ? d.apply(r[a], w) : void 0, a && (h && h.exports !== b && h.exports !== r[a] ? r[a] = h.exports : m === b && v || (r[a] = m))
							} else a && (r[a] = d)
						}, a = c = o = function (a, c, d, e, f) {
							if ("string" == typeof a) return q[a] ? q[a](c) : j(p(a, l(c)).f);
							if (!a.splice) {
								if (t = a, t.deps && o(t.deps, t.callback), !c) return;
								c.splice ? (a = c, c = d, d = null) : a = b
							}
							return c = c || function () {}, "function" == typeof d && (d = e, e = f), e ? n(b, a, c, d) : setTimeout(function () {
								n(b, a, c, d)
							}, 4), o
						}, o.config = function (a) {
							return o(a)
						}, a._defined = r, d = function (a, b, c) {
							if ("string" != typeof a) throw new Error("See almond README: incorrect module build, no module name");
							b.splice || (c = b, b = []), e(r, a) || e(s, a) || (s[a] = [a, b, c])
						}, d.amd = {
							jQuery: !0
						}
					}(), b.requirejs = a, b.require = c, b.define = d
				}
			}(), b.define("almond", function () {}), b.define("jquery", [], function () {
				var b = a || $;
				return null == b && console && console.error && console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."), b
			}), b.define("select2/utils", ["jquery"], function (a) {
				function b(a) {
					var b = a.prototype,
						c = [];
					for (var d in b) {
						"function" == typeof b[d] && ("constructor" !== d && c.push(d))
					}
					return c
				}
				var c = {};
				c.Extend = function (a, b) {
					function c() {
						this.constructor = a
					}
					var d = {}.hasOwnProperty;
					for (var e in b) d.call(b, e) && (a[e] = b[e]);
					return c.prototype = b.prototype, a.prototype = new c, a.__super__ = b.prototype, a
				}, c.Decorate = function (a, c) {
					function d() {
						var b = Array.prototype.unshift,
							d = c.prototype.constructor.length,
							e = a.prototype.constructor;
						d > 0 && (b.call(arguments, a.prototype.constructor), e = c.prototype.constructor), e.apply(this, arguments)
					}

					function e() {
						this.constructor = d
					}
					var f = b(c),
						g = b(a);
					c.displayName = a.displayName, d.prototype = new e;
					for (var h = 0; h < g.length; h++) {
						var i = g[h];
						d.prototype[i] = a.prototype[i]
					}
					for (var j = (function (a) {
							var b = function () {};
							a in d.prototype && (b = d.prototype[a]);
							var e = c.prototype[a];
							return function () {
								return Array.prototype.unshift.call(arguments, b), e.apply(this, arguments)
							}
						}), k = 0; k < f.length; k++) {
						var l = f[k];
						d.prototype[l] = j(l)
					}
					return d
				};
				var d = function () {
					this.listeners = {}
				};
				d.prototype.on = function (a, b) {
					this.listeners = this.listeners || {}, a in this.listeners ? this.listeners[a].push(b) : this.listeners[a] = [b]
				}, d.prototype.trigger = function (a) {
					var b = Array.prototype.slice,
						c = b.call(arguments, 1);
					this.listeners = this.listeners || {}, null == c && (c = []), 0 === c.length && c.push({}), c[0]._type = a, a in this.listeners && this.invoke(this.listeners[a], b.call(arguments, 1)), "*" in this.listeners && this.invoke(this.listeners["*"], arguments)
				}, d.prototype.invoke = function (a, b) {
					for (var c = 0, d = a.length; c < d; c++) a[c].apply(this, b)
				}, c.Observable = d, c.generateChars = function (a) {
					for (var b = "", c = 0; c < a; c++) {
						b += Math.floor(36 * Math.random()).toString(36)
					}
					return b
				}, c.bind = function (a, b) {
					return function () {
						a.apply(b, arguments)
					}
				}, c._convertData = function (a) {
					for (var b in a) {
						var c = b.split("-"),
							d = a;
						if (1 !== c.length) {
							for (var e = 0; e < c.length; e++) {
								var f = c[e];
								f = f.substring(0, 1).toLowerCase() + f.substring(1), f in d || (d[f] = {}), e == c.length - 1 && (d[f] = a[b]), d = d[f]
							}
							delete a[b]
						}
					}
					return a
				}, c.hasScroll = function (b, c) {
					var d = a(c),
						e = c.style.overflowX,
						f = c.style.overflowY;
					return (e !== f || "hidden" !== f && "visible" !== f) && ("scroll" === e || "scroll" === f || (d.innerHeight() < c.scrollHeight || d.innerWidth() < c.scrollWidth))
				}, c.escapeMarkup = function (a) {
					var b = {
						"\\": "&#92;",
						"&": "&amp;",
						"<": "&lt;",
						">": "&gt;",
						'"': "&quot;",
						"'": "&#39;",
						"/": "&#47;"
					};
					return "string" != typeof a ? a : String(a).replace(/[&<>"'\/\\]/g, function (a) {
						return b[a]
					})
				}, c.appendMany = function (b, c) {
					if ("1.7" === a.fn.jquery.substr(0, 3)) {
						var d = a();
						a.map(c, function (a) {
							d = d.add(a)
						}), c = d
					}
					b.append(c)
				}, c.__cache = {};
				var e = 0;
				return c.GetUniqueElementId = function (a) {
					var b = a.getAttribute("data-select2-id");
					return null == b && (a.id ? (b = a.id, a.setAttribute("data-select2-id", b)) : (a.setAttribute("data-select2-id", ++e), b = e.toString())), b
				}, c.StoreData = function (a, b, d) {
					var e = c.GetUniqueElementId(a);
					c.__cache[e] || (c.__cache[e] = {}), c.__cache[e][b] = d
				}, c.GetData = function (b, d) {
					var e = c.GetUniqueElementId(b);
					return d ? c.__cache[e] && null != c.__cache[e][d] ? c.__cache[e][d] : a(b).data(d) : c.__cache[e]
				}, c.RemoveData = function (a) {
					var b = c.GetUniqueElementId(a);
					null != c.__cache[b] && delete c.__cache[b]
				}, c
			}), b.define("select2/results", ["jquery", "./utils"], function (a, b) {
				function c(a, b, d) {
					this.$element = a, this.data = d, this.options = b, c.__super__.constructor.call(this)
				}
				return b.Extend(c, b.Observable), c.prototype.render = function () {
					var b = a('<ul class="select2-results__options" role="tree"></ul>');
					return this.options.get("multiple") && b.attr("aria-multiselectable", "true"), this.$results = b, b
				}, c.prototype.clear = function () {
					this.$results.empty()
				}, c.prototype.displayMessage = function (b) {
					var c = this.options.get("escapeMarkup");
					this.clear(), this.hideLoading();
					var d = a('<li role="treeitem" aria-live="assertive" class="select2-results__option"></li>'),
						e = this.options.get("translations").get(b.message);
					d.append(c(e(b.args))), d[0].className += " select2-results__message", this.$results.append(d)
				}, c.prototype.hideMessages = function () {
					this.$results.find(".select2-results__message").remove()
				}, c.prototype.append = function (a) {
					this.hideLoading();
					var b = [];
					if (null == a.results || 0 === a.results.length) return void(0 === this.$results.children().length && this.trigger("results:message", {
						message: "noResults"
					}));
					a.results = this.sort(a.results);
					for (var c = 0; c < a.results.length; c++) {
						var d = a.results[c],
							e = this.option(d);
						b.push(e)
					}
					this.$results.append(b)
				}, c.prototype.position = function (a, b) {
					b.find(".select2-results").append(a)
				}, c.prototype.sort = function (a) {
					return this.options.get("sorter")(a)
				}, c.prototype.highlightFirstItem = function () {
					var a = this.$results.find(".select2-results__option[aria-selected]"),
						b = a.filter("[aria-selected=true]");
					b.length > 0 ? b.first().trigger("mouseenter") : a.first().trigger("mouseenter"), this.ensureHighlightVisible()
				}, c.prototype.setClasses = function () {
					var c = this;
					this.data.current(function (d) {
						var e = a.map(d, function (a) {
							return a.id.toString()
						});
						c.$results.find(".select2-results__option[aria-selected]").each(function () {
							var c = a(this),
								d = b.GetData(this, "data"),
								f = "" + d.id;
							null != d.element && d.element.selected || null == d.element && a.inArray(f, e) > -1 ? c.attr("aria-selected", "true") : c.attr("aria-selected", "false")
						})
					})
				}, c.prototype.showLoading = function (a) {
					this.hideLoading();
					var b = this.options.get("translations").get("searching"),
						c = {
							disabled: !0,
							loading: !0,
							text: b(a)
						},
						d = this.option(c);
					d.className += " loading-results", this.$results.prepend(d)
				}, c.prototype.hideLoading = function () {
					this.$results.find(".loading-results").remove()
				}, c.prototype.option = function (c) {
					var d = document.createElement("li");
					d.className = "select2-results__option";
					var e = {
						role: "treeitem",
						"aria-selected": "false"
					};
					c.disabled && (delete e["aria-selected"], e["aria-disabled"] = "true"), null == c.id && delete e["aria-selected"], null != c._resultId && (d.id = c._resultId), c.title && (d.title = c.title), c.children && (e.role = "group", e["aria-label"] = c.text, delete e["aria-selected"]);
					for (var f in e) {
						var g = e[f];
						d.setAttribute(f, g)
					}
					if (c.children) {
						var h = a(d),
							i = document.createElement("strong");
						i.className = "select2-results__group";
						a(i);
						this.template(c, i);
						for (var j = [], k = 0; k < c.children.length; k++) {
							var l = c.children[k],
								m = this.option(l);
							j.push(m)
						}
						var n = a("<ul></ul>", {
							class: "select2-results__options select2-results__options--nested"
						});
						n.append(j), h.append(i), h.append(n)
					} else this.template(c, d);
					return b.StoreData(d, "data", c), d
				}, c.prototype.bind = function (c, d) {
					var e = this,
						f = c.id + "-results";
					this.$results.attr("id", f), c.on("results:all", function (a) {
						e.clear(), e.append(a.data), c.isOpen() && (e.setClasses(), e.highlightFirstItem())
					}), c.on("results:append", function (a) {
						e.append(a.data), c.isOpen() && e.setClasses()
					}), c.on("query", function (a) {
						e.hideMessages(), e.showLoading(a)
					}), c.on("select", function () {
						c.isOpen() && (e.setClasses(), e.highlightFirstItem())
					}), c.on("unselect", function () {
						c.isOpen() && (e.setClasses(), e.highlightFirstItem())
					}), c.on("open", function () {
						e.$results.attr("aria-expanded", "true"), e.$results.attr("aria-hidden", "false"), e.setClasses(), e.ensureHighlightVisible()
					}), c.on("close", function () {
						e.$results.attr("aria-expanded", "false"), e.$results.attr("aria-hidden", "true"), e.$results.removeAttr("aria-activedescendant")
					}), c.on("results:toggle", function () {
						var a = e.getHighlightedResults();
						0 !== a.length && a.trigger("mouseup")
					}), c.on("results:select", function () {
						var a = e.getHighlightedResults();
						if (0 !== a.length) {
							var c = b.GetData(a[0], "data");
							"true" == a.attr("aria-selected") ? e.trigger("close", {}) : e.trigger("select", {
								data: c
							})
						}
					}), c.on("results:previous", function () {
						var a = e.getHighlightedResults(),
							b = e.$results.find("[aria-selected]"),
							c = b.index(a);
						if (0 !== c) {
							var d = c - 1;
							0 === a.length && (d = 0);
							var f = b.eq(d);
							f.trigger("mouseenter");
							var g = e.$results.offset().top,
								h = f.offset().top,
								i = e.$results.scrollTop() + (h - g);
							0 === d ? e.$results.scrollTop(0) : h - g < 0 && e.$results.scrollTop(i)
						}
					}), c.on("results:next", function () {
						var a = e.getHighlightedResults(),
							b = e.$results.find("[aria-selected]"),
							c = b.index(a),
							d = c + 1;
						if (!(d >= b.length)) {
							var f = b.eq(d);
							f.trigger("mouseenter");
							var g = e.$results.offset().top + e.$results.outerHeight(!1),
								h = f.offset().top + f.outerHeight(!1),
								i = e.$results.scrollTop() + h - g;
							0 === d ? e.$results.scrollTop(0) : h > g && e.$results.scrollTop(i)
						}
					}), c.on("results:focus", function (a) {
						a.element.addClass("select2-results__option--highlighted")
					}), c.on("results:message", function (a) {
						e.displayMessage(a)
					}), a.fn.mousewheel && this.$results.on("mousewheel", function (a) {
						var b = e.$results.scrollTop(),
							c = e.$results.get(0).scrollHeight - b + a.deltaY,
							d = a.deltaY > 0 && b - a.deltaY <= 0,
							f = a.deltaY < 0 && c <= e.$results.height();
						d ? (e.$results.scrollTop(0), a.preventDefault(), a.stopPropagation()) : f && (e.$results.scrollTop(e.$results.get(0).scrollHeight - e.$results.height()), a.preventDefault(), a.stopPropagation())
					}), this.$results.on("mouseup", ".select2-results__option[aria-selected]", function (c) {
						var d = a(this),
							f = b.GetData(this, "data");
						if ("true" === d.attr("aria-selected")) return void(e.options.get("multiple") ? e.trigger("unselect", {
							originalEvent: c,
							data: f
						}) : e.trigger("close", {}));
						e.trigger("select", {
							originalEvent: c,
							data: f
						})
					}), this.$results.on("mouseenter", ".select2-results__option[aria-selected]", function (c) {
						var d = b.GetData(this, "data");
						e.getHighlightedResults().removeClass("select2-results__option--highlighted"), e.trigger("results:focus", {
							data: d,
							element: a(this)
						})
					})
				}, c.prototype.getHighlightedResults = function () {
					return this.$results.find(".select2-results__option--highlighted")
				}, c.prototype.destroy = function () {
					this.$results.remove()
				}, c.prototype.ensureHighlightVisible = function () {
					var a = this.getHighlightedResults();
					if (0 !== a.length) {
						var b = this.$results.find("[aria-selected]"),
							c = b.index(a),
							d = this.$results.offset().top,
							e = a.offset().top,
							f = this.$results.scrollTop() + (e - d),
							g = e - d;
						f -= 2 * a.outerHeight(!1), c <= 2 ? this.$results.scrollTop(0) : (g > this.$results.outerHeight() || g < 0) && this.$results.scrollTop(f)
					}
				}, c.prototype.template = function (b, c) {
					var d = this.options.get("templateResult"),
						e = this.options.get("escapeMarkup"),
						f = d(b, c);
					null == f ? c.style.display = "none" : "string" == typeof f ? c.innerHTML = e(f) : a(c).append(f)
				}, c
			}), b.define("select2/keys", [], function () {
				return {
					BACKSPACE: 8,
					TAB: 9,
					ENTER: 13,
					SHIFT: 16,
					CTRL: 17,
					ALT: 18,
					ESC: 27,
					SPACE: 32,
					PAGE_UP: 33,
					PAGE_DOWN: 34,
					END: 35,
					HOME: 36,
					LEFT: 37,
					UP: 38,
					RIGHT: 39,
					DOWN: 40,
					DELETE: 46
				}
			}), b.define("select2/selection/base", ["jquery", "../utils", "../keys"], function (a, b, c) {
				function d(a, b) {
					this.$element = a, this.options = b, d.__super__.constructor.call(this)
				}
				return b.Extend(d, b.Observable), d.prototype.render = function () {
					var c = a('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');
					return this._tabindex = 0, null != b.GetData(this.$element[0], "old-tabindex") ? this._tabindex = b.GetData(this.$element[0], "old-tabindex") : null != this.$element.attr("tabindex") && (this._tabindex = this.$element.attr("tabindex")), c.attr("title", this.$element.attr("title")), c.attr("tabindex", this._tabindex), this.$selection = c, c
				}, d.prototype.bind = function (a, b) {
					var d = this,
						e = (a.id, a.id + "-results");
					this.container = a, this.$selection.on("focus", function (a) {
						d.trigger("focus", a)
					}), this.$selection.on("blur", function (a) {
						d._handleBlur(a)
					}), this.$selection.on("keydown", function (a) {
						d.trigger("keypress", a), a.which === c.SPACE && a.preventDefault()
					}), a.on("results:focus", function (a) {
						d.$selection.attr("aria-activedescendant", a.data._resultId)
					}), a.on("selection:update", function (a) {
						d.update(a.data)
					}), a.on("open", function () {
						d.$selection.attr("aria-expanded", "true"), d.$selection.attr("aria-owns", e), d._attachCloseHandler(a)
					}), a.on("close", function () {
						d.$selection.attr("aria-expanded", "false"), d.$selection.removeAttr("aria-activedescendant"), d.$selection.removeAttr("aria-owns"), d.$selection.focus(), d._detachCloseHandler(a)
					}), a.on("enable", function () {
						d.$selection.attr("tabindex", d._tabindex)
					}), a.on("disable", function () {
						d.$selection.attr("tabindex", "-1")
					})
				}, d.prototype._handleBlur = function (b) {
					var c = this;
					window.setTimeout(function () {
						document.activeElement == c.$selection[0] || a.contains(c.$selection[0], document.activeElement) || c.trigger("blur", b)
					}, 1)
				}, d.prototype._attachCloseHandler = function (c) {
					a(document.body).on("mousedown.select2." + c.id, function (c) {
						var d = a(c.target),
							e = d.closest(".select2");
						a(".select2.select2-container--open").each(function () {
							a(this), this != e[0] && b.GetData(this, "element").select2("close")
						})
					})
				}, d.prototype._detachCloseHandler = function (b) {
					a(document.body).off("mousedown.select2." + b.id)
				}, d.prototype.position = function (a, b) {
					b.find(".selection").append(a)
				}, d.prototype.destroy = function () {
					this._detachCloseHandler(this.container)
				}, d.prototype.update = function (a) {
					throw new Error("The `update` method must be defined in child classes.")
				}, d
			}), b.define("select2/selection/single", ["jquery", "./base", "../utils", "../keys"], function (a, b, c, d) {
				function e() {
					e.__super__.constructor.apply(this, arguments)
				}
				return c.Extend(e, b), e.prototype.render = function () {
					var a = e.__super__.render.call(this);
					return a.addClass("select2-selection--single"), a.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'), a
				}, e.prototype.bind = function (a, b) {
					var c = this;
					e.__super__.bind.apply(this, arguments);
					var d = a.id + "-container";
					this.$selection.find(".select2-selection__rendered").attr("id", d).attr("role", "textbox").attr("aria-readonly", "true"), this.$selection.attr("aria-labelledby", d), this.$selection.on("mousedown", function (a) {
						1 === a.which && c.trigger("toggle", {
							originalEvent: a
						})
					}), this.$selection.on("focus", function (a) {}), this.$selection.on("blur", function (a) {}), a.on("focus", function (b) {
						a.isOpen() || c.$selection.focus()
					})
				}, e.prototype.clear = function () {
					var a = this.$selection.find(".select2-selection__rendered");
					a.empty(), a.removeAttr("title")
				}, e.prototype.display = function (a, b) {
					var c = this.options.get("templateSelection");
					return this.options.get("escapeMarkup")(c(a, b))
				}, e.prototype.selectionContainer = function () {
					return a("<span></span>")
				}, e.prototype.update = function (a) {
					if (0 === a.length) return void this.clear();
					var b = a[0],
						c = this.$selection.find(".select2-selection__rendered"),
						d = this.display(b, c);
					c.empty().append(d), c.attr("title", b.title || b.text)
				}, e
			}), b.define("select2/selection/multiple", ["jquery", "./base", "../utils"], function (a, b, c) {
				function d(a, b) {
					d.__super__.constructor.apply(this, arguments)
				}
				return c.Extend(d, b), d.prototype.render = function () {
					var a = d.__super__.render.call(this);
					return a.addClass("select2-selection--multiple"), a.html('<ul class="select2-selection__rendered"></ul>'), a
				}, d.prototype.bind = function (b, e) {
					var f = this;
					d.__super__.bind.apply(this, arguments), this.$selection.on("click", function (a) {
						f.trigger("toggle", {
							originalEvent: a
						})
					}), this.$selection.on("click", ".select2-selection__choice__remove", function (b) {
						if (!f.options.get("disabled")) {
							var d = a(this),
								e = d.parent(),
								g = c.GetData(e[0], "data");
							f.trigger("unselect", {
								originalEvent: b,
								data: g
							})
						}
					})
				}, d.prototype.clear = function () {
					var a = this.$selection.find(".select2-selection__rendered");
					a.empty(), a.removeAttr("title")
				}, d.prototype.display = function (a, b) {
					var c = this.options.get("templateSelection");
					return this.options.get("escapeMarkup")(c(a, b))
				}, d.prototype.selectionContainer = function () {
					return a('<li class="select2-selection__choice"><span class="select2-selection__choice__remove" role="presentation">&times;</span></li>')
				}, d.prototype.update = function (a) {
					if (this.clear(), 0 !== a.length) {
						for (var b = [], d = 0; d < a.length; d++) {
							var e = a[d],
								f = this.selectionContainer(),
								g = this.display(e, f);
							f.append(g), f.attr("title", e.title || e.text), c.StoreData(f[0], "data", e), b.push(f)
						}
						var h = this.$selection.find(".select2-selection__rendered");
						c.appendMany(h, b)
					}
				}, d
			}), b.define("select2/selection/placeholder", ["../utils"], function (a) {
				function b(a, b, c) {
					this.placeholder = this.normalizePlaceholder(c.get("placeholder")), a.call(this, b, c)
				}
				return b.prototype.normalizePlaceholder = function (a, b) {
					return "string" == typeof b && (b = {
						id: "",
						text: b
					}), b
				}, b.prototype.createPlaceholder = function (a, b) {
					var c = this.selectionContainer();
					return c.html(this.display(b)), c.addClass("select2-selection__placeholder").removeClass("select2-selection__choice"), c
				}, b.prototype.update = function (a, b) {
					var c = 1 == b.length && b[0].id != this.placeholder.id;
					if (b.length > 1 || c) return a.call(this, b);
					this.clear();
					var d = this.createPlaceholder(this.placeholder);
					this.$selection.find(".select2-selection__rendered").append(d)
				}, b
			}), b.define("select2/selection/allowClear", ["jquery", "../keys", "../utils"], function (a, b, c) {
				function d() {}
				return d.prototype.bind = function (a, b, c) {
					var d = this;
					a.call(this, b, c), null == this.placeholder && this.options.get("debug") && window.console && console.error && console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."), this.$selection.on("mousedown", ".select2-selection__clear", function (a) {
						d._handleClear(a)
					}), b.on("keypress", function (a) {
						d._handleKeyboardClear(a, b)
					})
				}, d.prototype._handleClear = function (a, b) {
					if (!this.options.get("disabled")) {
						var d = this.$selection.find(".select2-selection__clear");
						if (0 !== d.length) {
							b.stopPropagation();
							var e = c.GetData(d[0], "data"),
								f = this.$element.val();
							this.$element.val(this.placeholder.id);
							var g = {
								data: e
							};
							if (this.trigger("clear", g), g.prevented) return void this.$element.val(f);
							for (var h = 0; h < e.length; h++)
								if (g = {
										data: e[h]
									}, this.trigger("unselect", g), g.prevented) return void this.$element.val(f);
							this.$element.trigger("change"), this.trigger("toggle", {})
						}
					}
				}, d.prototype._handleKeyboardClear = function (a, c, d) {
					d.isOpen() || c.which != b.DELETE && c.which != b.BACKSPACE || this._handleClear(c)
				}, d.prototype.update = function (b, d) {
					if (b.call(this, d), !(this.$selection.find(".select2-selection__placeholder").length > 0 || 0 === d.length)) {
						var e = a('<span class="select2-selection__clear">&times;</span>');
						c.StoreData(e[0], "data", d), this.$selection.find(".select2-selection__rendered").prepend(e)
					}
				}, d
			}), b.define("select2/selection/search", ["jquery", "../utils", "../keys"], function (a, b, c) {
				function d(a, b, c) {
					a.call(this, b, c)
				}
				return d.prototype.render = function (b) {
					var c = a('<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" /></li>');
					this.$searchContainer = c, this.$search = c.find("input");
					var d = b.call(this);
					return this._transferTabIndex(), d
				}, d.prototype.bind = function (a, d, e) {
					var f = this;
					a.call(this, d, e), d.on("open", function () {
						f.$search.trigger("focus")
					}), d.on("close", function () {
						f.$search.val(""), f.$search.removeAttr("aria-activedescendant"), f.$search.trigger("focus")
					}), d.on("enable", function () {
						f.$search.prop("disabled", !1), f._transferTabIndex()
					}), d.on("disable", function () {
						f.$search.prop("disabled", !0)
					}), d.on("focus", function (a) {
						f.$search.trigger("focus")
					}), d.on("results:focus", function (a) {
						f.$search.attr("aria-activedescendant", a.id)
					}), this.$selection.on("focusin", ".select2-search--inline", function (a) {
						f.trigger("focus", a)
					}), this.$selection.on("focusout", ".select2-search--inline", function (a) {
						f._handleBlur(a)
					}), this.$selection.on("keydown", ".select2-search--inline", function (a) {
						if (a.stopPropagation(), f.trigger("keypress", a), f._keyUpPrevented = a.isDefaultPrevented(), a.which === c.BACKSPACE && "" === f.$search.val()) {
							var d = f.$searchContainer.prev(".select2-selection__choice");
							if (d.length > 0) {
								var e = b.GetData(d[0], "data");
								f.searchRemoveChoice(e), a.preventDefault()
							}
						}
					});
					var g = document.documentMode,
						h = g && g <= 11;
					this.$selection.on("input.searchcheck", ".select2-search--inline", function (a) {
						if (h) return void f.$selection.off("input.search input.searchcheck");
						f.$selection.off("keyup.search")
					}), this.$selection.on("keyup.search input.search", ".select2-search--inline", function (a) {
						if (h && "input" === a.type) return void f.$selection.off("input.search input.searchcheck");
						var b = a.which;
						b != c.SHIFT && b != c.CTRL && b != c.ALT && b != c.TAB && f.handleSearch(a)
					})
				}, d.prototype._transferTabIndex = function (a) {
					this.$search.attr("tabindex", this.$selection.attr("tabindex")), this.$selection.attr("tabindex", "-1")
				}, d.prototype.createPlaceholder = function (a, b) {
					this.$search.attr("placeholder", b.text)
				}, d.prototype.update = function (a, b) {
					var c = this.$search[0] == document.activeElement;
					this.$search.attr("placeholder", ""), a.call(this, b), this.$selection.find(".select2-selection__rendered").append(this.$searchContainer), this.resizeSearch(), c && this.$search.focus()
				}, d.prototype.handleSearch = function () {
					if (this.resizeSearch(), !this._keyUpPrevented) {
						var a = this.$search.val();
						this.trigger("query", {
							term: a
						})
					}
					this._keyUpPrevented = !1
				}, d.prototype.searchRemoveChoice = function (a, b) {
					this.trigger("unselect", {
						data: b
					}), this.$search.val(b.text), this.handleSearch()
				}, d.prototype.resizeSearch = function () {
					this.$search.css("width", "25px");
					var a = "";
					if ("" !== this.$search.attr("placeholder")) a = this.$selection.find(".select2-selection__rendered").innerWidth();
					else {
						a = .75 * (this.$search.val().length + 1) + "em"
					}
					this.$search.css("width", a)
				}, d
			}), b.define("select2/selection/eventRelay", ["jquery"], function (a) {
				function b() {}
				return b.prototype.bind = function (b, c, d) {
					var e = this,
						f = ["open", "opening", "close", "closing", "select", "selecting", "unselect", "unselecting", "clear", "clearing"],
						g = ["opening", "closing", "selecting", "unselecting", "clearing"];
					b.call(this, c, d), c.on("*", function (b, c) {
						if (-1 !== a.inArray(b, f)) {
							c = c || {};
							var d = a.Event("select2:" + b, {
								params: c
							});
							e.$element.trigger(d), -1 !== a.inArray(b, g) && (c.prevented = d.isDefaultPrevented())
						}
					})
				}, b
			}), b.define("select2/translation", ["jquery", "require"], function (a, b) {
				function c(a) {
					this.dict = a || {}
				}
				return c.prototype.all = function () {
					return this.dict
				}, c.prototype.get = function (a) {
					return this.dict[a]
				}, c.prototype.extend = function (b) {
					this.dict = a.extend({}, b.all(), this.dict)
				}, c._cache = {}, c.loadPath = function (a) {
					if (!(a in c._cache)) {
						var d = b(a);
						c._cache[a] = d
					}
					return new c(c._cache[a])
				}, c
			}), b.define("select2/diacritics", [], function () {
				return {
					"Ⓐ": "A",
					"Ａ": "A",
					"À": "A",
					"Á": "A",
					"Â": "A",
					"Ầ": "A",
					"Ấ": "A",
					"Ẫ": "A",
					"Ẩ": "A",
					"Ã": "A",
					"Ā": "A",
					"Ă": "A",
					"Ằ": "A",
					"Ắ": "A",
					"Ẵ": "A",
					"Ẳ": "A",
					"Ȧ": "A",
					"Ǡ": "A",
					"Ä": "A",
					"Ǟ": "A",
					"Ả": "A",
					"Å": "A",
					"Ǻ": "A",
					"Ǎ": "A",
					"Ȁ": "A",
					"Ȃ": "A",
					"Ạ": "A",
					"Ậ": "A",
					"Ặ": "A",
					"Ḁ": "A",
					"Ą": "A",
					"Ⱥ": "A",
					"Ɐ": "A",
					"Ꜳ": "AA",
					"Æ": "AE",
					"Ǽ": "AE",
					"Ǣ": "AE",
					"Ꜵ": "AO",
					"Ꜷ": "AU",
					"Ꜹ": "AV",
					"Ꜻ": "AV",
					"Ꜽ": "AY",
					"Ⓑ": "B",
					"Ｂ": "B",
					"Ḃ": "B",
					"Ḅ": "B",
					"Ḇ": "B",
					"Ƀ": "B",
					"Ƃ": "B",
					"Ɓ": "B",
					"Ⓒ": "C",
					"Ｃ": "C",
					"Ć": "C",
					"Ĉ": "C",
					"Ċ": "C",
					"Č": "C",
					"Ç": "C",
					"Ḉ": "C",
					"Ƈ": "C",
					"Ȼ": "C",
					"Ꜿ": "C",
					"Ⓓ": "D",
					"Ｄ": "D",
					"Ḋ": "D",
					"Ď": "D",
					"Ḍ": "D",
					"Ḑ": "D",
					"Ḓ": "D",
					"Ḏ": "D",
					"Đ": "D",
					"Ƌ": "D",
					"Ɗ": "D",
					"Ɖ": "D",
					"Ꝺ": "D",
					"Ǳ": "DZ",
					"Ǆ": "DZ",
					"ǲ": "Dz",
					"ǅ": "Dz",
					"Ⓔ": "E",
					"Ｅ": "E",
					"È": "E",
					"É": "E",
					"Ê": "E",
					"Ề": "E",
					"Ế": "E",
					"Ễ": "E",
					"Ể": "E",
					"Ẽ": "E",
					"Ē": "E",
					"Ḕ": "E",
					"Ḗ": "E",
					"Ĕ": "E",
					"Ė": "E",
					"Ë": "E",
					"Ẻ": "E",
					"Ě": "E",
					"Ȅ": "E",
					"Ȇ": "E",
					"Ẹ": "E",
					"Ệ": "E",
					"Ȩ": "E",
					"Ḝ": "E",
					"Ę": "E",
					"Ḙ": "E",
					"Ḛ": "E",
					"Ɛ": "E",
					"Ǝ": "E",
					"Ⓕ": "F",
					"Ｆ": "F",
					"Ḟ": "F",
					"Ƒ": "F",
					"Ꝼ": "F",
					"Ⓖ": "G",
					"Ｇ": "G",
					"Ǵ": "G",
					"Ĝ": "G",
					"Ḡ": "G",
					"Ğ": "G",
					"Ġ": "G",
					"Ǧ": "G",
					"Ģ": "G",
					"Ǥ": "G",
					"Ɠ": "G",
					"Ꞡ": "G",
					"Ᵹ": "G",
					"Ꝿ": "G",
					"Ⓗ": "H",
					"Ｈ": "H",
					"Ĥ": "H",
					"Ḣ": "H",
					"Ḧ": "H",
					"Ȟ": "H",
					"Ḥ": "H",
					"Ḩ": "H",
					"Ḫ": "H",
					"Ħ": "H",
					"Ⱨ": "H",
					"Ⱶ": "H",
					"Ɥ": "H",
					"Ⓘ": "I",
					"Ｉ": "I",
					"Ì": "I",
					"Í": "I",
					"Î": "I",
					"Ĩ": "I",
					"Ī": "I",
					"Ĭ": "I",
					"İ": "I",
					"Ï": "I",
					"Ḯ": "I",
					"Ỉ": "I",
					"Ǐ": "I",
					"Ȉ": "I",
					"Ȋ": "I",
					"Ị": "I",
					"Į": "I",
					"Ḭ": "I",
					"Ɨ": "I",
					"Ⓙ": "J",
					"Ｊ": "J",
					"Ĵ": "J",
					"Ɉ": "J",
					"Ⓚ": "K",
					"Ｋ": "K",
					"Ḱ": "K",
					"Ǩ": "K",
					"Ḳ": "K",
					"Ķ": "K",
					"Ḵ": "K",
					"Ƙ": "K",
					"Ⱪ": "K",
					"Ꝁ": "K",
					"Ꝃ": "K",
					"Ꝅ": "K",
					"Ꞣ": "K",
					"Ⓛ": "L",
					"Ｌ": "L",
					"Ŀ": "L",
					"Ĺ": "L",
					"Ľ": "L",
					"Ḷ": "L",
					"Ḹ": "L",
					"Ļ": "L",
					"Ḽ": "L",
					"Ḻ": "L",
					"Ł": "L",
					"Ƚ": "L",
					"Ɫ": "L",
					"Ⱡ": "L",
					"Ꝉ": "L",
					"Ꝇ": "L",
					"Ꞁ": "L",
					"Ǉ": "LJ",
					"ǈ": "Lj",
					"Ⓜ": "M",
					"Ｍ": "M",
					"Ḿ": "M",
					"Ṁ": "M",
					"Ṃ": "M",
					"Ɱ": "M",
					"Ɯ": "M",
					"Ⓝ": "N",
					"Ｎ": "N",
					"Ǹ": "N",
					"Ń": "N",
					"Ñ": "N",
					"Ṅ": "N",
					"Ň": "N",
					"Ṇ": "N",
					"Ņ": "N",
					"Ṋ": "N",
					"Ṉ": "N",
					"Ƞ": "N",
					"Ɲ": "N",
					"Ꞑ": "N",
					"Ꞥ": "N",
					"Ǌ": "NJ",
					"ǋ": "Nj",
					"Ⓞ": "O",
					"Ｏ": "O",
					"Ò": "O",
					"Ó": "O",
					"Ô": "O",
					"Ồ": "O",
					"Ố": "O",
					"Ỗ": "O",
					"Ổ": "O",
					"Õ": "O",
					"Ṍ": "O",
					"Ȭ": "O",
					"Ṏ": "O",
					"Ō": "O",
					"Ṑ": "O",
					"Ṓ": "O",
					"Ŏ": "O",
					"Ȯ": "O",
					"Ȱ": "O",
					"Ö": "O",
					"Ȫ": "O",
					"Ỏ": "O",
					"Ő": "O",
					"Ǒ": "O",
					"Ȍ": "O",
					"Ȏ": "O",
					"Ơ": "O",
					"Ờ": "O",
					"Ớ": "O",
					"Ỡ": "O",
					"Ở": "O",
					"Ợ": "O",
					"Ọ": "O",
					"Ộ": "O",
					"Ǫ": "O",
					"Ǭ": "O",
					"Ø": "O",
					"Ǿ": "O",
					"Ɔ": "O",
					"Ɵ": "O",
					"Ꝋ": "O",
					"Ꝍ": "O",
					"Ƣ": "OI",
					"Ꝏ": "OO",
					"Ȣ": "OU",
					"Ⓟ": "P",
					"Ｐ": "P",
					"Ṕ": "P",
					"Ṗ": "P",
					"Ƥ": "P",
					"Ᵽ": "P",
					"Ꝑ": "P",
					"Ꝓ": "P",
					"Ꝕ": "P",
					"Ⓠ": "Q",
					"Ｑ": "Q",
					"Ꝗ": "Q",
					"Ꝙ": "Q",
					"Ɋ": "Q",
					"Ⓡ": "R",
					"Ｒ": "R",
					"Ŕ": "R",
					"Ṙ": "R",
					"Ř": "R",
					"Ȑ": "R",
					"Ȓ": "R",
					"Ṛ": "R",
					"Ṝ": "R",
					"Ŗ": "R",
					"Ṟ": "R",
					"Ɍ": "R",
					"Ɽ": "R",
					"Ꝛ": "R",
					"Ꞧ": "R",
					"Ꞃ": "R",
					"Ⓢ": "S",
					"Ｓ": "S",
					"ẞ": "S",
					"Ś": "S",
					"Ṥ": "S",
					"Ŝ": "S",
					"Ṡ": "S",
					"Š": "S",
					"Ṧ": "S",
					"Ṣ": "S",
					"Ṩ": "S",
					"Ș": "S",
					"Ş": "S",
					"Ȿ": "S",
					"Ꞩ": "S",
					"Ꞅ": "S",
					"Ⓣ": "T",
					"Ｔ": "T",
					"Ṫ": "T",
					"Ť": "T",
					"Ṭ": "T",
					"Ț": "T",
					"Ţ": "T",
					"Ṱ": "T",
					"Ṯ": "T",
					"Ŧ": "T",
					"Ƭ": "T",
					"Ʈ": "T",
					"Ⱦ": "T",
					"Ꞇ": "T",
					"Ꜩ": "TZ",
					"Ⓤ": "U",
					"Ｕ": "U",
					"Ù": "U",
					"Ú": "U",
					"Û": "U",
					"Ũ": "U",
					"Ṹ": "U",
					"Ū": "U",
					"Ṻ": "U",
					"Ŭ": "U",
					"Ü": "U",
					"Ǜ": "U",
					"Ǘ": "U",
					"Ǖ": "U",
					"Ǚ": "U",
					"Ủ": "U",
					"Ů": "U",
					"Ű": "U",
					"Ǔ": "U",
					"Ȕ": "U",
					"Ȗ": "U",
					"Ư": "U",
					"Ừ": "U",
					"Ứ": "U",
					"Ữ": "U",
					"Ử": "U",
					"Ự": "U",
					"Ụ": "U",
					"Ṳ": "U",
					"Ų": "U",
					"Ṷ": "U",
					"Ṵ": "U",
					"Ʉ": "U",
					"Ⓥ": "V",
					"Ｖ": "V",
					"Ṽ": "V",
					"Ṿ": "V",
					"Ʋ": "V",
					"Ꝟ": "V",
					"Ʌ": "V",
					"Ꝡ": "VY",
					"Ⓦ": "W",
					"Ｗ": "W",
					"Ẁ": "W",
					"Ẃ": "W",
					"Ŵ": "W",
					"Ẇ": "W",
					"Ẅ": "W",
					"Ẉ": "W",
					"Ⱳ": "W",
					"Ⓧ": "X",
					"Ｘ": "X",
					"Ẋ": "X",
					"Ẍ": "X",
					"Ⓨ": "Y",
					"Ｙ": "Y",
					"Ỳ": "Y",
					"Ý": "Y",
					"Ŷ": "Y",
					"Ỹ": "Y",
					"Ȳ": "Y",
					"Ẏ": "Y",
					"Ÿ": "Y",
					"Ỷ": "Y",
					"Ỵ": "Y",
					"Ƴ": "Y",
					"Ɏ": "Y",
					"Ỿ": "Y",
					"Ⓩ": "Z",
					"Ｚ": "Z",
					"Ź": "Z",
					"Ẑ": "Z",
					"Ż": "Z",
					"Ž": "Z",
					"Ẓ": "Z",
					"Ẕ": "Z",
					"Ƶ": "Z",
					"Ȥ": "Z",
					"Ɀ": "Z",
					"Ⱬ": "Z",
					"Ꝣ": "Z",
					"ⓐ": "a",
					"ａ": "a",
					"ẚ": "a",
					"à": "a",
					"á": "a",
					"â": "a",
					"ầ": "a",
					"ấ": "a",
					"ẫ": "a",
					"ẩ": "a",
					"ã": "a",
					"ā": "a",
					"ă": "a",
					"ằ": "a",
					"ắ": "a",
					"ẵ": "a",
					"ẳ": "a",
					"ȧ": "a",
					"ǡ": "a",
					"ä": "a",
					"ǟ": "a",
					"ả": "a",
					"å": "a",
					"ǻ": "a",
					"ǎ": "a",
					"ȁ": "a",
					"ȃ": "a",
					"ạ": "a",
					"ậ": "a",
					"ặ": "a",
					"ḁ": "a",
					"ą": "a",
					"ⱥ": "a",
					"ɐ": "a",
					"ꜳ": "aa",
					"æ": "ae",
					"ǽ": "ae",
					"ǣ": "ae",
					"ꜵ": "ao",
					"ꜷ": "au",
					"ꜹ": "av",
					"ꜻ": "av",
					"ꜽ": "ay",
					"ⓑ": "b",
					"ｂ": "b",
					"ḃ": "b",
					"ḅ": "b",
					"ḇ": "b",
					"ƀ": "b",
					"ƃ": "b",
					"ɓ": "b",
					"ⓒ": "c",
					"ｃ": "c",
					"ć": "c",
					"ĉ": "c",
					"ċ": "c",
					"č": "c",
					"ç": "c",
					"ḉ": "c",
					"ƈ": "c",
					"ȼ": "c",
					"ꜿ": "c",
					"ↄ": "c",
					"ⓓ": "d",
					"ｄ": "d",
					"ḋ": "d",
					"ď": "d",
					"ḍ": "d",
					"ḑ": "d",
					"ḓ": "d",
					"ḏ": "d",
					"đ": "d",
					"ƌ": "d",
					"ɖ": "d",
					"ɗ": "d",
					"ꝺ": "d",
					"ǳ": "dz",
					"ǆ": "dz",
					"ⓔ": "e",
					"ｅ": "e",
					"è": "e",
					"é": "e",
					"ê": "e",
					"ề": "e",
					"ế": "e",
					"ễ": "e",
					"ể": "e",
					"ẽ": "e",
					"ē": "e",
					"ḕ": "e",
					"ḗ": "e",
					"ĕ": "e",
					"ė": "e",
					"ë": "e",
					"ẻ": "e",
					"ě": "e",
					"ȅ": "e",
					"ȇ": "e",
					"ẹ": "e",
					"ệ": "e",
					"ȩ": "e",
					"ḝ": "e",
					"ę": "e",
					"ḙ": "e",
					"ḛ": "e",
					"ɇ": "e",
					"ɛ": "e",
					"ǝ": "e",
					"ⓕ": "f",
					"ｆ": "f",
					"ḟ": "f",
					"ƒ": "f",
					"ꝼ": "f",
					"ⓖ": "g",
					"ｇ": "g",
					"ǵ": "g",
					"ĝ": "g",
					"ḡ": "g",
					"ğ": "g",
					"ġ": "g",
					"ǧ": "g",
					"ģ": "g",
					"ǥ": "g",
					"ɠ": "g",
					"ꞡ": "g",
					"ᵹ": "g",
					"ꝿ": "g",
					"ⓗ": "h",
					"ｈ": "h",
					"ĥ": "h",
					"ḣ": "h",
					"ḧ": "h",
					"ȟ": "h",
					"ḥ": "h",
					"ḩ": "h",
					"ḫ": "h",
					"ẖ": "h",
					"ħ": "h",
					"ⱨ": "h",
					"ⱶ": "h",
					"ɥ": "h",
					"ƕ": "hv",
					"ⓘ": "i",
					"ｉ": "i",
					"ì": "i",
					"í": "i",
					"î": "i",
					"ĩ": "i",
					"ī": "i",
					"ĭ": "i",
					"ï": "i",
					"ḯ": "i",
					"ỉ": "i",
					"ǐ": "i",
					"ȉ": "i",
					"ȋ": "i",
					"ị": "i",
					"į": "i",
					"ḭ": "i",
					"ɨ": "i",
					"ı": "i",
					"ⓙ": "j",
					"ｊ": "j",
					"ĵ": "j",
					"ǰ": "j",
					"ɉ": "j",
					"ⓚ": "k",
					"ｋ": "k",
					"ḱ": "k",
					"ǩ": "k",
					"ḳ": "k",
					"ķ": "k",
					"ḵ": "k",
					"ƙ": "k",
					"ⱪ": "k",
					"ꝁ": "k",
					"ꝃ": "k",
					"ꝅ": "k",
					"ꞣ": "k",
					"ⓛ": "l",
					"ｌ": "l",
					"ŀ": "l",
					"ĺ": "l",
					"ľ": "l",
					"ḷ": "l",
					"ḹ": "l",
					"ļ": "l",
					"ḽ": "l",
					"ḻ": "l",
					"ſ": "l",
					"ł": "l",
					"ƚ": "l",
					"ɫ": "l",
					"ⱡ": "l",
					"ꝉ": "l",
					"ꞁ": "l",
					"ꝇ": "l",
					"ǉ": "lj",
					"ⓜ": "m",
					"ｍ": "m",
					"ḿ": "m",
					"ṁ": "m",
					"ṃ": "m",
					"ɱ": "m",
					"ɯ": "m",
					"ⓝ": "n",
					"ｎ": "n",
					"ǹ": "n",
					"ń": "n",
					"ñ": "n",
					"ṅ": "n",
					"ň": "n",
					"ṇ": "n",
					"ņ": "n",
					"ṋ": "n",
					"ṉ": "n",
					"ƞ": "n",
					"ɲ": "n",
					"ŉ": "n",
					"ꞑ": "n",
					"ꞥ": "n",
					"ǌ": "nj",
					"ⓞ": "o",
					"ｏ": "o",
					"ò": "o",
					"ó": "o",
					"ô": "o",
					"ồ": "o",
					"ố": "o",
					"ỗ": "o",
					"ổ": "o",
					"õ": "o",
					"ṍ": "o",
					"ȭ": "o",
					"ṏ": "o",
					"ō": "o",
					"ṑ": "o",
					"ṓ": "o",
					"ŏ": "o",
					"ȯ": "o",
					"ȱ": "o",
					"ö": "o",
					"ȫ": "o",
					"ỏ": "o",
					"ő": "o",
					"ǒ": "o",
					"ȍ": "o",
					"ȏ": "o",
					"ơ": "o",
					"ờ": "o",
					"ớ": "o",
					"ỡ": "o",
					"ở": "o",
					"ợ": "o",
					"ọ": "o",
					"ộ": "o",
					"ǫ": "o",
					"ǭ": "o",
					"ø": "o",
					"ǿ": "o",
					"ɔ": "o",
					"ꝋ": "o",
					"ꝍ": "o",
					"ɵ": "o",
					"ƣ": "oi",
					"ȣ": "ou",
					"ꝏ": "oo",
					"ⓟ": "p",
					"ｐ": "p",
					"ṕ": "p",
					"ṗ": "p",
					"ƥ": "p",
					"ᵽ": "p",
					"ꝑ": "p",
					"ꝓ": "p",
					"ꝕ": "p",
					"ⓠ": "q",
					"ｑ": "q",
					"ɋ": "q",
					"ꝗ": "q",
					"ꝙ": "q",
					"ⓡ": "r",
					"ｒ": "r",
					"ŕ": "r",
					"ṙ": "r",
					"ř": "r",
					"ȑ": "r",
					"ȓ": "r",
					"ṛ": "r",
					"ṝ": "r",
					"ŗ": "r",
					"ṟ": "r",
					"ɍ": "r",
					"ɽ": "r",
					"ꝛ": "r",
					"ꞧ": "r",
					"ꞃ": "r",
					"ⓢ": "s",
					"ｓ": "s",
					"ß": "s",
					"ś": "s",
					"ṥ": "s",
					"ŝ": "s",
					"ṡ": "s",
					"š": "s",
					"ṧ": "s",
					"ṣ": "s",
					"ṩ": "s",
					"ș": "s",
					"ş": "s",
					"ȿ": "s",
					"ꞩ": "s",
					"ꞅ": "s",
					"ẛ": "s",
					"ⓣ": "t",
					"ｔ": "t",
					"ṫ": "t",
					"ẗ": "t",
					"ť": "t",
					"ṭ": "t",
					"ț": "t",
					"ţ": "t",
					"ṱ": "t",
					"ṯ": "t",
					"ŧ": "t",
					"ƭ": "t",
					"ʈ": "t",
					"ⱦ": "t",
					"ꞇ": "t",
					"ꜩ": "tz",
					"ⓤ": "u",
					"ｕ": "u",
					"ù": "u",
					"ú": "u",
					"û": "u",
					"ũ": "u",
					"ṹ": "u",
					"ū": "u",
					"ṻ": "u",
					"ŭ": "u",
					"ü": "u",
					"ǜ": "u",
					"ǘ": "u",
					"ǖ": "u",
					"ǚ": "u",
					"ủ": "u",
					"ů": "u",
					"ű": "u",
					"ǔ": "u",
					"ȕ": "u",
					"ȗ": "u",
					"ư": "u",
					"ừ": "u",
					"ứ": "u",
					"ữ": "u",
					"ử": "u",
					"ự": "u",
					"ụ": "u",
					"ṳ": "u",
					"ų": "u",
					"ṷ": "u",
					"ṵ": "u",
					"ʉ": "u",
					"ⓥ": "v",
					"ｖ": "v",
					"ṽ": "v",
					"ṿ": "v",
					"ʋ": "v",
					"ꝟ": "v",
					"ʌ": "v",
					"ꝡ": "vy",
					"ⓦ": "w",
					"ｗ": "w",
					"ẁ": "w",
					"ẃ": "w",
					"ŵ": "w",
					"ẇ": "w",
					"ẅ": "w",
					"ẘ": "w",
					"ẉ": "w",
					"ⱳ": "w",
					"ⓧ": "x",
					"ｘ": "x",
					"ẋ": "x",
					"ẍ": "x",
					"ⓨ": "y",
					"ｙ": "y",
					"ỳ": "y",
					"ý": "y",
					"ŷ": "y",
					"ỹ": "y",
					"ȳ": "y",
					"ẏ": "y",
					"ÿ": "y",
					"ỷ": "y",
					"ẙ": "y",
					"ỵ": "y",
					"ƴ": "y",
					"ɏ": "y",
					"ỿ": "y",
					"ⓩ": "z",
					"ｚ": "z",
					"ź": "z",
					"ẑ": "z",
					"ż": "z",
					"ž": "z",
					"ẓ": "z",
					"ẕ": "z",
					"ƶ": "z",
					"ȥ": "z",
					"ɀ": "z",
					"ⱬ": "z",
					"ꝣ": "z",
					"Ά": "Α",
					"Έ": "Ε",
					"Ή": "Η",
					"Ί": "Ι",
					"Ϊ": "Ι",
					"Ό": "Ο",
					"Ύ": "Υ",
					"Ϋ": "Υ",
					"Ώ": "Ω",
					"ά": "α",
					"έ": "ε",
					"ή": "η",
					"ί": "ι",
					"ϊ": "ι",
					"ΐ": "ι",
					"ό": "ο",
					"ύ": "υ",
					"ϋ": "υ",
					"ΰ": "υ",
					"ω": "ω",
					"ς": "σ"
				}
			}), b.define("select2/data/base", ["../utils"], function (a) {
				function b(a, c) {
					b.__super__.constructor.call(this)
				}
				return a.Extend(b, a.Observable), b.prototype.current = function (a) {
					throw new Error("The `current` method must be defined in child classes.")
				}, b.prototype.query = function (a, b) {
					throw new Error("The `query` method must be defined in child classes.")
				}, b.prototype.bind = function (a, b) {}, b.prototype.destroy = function () {}, b.prototype.generateResultId = function (b, c) {
					var d = b.id + "-result-";
					return d += a.generateChars(4), null != c.id ? d += "-" + c.id.toString() : d += "-" + a.generateChars(4), d
				}, b
			}), b.define("select2/data/select", ["./base", "../utils", "jquery"], function (a, b, c) {
				function d(a, b) {
					this.$element = a, this.options = b, d.__super__.constructor.call(this)
				}
				return b.Extend(d, a), d.prototype.current = function (a) {
					var b = [],
						d = this;
					this.$element.find(":selected").each(function () {
						var a = c(this),
							e = d.item(a);
						b.push(e)
					}), a(b)
				}, d.prototype.select = function (a) {
					var b = this;
					if (a.selected = !0, c(a.element).is("option")) return a.element.selected = !0, void this.$element.trigger("change");
					if (this.$element.prop("multiple")) this.current(function (d) {
						var e = [];
						a = [a], a.push.apply(a, d);
						for (var f = 0; f < a.length; f++) {
							var g = a[f].id; - 1 === c.inArray(g, e) && e.push(g)
						}
						b.$element.val(e), b.$element.trigger("change")
					});
					else {
						var d = a.id;
						this.$element.val(d), this.$element.trigger("change")
					}
				}, d.prototype.unselect = function (a) {
					var b = this;
					if (this.$element.prop("multiple")) {
						if (a.selected = !1, c(a.element).is("option")) return a.element.selected = !1, void this.$element.trigger("change");
						this.current(function (d) {
							for (var e = [], f = 0; f < d.length; f++) {
								var g = d[f].id;
								g !== a.id && -1 === c.inArray(g, e) && e.push(g)
							}
							b.$element.val(e), b.$element.trigger("change")
						})
					}
				}, d.prototype.bind = function (a, b) {
					var c = this;
					this.container = a, a.on("select", function (a) {
						c.select(a.data)
					}), a.on("unselect", function (a) {
						c.unselect(a.data)
					})
				}, d.prototype.destroy = function () {
					this.$element.find("*").each(function () {
						b.RemoveData(this)
					})
				}, d.prototype.query = function (a, b) {
					var d = [],
						e = this;
					this.$element.children().each(function () {
						var b = c(this);
						if (b.is("option") || b.is("optgroup")) {
							var f = e.item(b),
								g = e.matches(a, f);
							null !== g && d.push(g)
						}
					}), b({
						results: d
					})
				}, d.prototype.addOptions = function (a) {
					b.appendMany(this.$element, a)
				}, d.prototype.option = function (a) {
					var d;
					a.children ? (d = document.createElement("optgroup"), d.label = a.text) : (d = document.createElement("option"), void 0 !== d.textContent ? d.textContent = a.text : d.innerText = a.text), void 0 !== a.id && (d.value = a.id), a.disabled && (d.disabled = !0), a.selected && (d.selected = !0), a.title && (d.title = a.title);
					var e = c(d),
						f = this._normalizeItem(a);
					return f.element = d, b.StoreData(d, "data", f), e
				}, d.prototype.item = function (a) {
					var d = {};
					if (null != (d = b.GetData(a[0], "data"))) return d;
					if (a.is("option")) d = {
						id: a.val(),
						text: a.text(),
						disabled: a.prop("disabled"),
						selected: a.prop("selected"),
						title: a.prop("title")
					};
					else if (a.is("optgroup")) {
						d = {
							text: a.prop("label"),
							children: [],
							title: a.prop("title")
						};
						for (var e = a.children("option"), f = [], g = 0; g < e.length; g++) {
							var h = c(e[g]),
								i = this.item(h);
							f.push(i)
						}
						d.children = f
					}
					return d = this._normalizeItem(d), d.element = a[0], b.StoreData(a[0], "data", d), d
				}, d.prototype._normalizeItem = function (a) {
					a !== Object(a) && (a = {
						id: a,
						text: a
					}), a = c.extend({}, {
						text: ""
					}, a);
					var b = {
						selected: !1,
						disabled: !1
					};
					return null != a.id && (a.id = a.id.toString()), null != a.text && (a.text = a.text.toString()), null == a._resultId && a.id && null != this.container && (a._resultId = this.generateResultId(this.container, a)), c.extend({}, b, a)
				}, d.prototype.matches = function (a, b) {
					return this.options.get("matcher")(a, b)
				}, d
			}), b.define("select2/data/array", ["./select", "../utils", "jquery"], function (a, b, c) {
				function d(a, b) {
					var c = b.get("data") || [];
					d.__super__.constructor.call(this, a, b), this.addOptions(this.convertToOptions(c))
				}
				return b.Extend(d, a), d.prototype.select = function (a) {
					var b = this.$element.find("option").filter(function (b, c) {
						return c.value == a.id.toString()
					});
					0 === b.length && (b = this.option(a), this.addOptions(b)), d.__super__.select.call(this, a)
				}, d.prototype.convertToOptions = function (a) {
					function d(a) {
						return function () {
							return c(this).val() == a.id
						}
					}
					for (var e = this, f = this.$element.find("option"), g = f.map(function () {
							return e.item(c(this)).id
						}).get(), h = [], i = 0; i < a.length; i++) {
						var j = this._normalizeItem(a[i]);
						if (c.inArray(j.id, g) >= 0) {
							var k = f.filter(d(j)),
								l = this.item(k),
								m = c.extend(!0, {}, j, l),
								n = this.option(m);
							k.replaceWith(n)
						} else {
							var o = this.option(j);
							if (j.children) {
								var p = this.convertToOptions(j.children);
								b.appendMany(o, p)
							}
							h.push(o)
						}
					}
					return h
				}, d
			}), b.define("select2/data/ajax", ["./array", "../utils", "jquery"], function (a, b, c) {
				function d(a, b) {
					this.ajaxOptions = this._applyDefaults(b.get("ajax")), null != this.ajaxOptions.processResults && (this.processResults = this.ajaxOptions.processResults), d.__super__.constructor.call(this, a, b)
				}
				return b.Extend(d, a), d.prototype._applyDefaults = function (a) {
					var b = {
						data: function (a) {
							return c.extend({}, a, {
								q: a.term
							})
						},
						transport: function (a, b, d) {
							var e = c.ajax(a);
							return e.then(b), e.fail(d), e
						}
					};
					return c.extend({}, b, a, !0)
				}, d.prototype.processResults = function (a) {
					return a
				}, d.prototype.query = function (a, b) {
					function d() {
						var d = f.transport(f, function (d) {
							var f = e.processResults(d, a);
							e.options.get("debug") && window.console && console.error && (f && f.results && c.isArray(f.results) || console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")), b(f)
						}, function () {
							"status" in d && (0 === d.status || "0" === d.status) || e.trigger("results:message", {
								message: "errorLoading"
							})
						});
						e._request = d
					}
					var e = this;
					null != this._request && (c.isFunction(this._request.abort) && this._request.abort(), this._request = null);
					var f = c.extend({
						type: "GET"
					}, this.ajaxOptions);
					"function" == typeof f.url && (f.url = f.url.call(this.$element, a)), "function" == typeof f.data && (f.data = f.data.call(this.$element, a)), this.ajaxOptions.delay && null != a.term ? (this._queryTimeout && window.clearTimeout(this._queryTimeout), this._queryTimeout = window.setTimeout(d, this.ajaxOptions.delay)) : d()
				}, d
			}), b.define("select2/data/tags", ["jquery"], function (a) {
				function b(b, c, d) {
					var e = d.get("tags"),
						f = d.get("createTag");
					void 0 !== f && (this.createTag = f);
					var g = d.get("insertTag");
					if (void 0 !== g && (this.insertTag = g), b.call(this, c, d), a.isArray(e))
						for (var h = 0; h < e.length; h++) {
							var i = e[h],
								j = this._normalizeItem(i),
								k = this.option(j);
							this.$element.append(k)
						}
				}
				return b.prototype.query = function (a, b, c) {
					function d(a, f) {
						for (var g = a.results, h = 0; h < g.length; h++) {
							var i = g[h],
								j = null != i.children && !d({
									results: i.children
								}, !0);
							if ((i.text || "").toUpperCase() === (b.term || "").toUpperCase() || j) return !f && (a.data = g, void c(a))
						}
						if (f) return !0;
						var k = e.createTag(b);
						if (null != k) {
							var l = e.option(k);
							l.attr("data-select2-tag", !0), e.addOptions([l]), e.insertTag(g, k)
						}
						a.results = g, c(a)
					}
					var e = this;
					if (this._removeOldTags(), null == b.term || null != b.page) return void a.call(this, b, c);
					a.call(this, b, d)
				}, b.prototype.createTag = function (b, c) {
					var d = a.trim(c.term);
					return "" === d ? null : {
						id: d,
						text: d
					}
				}, b.prototype.insertTag = function (a, b, c) {
					b.unshift(c)
				}, b.prototype._removeOldTags = function (b) {
					this._lastTag;
					this.$element.find("option[data-select2-tag]").each(function () {
						this.selected || a(this).remove()
					})
				}, b
			}), b.define("select2/data/tokenizer", ["jquery"], function (a) {
				function b(a, b, c) {
					var d = c.get("tokenizer");
					void 0 !== d && (this.tokenizer = d), a.call(this, b, c)
				}
				return b.prototype.bind = function (a, b, c) {
					a.call(this, b, c), this.$search = b.dropdown.$search || b.selection.$search || c.find(".select2-search__field")
				}, b.prototype.query = function (b, c, d) {
					function e(b) {
						var c = g._normalizeItem(b);
						if (!g.$element.find("option").filter(function () {
								return a(this).val() === c.id
							}).length) {
							var d = g.option(c);
							d.attr("data-select2-tag", !0), g._removeOldTags(), g.addOptions([d])
						}
						f(c)
					}

					function f(a) {
						g.trigger("select", {
							data: a
						})
					}
					var g = this;
					c.term = c.term || "";
					var h = this.tokenizer(c, this.options, e);
					h.term !== c.term && (this.$search.length && (this.$search.val(h.term), this.$search.focus()), c.term = h.term), b.call(this, c, d)
				}, b.prototype.tokenizer = function (b, c, d, e) {
					for (var f = d.get("tokenSeparators") || [], g = c.term, h = 0, i = this.createTag || function (a) {
							return {
								id: a.term,
								text: a.term
							}
						}; h < g.length;) {
						var j = g[h];
						if (-1 !== a.inArray(j, f)) {
							var k = g.substr(0, h),
								l = a.extend({}, c, {
									term: k
								}),
								m = i(l);
							null != m ? (e(m), g = g.substr(h + 1) || "", h = 0) : h++
						} else h++
					}
					return {
						term: g
					}
				}, b
			}), b.define("select2/data/minimumInputLength", [], function () {
				function a(a, b, c) {
					this.minimumInputLength = c.get("minimumInputLength"), a.call(this, b, c)
				}
				return a.prototype.query = function (a, b, c) {
					if (b.term = b.term || "", b.term.length < this.minimumInputLength) return void this.trigger("results:message", {
						message: "inputTooShort",
						args: {
							minimum: this.minimumInputLength,
							input: b.term,
							params: b
						}
					});
					a.call(this, b, c)
				}, a
			}), b.define("select2/data/maximumInputLength", [], function () {
				function a(a, b, c) {
					this.maximumInputLength = c.get("maximumInputLength"), a.call(this, b, c)
				}
				return a.prototype.query = function (a, b, c) {
					if (b.term = b.term || "", this.maximumInputLength > 0 && b.term.length > this.maximumInputLength) return void this.trigger("results:message", {
						message: "inputTooLong",
						args: {
							maximum: this.maximumInputLength,
							input: b.term,
							params: b
						}
					});
					a.call(this, b, c)
				}, a
			}), b.define("select2/data/maximumSelectionLength", [], function () {
				function a(a, b, c) {
					this.maximumSelectionLength = c.get("maximumSelectionLength"), a.call(this, b, c)
				}
				return a.prototype.query = function (a, b, c) {
					var d = this;
					this.current(function (e) {
						var f = null != e ? e.length : 0;
						if (d.maximumSelectionLength > 0 && f >= d.maximumSelectionLength) return void d.trigger("results:message", {
							message: "maximumSelected",
							args: {
								maximum: d.maximumSelectionLength
							}
						});
						a.call(d, b, c)
					})
				}, a
			}), b.define("select2/dropdown", ["jquery", "./utils"], function (a, b) {
				function c(a, b) {
					this.$element = a, this.options = b, c.__super__.constructor.call(this)
				}
				return b.Extend(c, b.Observable), c.prototype.render = function () {
					var b = a('<span class="select2-dropdown"><span class="select2-results"></span></span>');
					return b.attr("dir", this.options.get("dir")), this.$dropdown = b, b
				}, c.prototype.bind = function () {}, c.prototype.position = function (a, b) {}, c.prototype.destroy = function () {
					this.$dropdown.remove()
				}, c
			}), b.define("select2/dropdown/search", ["jquery", "../utils"], function (a, b) {
				function c() {}
				return c.prototype.render = function (b) {
					var c = b.call(this),
						d = a('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" /></span>');
					return this.$searchContainer = d, this.$search = d.find("input"), c.prepend(d), c
				}, c.prototype.bind = function (b, c, d) {
					var e = this;
					b.call(this, c, d), this.$search.on("keydown", function (a) {
						e.trigger("keypress", a), e._keyUpPrevented = a.isDefaultPrevented()
					}), this.$search.on("input", function (b) {
						a(this).off("keyup")
					}), this.$search.on("keyup input", function (a) {
						e.handleSearch(a)
					}), c.on("open", function () {
						e.$search.attr("tabindex", 0), e.$search.focus(), window.setTimeout(function () {
							e.$search.focus()
						}, 0)
					}), c.on("close", function () {
						e.$search.attr("tabindex", -1), e.$search.val(""), e.$search.blur()
					}), c.on("focus", function () {
						c.isOpen() || e.$search.focus()
					}), c.on("results:all", function (a) {
						if (null == a.query.term || "" === a.query.term) {
							e.showSearch(a) ? e.$searchContainer.removeClass("select2-search--hide") : e.$searchContainer.addClass("select2-search--hide")
						}
					})
				}, c.prototype.handleSearch = function (a) {
					if (!this._keyUpPrevented) {
						var b = this.$search.val();
						this.trigger("query", {
							term: b
						})
					}
					this._keyUpPrevented = !1
				}, c.prototype.showSearch = function (a, b) {
					return !0
				}, c
			}), b.define("select2/dropdown/hidePlaceholder", [], function () {
				function a(a, b, c, d) {
					this.placeholder = this.normalizePlaceholder(c.get("placeholder")), a.call(this, b, c, d)
				}
				return a.prototype.append = function (a, b) {
					b.results = this.removePlaceholder(b.results), a.call(this, b)
				}, a.prototype.normalizePlaceholder = function (a, b) {
					return "string" == typeof b && (b = {
						id: "",
						text: b
					}), b
				}, a.prototype.removePlaceholder = function (a, b) {
					for (var c = b.slice(0), d = b.length - 1; d >= 0; d--) {
						var e = b[d];
						this.placeholder.id === e.id && c.splice(d, 1)
					}
					return c
				}, a
			}), b.define("select2/dropdown/infiniteScroll", ["jquery"], function (a) {
				function b(a, b, c, d) {
					this.lastParams = {}, a.call(this, b, c, d), this.$loadingMore = this.createLoadingMore(), this.loading = !1
				}
				return b.prototype.append = function (a, b) {
					this.$loadingMore.remove(), this.loading = !1, a.call(this, b), this.showLoadingMore(b) && this.$results.append(this.$loadingMore)
				}, b.prototype.bind = function (b, c, d) {
					var e = this;
					b.call(this, c, d), c.on("query", function (a) {
						e.lastParams = a, e.loading = !0
					}), c.on("query:append", function (a) {
						e.lastParams = a, e.loading = !0
					}), this.$results.on("scroll", function () {
						var b = a.contains(document.documentElement, e.$loadingMore[0]);
						if (!e.loading && b) {
							e.$results.offset().top + e.$results.outerHeight(!1) + 50 >= e.$loadingMore.offset().top + e.$loadingMore.outerHeight(!1) && e.loadMore()
						}
					})
				}, b.prototype.loadMore = function () {
					this.loading = !0;
					var b = a.extend({}, {
						page: 1
					}, this.lastParams);
					b.page++, this.trigger("query:append", b)
				}, b.prototype.showLoadingMore = function (a, b) {
					return b.pagination && b.pagination.more
				}, b.prototype.createLoadingMore = function () {
					var b = a('<li class="select2-results__option select2-results__option--load-more"role="treeitem" aria-disabled="true"></li>'),
						c = this.options.get("translations").get("loadingMore");
					return b.html(c(this.lastParams)), b
				}, b
			}), b.define("select2/dropdown/attachBody", ["jquery", "../utils"], function (a, b) {
				function c(b, c, d) {
					this.$dropdownParent = d.get("dropdownParent") || a(document.body), b.call(this, c, d)
				}
				return c.prototype.bind = function (a, b, c) {
					var d = this,
						e = !1;
					a.call(this, b, c), b.on("open", function () {
						d._showDropdown(), d._attachPositioningHandler(b), e || (e = !0, b.on("results:all", function () {
							d._positionDropdown(), d._resizeDropdown()
						}), b.on("results:append", function () {
							d._positionDropdown(), d._resizeDropdown()
						}))
					}), b.on("close", function () {
						d._hideDropdown(), d._detachPositioningHandler(b)
					}), this.$dropdownContainer.on("mousedown", function (a) {
						a.stopPropagation()
					})
				}, c.prototype.destroy = function (a) {
					a.call(this), this.$dropdownContainer.remove()
				}, c.prototype.position = function (a, b, c) {
					b.attr("class", c.attr("class")), b.removeClass("select2"), b.addClass("select2-container--open"), b.css({
						position: "absolute",
						top: -999999
					}), this.$container = c
				}, c.prototype.render = function (b) {
					var c = a("<span></span>"),
						d = b.call(this);
					return c.append(d), this.$dropdownContainer = c, c
				}, c.prototype._hideDropdown = function (a) {
					this.$dropdownContainer.detach()
				}, c.prototype._attachPositioningHandler = function (c, d) {
					var e = this,
						f = "scroll.select2." + d.id,
						g = "resize.select2." + d.id,
						h = "orientationchange.select2." + d.id,
						i = this.$container.parents().filter(b.hasScroll);
					i.each(function () {
						b.StoreData(this, "select2-scroll-position", {
							x: a(this).scrollLeft(),
							y: a(this).scrollTop()
						})
					}), i.on(f, function (c) {
						var d = b.GetData(this, "select2-scroll-position");
						a(this).scrollTop(d.y)
					}), a(window).on(f + " " + g + " " + h, function (a) {
						e._positionDropdown(), e._resizeDropdown()
					})
				}, c.prototype._detachPositioningHandler = function (c, d) {
					var e = "scroll.select2." + d.id,
						f = "resize.select2." + d.id,
						g = "orientationchange.select2." + d.id;
					this.$container.parents().filter(b.hasScroll).off(e), a(window).off(e + " " + f + " " + g)
				}, c.prototype._positionDropdown = function () {
					var b = a(window),
						c = this.$dropdown.hasClass("select2-dropdown--above"),
						d = this.$dropdown.hasClass("select2-dropdown--below"),
						e = null,
						f = this.$container.offset();
					f.bottom = f.top + this.$container.outerHeight(!1);
					var g = {
						height: this.$container.outerHeight(!1)
					};
					g.top = f.top, g.bottom = f.top + g.height;
					var h = {
							height: this.$dropdown.outerHeight(!1)
						},
						i = {
							top: b.scrollTop(),
							bottom: b.scrollTop() + b.height()
						},
						j = i.top < f.top - h.height,
						k = i.bottom > f.bottom + h.height,
						l = {
							left: f.left,
							top: g.bottom
						},
						m = this.$dropdownParent;
					"static" === m.css("position") && (m = m.offsetParent());
					var n = m.offset();
					l.top -= n.top, l.left -= n.left, c || d || (e = "below"), k || !j || c ? !j && k && c && (e = "below") : e = "above", ("above" == e || c && "below" !== e) && (l.top = g.top - n.top - h.height), null != e && (this.$dropdown.removeClass("select2-dropdown--below select2-dropdown--above").addClass("select2-dropdown--" + e), this.$container.removeClass("select2-container--below select2-container--above").addClass("select2-container--" + e)), this.$dropdownContainer.css(l)
				}, c.prototype._resizeDropdown = function () {
					var a = {
						width: this.$container.outerWidth(!1) + "px"
					};
					this.options.get("dropdownAutoWidth") && (a.minWidth = a.width, a.position = "relative", a.width = "auto"), this.$dropdown.css(a)
				}, c.prototype._showDropdown = function (a) {
					this.$dropdownContainer.appendTo(this.$dropdownParent), this._positionDropdown(), this._resizeDropdown()
				}, c
			}), b.define("select2/dropdown/minimumResultsForSearch", [], function () {
				function a(b) {
					for (var c = 0, d = 0; d < b.length; d++) {
						var e = b[d];
						e.children ? c += a(e.children) : c++
					}
					return c
				}

				function b(a, b, c, d) {
					this.minimumResultsForSearch = c.get("minimumResultsForSearch"), this.minimumResultsForSearch < 0 && (this.minimumResultsForSearch = 1 / 0), a.call(this, b, c, d)
				}
				return b.prototype.showSearch = function (b, c) {
					return !(a(c.data.results) < this.minimumResultsForSearch) && b.call(this, c)
				}, b
			}), b.define("select2/dropdown/selectOnClose", ["../utils"], function (a) {
				function b() {}
				return b.prototype.bind = function (a, b, c) {
					var d = this;
					a.call(this, b, c), b.on("close", function (a) {
						d._handleSelectOnClose(a)
					})
				}, b.prototype._handleSelectOnClose = function (b, c) {
					if (c && null != c.originalSelect2Event) {
						var d = c.originalSelect2Event;
						if ("select" === d._type || "unselect" === d._type) return
					}
					var e = this.getHighlightedResults();
					if (!(e.length < 1)) {
						var f = a.GetData(e[0], "data");
						null != f.element && f.element.selected || null == f.element && f.selected || this.trigger("select", {
							data: f
						})
					}
				}, b
			}), b.define("select2/dropdown/closeOnSelect", [], function () {
				function a() {}
				return a.prototype.bind = function (a, b, c) {
					var d = this;
					a.call(this, b, c), b.on("select", function (a) {
						d._selectTriggered(a)
					}), b.on("unselect", function (a) {
						d._selectTriggered(a)
					})
				}, a.prototype._selectTriggered = function (a, b) {
					var c = b.originalEvent;
					c && c.ctrlKey || this.trigger("close", {
						originalEvent: c,
						originalSelect2Event: b
					})
				}, a
			}), b.define("select2/i18n/en", [], function () {
				return {
					errorLoading: function () {
						return "The results could not be loaded."
					},
					inputTooLong: function (a) {
						var b = a.input.length - a.maximum,
							c = "Please delete " + b + " character";
						return 1 != b && (c += "s"), c
					},
					inputTooShort: function (a) {
						return "Please enter " + (a.minimum - a.input.length) + " or more characters"
					},
					loadingMore: function () {
						return "Loading more results…"
					},
					maximumSelected: function (a) {
						var b = "You can only select " + a.maximum + " item";
						return 1 != a.maximum && (b += "s"), b
					},
					noResults: function () {
						return "No results found"
					},
					searching: function () {
						return "Searching…"
					}
				}
			}), b.define("select2/defaults", ["jquery", "require", "./results", "./selection/single", "./selection/multiple", "./selection/placeholder", "./selection/allowClear", "./selection/search", "./selection/eventRelay", "./utils", "./translation", "./diacritics", "./data/select", "./data/array", "./data/ajax", "./data/tags", "./data/tokenizer", "./data/minimumInputLength", "./data/maximumInputLength", "./data/maximumSelectionLength", "./dropdown", "./dropdown/search", "./dropdown/hidePlaceholder", "./dropdown/infiniteScroll", "./dropdown/attachBody", "./dropdown/minimumResultsForSearch", "./dropdown/selectOnClose", "./dropdown/closeOnSelect", "./i18n/en"], function (a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C) {
				function D() {
					this.reset()
				}
				return D.prototype.apply = function (l) {
					if (l = a.extend(!0, {}, this.defaults, l), null == l.dataAdapter) {
						if (null != l.ajax ? l.dataAdapter = o : null != l.data ? l.dataAdapter = n : l.dataAdapter = m, l.minimumInputLength > 0 && (l.dataAdapter = j.Decorate(l.dataAdapter, r)), l.maximumInputLength > 0 && (l.dataAdapter = j.Decorate(l.dataAdapter, s)), l.maximumSelectionLength > 0 && (l.dataAdapter = j.Decorate(l.dataAdapter, t)), l.tags && (l.dataAdapter = j.Decorate(l.dataAdapter, p)), null == l.tokenSeparators && null == l.tokenizer || (l.dataAdapter = j.Decorate(l.dataAdapter, q)), null != l.query) {
							var C = b(l.amdBase + "compat/query");
							l.dataAdapter = j.Decorate(l.dataAdapter, C)
						}
						if (null != l.initSelection) {
							var D = b(l.amdBase + "compat/initSelection");
							l.dataAdapter = j.Decorate(l.dataAdapter, D)
						}
					}
					if (null == l.resultsAdapter && (l.resultsAdapter = c, null != l.ajax && (l.resultsAdapter = j.Decorate(l.resultsAdapter, x)), null != l.placeholder && (l.resultsAdapter = j.Decorate(l.resultsAdapter, w)), l.selectOnClose && (l.resultsAdapter = j.Decorate(l.resultsAdapter, A))), null == l.dropdownAdapter) {
						if (l.multiple) l.dropdownAdapter = u;
						else {
							var E = j.Decorate(u, v);
							l.dropdownAdapter = E
						}
						if (0 !== l.minimumResultsForSearch && (l.dropdownAdapter = j.Decorate(l.dropdownAdapter, z)), l.closeOnSelect && (l.dropdownAdapter = j.Decorate(l.dropdownAdapter, B)), null != l.dropdownCssClass || null != l.dropdownCss || null != l.adaptDropdownCssClass) {
							var F = b(l.amdBase + "compat/dropdownCss");
							l.dropdownAdapter = j.Decorate(l.dropdownAdapter, F)
						}
						l.dropdownAdapter = j.Decorate(l.dropdownAdapter, y)
					}
					if (null == l.selectionAdapter) {
						if (l.multiple ? l.selectionAdapter = e : l.selectionAdapter = d, null != l.placeholder && (l.selectionAdapter = j.Decorate(l.selectionAdapter, f)), l.allowClear && (l.selectionAdapter = j.Decorate(l.selectionAdapter, g)), l.multiple && (l.selectionAdapter = j.Decorate(l.selectionAdapter, h)), null != l.containerCssClass || null != l.containerCss || null != l.adaptContainerCssClass) {
							var G = b(l.amdBase + "compat/containerCss");
							l.selectionAdapter = j.Decorate(l.selectionAdapter, G)
						}
						l.selectionAdapter = j.Decorate(l.selectionAdapter, i)
					}
					if ("string" == typeof l.language)
						if (l.language.indexOf("-") > 0) {
							var H = l.language.split("-"),
								I = H[0];
							l.language = [l.language, I]
						} else l.language = [l.language];
					if (a.isArray(l.language)) {
						var J = new k;
						l.language.push("en");
						for (var K = l.language, L = 0; L < K.length; L++) {
							var M = K[L],
								N = {};
							try {
								N = k.loadPath(M)
							} catch (a) {
								try {
									M = this.defaults.amdLanguageBase + M, N = k.loadPath(M)
								} catch (a) {
									l.debug && window.console && console.warn && console.warn('Select2: The language file for "' + M + '" could not be automatically loaded. A fallback will be used instead.');
									continue
								}
							}
							J.extend(N)
						}
						l.translations = J
					} else {
						var O = k.loadPath(this.defaults.amdLanguageBase + "en"),
							P = new k(l.language);
						P.extend(O), l.translations = P
					}
					return l
				}, D.prototype.reset = function () {
					function b(a) {
						function b(a) {
							return l[a] || a
						}
						return a.replace(/[^\u0000-\u007E]/g, b)
					}

					function c(d, e) {
						if ("" === a.trim(d.term)) return e;
						if (e.children && e.children.length > 0) {
							for (var f = a.extend(!0, {}, e), g = e.children.length - 1; g >= 0; g--) {
								null == c(d, e.children[g]) && f.children.splice(g, 1)
							}
							return f.children.length > 0 ? f : c(d, f)
						}
						var h = b(e.text).toUpperCase(),
							i = b(d.term).toUpperCase();
						return h.indexOf(i) > -1 ? e : null
					}
					this.defaults = {
						amdBase: "./",
						amdLanguageBase: "./i18n/",
						closeOnSelect: !0,
						debug: !1,
						dropdownAutoWidth: !1,
						escapeMarkup: j.escapeMarkup,
						language: C,
						matcher: c,
						minimumInputLength: 0,
						maximumInputLength: 0,
						maximumSelectionLength: 0,
						minimumResultsForSearch: 0,
						selectOnClose: !1,
						sorter: function (a) {
							return a
						},
						templateResult: function (a) {
							return a.text
						},
						templateSelection: function (a) {
							return a.text
						},
						theme: "default",
						width: "resolve"
					}
				}, D.prototype.set = function (b, c) {
					var d = a.camelCase(b),
						e = {};
					e[d] = c;
					var f = j._convertData(e);
					a.extend(!0, this.defaults, f)
				}, new D
			}), b.define("select2/options", ["require", "jquery", "./defaults", "./utils"], function (a, b, c, d) {
				function e(b, e) {
					if (this.options = b, null != e && this.fromElement(e), this.options = c.apply(this.options), e && e.is("input")) {
						var f = a(this.get("amdBase") + "compat/inputData");
						this.options.dataAdapter = d.Decorate(this.options.dataAdapter, f)
					}
				}
				return e.prototype.fromElement = function (a) {
					var c = ["select2"];
					null == this.options.multiple && (this.options.multiple = a.prop("multiple")), null == this.options.disabled && (this.options.disabled = a.prop("disabled")), null == this.options.language && (a.prop("lang") ? this.options.language = a.prop("lang").toLowerCase() : a.closest("[lang]").prop("lang") && (this.options.language = a.closest("[lang]").prop("lang"))), null == this.options.dir && (a.prop("dir") ? this.options.dir = a.prop("dir") : a.closest("[dir]").prop("dir") ? this.options.dir = a.closest("[dir]").prop("dir") : this.options.dir = "ltr"), a.prop("disabled", this.options.disabled), a.prop("multiple", this.options.multiple), d.GetData(a[0], "select2Tags") && (this.options.debug && window.console && console.warn && console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'), d.StoreData(a[0], "data", d.GetData(a[0], "select2Tags")), d.StoreData(a[0], "tags", !0)), d.GetData(a[0], "ajaxUrl") && (this.options.debug && window.console && console.warn && console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."), a.attr("ajax--url", d.GetData(a[0], "ajaxUrl")), d.StoreData(a[0], "ajax-Url", d.GetData(a[0], "ajaxUrl")));
					var e = {};
					e = b.fn.jquery && "1." == b.fn.jquery.substr(0, 2) && a[0].dataset ? b.extend(!0, {}, a[0].dataset, d.GetData(a[0])) : d.GetData(a[0]);
					var f = b.extend(!0, {}, e);
					f = d._convertData(f);
					for (var g in f) b.inArray(g, c) > -1 || (b.isPlainObject(this.options[g]) ? b.extend(this.options[g], f[g]) : this.options[g] = f[g]);
					return this
				}, e.prototype.get = function (a) {
					return this.options[a]
				}, e.prototype.set = function (a, b) {
					this.options[a] = b
				}, e
			}), b.define("select2/core", ["jquery", "./options", "./utils", "./keys"], function (a, b, c, d) {
				var e = function (a, d) {
					null != c.GetData(a[0], "select2") && c.GetData(a[0], "select2").destroy(), this.$element = a, this.id = this._generateId(a), d = d || {}, this.options = new b(d, a), e.__super__.constructor.call(this);
					var f = a.attr("tabindex") || 0;
					c.StoreData(a[0], "old-tabindex", f), a.attr("tabindex", "-1");
					var g = this.options.get("dataAdapter");
					this.dataAdapter = new g(a, this.options);
					var h = this.render();
					this._placeContainer(h);
					var i = this.options.get("selectionAdapter");
					this.selection = new i(a, this.options), this.$selection = this.selection.render(), this.selection.position(this.$selection, h);
					var j = this.options.get("dropdownAdapter");
					this.dropdown = new j(a, this.options), this.$dropdown = this.dropdown.render(), this.dropdown.position(this.$dropdown, h);
					var k = this.options.get("resultsAdapter");
					this.results = new k(a, this.options, this.dataAdapter), this.$results = this.results.render(), this.results.position(this.$results, this.$dropdown);
					var l = this;
					this._bindAdapters(), this._registerDomEvents(), this._registerDataEvents(), this._registerSelectionEvents(), this._registerDropdownEvents(), this._registerResultsEvents(), this._registerEvents(), this.dataAdapter.current(function (a) {
						l.trigger("selection:update", {
							data: a
						})
					}), a.addClass("select2-hidden-accessible"), a.attr("aria-hidden", "true"), this._syncAttributes(), c.StoreData(a[0], "select2", this)
				};
				return c.Extend(e, c.Observable), e.prototype._generateId = function (a) {
					var b = "";
					return b = null != a.attr("id") ? a.attr("id") : null != a.attr("name") ? a.attr("name") + "-" + c.generateChars(2) : c.generateChars(4), b = b.replace(/(:|\.|\[|\]|,)/g, ""), b = "select2-" + b
				}, e.prototype._placeContainer = function (a) {
					a.insertAfter(this.$element);
					var b = this._resolveWidth(this.$element, this.options.get("width"));
					null != b && a.css("width", b)
				}, e.prototype._resolveWidth = function (a, b) {
					var c = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;
					if ("resolve" == b) {
						var d = this._resolveWidth(a, "style");
						return null != d ? d : this._resolveWidth(a, "element")
					}
					if ("element" == b) {
						var e = a.outerWidth(!1);
						return e <= 0 ? "auto" : e + "px"
					}
					if ("style" == b) {
						var f = a.attr("style");
						if ("string" != typeof f) return null;
						for (var g = f.split(";"), h = 0, i = g.length; h < i; h += 1) {
							var j = g[h].replace(/\s/g, ""),
								k = j.match(c);
							if (null !== k && k.length >= 1) return k[1]
						}
						return null
					}
					return b
				}, e.prototype._bindAdapters = function () {
					this.dataAdapter.bind(this, this.$container), this.selection.bind(this, this.$container), this.dropdown.bind(this, this.$container), this.results.bind(this, this.$container)
				}, e.prototype._registerDomEvents = function () {
					var b = this;
					this.$element.on("change.select2", function () {
						b.dataAdapter.current(function (a) {
							b.trigger("selection:update", {
								data: a
							})
						})
					}), this.$element.on("focus.select2", function (a) {
						b.trigger("focus", a)
					}), this._syncA = c.bind(this._syncAttributes, this), this._syncS = c.bind(this._syncSubtree, this), this.$element[0].attachEvent && this.$element[0].attachEvent("onpropertychange", this._syncA);
					var d = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
					null != d ? (this._observer = new d(function (c) {
						a.each(c, b._syncA), a.each(c, b._syncS)
					}), this._observer.observe(this.$element[0], {
						attributes: !0,
						childList: !0,
						subtree: !1
					})) : this.$element[0].addEventListener && (this.$element[0].addEventListener("DOMAttrModified", b._syncA, !1), this.$element[0].addEventListener("DOMNodeInserted", b._syncS, !1), this.$element[0].addEventListener("DOMNodeRemoved", b._syncS, !1))
				}, e.prototype._registerDataEvents = function () {
					var a = this;
					this.dataAdapter.on("*", function (b, c) {
						a.trigger(b, c)
					})
				}, e.prototype._registerSelectionEvents = function () {
					var b = this,
						c = ["toggle", "focus"];
					this.selection.on("toggle", function () {
						b.toggleDropdown()
					}), this.selection.on("focus", function (a) {
						b.focus(a)
					}), this.selection.on("*", function (d, e) {
						-1 === a.inArray(d, c) && b.trigger(d, e)
					})
				}, e.prototype._registerDropdownEvents = function () {
					var a = this;
					this.dropdown.on("*", function (b, c) {
						a.trigger(b, c)
					})
				}, e.prototype._registerResultsEvents = function () {
					var a = this;
					this.results.on("*", function (b, c) {
						a.trigger(b, c)
					})
				}, e.prototype._registerEvents = function () {
					var a = this;
					this.on("open", function () {
						a.$container.addClass("select2-container--open")
					}), this.on("close", function () {
						a.$container.removeClass("select2-container--open")
					}), this.on("enable", function () {
						a.$container.removeClass("select2-container--disabled")
					}), this.on("disable", function () {
						a.$container.addClass("select2-container--disabled")
					}), this.on("blur", function () {
						a.$container.removeClass("select2-container--focus")
					}), this.on("query", function (b) {
						a.isOpen() || a.trigger("open", {}), this.dataAdapter.query(b, function (c) {
							a.trigger("results:all", {
								data: c,
								query: b
							})
						})
					}), this.on("query:append", function (b) {
						this.dataAdapter.query(b, function (c) {
							a.trigger("results:append", {
								data: c,
								query: b
							})
						})
					}), this.on("keypress", function (b) {
						var c = b.which;
						a.isOpen() ? c === d.ESC || c === d.TAB || c === d.UP && b.altKey ? (a.close(), b.preventDefault()) : c === d.ENTER ? (a.trigger("results:select", {}), b.preventDefault()) : c === d.SPACE && b.ctrlKey ? (a.trigger("results:toggle", {}), b.preventDefault()) : c === d.UP ? (a.trigger("results:previous", {}), b.preventDefault()) : c === d.DOWN && (a.trigger("results:next", {}), b.preventDefault()) : (c === d.ENTER || c === d.SPACE || c === d.DOWN && b.altKey) && (a.open(), b.preventDefault())
					})
				}, e.prototype._syncAttributes = function () {
					this.options.set("disabled", this.$element.prop("disabled")), this.options.get("disabled") ? (this.isOpen() && this.close(), this.trigger("disable", {})) : this.trigger("enable", {})
				}, e.prototype._syncSubtree = function (a, b) {
					var c = !1,
						d = this;
					if (!a || !a.target || "OPTION" === a.target.nodeName || "OPTGROUP" === a.target.nodeName) {
						if (b)
							if (b.addedNodes && b.addedNodes.length > 0)
								for (var e = 0; e < b.addedNodes.length; e++) {
									var f = b.addedNodes[e];
									f.selected && (c = !0)
								} else b.removedNodes && b.removedNodes.length > 0 && (c = !0);
							else c = !0;
						c && this.dataAdapter.current(function (a) {
							d.trigger("selection:update", {
								data: a
							})
						})
					}
				}, e.prototype.trigger = function (a, b) {
					var c = e.__super__.trigger,
						d = {
							open: "opening",
							close: "closing",
							select: "selecting",
							unselect: "unselecting",
							clear: "clearing"
						};
					if (void 0 === b && (b = {}), a in d) {
						var f = d[a],
							g = {
								prevented: !1,
								name: a,
								args: b
							};
						if (c.call(this, f, g), g.prevented) return void(b.prevented = !0)
					}
					c.call(this, a, b)
				}, e.prototype.toggleDropdown = function () {
					this.options.get("disabled") || (this.isOpen() ? this.close() : this.open())
				}, e.prototype.open = function () {
					this.isOpen() || this.trigger("query", {})
				}, e.prototype.close = function () {
					this.isOpen() && this.trigger("close", {})
				}, e.prototype.isOpen = function () {
					return this.$container.hasClass("select2-container--open")
				}, e.prototype.hasFocus = function () {
					return this.$container.hasClass("select2-container--focus")
				}, e.prototype.focus = function (a) {
					this.hasFocus() || (this.$container.addClass("select2-container--focus"), this.trigger("focus", {}))
				}, e.prototype.enable = function (a) {
					this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'), null != a && 0 !== a.length || (a = [!0]);
					var b = !a[0];
					this.$element.prop("disabled", b)
				}, e.prototype.data = function () {
					this.options.get("debug") && arguments.length > 0 && window.console && console.warn && console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');
					var a = [];
					return this.dataAdapter.current(function (b) {
						a = b
					}), a
				}, e.prototype.val = function (b) {
					if (this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'), null == b || 0 === b.length) return this.$element.val();
					var c = b[0];
					a.isArray(c) && (c = a.map(c, function (a) {
						return a.toString()
					})), this.$element.val(c).trigger("change")
				}, e.prototype.destroy = function () {
					this.$container.remove(), this.$element[0].detachEvent && this.$element[0].detachEvent("onpropertychange", this._syncA), null != this._observer ? (this._observer.disconnect(), this._observer = null) : this.$element[0].removeEventListener && (this.$element[0].removeEventListener("DOMAttrModified", this._syncA, !1), this.$element[0].removeEventListener("DOMNodeInserted", this._syncS, !1), this.$element[0].removeEventListener("DOMNodeRemoved", this._syncS, !1)), this._syncA = null, this._syncS = null, this.$element.off(".select2"), this.$element.attr("tabindex", c.GetData(this.$element[0], "old-tabindex")), this.$element.removeClass("select2-hidden-accessible"), this.$element.attr("aria-hidden", "false"), c.RemoveData(this.$element[0]), this.dataAdapter.destroy(), this.selection.destroy(), this.dropdown.destroy(), this.results.destroy(), this.dataAdapter = null, this.selection = null, this.dropdown = null, this.results = null
				}, e.prototype.render = function () {
					var b = a('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');
					return b.attr("dir", this.options.get("dir")), this.$container = b, this.$container.addClass("select2-container--" + this.options.get("theme")), c.StoreData(b[0], "element", this.$element), b
				}, e
			}), b.define("jquery-mousewheel", ["jquery"], function (a) {
				return a
			}), b.define("jquery.select2", ["jquery", "jquery-mousewheel", "./select2/core", "./select2/defaults", "./select2/utils"], function (a, b, c, d, e) {
				if (null == a.fn.select2) {
					var f = ["open", "close", "destroy"];
					a.fn.select2 = function (b) {
						if ("object" == typeof (b = b || {})) return this.each(function () {
							var d = a.extend(!0, {}, b);
							new c(a(this), d)
						}), this;
						if ("string" == typeof b) {
							var d, g = Array.prototype.slice.call(arguments, 1);
							return this.each(function () {
								var a = e.GetData(this, "select2");
								null == a && window.console && console.error && console.error("The select2('" + b + "') method was called on an element that is not using Select2."), d = a[b].apply(a, g)
							}), a.inArray(b, f) > -1 ? this : d
						}
						throw new Error("Invalid arguments for Select2: " + b)
					}
				}
				return null == a.fn.select2.defaults && (a.fn.select2.defaults = d), c
			}), {
				define: b.define,
				require: b.require
			}
		}(),
		c = b.require("jquery.select2");
	return a.fn.select2.amd = b, c
})



if ( $('html').attr('lang') == 'fa' ) {
	function show_report() {
		var bug_report_modal = $("#bug-report-modal");
		
		if ( bug_report_modal.find('#tgju-crm-from-33').html() == '' ) {
			var script = document.createElement("script");
			script.setAttribute("src", "//crm.tgju.org/fa/form/render/33?captcha=true");
			bug_report_modal.find('#tgju-crm-from-33').after(script);

			setTimeout(function() {
				bug_report_modal.show();
			}, 1000);
		} else {
			bug_report_modal.show();
		}
		
		var report_interval_id = setInterval(function() {
			if ( $('iframe[title="recaptcha challenge"]').length ) {
				if ( !$('iframe[title="recaptcha challenge"]').hasClass('done') ) {
					$('iframe[title="recaptcha challenge"]').parent().parent().css('z-index', 9999999999);
					$('iframe[title="recaptcha challenge"]').addClass('done');
				} else {
					clearInterval(report_interval_id);
				}
			}
		}, 500);
	}

	function close_flashcard() {
		$('cloudflare-app[app="flashcard"]').remove();
		createCookie('tgju-flashcard', 'true', 1);
	}
	
	$(document).ready(function () {
		$(document).on('click', '.close-bottom-left-ad', function() {
			$(this).parents('.bottom-left-ad').remove();
			createCookie('tgju-flashcard', 'true', 1);
		});
	
		if ( !readCookie('tgju-flashcard') && floating_ad ) {
			setTimeout(function() {
				$('.bottom-left-ad').html('<i class="fa fa-times close-bottom-left-ad"></i>'
						+ '<a href="'+floating_ad.url+'" target="_blank">'
						+ '<img src="'+floating_ad.img+'">'
					+ '</a>').show();
			}, 10000);
		}

		if ( $('body').hasClass('homepage') && $(window).width() < 760 ) {
			$('.worldmap-grandparent').remove();
		}
	});
}

function screenSize() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return {width: x, height: y};
}

function world_map_change(code)
{
	window.location = 'atlas/' + code.toLowerCase();

	// var item = $('#world-map-select option[data-flag="'+code.toLowerCase()+'"]');
	
	// if (item.length) {
	// 	var title = '<span class="mini-flag flag-'+code.toLowerCase()+'"></span>' + item.text();
	// 	$('.world-map-detail').html('<li>'+title+'</li>'
	// 		+ '<li>کد کشور: '+code.toUpperCase()+'</li>'
	// 		+ '<li><a href="atlas/'+code.toLowerCase()+'" target="_blank">توضیحات بیشتر</a></li>');
	// }
}

// Map Tooltips
$(window).on("blur focus", function(e) {
	var prevType = $(this).data("prevType");
	$(this).data("prevType", e.type);
});

if ( $(window).width() > 760 ) {
	var ready_to_fire_worldmap = true;
	setInterval(function() {
		if ( ready_to_fire_worldmap && typeof window.world_map_countries_arr != 'undefined' && window.world_map_countries_arr.length ) {
			ready_to_fire_worldmap = false;

			var add_delay = 0;
			var i, j, temparray, chunk = 6;
			for (i=0, j=window.world_map_countries_arr.length; i<j; i+=chunk) {
				temparray = window.world_map_countries_arr.slice(i, i+chunk);

				(function(){
					var this_temparray = temparray;
					setTimeout(function() {
						fire_worldmap_price_tooltip(this_temparray);
					}, add_delay);
				})();
				add_delay += 6000;
			}
			
			setTimeout(function() {
				ready_to_fire_worldmap = true;
			}, add_delay - 3000);
		}
	}, 2000);
}

function fire_worldmap_price_tooltip(countries_arr) {
	if ( $(window).width() < 760 ) return;
	
	var add_delay = 0;
	for ( var i = 0; i < countries_arr.length; i++ ) {
		(function(){
			var this_country = countries_arr[i];
			setTimeout(function() {
				worldmap_price_tooltip(this_country['code'], this_country, 'add');
			}, add_delay);
		})();

		add_delay += 6000 / countries_arr.length;
	}
}

function worldmap_price_tooltip(code, info, action) {
	if ( typeof action == 'undefined' ) var action = 'add';

	// if ( $(window).data("prevType") != 'focus' ) return;
	var this_country_elm = $("path[data-code='"+code+"']");
	if ( !this_country_elm.length ) return;
	var $worldmap_price_tooltip = $(".worldmap_price_tooltip[data-id='"+code+"']");

	if ( action == 'add' ) {
		if ( !$worldmap_price_tooltip.length ) {
			var flag = '<span class="mini-flag flag-'+ code.toLowerCase() +'"></span>';
			var price_tooltip_html = ''+
				'<div class="worldmap_price_tooltip-info">'+
					'<span class="worldmap_price_tooltip-title">'+info.title+' : </span>'+
					'<span class="worldmap_price_tooltip-price">'+number2en(info.price)+'</span>'+
				'</div>'+
				'';
			$('body').append('<div class="worldmap_price_tooltip" data-id="'+code+'">'+flag + price_tooltip_html+'</div>');
			$worldmap_price_tooltip = $(".worldmap_price_tooltip[data-id='"+code+"']");
		} else {
			$worldmap_price_tooltip.find('worldmap_price_tooltip-title').text(info.title+' : ');
			$worldmap_price_tooltip.find('worldmap_price_tooltip-price').text(number2en(info.price));
		}

		$worldmap_price_tooltip
			.css({
				left: this_country_elm.position().left + (this_country_elm[0].getBBox().width / 2) + 10,
				top: this_country_elm.position().top + (this_country_elm[0].getBBox().height / 2) - 20
			})
			.show();

		setTimeout(function() {
			$worldmap_price_tooltip.hide();
		}, 3000);
	} else {
		$worldmap_price_tooltip.hide();
	}
}

$(document).ready(function() {
	$(document).on('click', '.top-menu-languages-btn', function(e) {
		e.preventDefault();
		$('.top-menu-languages-wrapper').slideToggle();
		$(this).toggleClass('open');
		$('#site-header').toggleClass('top-menu-open');
	});
	
	$(document).on('click', '.languages-modal-btn', function(e) {
		e.preventDefault();
		$('#languages-modal').show();
	});

	$(document).on('input keyup', '.languages-modal-filter', function(e) {
		if (e.keyCode == 27) $(this).val('');
		languagesModalFilter($(this));
	});

	$(document).on('click', '.languages-modal-filter-clear', function() {
		languagesModalFilter($('.languages-modal-filter').val(''));
	});

	$(document).on('click', '.languages-modal-btn', function () {
		if ( !$('#languages-modal .modal-logo img').length ) {
			$('#languages-modal .modal-logo').html('<img src="http://static.tgju.org/images/tlogo.png" alt="TGJU Logo" width="25px;">');
		}
	});
});

function languagesModalFilter(input) {
	var filter = input.val().toUpperCase();
	var li = $(".languages-modal-inner ul li");
	li.each(function (i) {
		var title = $(li[i]).find('.country-title');
		if ( title.text().toUpperCase().indexOf(filter) > -1 ) $(li[i]).show();
		else $(li[i]).hide();
	});

	if ( filter == '' ) $(".languages-modal-filter-clear").hide();
	else $(".languages-modal-filter-clear").show();
}

function header_tab_load(obj, menu_index, tab_name) {
	var this_section = $(obj).closest('.section');
	var this_ul = this_section.find('ul').eq(0);
	
	if ( $(obj).attr('data-loading') && $(obj).attr('data-loading') == '1' ) return;

	if ( !this_ul.find('li[data-header-links-tab="'+tab_name+'"]').length ) {
		$(obj).attr('data-loading', 1);
		tinyxhr('/?act=site-header-menu&client=ajax&noview&name=' + tab_name, function (err, data, xhr) {
			if ( !err ) {
				$(obj).attr('data-loading', 0);
				this_ul.append(data);
				header_links_tab(obj, menu_index, tab_name);
			}
		});
	} else {
		header_links_tab(obj, menu_index, tab_name);
	}
}

/**
 * searchSuggestion
 */
$(document).ready(function () {
	if ( $('#msSearch-select').length ) {
		$('#msSearch-select').select2({
			minimumResultsForSearch: -1
		});
		var xhr, goSuggestionTimeout, query, msPlaceHolder_val;
		var msSearchPage = 1;
		var msSearch_suggestions_wrapper = $(".msSearch-suggestions-wrapper");
		var msSearch_container = $(".msSearch-suggestions-container");
		var msSearch_select = $("#msSearch-select");
		var msSearch_inputContainer = $(".msSearch-inputContainer");
		var msSearch_input = msSearch_inputContainer.find(".msSearch-input");
		var msSearch_selectedSection = 0;
		var msSearch_preload = msSearch_inputContainer.find(".msSearch-preload");
		var msSearch_caret = msSearch_inputContainer.find(".msSearch-caret");
		var msSearch_search_icon = msSearch_inputContainer.find(".msSearch-search-icon");
		var msSearch_tabs_container = $(".msSearch-tabs-container");
		if (!window.mainSearchPage) window.mainSearchPage = 0;
		msSearch_input.focus(function () {
			/*if ($(this).val() == '' && msSearch_select.val() == 0 && !msSearch_container.is(':visible')) {
				if ( !msSearch_container.find('#columns').length ) {
					msSearch_container.html("<div id='columns'></div>");
					diagrams_render();
				}
			}*/
			if ( msSearch_container.html() != '' ) msSearch_container.fadeIn(50);
		});
		msSearch_input.on('input', function (e) {
			goMsSearchAjax();
		});
		msSearch_input.on('keydown', function (e) {
			clearTimeout(goSuggestionTimeout);
			if (e.keyCode == 13) {
				document.location.href = "search?q=" + $(this).val() + "&section=" + msSearch_select.val();
			}
		});
		$('.msSearch-search-icon').on('click', function () {
			document.location.href = "search?q=" + msSearch_input.val() + "&section=" + msSearch_select.val();
		});
		$(document).on('click', '.msSearch-loadMore', function () {
			$(this).addClass('msLoading').prop('disabled', true);
			var msMore_section = msSearch_select.val();
			var msMore_query = msSearch_input.val();

			var search_base_url = ( msMore_section == '16' ) ? 'http://platform.tgju.shop/fa/api/shop-search' : 'searchSuggestion';

			$.ajax({
				url: search_base_url + "/" + msMore_section + "/" + window.mainSearchPage + "/" + msSearchPage + "/" + msMore_query,
				type: msMore_section == '16' ? 'get' : 'post',
				success: function (data) {
					msSearchPage++;
					msSearch_container.append(data);
				}
			});
		});
		$(document).on('click', function (e) {
			if (!msSearch_suggestions_wrapper.find('*').is(e.target) && !msSearch_search_icon.is(e.target) && !msSearch_input.is(e.target) && !window.mainSearchPage) {
				msSearch_container.fadeOut(50);
			}
		});
		$(msSearch_search_icon).on('click', function (e) {
			msSearch_input.focus();
		});
		$(document).on('change', "#msSearch-select", function (e) {
			msSearch_selectedSection = msSearch_select.val();
			if (msSearch_selectedSection == 0) msPlaceHolder_val = trans('master.search-placeholder-all');
			else msPlaceHolder_val = trans('master.search-placeholder-others') +' ' + msSearch_select.find('option[value="'+msSearch_selectedSection+'"]').text();
			msSearch_input.attr('placeholder', msPlaceHolder_val);
			if (msSearch_input.val() != '') goMsSearchAjax();
			msSearchPage = 2;
		});
		var base_url = window.location.origin;
		var full_url = window.location.href;
	
		var is_matched_url = $('.msSearch-grandParent').attr('data-is_matched_url');
		var goToSection = $('.msSearch-grandParent').attr('data-go_to_section');
		if (is_matched_url) msSearch_select.val(goToSection).trigger('change');
	
		// function activateSectionTab(section) {
		// 	$(".msSearch-tab").removeClass('active');
		// 	section.addClass('active');
		// 	section.prevAll(".msSearch-separator").eq(0).hide();
		// 	section.nextAll(".msSearch-separator").eq(0).hide();
		// }
	
		function goMsSearchAjax() {
			query = msSearch_input.val();
			if (query.length >= 2) {
				clearTimeout(goSuggestionTimeout);
				if (xhr && xhr.readyState != 4) xhr.abort();
				goSuggestionTimeout = setTimeout(function () {
					// msSearch_caret.hide();
					if (!msSearch_preload.find('img').length) {
						msSearch_preload.html('<img src="files/Dual-Ring.gif" alt="preload" title="preload">');
					}
					msSearch_preload.fadeIn(150);
					msSearch_container.html('');
					msSearch_container.hide();

					var search_base_url = ( msSearch_selectedSection == '16' ) ? 'http://platform.tgju.shop/fa/api/shop-search' : 'searchSuggestion';

					xhr = $.ajax({
						url: search_base_url + '/' + msSearch_selectedSection + '/' + window.mainSearchPage + '/1/' + query,
						type: msSearch_selectedSection == '16' ? 'get' : 'post',
						data: {},
						success: function (data) {
							msSearchPage = 2;
							msSearch_container.html(data);
							msSearch_container.fadeIn(50);
							msSearch_preload.fadeOut(150);
							// msSearch_caret.show();
						},
						error: function () {
							msSearch_preload.fadeOut(150);
							// msSearch_caret.show();
						}
					});
				}, 300);
			} else {
				clearTimeout(goSuggestionTimeout);
				if (xhr && xhr.readyState != 4) xhr.abort();
			}
		}
	
		$(document).on('click', '.indicator-modal-btn', function(e) {
			e.preventDefault();
			$('#indicator-modal').show();
			if ( $('#indicator-modal #columns2').html() == '' ) diagrams_render2();
		});
	}
});

/**
 * Start of worldmap
 */

$(document).ready(function () {
	if ( $('#worldmap-wrapper-container').length ) {
		var worldmap_lazyload = document.location.pathname === "/";
	
		function worldmap_load() {
			var url = "http://static.tgju.org/html/worldmap.html";
			if (window.XDomainRequest && detectIE()) {
				var xdr = new XDomainRequest();
				xdr.open("GET", url, false);
				xdr.onload = function () {
				var res = xdr.responseText;
				if (res == null || typeof (res) == 'undefined')
				{
					res = data.firstChild.textContent;
				}
				worldmap_after_load(res);
			};
			xdr.send();
			} else {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200 || xmlhttp.status == 304) {
				worldmap_after_load(xmlhttp.responseText);
				} else {
				setTimeout(function(){ console.log("Request failed!") }, 0);
				}
			}
			}
			xmlhttp.open("GET", url, true);
			xmlhttp.send();
			}
		}
	
		setTimeout(function() {
			worldmap_load();
		}, worldmap_lazyload ? 7000 : 50);
	
		function worldmap_tooltip() {
			if ( $(".worldmap").length ) {
				$description = $(".worldmap-tooltip");
	
				$(".worldmap [data-code]").click(function() {
					var code = $(this).attr("data-code").toLowerCase();
					world_map_change(code);
				});
	
				$(".worldmap [data-code]").hover(function () {
	
					$description.addClass("active");
					
					var code = $(this).attr("data-code")
	
					var item = $("#world-map-select option[data-flag="+code.toLowerCase()+"]");
					var farsi = "";
					if (item.length) {
						farsi = item.text();
					}
					var flag = "<span class=\"mini-flag flag-"+code.toLowerCase()+"\"></span>";
	
					$description.html( flag + (farsi ? farsi : code) );
				}, function () {
					$description.removeClass("active");
				});
	
				$(".worldmap path[data-code]").mousemove(function(e) {
					$description.css({
						left: e.pageX,
						top: e.pageY - 70
					});
				});
			}
		}
	
		function worldmap_after_load(world_data) {
			document.getElementById('worldmap-wrapper-container').innerHTML = world_data;
			worldmap_tooltip();
		}
	}
});

$(function () {
	$('select.custom-select').each(function () {
		var $this = $(this),
			numberOfOptions = $(this).children('option').length;

		$this.addClass('select-hidden');
		$this.wrap('<div class="select-custom-div"></div>');
		$this.after('<div class="select-styled"></div>');

		var $styledSelect = $this.next('div.select-styled');
		$styledSelect.html($this.children('option').eq(0).html());

		var $list = $('<ul />', {
			'class': 'select-options'
		}).insertAfter($styledSelect);

		var selected_option = '';
		for (var i = 0; i < numberOfOptions; i++) {
			prepend = $this.children('option').eq(i).attr('data-prepend');
			if ( $this.children('option').eq(i).attr('selected') ) selected_option = $this.children('option').eq(i).attr('data-flag');
			$('<li />', {
				html: (prepend ? prepend : '')+$this.children('option').eq(i).html(),
				rel: $this.children('option').eq(i).val(),
				flag: $this.children('option').eq(i).attr('data-flag'),
			}).appendTo($list);
		}

		var $listItems = $list.children('li');

		$styledSelect.click(function (e) {
			e.stopPropagation();
			$('div.select-styled.active').not(this).each(function () {
				$(this).removeClass('active').next('ul.select-options').hide();
			});
			$(this).toggleClass('active').next('ul.select-options').toggle();
		});

		$listItems.click(function (e) {
			e.stopPropagation();
			$styledSelect.html($(this).html()).removeClass('active');
			$this.val($(this).attr('rel'));
			$this.trigger('change');
			$list.hide();
			//console.log($this.val());
		});

		if ( selected_option ) {
			setTimeout(function() {
				$list.find('li[flag="'+selected_option+'"]').click();
			}, 100);
		}

		$(document).click(function () {
			$styledSelect.removeClass('active');
			$list.hide();
		});

	});		
});
/**
 * End of worldmap
 */

function stocks_format_price(input, force_format) {
	input = String(input);
	if ( typeof force_format == 'undefined' ) {
		force_format = false;
	}

	// first strip any formatting;
	input = ( 0 + Number(input.replace(",", "")) );

	// is this a number?
	if ( isNaN(input) ) return false;
	value = false;

	// now filter it;
	if ( (force_format == false && input > 1000000000000) || force_format == 1000000000000 ) {
		value = Number((input / 1000000000000).toFixed(3)) + ' <span class="currency-type">هزار میلیارد</span>';
	} else if ( (force_format == false && input > 1000000000) || force_format == 1000000000 ) {
		value = Number((input / 1000000000).toFixed(3)) + ' <span class="currency-type">میلیارد</span>';
	} else if ( (force_format == false && input > 1000000) || force_format == 1000000 ) {
		value = Number((input / 1000000).toFixed(3)) + ' <span class="currency-type">میلیون</span>';
	}

	if ( !value ) {
		input = number_format_decimal(input, 2);
	} else {
		input = value;
	}
	
	return input;
}

function stocks_change(value, percentage, price, keep_minus) {
	if ( typeof percentage == 'undefined' ) percentage = false;
	if ( typeof price == 'undefined' ) price = 0;
	if ( typeof keep_minus == 'undefined' ) keep_minus = false;


	var _class;
	if ( Number(value) == 0 ) _class = 'change-no';
	else if ( String(value).indexOf('-') >= 0 ) _class = 'change-down';
	else _class = 'change-up';

	value = String(value).replace('-', '');
	_class = percentage ? _class + ' change-percentage' : _class;

	if ( price ) value = stocks_format_price(value);

	if ( percentage ) {
		value = Number(Number(value).toFixed(2));
	}

	if ( !isNaN(value) ) {
		value = number_format_decimal(value, 2);
	}

	if ( keep_minus && _class == 'change-down' ) {
		value_parts = String(value).split(' ');
		value_parts[0] = value_parts[0] + ' -';
		value = value_parts.join(' ');
	}

	return '<span class="change ' + _class + '">' + value + '</span>';
}

$(document).ready(function() {
	setTimeout(function() {
		$('img[data-ad-src]').each(function(i, elm) {
			if ( $(window).width() < 760 && !$(elm).parents('.advertise').hasClass('advertise-mobile') ) return true;
			else if ( $(window).width() >= 760 && $(elm).parents('.advertise').hasClass('advertise-mobile') ) return true;

			var is_shop_banner = $(this).closest('ul.advertise').hasClass('shop-banners') ? true : false;

			var src = $(this).attr('data-ad-src');
			
			$(this).attr('src', src);
			$(this).removeAttr('data-ad-src');
			$(this).show();
			$(this).parent().show();

			if ( is_shop_banner ) $(this).closest('li').addClass('show');
		});
	}, 1);

	$(document).on('click', '.app-for-mobile-close', function () {
		$('.app-for-mobile').remove();
		$('body').removeClass('app-bar-visible');
		createCookie('app-for-mobile-hide', 'true', 1);
	});

	if ( $(window).width() < 760 && !readCookie('app-for-mobile-hide') && $('html').attr('lang') == 'fa' ) {
		$('body').addClass('app-bar-visible');
	}
});