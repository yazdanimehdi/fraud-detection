import warnings

from sklearn.model_selection import RepeatedStratifiedKFold, KFold, cross_val_score, StratifiedKFold, train_test_split
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.metrics import log_loss, roc_auc_score, accuracy_score, confusion_matrix, f1_score
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt

df = pd.read_csv('pima.csv')


def cat(y):
    if y == 'negative':
        return 0
    elif y == 'positive':
        return 1


df['positive'] = df['positive'].apply(cat)
X = df.drop('positive', axis=1).as_matrix()
y = df['positive'].as_matrix()

# df = pd.read_csv('creditcard.csv')
# df.drop('Time', axis=1, inplace=True)
# df.dropna(axis=0, inplace=True)
# bad_loans = len(df[df['Class'] == 1])
# good_loan_indices = df[df.Class == 0].index
# random_indices = np.random.choice(good_loan_indices, 10000, replace=False)
# bad_loans_indices = df[df.Class == 1].index
# under_sample_indices = np.concatenate([bad_loans_indices, random_indices])
# under_sample = df.loc[under_sample_indices]
# under_sample = under_sample.dropna(axis=0)
# X = under_sample.drop('Class', axis=1).as_matrix()
# y = under_sample['Class'].as_matrix()
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=4)
cluster_min = KMeans()
cluster_min.fit(X_train[y_train == 1])
labels_min = cluster_min.labels_
cluster_maj = KMeans()
cluster_maj.fit(X_train[y_train == 0])
labels_maj = cluster_maj.labels_
labels_maj_set = set(labels_maj)
labels_min_set = set(labels_min)

clusters_maj = [[] for j in range(len(labels_maj_set))]
for item in labels_maj_set:
    for i in range(0, X_train[y_train == 0].shape[0]):
        if labels_maj[i] == item:
            clusters_maj[item].append(X_train[y_train == 0][i])

clusters_min = [[] for j in range(len(labels_min_set))]
for item in labels_min_set:
    for i in range(0, X_train[y_train == 1].shape[0]):
        if labels_min[i] == item:
            clusters_min[item].append(X_train[y_train == 1][i])

lr = LogisticRegression(max_iter=5000)
lr.fit(X_train, y_train)

proba_min_cluster = [[] for i in range(len(labels_min_set))]
for item in range(len(clusters_min)):
    for node in clusters_min[item]:
        proba_min_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

proba_maj_cluster = [[] for i in range(len(labels_maj_set))]
for item in range(len(clusters_maj)):
    for node in clusters_maj[item]:
        proba_maj_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

min_data = []
for item in proba_min_cluster:
    array = np.array(item)
    min_data.append((np.average(array), np.std(array)))

maj_data = []
for item in proba_maj_cluster:
    array = np.array(item)
    maj_data.append((np.average(array), np.std(array)))


new_maj = []
for cl in proba_maj_cluster:
    array = np.array(cl)
    median = np.median(array)
    flag = False
    while not flag:
        flag = True
        B = plt.boxplot(array)
        median = np.median(array)
        low = B['whiskers'][0].get_ydata()
        high = B['whiskers'][1].get_ydata()
        for item in array:
            if item > high[1] or item < low[1]:
                flag = False
                i, = np.where(array == item)
                array = np.delete(array, i[0])
    B = plt.boxplot(array)
    median = np.median(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    new_maj.append((median, low[1], high[1]))

new_min = []
for cl in proba_min_cluster:
    array = np.array(cl)
    median = np.median(array)
    flag = False
    while not flag:
        flag = True
        B = plt.boxplot(array)
        median = np.median(array)
        low = B['whiskers'][0].get_ydata()
        high = B['whiskers'][1].get_ydata()
        for item in array:
            if item > high[1] or item < low[1]:
                flag = False
                i, = np.where(array == item)
                array = np.delete(array, i[0])
    B = plt.boxplot(array)
    median = np.median(array)
    low = B['whiskers'][0].get_ydata()
    high = B['whiskers'][1].get_ydata()
    new_min.append((median, low[1], high[1]))

y_pred_new = []
for j in range(X_test.shape[0]):
    label_new_min = cluster_min.predict(X_test[j].reshape(1, -1))
    label_new_maj = cluster_maj.predict(X_test[j].reshape(1, -1))

    if lr.predict_proba(X_test[j].reshape(1, -1))[0][1] >= 0.3:
        label_new = int(label_new_min)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if st.chisquare(X_test[j], clusters_min[label_new])[1] > 0.99:
            y_pred_new.append(1)
        else:
            label_new = int(label_new_maj)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= new_maj[label_new][2] * 0.6:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)

    else:
        label_new = int(label_new_maj)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if new_maj[label_new][1] >= probab:
            y_pred_new.append(0)

        else:
            label_new = int(label_new_min)
            probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
            if probab >= new_min[label_new][2]/3:
                y_pred_new.append(1)
            else:
                y_pred_new.append(0)
                if y_test[j] == 1:
                    print(False)
                    print(new_min[label_new][2])
                    print(probab)

Matrix = confusion_matrix(y_test, y_pred_new)
TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc = TN / (TN + FN)

y_pred_without = lr.predict(X_test)
Matrix = confusion_matrix(y_test, y_pred_without)

TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy_w = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc_w = TN / (TN + FN)

print(Accuracy)
print(Fraud_acc)
print(Accuracy_w)
print(Fraud_acc_w)





# def best_fit_distribution(data, bins=200, ax=None):
#     # Get histogram of original data
#     y, x = np.histogram(data, bins=bins, density=True)
#     x = (x + np.roll(x, -1))[:-1] / 2.0
#
#     # Distributions to check
#     DISTRIBUTIONS = [
#         st.alpha,st.chi,st.chi2,st.cosine,
#         st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm, st.gennorm,st.genexpon,
#         st.genextreme,st.gausshyper,st.gamma,st.gengamma
#     ]
#
#     # Best holders
#     best_distribution = st.norm
#     best_params = (0.0, 1.0)
#     best_sse = np.inf
#
#     # Estimate distribution parameters from data
#     for distribution in DISTRIBUTIONS:
#
#         # Try to fit the distribution
#         try:
#             # Ignore warnings from data that can't be fit
#             with warnings.catch_warnings():
#                 warnings.filterwarnings('ignore')
#
#                 # fit dist to data
#                 params = distribution.fit(data)
#                 # Separate parts of parameters
#                 arg = params[:-2]
#                 loc = params[-2]
#                 scale = params[-1]
#
#                 # Calculate fitted PDF and error with fit in distribution
#                 pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
#                 sse = np.sum(np.power(y - pdf, 2.0))
#
#                 # if axis pass in add to plot
#                 try:
#                     if ax:
#                         pd.Series(pdf, x).plot(ax=ax)
#                 except Exception:
#                     pass
#
#                 # identify if this distribution is better
#                 if best_sse > sse > 0:
#                     best_distribution = distribution
#                     best_params = params
#                     best_sse = sse
#
#         except Exception as e:
#             print(e)
#             pass
#
#     return (best_distribution.name, best_params)
#
# for item in new_maj:
#     print(best_fit_distribution(item))