from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv('creditcard.csv')
df.drop('Time', axis=1, inplace=True)
df.dropna(axis=0, inplace=True)
bad_loans = len(df[df['Class'] == 1])
good_loan_indices = df[df.Class == 0].index
random_indices = np.random.choice(good_loan_indices, 10000, replace=False)
bad_loans_indices = df[df.Class == 1].index
under_sample_indices = np.concatenate([bad_loans_indices, random_indices])
under_sample = df.loc[under_sample_indices]

under_sample = under_sample.dropna(axis=0)
X = under_sample.drop('Class', axis=1)
y = under_sample['Class']
# X = df.drop('Class', axis=1)
# y = df['Class']
threshold_array = [i/100 for i in range(1, 100)]
# threshold_array = [0.2]
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.75, random_state=4)
tree = DecisionTreeClassifier()
svm = SVC(probability=True)
lr = LogisticRegression()
WN_array = [i for i in range(1, 10)]
WP_array = [i for i in range(1, 10)]
methods = [lr]
lr.fit(X_train, y_train)
n = lr.predict_proba(X_test)

for item in methods:
    threshold_acc_array = []
    item.fit(X_train, y_train)
    pred = item.predict_proba(X_test)
    for threshold in threshold_array:
        WBA_Array = []
        y_pred = np.where(item.predict_proba(X_test)[:, 1] > threshold, 1, 0)
        Matrix = confusion_matrix(y_test, y_pred)
        TP = Matrix[0][0]
        FP = Matrix[0][1]
        FN = Matrix[1][0]
        TN = Matrix[1][1]
        Accuracy = (TP + TN) / (TP + FP + TN + FN)
        Fraud_acc = TN / (TN + FN)
        for WN in WN_array:
            WP = 1 - WN
            WBA = WP*(TP/(TP+FP)) + WN*(TN/(TN+FN))
            WBA_Array.append((WBA, WN))
        WBA_array_np = np.array(WBA_Array)
        arg = WBA_array_np[:, 0].argmax()
        Threshold_acc = Accuracy + WBA_array_np[arg][0] / (2 * (1-WBA_array_np[arg][1])) + Fraud_acc
        threshold_acc_array.append((Threshold_acc, threshold, Accuracy, Fraud_acc))
    threshold_acc_array_np = np.array(threshold_acc_array)
    arg = threshold_acc_array_np[:, 0].argmax()
    print(threshold_acc_array_np[arg])

min_threshold = []
for item in methods:
    item.fit(X_train, y_train)
    prob = item.predict_proba(X_test)[:, 1]
    for WN in WN_array:
        for WP in WP_array:
            threshold_acc_array = []
            for threshold in threshold_array:
                y_pred = np.where(prob > threshold, 1, 0)
                Matrix = confusion_matrix(y_test, y_pred)
                TP = Matrix[0][0]
                FP = Matrix[0][1]
                FN = Matrix[1][0]
                TN = Matrix[1][1]
                Accuracy = TP / (TP + FP)
                Fraud_acc = TN / (TN + FN)
                WBA = WP*(FP/(TP+FP)) + WN*(FN/(TN+FN))
                Threshold_acc = WBA
                threshold_acc_array.append((Threshold_acc, threshold, Accuracy, Fraud_acc, WN, WP))
                print(WP)
            print(WN)

            threshold_acc_array_np = np.array(threshold_acc_array)
            arg = threshold_acc_array_np[:, 0].argmin()
            min_threshold.append(threshold_acc_array_np[arg])

df_thresh = pd.DataFrame(min_threshold)





