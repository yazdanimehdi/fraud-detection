import jdatetime
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

from sklearn.svm import SVC
import re
import datetime

df = pd.read_excel('varagh.xlsx')


# def cat(y):
#     if y == 'سلف':
#         return 1
#     elif y == 'نقدی':
#         return 2
#     elif y == 'نقدی (مچینگ)':
#         return 3
#     elif y == 'سلف (مچینگ)':
#         return 4


def shamsi_to_miladi(y):
    shamsi = jdatetime.datetime.strptime(y, format="%Y/%m/%d")
    a = jdatetime.JalaliToGregorian(shamsi.year, shamsi.month, shamsi.day)
    return f"{a.gyear}/{a.gmonth}/{a.gday}"

def miladi_to_timestamp(y):
    return datetime.datetime.strptime(y, format="%Y/%m/%d").timestamp()


def create_bins(lower_bound, width, quantity):
    bins = []
    for low in range(lower_bound,
                     lower_bound + quantity * width + 1, width):
        bins.append(low)
    return bins


bins = create_bins(lower_bound=4000,
                   width=100,
                   quantity=520)


df_dollar = pd.read_excel('dollarPrice.xlsx')

df_dollar['Date'] = pd.to_datetime(df_dollar['Date'])
df['تاریخ معامله'] = df['تاریخ معامله'].apply(shamsi_to_miladi)
df['تاریخ معامله'] = pd.to_datetime(df['تاریخ معامله'])
df.drop(['قیمت پایانی میانگین موزون', 'ارزش معامله (هزارریال)', 'بالاترین', 'حجم معامله (تن)'], axis=1, inplace=True)

df = df.drop(df.loc[df['عرضه (تن)'] == 0].index, axis=0)

# for item in df_dollar.iterrows():
#     a = re.sub(r',', '', item[1][1])
#     a = int(a)
#     df.loc[df['تاریخ معامله'] == item[1][0], 'دلار'] = a
df.dropna(axis=0, inplace=True)
X = df.drop(['پایین ترین', 'تاریخ معامله', 'تاریخ تحویل'], axis=1)
df['bin'] = pd.cut(df['پایین ترین'], bins=520, labels=range(0, 520))
y = df['bin']
X = pd.get_dummies(X)
# y = pd.get_dummies(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

svm = DecisionTreeClassifier()
svm.fit(X_train, y_train)
print(svm.score(X_test, y_test))

# lr = LinearRegression()
# upper = []
# lower = []
# exact = []
x = pd.read_excel('train.xlsx')

# print(lr.score(X_test, y_test))
print(svm.predict(x))
# for t in range(0, 1):
#     nn = MLPRegressor(max_iter=10000)
#     nn.fit(X_train, y_train)
#     upper.append(nn.predict(x)*(2-nn.score(X_test, y_test)))
#     exact.append(nn.predict(x))
#     lower.append(nn.predict(x)*(nn.score(X_test, y_test)))
#     print(t)
#
# print(np.array(upper).mean())
# print(np.array(exact).mean())
# print(np.array(lower).mean())
