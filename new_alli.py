from sklearn.model_selection import RepeatedStratifiedKFold, KFold, cross_val_score, StratifiedKFold, train_test_split
from sklearn.neighbors import NearestNeighbors, KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.metrics import log_loss, roc_auc_score, accuracy_score, confusion_matrix, f1_score
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
import pandas as pd
import numpy as np
import scipy.stats as st

df = pd.read_csv('pima.csv')


def cat(y):
    if y == 'negative':
        return 0
    elif y == 'positive':
        return 1


df['positive'] = df['positive'].apply(cat)
X = df.drop('positive', axis=1).as_matrix()
y = df['positive'].as_matrix()
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=4)
cluster_min = KMeans()
cluster_min.fit(X_train[y_train == 1])
labels_min = cluster_min.labels_
cluster_maj = KMeans()
cluster_maj.fit(X_train[y_train == 0])
labels_maj = cluster_maj.labels_
labels_maj_set = set(labels_maj)
labels_min_set = set(labels_min)

clusters_maj = [[] for j in range(len(labels_maj_set))]
for item in labels_maj_set:
    for i in range(0, X_train[y_train == 0].shape[0]):
        if labels_maj[i] == item:
            clusters_maj[item].append(X_train[y_train == 0][i])

clusters_min = [[] for j in range(len(labels_min_set))]
for item in labels_min_set:
    for i in range(0, X_train[y_train == 1].shape[0]):
        if labels_min[i] == item:
            clusters_min[item].append(X_train[y_train == 1][i])

lr = LogisticRegression(max_iter=5000)
lr.fit(X_train, y_train)

proba_min_cluster = [[] for i in range(len(labels_min_set))]
for item in range(len(clusters_min)):
    for node in clusters_min[item]:
        proba_min_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

proba_maj_cluster = [[] for i in range(len(labels_maj_set))]
for item in range(len(clusters_maj)):
    for node in clusters_maj[item]:
        proba_maj_cluster[item].append(lr.predict_proba(node.reshape(1, -1))[0][1])

min_data = []
for item in proba_min_cluster:
    array = np.array(item)
    min_data.append((np.average(array), np.std(array)))

maj_data = []
for item in proba_maj_cluster:
    array = np.array(item)
    maj_data.append((np.average(array), np.std(array)))

y_pred_new = []
for j in range(X_test.shape[0]):
    label_new_min = cluster_min.predict(X_test[j].reshape(1, -1))
    label_new_maj = cluster_maj.predict(X_test[j].reshape(1, -1))

    if lr.predict(X_test[j].reshape(1, -1)) == 1:
        label_new = int(label_new_min)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if probab >= min_data[label_new][0] - min_data[label_new][1]:
            y_pred_new.append(1)
        else:
            y_pred_new.append(0)
    else:
        label_new = int(label_new_maj)
        probab = lr.predict_proba(X_test[j].reshape(1, -1))[0][1]
        if probab >= maj_data[label_new][0] + maj_data[label_new][1]:
            y_pred_new.append(1)
        else:
            y_pred_new.append(0)


Matrix = confusion_matrix(y_test, y_pred_new)
TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc = TN / (TN + FN)

y_pred_without = lr.predict(X_test)
Matrix = confusion_matrix(y_test, y_pred_without)

TP = Matrix[0][0]
FP = Matrix[0][1]
FN = Matrix[1][0]
TN = Matrix[1][1]
Accuracy_w = (TP + TN) / (TP + FP + TN + FN)
Fraud_acc_w = TN / (TN + FN)

print(Accuracy)
print(Fraud_acc)
print(Accuracy_w)
print(Fraud_acc_w)

def best_fit_distribution(data, bins=200, ax=None):
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with st.warnings.catch_warnings():
                st.warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)

